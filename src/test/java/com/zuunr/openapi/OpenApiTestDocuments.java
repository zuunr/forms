/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.openapi;

import java.io.File;
import java.net.URL;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectFactory;

/**
 * @author Niklas Eldberger
 */
public class OpenApiTestDocuments {

    private static OpenApiTestDocuments singleton = new OpenApiTestDocuments();
    private JsonObjectFactory jsonObjectFactory = new JsonObjectFactory();

    private File getFile(String resource) {
        try {
            URL samplesFileUrl = this.getClass().getResource(resource);
            return new File(samplesFileUrl.toURI());
        } catch (Exception e) {
            throw new RuntimeException("Failed to read file: " + resource, e);
        }
    }

    public JsonObject getOpenApiDoc(String fileName) {
        return jsonObjectFactory.createJsonObject(getFile(fileName));
    }
}
