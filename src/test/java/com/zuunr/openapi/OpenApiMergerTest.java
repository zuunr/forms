/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.openapi;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.jsonschema.JsonSchemaComponentsInliner;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

/**
 * @author Niklas Eldberger
 */
class OpenApiMergerTest {
    private final OpenApiMerger openApiMerger = new OpenApiMerger();
    private final JsonSchemaComponentsInliner inliner = new JsonSchemaComponentsInliner();
    private final OpenApiTestDocuments openApiTestDocuments = new OpenApiTestDocuments();
    private final JsonObject PET_STORE = openApiTestDocuments.getOpenApiDoc("petstore.json");
    private final JsonObject PET_STORE_NO_XML = openApiTestDocuments.getOpenApiDoc("petstore-without-xml-examples-and-defaults.json");

    @Test
    void impossibleResponse() {
        JsonObject inlinedOpenApiDoc = inliner.inline(openApiTestDocuments.getOpenApiDoc("openapi-example1.json"), 100);
        JsonObject restrictResponse = inliner.inline(openApiTestDocuments.getOpenApiDoc("openapi-example1-ROLE_where_response_201_status_is_empty_enum.json"), 100);
        JsonObject mergedOpenApi = openApiMerger.intersectionOf(inlinedOpenApiDoc, restrictResponse);
        JsonArray pathToResponseBodySchema = JsonArray.of("paths", "/tasks", "post", "responses", "201", "content", "application/json", "schema", "properties", "status", "enum");
        JsonArray jsonSchemaForRequestBody = mergedOpenApi.get(pathToResponseBodySchema).getValue(JsonArray.class);
        assertThat("status should have impossible enum[] but not remove OpenAPI doc path of /tasks post", jsonSchemaForRequestBody, is(JsonArray.EMPTY));
    }

    @Test
    void impossibleRequestBody() {
        JsonObject inlinedOpenApiDoc = inliner.inline(openApiTestDocuments.getOpenApiDoc("openapi-example1.json"), 100);
        JsonObject restrictResponse = inliner.inline(openApiTestDocuments.getOpenApiDoc("openapi-example1-ROLE_where_requestBody_status_is_empty_enum.json"), 100);
        JsonObject mergedOpenApi = openApiMerger.intersectionOf(inlinedOpenApiDoc, restrictResponse);
        JsonArray pathToRequestBodySchema = JsonArray.of("paths");
        JsonValue jsonSchemaForRequestBody = mergedOpenApi.get(pathToRequestBodySchema, JsonObject.EMPTY);
        assertThat("requestBody status should have impossible enum[] and therefore the path of /tasks should be removed from OpenAPI doc", jsonSchemaForRequestBody, is(JsonObject.EMPTY.jsonValue()));
    }

    @Test
    void inline1() {
        JsonObject inlined = inliner.inline(openApiTestDocuments.getOpenApiDoc("petstore-without-xml-examples-and-defaults.json"), 100);
        assertThat(inlined, notNullValue());
    }

    /*
    @Test
    void inline2() {
        JsonObject inlined = inliner.inline(openApiTestDocuments.getOpenApiDoc("openapi3-generated-from-swagger-2.json"), 100);
        JsonObject merged = openApiMerger.intersectionOf(inlined, inlined);

        assertThat(merged, notNullValue());
    }
    */
}
