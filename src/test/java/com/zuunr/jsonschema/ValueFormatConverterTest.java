/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.jsonschema;

import com.zuunr.forms.Form;
import com.zuunr.forms.FormField;
import com.zuunr.forms.FormFields;
import com.zuunr.forms.ValueFormat;
import com.zuunr.forms.formfield.Type;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.jsonschema.ValueFormatConverter;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Niklas Eldberger
 */
class ValueFormatConverterTest {

    public static final ValueFormatConverter converter = new ValueFormatConverter();

    @Test
    void translateNonExclusive() {
        ValueFormat valueFormat = ValueFormat.EMPTY.builder()
                .type(Type.OBJECT)
                .form(Form.EMPTY.builder()
                        .exclusive(false)
                        .value(FormFields.EMPTY)
                        .build())
                .build();

        JsonObject result = converter.translate(valueFormat);
        JsonObject expected = JsonObject.EMPTY
                .put("type", JsonArray.of("object"))
                .put("additionalProperties", true)
                .put("properties", JsonObject.EMPTY);
        assertThat(result, is(expected));
    }

    @Test
    void translateExclusive() {
        ValueFormat valueFormat = ValueFormat.EMPTY.builder()
                .type(Type.OBJECT)
                .form(Form.EMPTY.builder()
                        .value(FormFields.EMPTY)
                        .build())
                .build();

        JsonObject result = converter.translate(valueFormat);
        JsonObject expected = JsonObject.EMPTY
                .put("type", JsonArray.of("object"))
                .put("properties", JsonObject.EMPTY);
        assertThat(result, is(expected));
    }

    @Test
    void translateRequired() {
        ValueFormat valueFormat = ValueFormat.EMPTY.builder()
                .type(Type.OBJECT)
                .form(Form.EMPTY.builder()
                        .value(FormFields.EMPTY.builder()
                                .add(FormField.builder("field1").required(true)).build())
                        .build())
                .build();

        JsonObject result = converter.translate(valueFormat.asCompactValueFormat());
        JsonObject expected = JsonObject.EMPTY
                .put("type", JsonArray.of("object"))
                .put("required", JsonArray.of("field1"))
                .put("properties", JsonObject.EMPTY
                        .put("field1", JsonObject.EMPTY
                                .put("type", JsonArray.EMPTY
                                        .add("string"))));

        // "properties":{"field1":{"minLength":0,"maxLength":2147483647,"type":["string"]}}
        assertThat(result, is(expected));
    }
}
