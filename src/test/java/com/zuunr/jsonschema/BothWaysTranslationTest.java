/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.jsonschema;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.jupiter.api.Test;

import com.zuunr.forms.Form;
import com.zuunr.forms.FormField;
import com.zuunr.forms.FormFields;
import com.zuunr.forms.ValueFormat;
import com.zuunr.forms.formfield.Enum;
import com.zuunr.forms.formfield.Type;
import com.zuunr.json.JsonObject;

/**
 * @author Niklas Eldberger
 */
class BothWaysTranslationTest {

    public static final ValueFormatConverter valueFormatConverter = new ValueFormatConverter();
    public static final JsonSchemaConverter jsonSchemaConverter = new JsonSchemaConverter();

    @Test
    void translateNonExclusive() {
        ValueFormat valueFormat = ValueFormat.EMPTY.builder()
                .type(Type.OBJECT)
                .form(Form.EMPTY.builder()
                        .exclusive(false)
                        .value(FormFields.EMPTY)
                        .build())
                .build().asCompactValueFormat();

        ValueFormat result = jsonSchemaConverter.translate(valueFormatConverter.translate(valueFormat)).asCompactValueFormat();
        assertThat(result, is(valueFormat));
    }

    @Test
    void translateArray(){
        ValueFormat valueFormat = ValueFormat.EMPTY.builder()
                .type(Type.ARRAY)
                .eform(Form.EMPTY.builder()
                        .exclusive(false)
                        .value(FormFields.EMPTY.builder()
                                .add(FormField.builder("gender").desc("This is the description").pattern("FEMALE|MALE"))
                                .build())
                        .build())
                .build().asCompactValueFormat();

        JsonObject jsonSchema = valueFormatConverter.translate(valueFormat);
        ValueFormat result = jsonSchemaConverter.translate(jsonSchema).asCompactValueFormat();
        assertThat(result.asExplicitValueFormat(), is(valueFormat.asExplicitValueFormat()));
    }

    @Test
    void translateObject(){

        Form form = Form.EMPTY.builder()
                .exclusive(false)
                .value(FormFields.EMPTY.builder()
                        .add(FormField.builder("gender")
                                .desc("somedesc")
                                .pattern("FEMALE|MALE")
                                .enumeration(Enum.EMPTY.builder()
                                        .add("FEMALE")
                                        .add("MALE")
                                        .build()))
                        .build())
                .build();

        ValueFormat valueFormat = ValueFormat.EMPTY.builder().type(Type.OBJECT).form(form).build();

        JsonObject jsonSchema = valueFormatConverter.translate(valueFormat);
        ValueFormat result = jsonSchemaConverter.translate(jsonSchema).asCompactValueFormat();
        assertThat(result, is(valueFormat));
    }
}
