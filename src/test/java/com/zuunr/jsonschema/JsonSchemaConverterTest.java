/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.jsonschema;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.jupiter.api.Test;

import com.zuunr.forms.ValueFormat;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.tool.JsonUtil;

/**
 * @author Niklas Eldberger
 */
class JsonSchemaConverterTest {

    private static final JsonSchemaConverter jsonSchemaConverter = new JsonSchemaConverter();

    @Test
    void anyJsonValue() {
        ValueFormat valueFormat = jsonSchemaConverter.translate(JsonUtil.create("{}"));
        JsonObject expectedFormField = JsonUtil.create("{'type':'undefined'}");
        assertThat(valueFormat.asJsonObject(), is(expectedFormField));
    }

    @Test
    void test() {
        ValueFormat valueFormat = jsonSchemaConverter.translate(JsonUtil.create("{'type':'string', 'enum':['A','B']}"));
        JsonObject expectedFormField = JsonUtil.create("{'type':'string', 'enum':['A','B']}");
        assertThat(valueFormat.asJsonObject(), is(expectedFormField));
    }

    @Test
    void translateMinimum() {
        ValueFormat valueFormat = jsonSchemaConverter.translate(JsonUtil.create("{'type':'integer', 'minimum':2}"));
        JsonObject expectedFormField = JsonUtil.create("{'type':'integer','min':2}");
        assertThat(valueFormat.asJsonObject(), is(expectedFormField));
    }

    @Test
    void typeAsStringTest() {
        ValueFormat valueFormat = jsonSchemaConverter.translate(JsonUtil.create("{'type':'string'}"));
        JsonObject expectedFormField = JsonUtil.create("{'type':'string'}");
        assertThat(valueFormat.asJsonObject(), is(expectedFormField));
    }

    @Test
    void arrayType() {
        ValueFormat valueFormat = jsonSchemaConverter.translate(JsonUtil.create("{'type':'array'}"));
        JsonObject expectedFormField = JsonUtil.create("{'element':{'type':'undefined'},'type':'array'}");
        assertThat(valueFormat.asJsonObject(), is(expectedFormField));
    }

    @Test
    void typeAsArrayTest() {
        ValueFormat valueFormat = jsonSchemaConverter.translate(JsonUtil.create("{'type':['string','null']}"));
        JsonObject expectedFormField = JsonUtil.create("{'type':'string','nullable':true}");
        assertThat(valueFormat.asJsonObject(), is(expectedFormField));
    }

    @Test
    void nullTypeTest() {
        ValueFormat valueFormat = jsonSchemaConverter.translate(JsonUtil.create("{'type':['null']}"));
        JsonObject expectedFormField = JsonUtil.create("{'type':'undefined','nullable':true}");
        assertThat(valueFormat.asJsonObject(), is(expectedFormField));
    }

    @Test
    void translateRequired() {
        ValueFormat valueFormat = jsonSchemaConverter.translate(JsonUtil.create("{'type':['object'], 'required': ['firstName'],'additionalProperties':false}"));
        ValueFormat expectedValueFormat = JsonUtil.create(ValueFormat.class, "{'type':'object','form':{'value':[{'name':'firstName','type':'undefined','required':true}]}}}");
        assertThat(valueFormat.asCompactValueFormat(), is(expectedValueFormat));
    }

    @Test
    void typeAsArrayreversedOrderTest() {
        ValueFormat valueFormat = jsonSchemaConverter.translate(JsonUtil.create("{'type':['null','string']}"));
        JsonObject expectedFormField = JsonUtil.create("{'type':'string','nullable':true}");
        assertThat(valueFormat.asJsonObject(), is(expectedFormField));
    }


    @Test
    void generateForm() {
        JsonObject objectSchema = JsonObject.EMPTY.put("type", "object").put("properties", JsonObject.EMPTY.put("property1", JsonObject.EMPTY.put("type", "string").put("enum", JsonArray.of("ENUM1", "ENUM2"))));
    }

    @Test
    void translateMinumum() {
        JsonObject objectSchema = JsonObject.EMPTY.put("type", "object").put("properties", JsonObject.EMPTY.put("property1", JsonObject.EMPTY.put("type", "string").put("enum", JsonArray.of("ENUM1", "ENUM2"))));
    }

    @Test
    void translateJsonSchemaArray() {
        JsonObject objectSchema = JsonObject.EMPTY
                .put("type", "array")
                .put("items", JsonObject.EMPTY
                        .put("type", "object")
                        .put("additionalProperties", false)
                        .put("properties", JsonObject.EMPTY
                                .put("property1", JsonObject.EMPTY
                                        .put("type", "string")
                                        .put("enum", JsonArray.of("ENUM1", "ENUM2")))));

        ValueFormat result = jsonSchemaConverter.translate(objectSchema);
        JsonObject expectedResult = JsonUtil.create("{'element':{'form':{'value':[{'name':'property1','enum':['ENUM1','ENUM2']}]},'type':'object'},'type':'array'}");
        assertThat(result.asCompactValueFormat().asJsonObject(), is(expectedResult));
    }

    @Test
    void translateJsonSchemaArrayAdditionalPropertiesNotSpecified() {
        JsonObject objectSchema = JsonObject.EMPTY
                .put("type", "array")
                .put("items", JsonObject.EMPTY
                        .put("type", "object")
                        .put("properties", JsonObject.EMPTY
                                .put("property1", JsonObject.EMPTY
                                        .put("type", "string")
                                        .put("enum", JsonArray.of("ENUM1", "ENUM2")))));

        ValueFormat result = jsonSchemaConverter.translate(objectSchema);
        JsonObject expectedResult = JsonUtil.create("{'element':{'form':{'value':[{'name':'property1','enum':['ENUM1','ENUM2']}]},'type':'object'},'type':'array'}");
        assertThat(result.asCompactValueFormat().asJsonObject(), is(expectedResult));
    }

    @Test
    void additionalPropertiesBooleanSchema() {
        ValueFormat valueFormat = jsonSchemaConverter.translate(JsonUtil.create("{'type':['object'], 'required': ['firstName'], 'additionalProperties': false}"));
        JsonObject expectedFormField = JsonUtil.create("{'type':'object','form':{'exclusive':true, 'value':[{'name':'firstName','type':'undefined','required':true}]}}}");
        assertThat(valueFormat.asJsonObject(), is(expectedFormField));
    }

    @Test
    void additionalPropertiesObjectSchema() {
        ValueFormat valueFormat = jsonSchemaConverter.translate(JsonUtil.create("{'type':['object'], 'required': ['firstName'], 'additionalProperties': {'not':{}}}}"));
        JsonObject expectedFormField = JsonUtil.create("{'type':'object','form':{'exclusive':true, 'value':[{'name':'firstName','type':'undefined','required':true}]}}}");
        assertThat(valueFormat.asJsonObject(), is(expectedFormField));
    }
}
