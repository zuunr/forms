/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.jsonschema;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Niklas Eldberger
 */
class JsonSchemaMergerTest {

    JsonSchemaMerger jsonSchemaMerger = JsonSchemaMerger.OPENAPI_3_0_STYLE;

    @Test
    void intersectionString1() {

        JsonObject jsonSchema1 = JsonObject.EMPTY.put("type", "string").put("pattern", "OK").put("default", "OK");
        JsonObject jsonSchema2 = JsonObject.EMPTY.put("type", "string").put("maxLength", 2).put("minLength", 2);

        JsonObject expectedResult = JsonObject.EMPTY
                .put("type", "string")
                .put("pattern", "OK")
                .put("maxLength", 2)
                .put("minLength", 2);

        JsonValue translatedSchema = jsonSchemaMerger.intersectionOf(jsonSchema1.jsonValue(), jsonSchema2.jsonValue(), true);
        assertThat(translatedSchema, is(expectedResult.jsonValue()));

        translatedSchema = jsonSchemaMerger.intersectionOf(jsonSchema2.jsonValue(), jsonSchema1.jsonValue(), true);
        assertThat(translatedSchema, is(expectedResult.jsonValue()));
    }



    @Test
    void intersectionArray1() {

        JsonObject jsonSchema1 = JsonObject.EMPTY
                .put("type", "object")
                .put("properties", JsonObject.EMPTY
                        .put("arrayField", JsonObject.EMPTY
                                .put("type", "array")
                                .put("items", JsonObject.EMPTY)));

        JsonObject jsonSchema2 = JsonObject.EMPTY
                .put("type", "object")
                .put("properties", JsonObject.EMPTY
                        .put("arrayField", JsonObject.EMPTY
                                .put("type", "array")
                                .put("items", JsonObject.EMPTY
                                        .put("type", "integer"))));


        JsonValue translatedSchema = jsonSchemaMerger.intersectionOf(jsonSchema1.jsonValue(), jsonSchema2.jsonValue(), true);
        assertThat(translatedSchema, is(jsonSchema2.jsonValue()));

        translatedSchema = jsonSchemaMerger.intersectionOf(jsonSchema2.jsonValue(), jsonSchema1.jsonValue(), true);
        assertThat(translatedSchema, is(jsonSchema2.jsonValue()));
    }

    @Test
    void unionArray1() {

        JsonObject jsonSchema1 = JsonObject.EMPTY
                .put("type", "object")
                .put("properties", JsonObject.EMPTY
                        .put("arrayField", JsonObject.EMPTY
                                .put("type", "array")
                                .put("items", JsonObject.EMPTY)));

        JsonObject jsonSchema2 = JsonObject.EMPTY
                .put("type", "object")
                .put("properties", JsonObject.EMPTY
                        .put("arrayField", JsonObject.EMPTY
                                .put("type", "array")
                                .put("items", JsonObject.EMPTY
                                        .put("type", "integer"))));


        JsonValue translatedSchema = jsonSchemaMerger.unionOf(jsonSchema1.jsonValue(), jsonSchema2.jsonValue());
        assertThat(translatedSchema, is(jsonSchema1.jsonValue()));

        translatedSchema = jsonSchemaMerger.unionOf(jsonSchema2.jsonValue(), jsonSchema1.jsonValue());
        assertThat(translatedSchema, is(jsonSchema1.jsonValue()));
    }

    @Test
    void unionOfAlwaysFailingJsonSchema(){

        JsonValue ALWAYS_FAILING_JSON_SCHEMA = JsonObject.EMPTY.put("type", "object").put("enum", JsonArray.EMPTY).jsonValue();
        JsonValue jsonValue = jsonSchemaMerger.unionOf(ALWAYS_FAILING_JSON_SCHEMA, ALWAYS_FAILING_JSON_SCHEMA);
        assertThat(jsonValue.get("enum").getJsonArray(), is(JsonArray.EMPTY));
    }
}
