/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.filter;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

/**
 * @author Niklas Eldberger
 */
class HrefResourcesTest {

    @Test
    @Disabled
    void test() {
        JsonObject form = JsonObject.EMPTY
                .put("href", "https://somedomain.com/theform")
                .put("value", JsonArray.EMPTY
                        .add(JsonObject.EMPTY.put("name", "firstName"))
                        .add(JsonObject.EMPTY.put("name", "lastName")
                                .put("href", "https://example.com/lastNameField"))
                );

        JsonObject fromCache1 = form.as(HrefResources.class).get("https://somedomain.com/theform");

        assertThat(form, is(fromCache1));

        JsonObject fromCache2 = form.as(HrefResources.class).get("https://example.com/lastNameField");

        assertThat(form, is(fromCache2));

        JsonObject fromCache3 = form.as(HrefResources.class).get("https://example.com/this_does_not_exist");

        assertThat(fromCache3, nullValue());
    }
}
