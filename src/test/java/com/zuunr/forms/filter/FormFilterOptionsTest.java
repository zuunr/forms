/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.filter;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.jupiter.api.Test;

import com.zuunr.forms.Form;
import com.zuunr.forms.FormField;
import com.zuunr.forms.FormFields;
import com.zuunr.forms.ValueFormat;
import com.zuunr.forms.formfield.Options;
import com.zuunr.forms.formfield.Type;
import com.zuunr.forms.formfield.options.Option;
import com.zuunr.forms.formfield.options.ValidationStep;
import com.zuunr.forms.formfield.options.ValidationSteps;
import com.zuunr.forms.formfield.options.Value;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.tool.JsonUtil;

import com.zuunr.json.util.JsonObjectWrapper;
/**
 * @author Niklas Eldberger
 */
class FormFilterOptionsTest {

    private static final FormFilter formFilter = new FormFilter();

    @Test
    void test1(){
        ValueFormat anygenderStep1 = ValueFormat.EMPTY.builder().type(Type.OBJECT).build();

        ValueFormat anygenderStep2 = ValueFormat.EMPTY.builder().type(Type.OBJECT).form(Form.EMPTY.builder().exclusive(false).value(FormFields.EMPTY.builder()
                .add(FormField.builder("name").pattern("Kim|Maria"))
                .build()).build()).build();


        ValueFormat maleStep1 = ValueFormat.EMPTY.builder().type(Type.OBJECT).form(Form.EMPTY.builder().exclusive(false).value(FormFields.EMPTY.builder()
                .add(FormField.builder("gender").pattern("MALE"))
                .build()).build()).build();

        ValueFormat maleStep2 = ValueFormat.EMPTY.builder().type(Type.OBJECT).form(Form.EMPTY.builder().exclusive(false).value(FormFields.EMPTY.builder()
                .add(FormField.builder("name").pattern("Robert|Adam"))
                .build()).build()).build();

        ValueFormat femaleStep1 = ValueFormat.EMPTY.builder().type(Type.OBJECT).form(Form.EMPTY.builder().exclusive(false).value(FormFields.EMPTY.builder()
                .add(FormField.builder("gender").pattern("FEMALE"))
                .build()).build()).build();

        ValueFormat femaleStep2 = ValueFormat.EMPTY.builder().type(Type.OBJECT).form(Form.EMPTY.builder().exclusive(false).value(FormFields.EMPTY.builder()
                .add(FormField.builder("name").maxlength(5))
                .build()).build()).build();

        ValueFormat femaleStep3 = ValueFormat.EMPTY.builder().type(Type.OBJECT).form(Form.EMPTY.builder().exclusive(false).value(FormFields.EMPTY.builder()
                .add(FormField.builder("name").pattern("Anne|Laura"))
                .build()).build()).build();


        Form form = Form.EMPTY.builder().value(FormFields.EMPTY.builder()
                .add(FormField.builder("newChild").type(Type.UNDEFINED).options(Options.builder().value(Value.EMPTY.builder()
                        .add(Option.builder("any gender").validationSteps(ValidationSteps.EMPTY.builder()
                                .add(ValidationStep.builder(anygenderStep1).build())
                                .add(ValidationStep.builder(anygenderStep2).build())
                                .build()).build())
                        .add(Option.builder("male").validationSteps(ValidationSteps.EMPTY.builder()
                                .add(ValidationStep.builder(maleStep1).build())
                                .add(ValidationStep.builder(maleStep2).build())
                                .build()).build())
                        .add(Option.builder("female")
                                .format(ValueFormat.EMPTY.builder().type(Type.OBJECT).form(Form.EMPTY.builder().value(FormFields.EMPTY.builder()
                                        .add(femaleStep1.form().formField("gender"))
                                        .add(FormField.builder("name")
                                                .maxlength(femaleStep2.form().formField("name").schema().maxlength().asInteger())
                                                .pattern(femaleStep3.form().formField("name").schema().pattern().asString()))
                                        .build()).build()).build())
                                .validationSteps(ValidationSteps.EMPTY.builder()
                                .add(ValidationStep.builder(femaleStep1).build())
                                .add(ValidationStep.builder(femaleStep2).build())
                                .add(ValidationStep.builder(femaleStep3).build())
                                .build()).build())
                        .build()).build()).build()).build()).build();

        FilterResult<JsonObjectWrapper> resultAdam = formFilter.filter(form, JsonObject.EMPTY.put("newChild", JsonObject.EMPTY.put("gender", "FEMALE").put("name", "Adam")),true, true);

        assertThat(resultAdam.waste.isEmpty(), is(false));
        assertThat(resultAdam.waste.asJsonObject().get(JsonArray.of("paths", 0, "path", 0)).getValue(String.class), is("newChild"));
        assertThat(resultAdam.waste.asJsonObject().get(JsonArray.of("paths", 0, "badValue", "gender")).getValue(String.class), is("FEMALE"));
        assertThat(resultAdam.waste.asJsonObject().get(JsonArray.of("paths", 0, "badValue", "name")).getValue(String.class), is("Adam"));
        assertThat(resultAdam.waste.asJsonObject().get(JsonArray.of("paths", 0, "violations", "options", "value", 0, "label")).getValue(String.class), is("female"));
        assertThat(resultAdam.waste.asJsonObject().get(JsonArray.of("paths", 0, "violations", "options", "value", 0, "error", "paths", 0, "path", 0)).getValue(String.class), is("name"));
        assertThat(resultAdam.waste.asJsonObject().get(JsonArray.of("paths", 0, "violations", "options", "value", 0, "error", "paths", 0, "badValue")).getValue(String.class), is("Adam"));
        assertThat(resultAdam.waste.asJsonObject().get(JsonArray.of("paths", 0, "violations", "options", "value", 0, "error", "paths", 0, "violations", "pattern")).getValue(String.class), is("Anne|Laura"));

        FilterResult<JsonObjectWrapper> resultRoberta = formFilter.filter(form, JsonObject.EMPTY.put("newChild", JsonObject.EMPTY.put("gender", "FEMALE").put("name", "Roberta")),true, true);

        assertThat(resultRoberta.waste.isEmpty(), is(false));


        assertThat(resultRoberta.waste.asJsonObject().get(JsonArray.of("paths", 0, "violations", "options", "value")).getValue(JsonArray.class).size(), is(2));
        // Order is not defined and test must not fail if errors are returned in different order
        int femaleIndex;
        int anygenderIndex;
        if (resultRoberta.waste.asJsonObject().get(JsonArray.of("paths", 0, "violations", "options", "value", 0, "label")).getValue(String.class).equals("female")) {
            femaleIndex = 0;
            anygenderIndex = 1;
        } else {
            femaleIndex = 1;
            anygenderIndex = 0;
        }

        assertThat(resultRoberta.waste.asJsonObject().get(JsonArray.of("paths", 0, "path", 0)).getValue(String.class), is("newChild"));
        assertThat(resultRoberta.waste.asJsonObject().get(JsonArray.of("paths", 0, "badValue", "gender")).getValue(String.class), is("FEMALE"));
        assertThat(resultRoberta.waste.asJsonObject().get(JsonArray.of("paths", 0, "badValue", "name")).getValue(String.class), is("Roberta"));
        assertThat(resultRoberta.waste.asJsonObject().get(JsonArray.of("paths", 0, "violations", "options", "value", femaleIndex, "label")).getValue(String.class), is("female"));
        assertThat(resultRoberta.waste.asJsonObject().get(JsonArray.of("paths", 0, "violations", "options", "value", femaleIndex, "error", "paths", 0, "path", 0)).getValue(String.class), is("name"));
        assertThat(resultRoberta.waste.asJsonObject().get(JsonArray.of("paths", 0, "violations", "options", "value", femaleIndex, "error", "paths", 0, "badValue")).getValue(String.class), is("Roberta"));
        assertThat(resultRoberta.waste.asJsonObject().get(JsonArray.of("paths", 0, "violations", "options", "value", femaleIndex, "error", "paths", 0, "violations", "maxlength")).asInteger(), is(5));

        assertThat(resultRoberta.waste.asJsonObject().get(JsonArray.of("paths", 0, "path", 0)).getValue(String.class), is("newChild"));
        assertThat(resultRoberta.waste.asJsonObject().get(JsonArray.of("paths", 0, "badValue", "gender")).getValue(String.class), is("FEMALE"));
        assertThat(resultRoberta.waste.asJsonObject().get(JsonArray.of("paths", 0, "badValue", "name")).getValue(String.class), is("Roberta"));
        assertThat(resultRoberta.waste.asJsonObject().get(JsonArray.of("paths", 0, "violations", "options", "value", anygenderIndex, "label")).getValue(String.class), is("any gender"));
        assertThat(resultRoberta.waste.asJsonObject().get(JsonArray.of("paths", 0, "violations", "options", "value", anygenderIndex, "error", "paths", 0, "path", 0)).getValue(String.class), is("name"));
        assertThat(resultRoberta.waste.asJsonObject().get(JsonArray.of("paths", 0, "violations", "options", "value", anygenderIndex, "error", "paths", 0, "badValue")).getValue(String.class), is("Roberta"));
        assertThat(resultRoberta.waste.asJsonObject().get(JsonArray.of("paths", 0, "violations", "options", "value", anygenderIndex, "error", "paths", 0, "violations", "pattern")).getString(), is("Kim|Maria"));

        FilterResult<JsonObjectWrapper> resultAnne = formFilter.filter(form, JsonObject.EMPTY.put("newChild", JsonObject.EMPTY.put("gender", "FEMALE").put("name", "Anne")),true, true);

        assertThat(resultAnne.waste.isEmpty(), is(true));

        }

    @Test
    void test() {


        ValueFormat stringStep1Format = ValueFormat.EMPTY.builder().type(Type.OBJECT).form(Form.EMPTY.builder().exclusive(false).value(FormFields.EMPTY.builder()
                .add(FormField.builder("type").pattern("string")).build()).build()).build();

        ValueFormat stringStep2Format = ValueFormat.EMPTY.builder().type(Type.OBJECT).form(Form.EMPTY.builder().exclusive(true).value(FormFields.EMPTY.builder()
                .add(FormField.builder("type").pattern("string"))
                .add(FormField.builder("pattern"))
                .build()).build()).build();

        ValueFormat objectStep1Format = ValueFormat.EMPTY.builder().type(Type.OBJECT).form(Form.EMPTY.builder().exclusive(false).value(FormFields.EMPTY.builder()
                .add(FormField.builder("type").pattern("object").required(true))
                .build()).build()).build();


        Form step2ErrorsForm = Form.EMPTY.builder().value(FormFields.EMPTY.builder()
                .add(FormField.builder("myfield").type(Type.UNDEFINED).options(Options.builder().value(Value.EMPTY.builder()
                        .add(Option.builder("type:string").validationSteps(ValidationSteps.EMPTY.builder()
                                .add(ValidationStep.builder(stringStep1Format).build())
                                .add(ValidationStep.builder(stringStep2Format).build())
                                .build()).build())
                        .add(Option.builder("type:object").validationSteps(ValidationSteps.EMPTY.builder()
                                .add(ValidationStep.builder(objectStep1Format).build())
                                .build()).build())
                        .add(Option.builder("type:string2").validationSteps(ValidationSteps.EMPTY.builder()
                                .add(ValidationStep.builder(stringStep1Format).build())
                                .add(ValidationStep.builder(stringStep2Format).build())
                                .build()).build())
                        .build()).build())).build()).build();

        FilterResult<JsonObjectWrapper> step2ErrorsFormResult = formFilter.filter(step2ErrorsForm, JsonObject.EMPTY.put("myfield", JsonObject.EMPTY.put("pattern", JsonObject.EMPTY)), true, true);

        assertThat(step2ErrorsFormResult.waste.isEmpty(), is(false));

        Form step1OkForm = Form.EMPTY.builder().value(FormFields.EMPTY.builder()
                .add(FormField.builder("myfield").type(Type.UNDEFINED).options(Options.builder().value(Value.EMPTY.builder()
                        .add(Option.builder("type:string").validationSteps(ValidationSteps.EMPTY.builder()
                                .add(ValidationStep.builder(stringStep1Format).build())
                                .add(ValidationStep.builder(stringStep2Format).build())
                                .build()).build())
                        .add(Option.builder("type:object").validationSteps(ValidationSteps.EMPTY.builder()
                                .add(ValidationStep.builder(objectStep1Format).build())
                                .build()).build())
                        .add(Option.builder("type:string2").validationSteps(ValidationSteps.EMPTY.builder()
                                .add(ValidationStep.builder(stringStep1Format).build())
                                .add(ValidationStep.builder(stringStep2Format).build())
                                .build()).build())
                        .add(Option.builder("type:string-super-simple").validationSteps(ValidationSteps.EMPTY.builder()
                                .add(ValidationStep.builder(stringStep1Format).build())
                                .build()).build())
                        .build()).build())).build()).build();

        FilterResult<JsonObjectWrapper> step1OkFormResult = formFilter.filter(step1OkForm, JsonObject.EMPTY.put("myfield", JsonObject.EMPTY.put("pattern", JsonObject.EMPTY)), true, true);

        assertThat(step1OkFormResult.waste.isEmpty(), is(true));
    }

    @Test
    void optionFormatTest() {
        JsonObject parameters = JsonUtil.create("{'fullName':'Kalle Andersson2'}");
        Form form = JsonUtil.create(Form.class, "{'value': [{'name': 'fullName','type':'string','options': {'value': [{'label': 'Andersson','format': {'pattern':'\\\\w* Andersson'}},{'label': 'Robertson','format': {'pattern':'\\\\w* Robertson'}}]}}]}");
        FilterResult<JsonObjectWrapper> result = formFilter.filter(form, parameters, true, true);
        assertThat(result.waste.isEmpty(), is(false));
    }

    @Test
    void optionFormatFiltrateTest() {
        JsonObject parameters = JsonUtil.create("{'fullName':'Kalle Robertson', 'age':'28'}");
        Form form = JsonUtil.create(Form.class, "{'value': [{'name': 'fullName','type':'string','options': {'value': [{'label': 'Andersson','format': {'pattern':'\\\\w* Andersson'}},{'label': 'Robertson','format': {'pattern':'\\\\w* Robertson'}}]}}]}");
        FilterResult<JsonObjectWrapper> result = formFilter.filter(form, parameters, true, true);
        assertThat(result.filtrate.asJsonValue().getJsonObject(), is(parameters.remove("age")));
    }
}
