/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.filter;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.jupiter.api.Test;

import com.zuunr.forms.Form;
import com.zuunr.forms.FormField;
import com.zuunr.forms.FormFields;
import com.zuunr.forms.formfield.Type;
import com.zuunr.forms.generation.FormMerger;
import com.zuunr.json.JsonObject;
import com.zuunr.json.util.JsonObjectWrapper;

/**
 * @author Niklas Eldberger
 */
class FormForFormTest {

    @Test
    void test() {
        Form formForm = Form.EMPTY.builder().value(FormFields.EMPTY.builder()
                .add(FormField.builder("href").type(Type.STRING))
                .add(FormField.builder("exclusive").type(Type.BOOLEAN).required(false))
                .add(FormField.builder("value")
                        .type(Type.ARRAY)
                        .eform(Form.EMPTY.builder().value(FormFields.EMPTY.builder()
                                .add(FormField.builder("name").required(true))
                                .add(FormField.builder("required").type(Type.BOOLEAN))
                                .add(FormField.builder("nullable").type(Type.BOOLEAN))
                                .add(FormField.builder("mustBeNull").type(Type.BOOLEAN))
                                .add(FormField.builder("type").pattern("object|decimal|integer|boolean|array|set|string"))
                                .add(FormField.builder("form").type(Type.OBJECT))
                                .add(FormField.builder("eform").type(Type.OBJECT))
                                .add(FormField.builder("element").type(Type.OBJECT))
                                .add(FormField.builder("pattern"))
                                .add(FormField.builder("min").type(Type.UNDEFINED))
                                .add(FormField.builder("max").type(Type.UNDEFINED))
                                .add(FormField.builder("minlength").type(Type.INTEGER))
                                .add(FormField.builder("maxlength").type(Type.INTEGER))
                                .add(FormField.builder("minsize").type(Type.INTEGER))
                                .add(FormField.builder("maxsize").type(Type.INTEGER))
                                .build())
                                .build()
                        )))
                .build();

        FormFilter formFilter = new FormFilter();
        FilterResult<JsonObjectWrapper> result = formFilter.filter(formForm, formForm.asJsonObject(), true, true);
        assertThat(result.waste.isEmpty(), is(true));

        Form baseRegistrationForm = Form.EMPTY.builder().value(FormFields.EMPTY.builder()
                .add(FormField
                        .builder("firstName")
                        .pattern("[A-Za-z]+").required(true))
                .add(FormField
                        .builder("lastName")
                        .pattern("[A-Za-z]+")
                        .required(true))
                .add(FormField
                        .builder("email")
                        .pattern("[A-Za-z0-9]([.][A-Za-z0-9])?@[A-Za-z0-9][.][A-Za-z0-9]([.][A-Za-z0-9])?")
                        .required(true))
                .add(FormField
                        .builder("gender")
                        .pattern("MALE|FEMALE").required(true))
                .add(FormField
                        .builder("password")
                        .minsize(8).required(true))
                .add(FormField
                        .builder("address").required(true))
                .add(FormField
                        .builder("country")
                        .pattern("EE|SE|TR|UA")
                        .required(true))
                .add(FormField
                        .builder("phoneNumber")
                        .pattern("\\+(9[976]\\d|8[987530]\\d|6[987]\\d|5[90]\\d|42\\d|3[875]\\d|2[98654321]\\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\\d{1,14}$\n")
                        .required(true))
                .add(FormField
                        .builder("birthDay")
                        .pattern("([12]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\\d|3[01]))")
                        .required(true))
        ).build();

        FilterResult<JsonObjectWrapper> registrationFormResult = formFilter.filter(formForm, baseRegistrationForm.asJsonObject(), true, true);
        assertThat(registrationFormResult.waste.isEmpty(), is(true));

        JsonObject registrationData = JsonObject.EMPTY.builder()
                .put("firstName", "Peter")
                .build();



        FilterResult<JsonObjectWrapper> registrationResult = formFilter.filter(baseRegistrationForm, registrationData, true, true);
        assertThat(registrationResult.waste.isEmpty(), is(false));

        Form franchiseRegistrationFormPatch = Form.EMPTY.builder().value(FormFields.EMPTY.builder()
                .add(FormField
                        .builder("firstName")
                        .pattern("[A-Za-z]+").required(true))
                .add(FormField
                        .builder("lastName")
                        .pattern("[A-Za-z]+")
                        .required(false)) // This one is changed
                .add(FormField
                        .builder("email")
                        .pattern("[A-Za-z0-9]([.][A-Za-z0-9])?@[A-Za-z0-9][.][A-Za-z0-9]([.][A-Za-z0-9])?")
                        .required(true))
                .add(FormField
                        .builder("gender")
                        .pattern("MALE|FEMALE").required(true))
                .add(FormField
                        .builder("password")
                        .minsize(8).required(true))
                .add(FormField
                        .builder("address").required(true))
                .add(FormField
                        .builder("country")
                        .pattern("EE|SE|TR|UA")
                        .required(true))
                .add(FormField
                        .builder("phoneNumber")
                        .pattern("\\+(9[976]\\d|8[987530]\\d|6[987]\\d|5[90]\\d|42\\d|3[875]\\d|2[98654321]\\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\\d{1,14}$\n")
                        .required(true))
                .add(FormField
                        .builder("birthDay")
                        .pattern("([12]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\\d|3[01]))")
                        .required(true))
        ).build();

        FormMerger merger = new FormMerger();

        Form mergedForm = merger.unionOf(baseRegistrationForm, franchiseRegistrationFormPatch);

        assertThat(mergedForm.asExplicitForm().formFields().of("lastName").required(), is(false));
    }
}
