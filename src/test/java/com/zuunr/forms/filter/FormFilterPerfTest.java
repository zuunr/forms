/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.filter;

import com.zuunr.forms.Form;
import com.zuunr.forms.FormField;
import com.zuunr.forms.FormFields;
import com.zuunr.forms.formfield.Type;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectBuilder;
import com.zuunr.json.JsonValue;
import com.zuunr.json.JsonValueTestUtil;
import com.zuunr.json.util.JsonObjectWrapper;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;

/**
 * @author Niklas Eldberger
 */
class FormFilterPerfTest {

    @Test
    @Disabled
    void givenFormWith10Fields_whenValidate10CorrectFields_max2NewJsonValuesAreCreatedInMax20microSecs() {

        JsonValue.cacheEnabled(true);
        TestConfig testConfig = new TestConfig()
                .setTestWithErrors(false)
                .setStringParamsPerForm(10)
                .setNestedParamsPerForm(0)
                .setErrorsPerNested(0)
                .setValidationIterations(100000)
                .setAssertMaxCreatedJsonValues(2L)
                //.setAssertMaxValidationTimePerIterationInNanos(20000L)
                ;
        perfTest(testConfig);

    }


    @Test
    @Disabled
    void givenFormWith110Fields_whenValidate110CorrectFields_max22NewJsonValuesAreCreated() {
        JsonValue.cacheEnabled(false);
        TestConfig testConfig = new TestConfig()
                .setTestWithErrors(false)
                .setStringParamsPerForm(10)
                .setNestedParamsPerForm(10)
                .setErrorsPerNested(0)
                .setValidationIterations(1000)
                .setAssertMaxCreatedJsonValues(22L)
                //.setAssertMaxValidationTimePerIterationInNanos(100000L)
                ;
        perfTest(testConfig);
    }


    void perfTest(TestConfig testConfig) {

        Form form = setupForm(testConfig);
        JsonObject jsonObject = setupTestData(testConfig);

        FormFilter formFilter = new FormFilter();

        // Run the filter once to be able to cache
        FilterResult<JsonObjectWrapper> filterResult = formFilter.filter(form, jsonObject, true, true);

        if (!testConfig.testWithErrors) {
            assertThat(filterResult.waste.isEmpty(), is(true));
        } else {
            assertThat(filterResult.waste.wasteItemsSize(), is(10 * testConfig.errorsPerNested));
        }

        long start = System.nanoTime();
        long startInstances = JsonValueTestUtil.jsonValueInstances();

        int iterations = testConfig.validationIterations;
        for (int i = 0; i < iterations; i++) {
            formFilter.filter(form, jsonObject, true, true);
        }

        long endTime = System.nanoTime();
        long timePerIteration = (endTime - start) / (long) iterations;
        long instancesCreatedPerValidation = (JsonValueTestUtil.jsonValueInstances() - startInstances) / iterations;
        System.out.println("nanos:                " + timePerIteration);
        System.out.println("JsonValue instances per validation:  " + instancesCreatedPerValidation);
        assertThat("JsonValue instances < 3", instancesCreatedPerValidation, lessThan(testConfig.assertMaxCreatedJsonValues + 1));
        if (testConfig.assertMaxValidationTimePerIterationInNanos != -1L) {
            assertThat("Validation time < " + testConfig.assertMaxValidationTimePerIterationInNanos, timePerIteration, is(lessThan(testConfig.assertMaxValidationTimePerIterationInNanos)));
        }
    }

    private Form setupForm(TestConfig testConfig) {


        boolean testWithErrors = testConfig.testWithErrors;

        int paramsPerForm = testConfig.stringParamsPerForm;
        int nestedParamsPerForm = testConfig.nestedParamsPerForm;

        FormFields.FormFieldsBuilder formFieldsBuilder = FormFields.EMPTY.builder();

        for (int formFieldIndex = 0; formFieldIndex < paramsPerForm; formFieldIndex++) {

            FormFields.FormFieldsBuilder nestedFormFieldsBuilder = FormFields.EMPTY.builder();

            if (nestedParamsPerForm > 0) {
                for (int nestedFormFieldIndex = 0; nestedFormFieldIndex < nestedParamsPerForm; nestedFormFieldIndex++) {
                    nestedFormFieldsBuilder.add(FormField.builder("stringField" + nestedFormFieldIndex).required(true).mustBeNull(false).minlength(10).maxlength(10).nullable(false).build());
                }
                formFieldsBuilder.add(FormField.builder("objectField" + formFieldIndex).type(Type.OBJECT).required(true).mustBeNull(false).nullable(false).form(Form.EMPTY.builder().value(nestedFormFieldsBuilder.build()).build()).build());
            }

            formFieldsBuilder.add(FormField.builder("stringField" + formFieldIndex).required(true).mustBeNull(false).minlength(10).maxlength(10).nullable(false).build());
        }

        Form form = Form.EMPTY.builder()
                .exclusive(true)
                .value(formFieldsBuilder)
                .build();
        return form;
    }

    private JsonObject setupTestData(TestConfig testConfig) {
        JsonObjectBuilder jsonObjectBuilder = JsonObject.EMPTY.builder();

        for (int paramIndex = 0; paramIndex < testConfig.stringParamsPerForm; paramIndex++) {

            JsonObjectBuilder nestedBuilder = JsonObject.EMPTY.builder();

            if (testConfig.nestedParamsPerForm > 0) {
                for (int nestedParamIndex = 0; nestedParamIndex < testConfig.nestedParamsPerForm; nestedParamIndex++) {
                    if (nestedParamIndex < testConfig.errorsPerNested && testConfig.testWithErrors) {
                        nestedBuilder.put("stringField" + nestedParamIndex, "value with wrong length");
                    } else {
                        nestedBuilder.put("stringField" + nestedParamIndex, "0123456789");
                    }
                }
                jsonObjectBuilder
                        .put("objectField" + paramIndex, nestedBuilder);
            }
            jsonObjectBuilder
                    .put("stringField" + paramIndex, "0123456789");
        }

        JsonObject jsonObject = jsonObjectBuilder.build();

        return jsonObject;
    }


    private class TestConfig {
        boolean testWithErrors = true;

        int stringParamsPerForm;
        int nestedParamsPerForm;
        int errorsPerNested;
        int validationIterations;
        long assertMaxCreatedJsonValues;
        long assertMaxValidationTimePerIterationInNanos = -1L;

        public TestConfig setTestWithErrors(boolean testWithErrors) {
            this.testWithErrors = testWithErrors;
            return this;
        }

        public TestConfig setStringParamsPerForm(int stringParamsPerForm) {
            this.stringParamsPerForm = stringParamsPerForm;
            return this;
        }

        public TestConfig setNestedParamsPerForm(int nestedParamsPerForm) {
            this.nestedParamsPerForm = nestedParamsPerForm;
            return this;
        }

        public TestConfig setErrorsPerNested(int errorsPerNested) {
            this.errorsPerNested = errorsPerNested;
            return this;
        }

        public TestConfig setValidationIterations(int validationIterations) {
            this.validationIterations = validationIterations;
            return this;
        }

        public TestConfig setAssertMaxCreatedJsonValues(long assertMaxCreatedJsonValues) {
            this.assertMaxCreatedJsonValues = assertMaxCreatedJsonValues;
            return this;
        }

        public TestConfig setAssertMaxValidationTimePerIterationInNanos(long assertMaxValidationTimePerIterationInNanos) {
            this.assertMaxValidationTimePerIterationInNanos = assertMaxValidationTimePerIterationInNanos;
            return this;
        }
    }
}
