/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.filter;

import com.zuunr.forms.Form;
import com.zuunr.forms.FormField;
import com.zuunr.forms.FormFields;
import com.zuunr.forms.ValueFormat;
import com.zuunr.forms.formfield.Options;
import com.zuunr.forms.formfield.Type;
import com.zuunr.forms.formfield.options.Option;
import com.zuunr.forms.formfield.options.Value;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.tool.JsonUtil;
import com.zuunr.json.util.JsonObjectWrapper;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

/**
 * @author Niklas Eldberger
 */
class FormForFormsTest {

    private static String formUrl = "https://zuunr.com/form";
    private static String stringUrl = "https://zuunr.com/string";
    private static String booleanUrl = "https://zuunr.com/boolean";
    private static String objectUrl = "https://zuunr.com/object";
    private static String arrayUrl = "https://zuunr.com/array";
    private static String integerUrl = "https://zuunr.com/integer";
    private static String decimalUrl = "https://zuunr.com/decimal";
    private static String optionsUrl = "https://zuunr.com/options";

    private static String stringFormFieldUrl = "https://zuunr.com/formfield/string";
    private static String booleanFormFieldUrl = "https://zuunr.com/formfield/boolean";
    private static String objectFormFieldUrl = "https://zuunr.com/formfield/object";
    private static String arrayFormFieldUrl = "https://zuunr.com/formfield/array";
    private static String integerFormFieldUrl = "https://zuunr.com/formfield/integer";
    private static String decimalFormFieldUrl = "https://zuunr.com/formfield/decimal";

    private static FormFields stringFormField = FormFields.EMPTY.builder()
            .add(FormField.builder("name").required(true).build())
            .add(FormField.builder("type").pattern("string").build())
            .add(FormField.builder("pattern").build())
            .add(FormField.builder("required").type(Type.BOOLEAN).build())
            .add(FormField.builder("nullable").type(Type.BOOLEAN).build())
            .add(FormField.builder("mustBeNull").type(Type.BOOLEAN))
            .add(FormField.builder("options").type(Type.OBJECT)
                    .form(Form.EMPTY.builder()
                            .href(optionsUrl)
                            .value(FormFields.EMPTY.builder()
                                    .add(FormField.builder("label").required(true).build())
                                    .add(FormField.builder("value").type(Type.UNDEFINED).build())
                                    .add(FormField.builder("format")
                                            .type(Type.OBJECT)
                                            .form(Form.EMPTY
                                                    .builder()
                                                    .value(FormFields.EMPTY
                                                            .builder()
                                                            .add(FormField.builder("type").pattern("object").build())
                                                            .add(FormField.builder("required").type(Type.BOOLEAN).build())
                                                            .add(FormField.builder("nullable").type(Type.BOOLEAN).build())
                                                            .add(FormField.builder("mustBeNull").type(Type.BOOLEAN))
                                                            .add(FormField.builder("options")
                                                                    .type(Type.OBJECT)
                                                                    .form(Form.EMPTY.builder()
                                                                            .href(optionsUrl).build()))
                                                            .add(FormField.builder("form").type(Type.OBJECT).form(Form.EMPTY.builder().href(formUrl).build()))
                                                            .build())
                                                    .build())
                                            .build())

                            ).build()).build()).build();

    private static FormFields stringFormat = stringFormField.asJsonArray().remove(0).as(FormFields.class);

    private static FormFields booleanFormField = FormFields.EMPTY.builder()
            .add(FormField.builder("name").required(true).build())
            .add(FormField.builder("type").pattern("boolean").build())
            .add(FormField.builder("required").type(Type.BOOLEAN).build())
            .add(FormField.builder("nullable").type(Type.BOOLEAN).build())
            .add(FormField.builder("mustBeNull").type(Type.BOOLEAN))
            .add(FormField.builder("options")
                    .type(Type.OBJECT)
                    .form(Form.EMPTY.builder()
                            .href(optionsUrl).build()).build()).build();

    private static FormFields booleanFormat = booleanFormField.asJsonArray().remove(0).as(FormFields.class);

    private static FormFields objectFormField = FormFields.EMPTY.builder()
            .add(FormField.builder("name").required(true).build())
            .add(FormField.builder("type").pattern("object").build())
            .add(FormField.builder("required").type(Type.BOOLEAN).build())
            .add(FormField.builder("nullable").type(Type.BOOLEAN).build())
            .add(FormField.builder("mustBeNull").type(Type.BOOLEAN))
            .add(FormField.builder("options")
                    .type(Type.OBJECT)
                    .form(Form.EMPTY.builder()
                            .href(optionsUrl).build()))
            .add(FormField.builder("form").type(Type.OBJECT).form(Form.EMPTY.builder().href(formUrl).build()).build()).build();

    private static FormFields objectFormat = objectFormField.asJsonArray().remove(0).as(FormFields.class);


    private static FormFields integerFormField = FormFields.EMPTY.builder()
            .add(FormField.builder("name").required(true).build())
            .add(FormField.builder("type").pattern("integer").build())
            .add(FormField.builder("required").type(Type.BOOLEAN).build())
            .add(FormField.builder("nullable").type(Type.BOOLEAN).build())
            .add(FormField.builder("min").type(Type.INTEGER))
            .add(FormField.builder("max").type(Type.INTEGER))
            .add(FormField.builder("mustBeNull").type(Type.BOOLEAN))
            .add(FormField.builder("options")
                    .type(Type.OBJECT)
                    .form(Form.EMPTY.builder()
                            .href(optionsUrl).build()).build()).build();

    private static FormFields integerFormat = integerFormField.asJsonArray().remove(0).as(FormFields.class);

    private static FormFields decimalFormField =
            FormFields.EMPTY.builder()
                    .add(FormField.builder("name").required(true).build())
                    .add(FormField.builder("type").pattern("decimal").build())
                    .add(FormField.builder("required").type(Type.BOOLEAN).build())
                    .add(FormField.builder("nullable").type(Type.BOOLEAN).build())
                    .add(FormField.builder("min").type(Type.DECIMAL))
                    .add(FormField.builder("max").type(Type.DECIMAL))
                    .add(FormField.builder("mustBeNull").type(Type.BOOLEAN))
                    .add(FormField.builder("options")
                            .type(Type.OBJECT)
                            .form(Form.EMPTY.builder()
                                    .href(optionsUrl).build()).build()).build();

    private static FormFields decimalFormat = decimalFormField.asJsonArray().remove(0).as(FormFields.class);

    private static FormFields arrayFormField = FormFields.EMPTY.builder()
            .add(FormField.builder("name").required(true).build())
            .add(FormField.builder("type").pattern("array|set").build())
            .add(FormField.builder("required").type(Type.BOOLEAN).build())
            .add(FormField.builder("nullable").type(Type.BOOLEAN).build())
            .add(FormField.builder("mustBeNull").type(Type.BOOLEAN))
            .add(FormField.builder("options")
                    .type(Type.OBJECT)
                    .form(Form.EMPTY.builder()
                            .href(optionsUrl).build())).add(FormField.builder("etype").pattern("array|boolean|decimal|integer|object|set|string").build())
            .add(FormField.builder("eform").type(Type.OBJECT).form(Form.EMPTY.builder().href(formUrl).build()))
            .add(FormField.builder("element")
                    .type(Type.OBJECT)
                    .options(Options.builder()
                            .value(Value.EMPTY.builder()
                                    .add(Option.builder("type:string")
                                            .format(ValueFormat.EMPTY.builder().type(Type.OBJECT).href(stringUrl).form(Form.EMPTY.builder().value(stringFormat)
                                                    .build()).build()).build())
                                    .add(Option.builder("type:boolean")
                                            .format(ValueFormat.EMPTY.builder().type(Type.OBJECT).href(booleanUrl).form(Form.EMPTY.builder().value(booleanFormat)
                                                    .build()).build()).build())
                                    .add(Option.builder("type:integer")
                                            .format(ValueFormat.EMPTY.builder().type(Type.OBJECT).href(integerUrl).form(Form.EMPTY.builder().value(integerFormat)
                                                    .build()).build()).build())
                                    .add(Option.builder("type:decimal")
                                            .format(ValueFormat.EMPTY.builder().type(Type.OBJECT).href(decimalUrl).form(Form.EMPTY.builder().value(decimalFormat)
                                                    .build()).build()).build())
                                    .add(Option.builder("type:object")
                                            .format(ValueFormat.EMPTY.builder().type(Type.OBJECT).href(objectUrl).form(Form.EMPTY.builder().value(objectFormat)
                                                    .build()).build()).build())
                                    .add(Option.builder("type:array")
                                            .format(ValueFormat.EMPTY.builder().type(Type.OBJECT).href(arrayUrl).form(Form.EMPTY.builder()
                                                    .value(FormFields.EMPTY.builder()
                                                            .add(FormField.builder("name").required(true).build())
                                                            .add(FormField.builder("type").pattern("array|set").build())
                                                            .add(FormField.builder("required").type(Type.BOOLEAN).build())
                                                            .add(FormField.builder("nullable").type(Type.BOOLEAN).build())
                                                            .add(FormField.builder("mustBeNull").type(Type.BOOLEAN))
                                                            .add(FormField.builder("options")
                                                                    .type(Type.OBJECT)
                                                                    .form(Form.EMPTY.builder()
                                                                            .href(optionsUrl).build())).add(FormField.builder("etype").pattern("array|boolean|decimal|integer|object|set|string").build())
                                                            .add(FormField.builder("eform").type(Type.OBJECT).form(Form.EMPTY.builder().href(formUrl).build()))
                                                            .add(FormField.builder("element")
                                                                    .type(Type.OBJECT)
                                                                    .options(Options.builder()
                                                                            .value(Value.EMPTY.builder()
                                                                                    .add(Option.builder("type:string")
                                                                                            .format(ValueFormat.EMPTY.builder().type(Type.OBJECT).href(stringUrl).build()).build())
                                                                                    .add(Option.builder("type:boolean")
                                                                                            .format(ValueFormat.EMPTY.builder().type(Type.OBJECT).href(booleanUrl).build()).build())
                                                                                    .add(Option.builder("type:object")
                                                                                            .format(ValueFormat.EMPTY.builder().type(Type.OBJECT).href(objectUrl).build()).build())
                                                                                    .add(Option.builder("type:array")
                                                                                            .format(ValueFormat.EMPTY.builder().type(Type.OBJECT).href(arrayUrl).build()).build())
                                                                                    .add(Option.builder("type:integer")
                                                                                            .format(ValueFormat.EMPTY.builder().type(Type.OBJECT).href(integerUrl).build()).build())
                                                                                    .add(Option.builder("type:decimal")
                                                                                            .format(ValueFormat.EMPTY.builder().type(Type.OBJECT).href(decimalUrl).build()).build())
                                                                                    .build())
                                                                            .build())
                                                                    .build()
                                                            ).build())
                                                    .build()).build()).build())
                                    .build())
                            .build())
                    .build()
            ).build();


    private static Form formsForm = Form.EMPTY.builder()
            .href(formUrl)
            .value(FormFields.EMPTY.builder()
                    .add(FormField.builder("href").type(Type.STRING))
                    .add(FormField.builder("exclusive").type(Type.BOOLEAN))
                    .add(FormField
                            .builder("value")
                            .type(Type.ARRAY)
                            .element(ValueFormat.EMPTY.builder().type(Type.OBJECT)
                                    .options(Options.builder()
                                            .value(Value.EMPTY.builder()
                                                    .add(Option.builder("type:string")
                                                            .format(ValueFormat.EMPTY.builder()
                                                                    .href(stringFormFieldUrl)
                                                                    .type(Type.OBJECT)
                                                                    .form(Form.EMPTY.builder()
                                                                            .value(stringFormField)
                                                                            .build())
                                                                    .build())
                                                            .build())
                                                    .add(Option.builder("type:boolean")
                                                            .format(ValueFormat.EMPTY.builder()
                                                                    .type(Type.OBJECT)
                                                                    .href(booleanFormFieldUrl)
                                                                    .form(Form.EMPTY.builder()
                                                                            .value(booleanFormField)
                                                                            .build())
                                                                    .build())
                                                            .build())
                                                    .add(Option.builder("type:integer")
                                                            .format(ValueFormat.EMPTY.builder()
                                                                    .type(Type.OBJECT)
                                                                    .href(integerFormFieldUrl)
                                                                    .form(Form.EMPTY.builder()
                                                                            .value(integerFormField)
                                                                            .build())
                                                                    .build())
                                                            .build())
                                                    .add(Option.builder("type:decimal")
                                                            .format(ValueFormat.EMPTY.builder()
                                                                    .type(Type.OBJECT)
                                                                    .href(decimalFormFieldUrl)
                                                                    .form(Form.EMPTY.builder()
                                                                            .value(decimalFormField)
                                                                            .build())
                                                                    .build())
                                                            .build())
                                                    .add(Option.builder("type:object")
                                                            .format(ValueFormat.EMPTY.builder()
                                                                    .type(Type.OBJECT)
                                                                    .href(objectFormFieldUrl)
                                                                    .form(Form.EMPTY.builder()
                                                                            .value(objectFormField)
                                                                            .build())
                                                                    .build())
                                                            .build())
                                                    .add(Option
                                                            .builder("type:array")
                                                            .format(ValueFormat.EMPTY.builder()
                                                                    .type(Type.OBJECT)
                                                                    .href(arrayFormFieldUrl)
                                                                    .form(Form.EMPTY.builder()
                                                                            .value(arrayFormField)
                                                                            .build())
                                                                    .build())
                                                            .build())

                                                    .build())
                                            .build()).build()).build()).build()).build();


    @Test
    void filterFormWithFormTest() {

        JsonObject customForm = JsonObject.EMPTY
                .put("value", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("name", "fullName"))
                        .add(JsonObject.EMPTY
                                .put("name", "age")
                                .put("type", "integer"))
                        .add(JsonObject.EMPTY
                                .put("name", "heightInMeters")
                                .put("type", "decimal"))
                        .add(JsonObject.EMPTY
                                .put("name", "male")
                                .put("type", "boolean"))
                        .add(JsonObject.EMPTY
                                        .put("name", "mother")
                                        .put("type", "object")
                                //.put("form", JsonObject.EMPTY.put("value", JsonArray.EMPTY
                                //        .add(JsonObject.EMPTY.put("name", "mothersName"))))
                        )
                        .add(JsonObject.EMPTY
                                .put("name", "friends")
                                .put("type", "array")
                                .put("etype", "object")
                                .put("eform", JsonObject.EMPTY.put("value", JsonArray.EMPTY
                                        .add(JsonObject.EMPTY.put("name", "mothersName"))
                                )))
                        .add(JsonObject.EMPTY
                                .put("name", "undefinedArray")
                                .put("type", "array")
                                .put("element", JsonObject.EMPTY.put("type", "object"))
                        )

                );

        FormFilter formFilter = new FormFilter();


        FilterResult<JsonObjectWrapper> filterResult = formFilter.filter(formsForm, customForm, true, true);


        assertThat(customForm, is(filterResult.filtrate.asJsonValue().getValue(JsonObject.class)));

    }


    //@Test
    void formFilterSelf() {
        FilterResult<JsonObjectWrapper> filterYourself = new FormFilter().filter(formsForm, formsForm.asJsonObject(), true, true);
        assertThat(filterYourself.filtrate.asJsonObject(), is(formsForm.asJsonObject()));
    }





    @Test
    void emptyFormValidationTest() {

        JsonObject arrayInArrayForm = JsonObject.EMPTY
                .put("value", JsonArray.EMPTY);

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(formsForm, arrayInArrayForm, true, true);
        assertThat(result.waste.asJsonObject().toString(), result.waste.isEmpty(), is(true));
    }

    @Test
    void singleStringFormValidationTest() {

        JsonObject arrayInArrayForm = JsonObject.EMPTY
                .put("value", JsonArray.EMPTY.add(JsonObject.EMPTY
                        .put("name", "firstName")));

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(formsForm, arrayInArrayForm, true, true);
        assertThat(result.waste.asJsonObject().toString(), result.waste.isEmpty(), is(true));
    }

    @Test
    void singleArrayFieldFormValidationTest() {

        JsonObject formToBeValidated = JsonUtil.create("{'value':[{'name':'contacts','type':'array'}]}");

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(formsForm, formToBeValidated, true, true);
        assertThat(result.waste.asJsonObject().toString(), result.waste.isEmpty(), is(true));
    }

    @Test
    void singleArrayFieldWithElementFormValidationTest() {

        JsonObject formToBeValidated = JsonUtil.create("{'value':[{'name':'contacts','type':'array','element':{'type':'string'}}]}");

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(formsForm, formToBeValidated, true, true);
        assertThat(result.waste.asJsonObject().toString(), result.waste.isEmpty(), is(true));
    }

    @Test
    void multiArrayFieldWithElementFormValidationTest() {

        JsonObject formToBeValidated = JsonUtil.create("{'value':[{'name':'contacts','type':'array','element':{'type':'string'}},{'name':'friends','type':'array','element':{'type':'string'}}]}");

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(formsForm, formToBeValidated, true, true);
        assertThat(result.waste.asJsonObject().toString(), result.waste.isEmpty(), is(true));
    }

    @Test
    void singleArrayFieldsWithObjectElementFormValidationTest() {

        JsonObject formToBeValidated = JsonUtil.create("{'value':[{'name':'contacts','type':'array','element':{'type':'object','form':{'value':[{'name':'firstName'}]}}}]}");

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(formsForm, formToBeValidated, true, true);
        assertThat(result.waste.asJsonObject().toString(), result.waste.isEmpty(), is(true));
    }

    @Test
    void singleArrayFieldsWithIntegerElementFormValidationTest() {

        JsonObject formToBeValidated = JsonUtil.create("{'value':[{'name':'contacts','type':'array','element':{'type':'integer'}}]}");

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(formsForm, formToBeValidated, true, true);
        assertThat(result.waste.asJsonObject().toString(), result.waste.isEmpty(), is(true));
    }

    @Test
    void singleArrayFieldsWithDecimalElementFormValidationTest() {

        JsonObject formToBeValidated = JsonUtil.create("{'value':[{'name':'contacts','type':'array','element':{'type':'decimal'}}]}");

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(formsForm, formToBeValidated, true, true);
        assertThat(result.waste.asJsonObject().toString(), result.waste.isEmpty(), is(true));
    }

    @Test
    void singleArrayFieldsWithBooleanElementFormValidationTest() {

        JsonObject formToBeValidatedOk = JsonUtil.create("{'value':[{'name':'contacts','type':'array','element':{'type':'boolean'}}]}");

        FilterResult<JsonObjectWrapper> okResult = new FormFilter().filter(formsForm, formToBeValidatedOk, true, true);
        assertThat(okResult.waste.asJsonObject().toString(), okResult.waste.isEmpty(), is(true));
    }

    @Test
    void singleArrayFieldsWithBooleanElementFormValidationFailsTest() {

        JsonObject formToBeValidatedInvalid = JsonUtil.create("{'value':[{'name':'contacts','type':'array','element':{'type':'boolean','min':false}}]}");

        FilterResult<JsonObjectWrapper> invalidResult = new FormFilter().filter(formsForm, formToBeValidatedInvalid, true, true);
        assertThat(invalidResult.waste.asJsonObject().toString(), invalidResult.waste.isEmpty(), is(false));

        assertThat(invalidResult.waste.asJsonObject().get(JsonArray.of("paths", 0, "path")), is(JsonArray.of("value", 0).jsonValue()));

        JsonArray options = invalidResult.waste.asJsonObject().get(JsonArray.of("paths", 0, "violations", "options", "value")).getValue(JsonArray.class);

        //Option option = null;
        for (JsonObject option : options.asList(JsonObject.class)) {
            if (option.get("label").equals(JsonValue.of("type:array"))) {
                JsonArray supportedParams = option.get(JsonArray.of("error", "paths", 0, "violations", "options", "value", 1, "error", "paths", 0, "violations", "supportedParams")).getValue(JsonArray.class);
                assertThat(supportedParams, is(JsonArray.of("type", "required", "nullable", "mustBeNull", "options")));
            }
        }

    }

    @Test
    void arrayInArray() {

        JsonObject arrayInArrayForm = JsonObject.EMPTY
                .put("value", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("name", "contacts")
                                .put("type", Type.ARRAY)
                                .put("element", JsonObject.EMPTY
                                        .put("type", Type.OBJECT)
                                )));

        Form form = arrayInArrayForm.as(Form.class);

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(formsForm, arrayInArrayForm, true, true);
        assertThat(result.waste.asJsonObject().toString(), result.waste.isEmpty(), is(true));
    }

    @Test
    void arrayInArray2() {

        JsonObject formJsonObject = JsonObject.EMPTY
                .put("value", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("name", "contacts")
                                .put("type", Type.ARRAY)
                                .put("element", JsonObject.EMPTY
                                        .put("type", Type.OBJECT)
                                )));

        Form form = formJsonObject.as(Form.class);

        JsonObject data = JsonObject.EMPTY
                .put("contacts", JsonArray.EMPTY
                        .add(JsonObject.EMPTY.put("a", "aval").put("b", "bval")));

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(form, data, true, true);
        assertThat(result.waste.asJsonObject().toString(), result.waste.isEmpty(), is(true));
        assertThat(result.filtrate.asJsonValue(), is(data.jsonValue()));
    }


    @Test
    void arrayInArrayFiltrateTest() {
        Form form = JsonUtil.create(Form.class, "{'value': [{'name': 'myarray','type': 'array','element': {'type': 'array','element': {'type': 'string'}}}]}");

        JsonObject data = JsonUtil.create("{'myarray':[['lastName',{},4],['1','2']]}");
        JsonObject expectedFiltrate = JsonUtil.create("{'myarray':[['lastName',null,null],['1','2']]}");


        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(form, data, true, true);

        assertThat(result.waste.getWasteItem(0).path, is(JsonArray.of("myarray", 0, 1)));
        assertThat(result.waste.getWasteItem(1).path, is(JsonArray.of("myarray", 0, 2)));
        assertThat(result.filtrate, nullValue());
    }

    @Test
    void arrayInArraySimpleFiltrateTest() {
        Form form = JsonUtil.create(Form.class, "{'value': [{'name': 'myarray','type': 'array','element': {'type': 'array','element': {'type': 'string'}}}]}");

        JsonObject data = JsonUtil.create("{'myarray':[[4]]}");
        JsonObject expectedFiltrate = JsonUtil.create("{'myarray':[[null]]}");

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(form, data, true, true);
        assertThat(result.filtrate, nullValue());
    }
}
