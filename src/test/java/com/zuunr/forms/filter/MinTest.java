/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.filter;

import static com.zuunr.json.tool.JsonUtil.create;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.jupiter.api.Test;

import com.zuunr.forms.Form;
import com.zuunr.json.JsonObject;
import com.zuunr.json.util.JsonObjectWrapper;

/**
 * @author Niklas Eldberger
 */
class MinTest {

    @Test
    void minString(){

        Form form = create(Form.class, "{'value': [{'name':'param', 'min':'2'}]}");

        JsonObject invalidData = create(JsonObject.class, "{'param':'1'}");
        JsonObject validData = create(JsonObject.class, "{'param':'2'}");

        FilterResult<JsonObjectWrapper> filterResult = new FormFilter().filter(form, invalidData, true, true);
        assertThat(filterResult.waste.isEmpty(), is(false));
        assertThat(filterResult.waste.getWasteItem(0).violations.get("min").getValue(String.class), is("2"));

        filterResult = new FormFilter().filter(form, validData, true, true);
        assertThat(filterResult.waste.isEmpty(), is(true));
    }
}
