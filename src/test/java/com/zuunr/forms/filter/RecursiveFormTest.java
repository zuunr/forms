/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.filter;

import com.zuunr.forms.Form;
import com.zuunr.forms.FormField;
import com.zuunr.forms.FormFields;
import com.zuunr.forms.formfield.Type;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.util.JsonObjectWrapper;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Niklas Eldberger
 */
class RecursiveFormTest {

    @Test
    void test() {
        Form form = Form.EMPTY.builder()
                .href("https://example.com/someform")
                .exclusive(true)
                .value(FormFields.EMPTY.builder()
                        .add(FormField
                                .builder("nestedObject")
                                .type(Type.OBJECT)
                                .required(false)
                                .form(Form.EMPTY.builder()
                                        .href("https://example.com/someform").build()))
                        .add(FormField.builder("nestedArray")
                                .type(Type.ARRAY)
                                .eform(Form.EMPTY.builder()
                                        .href("https://example.com/someform").build()))
                        .build()).build();

        FormFilter formFilter = new FormFilter();
        FilterResult<JsonObjectWrapper> filterResult = formFilter.filter(form, JsonObject.EMPTY.put("nestedObject", JsonObject.EMPTY.put("nestedObject", JsonObject.EMPTY.put("nestedArray", JsonArray.EMPTY.add(JsonObject.EMPTY.put("nestedObject", JsonObject.EMPTY))))), true, true);

        assertThat(filterResult.waste.isEmpty(), is(true));
    }
}
