package com.zuunr.forms.filter;

import com.zuunr.forms.Form;
import com.zuunr.forms.FormFields;
import com.zuunr.forms.filter.FilterResult;
import com.zuunr.forms.filter.FormFilter;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectSupport;
import com.zuunr.json.JsonValue;
import com.zuunr.json.util.JsonObjectWrapper;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


public class FormFilterTest2 {

    private FormFilter formFilter = new FormFilter();

    @Test
    void authorizeTest() {
        JsonObject userPermissionItem = JsonObject.EMPTY
                .put("user", JsonObject.EMPTY
                        .put("href", "https://zuunr.com/v1/access-control/api/users/1234"))
                .put("agreement", JsonObject.EMPTY
                        .put("href", "https://zuunr.com/v1/access-control/api/agreements/4567"))
                .put("collection", "https://zuunr.com/v1/customerSystem/api/customers")
                .put("request", JsonObject.EMPTY
                        .put("POST", JsonObject.EMPTY

                                .put("params", JsonObject.EMPTY
                                        .put("all", false)
                                        .put("whitelist", JsonArray.EMPTY
                                                .add("name")
                                                .add("economics.fullOrderValue"))
                                        .put("blacklist", JsonArray.EMPTY)))
                        .put("PATCH", JsonObject.EMPTY
                                .put("params", JsonObject.EMPTY
                                        .put("all", false)
                                        .put("whitelist", JsonArray.EMPTY
                                                .add("name")
                                                .add("economics.fullOrderValue"))
                                        .put("blacklist", JsonArray.EMPTY)))
                        .put("GET", JsonObject.EMPTY
                                .put("item", "")))
                .put("read", JsonObject.EMPTY
                        .put("include", JsonObject.EMPTY
                                .put("form", JsonObject.EMPTY
                                        .put("value", JsonArray.EMPTY
                                                .add(JsonObject.EMPTY
                                                        .put("name", "status")
                                                        .put("type", "string")
                                                        .put("pattern", "ACTIVE|INACTIVE"))
                                                .add(JsonObject.EMPTY
                                                        .put("name", "name"))
                                                .add(JsonObject.EMPTY
                                                        .put("name", "loyalityLevel")
                                                        .put("type", "integer")
                                                )
                                                .add(JsonObject.EMPTY
                                                        .put("name", "meta")
                                                        .put("type", "object"))
                                                .add(JsonObject.EMPTY
                                                        .put("name", "economics")
                                                        .put("type", "object")
                                                        .put("form", JsonObject.EMPTY
                                                                .put("value", JsonArray.EMPTY
                                                                        .add(JsonObject.EMPTY
                                                                                .put("name", "fullOrderValue")
                                                                                .put("type", "integer")
                                                                        ))))
                                                .add(JsonObject.EMPTY
                                                        .put("name", "partners")
                                                        .put("type", "array")
                                                        .put("eform", JsonObject.EMPTY
                                                                .put("value", JsonArray.EMPTY
                                                                        .add(JsonObject.EMPTY
                                                                                .put("name", "partnerName")))))
                                        ))));

        JsonObject requestBody = JsonObject.EMPTY
                .put("status", "ACTIVE") // PROSPECT, ACTIVE, HISTORICAL
                .put("name", "Some Company Inc.")
                .put("loyalityLevel", 5)
                .put("economics", JsonObject.EMPTY
                        .put("fullOrderValue", 200000)
                        .put("margin", 30000))
                .put("partners", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("descriptionOfRelation", "We intend to do this and that together...")
                                .put("partnerName", "The A Company"))
                        .add(JsonObject.EMPTY
                                .put("partnerName", "B Inc")));

        FilterResult filterResult = formFilter.filter(userPermissionItem.get(JsonArray.EMPTY.add("read").add("include").add("form")).as(Form.class), requestBody, true, true);

        JsonObject expectedResult = JsonObject.EMPTY
                .put("waste", JsonObject.EMPTY
                        .put("paths", JsonArray.EMPTY
                                .add(JsonObject.EMPTY
                                        .put("path", JsonArray.EMPTY
                                                .add("economics"))
                                        .put("badValue", JsonObject.EMPTY
                                                .put("margin", 30000)
                                                .put("fullOrderValue", 200000))
                                        .put("violations", JsonObject.EMPTY
                                                .put("supportedParams", JsonArray.EMPTY
                                                        .add("fullOrderValue"))))
                                .add(JsonObject.EMPTY
                                        .put("path", JsonArray.EMPTY
                                                .add("partners")
                                                .add(0))
                                        .put("badValue", JsonObject.EMPTY
                                                .put("descriptionOfRelation", "We intend to do this and that together...")
                                                .put("partnerName", "The A Company"))
                                        .put("violations", JsonObject.EMPTY
                                                .put("supportedParams", JsonArray.EMPTY
                                                        .add("partnerName"))))))
                .put("filtrate", JsonObject.EMPTY
                        .put("name", "Some Company Inc.")
                        .put("loyalityLevel", 5)
                        .put("economics", JsonObject.EMPTY
                                .put("fullOrderValue", 200000))
                        .put("partners", JsonArray.EMPTY
                                .add(JsonObject.EMPTY
                                        .put("partnerName", "The A Company"))
                                .add(JsonObject.EMPTY
                                        .put("partnerName", "B Inc")))
                        .put("status", "ACTIVE"));

        assertThat(filterResult.asJsonObject().asPrettyJson(), is(expectedResult.asPrettyJson()));

        FilterResult filterResultWasteOnly = formFilter.filter(userPermissionItem.get(JsonArray.EMPTY.add("read").add("include").add("form")).as(Form.class), requestBody, false, true);

        JsonObject expectedResultOfWasteOnly = JsonObject.EMPTY
                .put("waste", JsonObject.EMPTY
                        .put("paths", JsonArray.EMPTY
                                .add(JsonObject.EMPTY
                                        .put("path", JsonArray.EMPTY
                                                .add("economics"))
                                        .put("badValue", JsonObject.EMPTY
                                                .put("margin", 30000)
                                                .put("fullOrderValue", 200000))
                                        .put("violations", JsonObject.EMPTY
                                                .put("supportedParams", JsonArray.EMPTY
                                                        .add("fullOrderValue"))))
                                .add(JsonObject.EMPTY
                                        .put("path", JsonArray.EMPTY
                                                .add("partners")
                                                .add(0))
                                        .put("badValue", JsonObject.EMPTY
                                                .put("descriptionOfRelation", "We intend to do this and that together...")
                                                .put("partnerName", "The A Company"))
                                        .put("violations", JsonObject.EMPTY
                                                .put("supportedParams", JsonArray.EMPTY
                                                        .add("partnerName"))))))
                .put("filtrate", JsonValue.NULL);
        assertThat(filterResultWasteOnly.asJsonObject().asPrettyJson(), is(expectedResultOfWasteOnly.asPrettyJson()));

        FilterResult filterResultFiltrateOnly = formFilter.filter(userPermissionItem.get(JsonArray.EMPTY.add("read").add("include").add("form")).as(Form.class), requestBody, false, true);

        JsonObject expectedResultOfFiltrateOnly = JsonObject.EMPTY.put("waste", JsonObject.EMPTY.put("paths", JsonArray.EMPTY.add(JsonObject.EMPTY.put("path", JsonArray.EMPTY.add("economics")).put("badValue", JsonObject.EMPTY.put("margin", 30000).put("fullOrderValue", 200000)).put("violations", JsonObject.EMPTY.put("supportedParams", JsonArray.EMPTY.add("fullOrderValue")))).add(JsonObject.EMPTY.put("path", JsonArray.EMPTY.add("partners").add(0)).put("badValue", JsonObject.EMPTY.put("descriptionOfRelation", "We intend to do this and that together...").put("partnerName", "The A Company")).put("violations", JsonObject.EMPTY.put("supportedParams", JsonArray.EMPTY.add("partnerName")))))).put("filtrate", JsonValue.NULL);

        assertThat(filterResultFiltrateOnly.asJsonObject().asPrettyJson(), is(expectedResultOfFiltrateOnly.asPrettyJson()));
    }


    @Test
    void filterTest() {

        JsonObject form = JsonObject.EMPTY.put("form", JsonObject.EMPTY
                .put("value", JsonArray.EMPTY
                        .add(JsonObject.EMPTY.put("name", "body").put("type", "object").put("form", JsonObject.EMPTY
                                .put("value", JsonArray.EMPTY
                                        .add(JsonObject.EMPTY.put("name", "status").put("type", "string").put("pattern", "ACTIVE|INACTIVE"))
                                        .add(JsonObject.EMPTY.put("name", "name"))
                                        .add(JsonObject.EMPTY.put("name", "loyalityLevel").put("type", "integer"))
                                        .add(JsonObject.EMPTY.put("name", "meta").put("type", "object"))
                                        .add(JsonObject.EMPTY.put("name", "economics")
                                                .put("type", "object")
                                                .put("form", JsonObject.EMPTY
                                                        .put("value", JsonArray.EMPTY
                                                                .add(JsonObject.EMPTY.put("name", "fullOrderValue").put("type", "integer")))))
                                        .add(JsonObject.EMPTY
                                                .put("name", "partners").put("type", "array").put("eform", JsonObject.EMPTY
                                                        .put("value", JsonArray.EMPTY
                                                                .add(JsonObject.EMPTY.put("name", "partnerName"))))))))));

        JsonObject data = JsonObject.EMPTY.put("body", JsonObject.EMPTY
                .put("status", "NOT_ACTIVE") // PROSPECT, ACTIVE, HISTORICAL
                .put("name", "Some Company Inc.")
                .put("loyalityLevel", 5)
                .put("economics", JsonObject.EMPTY
                        .put("fullOrderValue", 200000)
                        .put("margin", 30000))

                .put("partners", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("descriptionOfRelation", "We intend to do this and that together...")
                                .put("partnerName", "The A Company"))
                        .add(JsonObject.EMPTY
                                .put("partnerName", "B Inc")))
        );
        FilterResult filterResult = formFilter.filter(form.get("form").as(Form.class), data, true, true);

        JsonObject expectedResult = JsonObject.EMPTY
                .put("waste", JsonObject.EMPTY
                        .put("paths", JsonArray.EMPTY
                                .add(JsonObject.EMPTY
                                        .put("path", JsonArray.EMPTY
                                                .add("body")
                                                .add("status"))
                                        .put("description", "")
                                        .put("badValue", "NOT_ACTIVE")
                                        .put("violations", JsonObject.EMPTY
                                                .put("pattern", "ACTIVE|INACTIVE")))
                                .add(JsonObject.EMPTY
                                        .put("path", JsonArray.EMPTY
                                                .add("body")
                                                .add("economics"))
                                        .put("badValue", JsonObject.EMPTY
                                                .put("margin", 30000)
                                                .put("fullOrderValue", 200000))
                                        .put("violations", JsonObject.EMPTY
                                                .put("supportedParams", JsonArray.EMPTY
                                                        .add("fullOrderValue"))))
                                .add(JsonObject.EMPTY
                                        .put("path", JsonArray.EMPTY
                                                .add("body")
                                                .add("partners")
                                                .add(0))
                                        .put("badValue", JsonObject.EMPTY
                                                .put("descriptionOfRelation", "We intend to do this and that together...")
                                                .put("partnerName", "The A Company"))
                                        .put("violations", JsonObject.EMPTY
                                                .put("supportedParams", JsonArray.EMPTY
                                                        .add("partnerName"))))))
                .put("filtrate", JsonObject.EMPTY
                        .put("body", JsonObject.EMPTY
                                .put("name", "Some Company Inc.")
                                .put("loyalityLevel", 5)
                                .put("economics", JsonObject.EMPTY
                                        .put("fullOrderValue", 200000))
                                .put("partners", JsonArray.EMPTY
                                        .add(JsonObject.EMPTY
                                                .put("partnerName", "The A Company"))
                                        .add(JsonObject.EMPTY
                                                .put("partnerName", "B Inc")))));

        assertThat(filterResult.asJsonObject().asPrettyJson(), is(expectedResult.asPrettyJson()));
    }


    @Test
    void simplerValidateDataTest() {
        JsonObject form = JsonObject.EMPTY.put("form", JsonObject.EMPTY
                .put("value", JsonArray.EMPTY
                        .add(JsonObject.EMPTY.put("name", "body").put("type", "array").put("eform", JsonObject.EMPTY
                                .put("value", JsonArray.EMPTY
                                        .add(JsonObject.EMPTY.put("name", "status").put("type", "string").put("pattern", "ACTIVE|INACTIVE")))))));

        JsonObject data = JsonObject.EMPTY.put("body", JsonArray.EMPTY.add(JsonObject.EMPTY.put("status", "ACTIVE")).add(JsonObject.EMPTY.put("status", "ACTIVATED")));
        FilterResult result = formFilter.filter(form.get("form").as(Form.class), data, true, true);

        JsonObject expectedResult = JsonObject.EMPTY
                .put("waste", JsonObject.EMPTY
                        .put("paths", JsonArray.EMPTY
                                .add(JsonObject.EMPTY
                                        .put("path", JsonArray.EMPTY
                                                .add("body").add(1).add("status"))
                                        .put("description", "")
                                        .put("badValue", "ACTIVATED")
                                        .put("violations", JsonObject.EMPTY
                                                .put("pattern", "ACTIVE|INACTIVE")))))
                .put("filtrate", JsonObject.EMPTY
                        .put("body", JsonArray.EMPTY
                                .add(JsonObject.EMPTY
                                        .put("status", "ACTIVE"))
                                .add(JsonObject.EMPTY)));
        assertThat(result.asJsonObject(), is(expectedResult));
    }

    @Test
    void fullFilterTest() {

        JsonArray form = JsonArray.EMPTY
                .add(JsonObject.EMPTY
                        .put("minlength", 10)
                        .put("nullable", false)
                        .put("maxlength", 10)
                        .put("name", "context")
                        .put("desc", "the id of the organisation")
                        .put("pattern", ".*")
                        .put("mustBeNull", false)
                        .put("type", "string"))
                .add(JsonObject.EMPTY
                        .put("minlength", 52)
                        .put("nullable", false)
                        .put("maxlength", 52)
                        .put("name", "href")
                        .put("pattern", "http://zuunr.com/v1/TheGreatSystem/api/employees/\\w+")
                        .put("mustBeNull", false)
                        .put("type", "string"))
                .add(JsonObject.EMPTY
                        .put("nullable", false)
                        .put("form", JsonObject.EMPTY
                                .put("value", JsonArray.EMPTY
                                        .add(JsonObject.EMPTY
                                                .put("equals", JsonArray.EMPTY
                                                        .add(JsonArray.EMPTY.add("meta").add("modifiedAt")))
                                                .put("minlength", 24)
                                                .put("nullable", false)
                                                .put("maxlength", 24)
                                                .put("name", "createdAt")
                                                .put("pattern", ".*")
                                                .put("mustBeNull", false)
                                                .put("type", "string")
                                                .put("required", true))
                                        .add(JsonObject.EMPTY
                                                .put("equals", JsonArray.EMPTY
                                                        .add(JsonArray.EMPTY.add("meta").add("createdAt")))
                                                .put("minlength", 24)
                                                .put("nullable", false)
                                                .put("maxlength", 24)
                                                .put("name", "modifiedAt")
                                                .put("pattern", ".*")
                                                .put("mustBeNull", false)
                                                .put("type", "string")
                                                .put("required", true))))
                        .put("name", "meta")
                        .put("mustBeNull", false)
                        .put("type", "object"))
                .add(JsonObject.EMPTY
                        .put("minlength", 8)
                        .put("nullable", false)
                        .put("maxlength", 8)
                        .put("name", "status")
                        .put("pattern", "STATUS_1|STATUS_2")
                        .put("mustBeNull", false)
                        .put("type", "string"))
                .add(JsonObject.EMPTY
                        .put("nullable", false)
                        .put("eform", JsonObject.EMPTY
                                .put("value", JsonArray.EMPTY
                                        .add(JsonObject.EMPTY
                                                .put("minlength", 5)
                                                .put("nullable", false)
                                                .put("maxlength", 7)
                                                .put("name", "roleName")
                                                .put("pattern", "ADMIN|SALES|SUPPORT")
                                                .put("mustBeNull", false)
                                                .put("type", "string")
                                                .put("required", true))))
                        .put("name", "roles")
                        .put("mustBeNull", false)
                        .put("type", "set"))
                .add(JsonObject.EMPTY
                        .put("minlength", 4)
                        .put("nullable", true)
                        .put("maxlength", 4)
                        .put("name", "gender")
                        .put("pattern", "MALE")
                        .put("mustBeNull", false)
                        .put("type", "string")
                        .put("required", false));

        JsonObject sample1 = JsonObject.EMPTY
                .put("href", "http://zuunr.com/v1/TheGreatSystem/api/employees/123")
                .put("status", "STATUS_2")
                .put("context", "company123")
                .put("gender", "MALE")
                .put("roles", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("roleName", "ADMIN"))
                        .add(JsonObject.EMPTY
                                .put("roleName", "SALES")))
                .put("meta", JsonObject.EMPTY
                        .put("createdAt", "2017-12-12T12:12:12:121Z")
                        .put("modifiedAt", "2017-12-12T12:12:12:121Z"));
        FilterResult filterResult = formFilter.filter(form, sample1, true, true);
        assertThat(filterResult.wasteAsJsonValue().get("paths").getValue(JsonArray.class).isEmpty(), is(true));

        JsonObject sample2 = JsonObject.EMPTY
                .put("href", "http://zuunr.com/v1/TheGreatSystem/api/employees/123") // ok!
                .put("status", "STATUS_2") // ok!
                .put("context", "short") //should be at least length 10
                .put("gender", "male") // should be "MALE"
                .put("roles", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("roleName", "ADMIN")) // ok!
                        .add(JsonObject.EMPTY
                                .put("roleName", "SALES"))) // ok!
                .put("meta", JsonObject.EMPTY
                        .put("createdAt", "2017-12-12T12:12:12:121Z") // Should be same as modifiedAt
                        .put("modifiedAt", "3333-12-12T12:12:12:121Z")); // Should be same as createdAt
        FilterResult filterResult2 = formFilter.filter(form, sample2, true, true);

        JsonObject expectedFilterResult2 = JsonObject.EMPTY.put("waste", JsonObject.EMPTY.put("paths", JsonArray.EMPTY.add(JsonObject.EMPTY.put("path", JsonArray.EMPTY.add("context")).put("description", "the id of the organisation").put("badValue", "short").put("violations", JsonObject.EMPTY.put("minlength", 10))).add(JsonObject.EMPTY.put("path", JsonArray.EMPTY.add("gender")).put("description", "").put("badValue", "male").put("violations", JsonObject.EMPTY.put("pattern", "MALE"))))).put("filtrate", JsonObject.EMPTY.put("href", "http://zuunr.com/v1/TheGreatSystem/api/employees/123").put("meta", JsonObject.EMPTY.put("createdAt", "2017-12-12T12:12:12:121Z").put("modifiedAt", "3333-12-12T12:12:12:121Z")).put("status", "STATUS_2").put("roles", JsonArray.EMPTY.add(JsonObject.EMPTY.put("roleName", "ADMIN")).add(JsonObject.EMPTY.put("roleName", "SALES"))));
        assertThat(filterResult2.asJsonObject().diff(expectedFilterResult2).asPrettyJson(), filterResult2.asJsonObject(), is(expectedFilterResult2));
    }


    @Test
    void filterNestedObjectTest() {

        JsonObject sample = JsonObject.EMPTY
                .put("nested", JsonObject.EMPTY
                        .put("partofmewillbewasted", JsonObject.EMPTY
                                .put("tobewasted", "I am wasted")
                                .put("tobekept", "I am a part of the filtrate")));


        JsonArray formFields = JsonArray.EMPTY
                .add(JsonObject.EMPTY
                        .put("name", "nested")
                        .put("type", "object")
                        .put("form", JsonObject.EMPTY.put("value", JsonArray.EMPTY
                                        .add(JsonObject.EMPTY
                                                .put("name", "partofmewillbewasted")
                                                .put("type", "object")
                                                .put("form", JsonObject.EMPTY.put("value", JsonArray.EMPTY
                                                        .add(JsonObject.EMPTY
                                                                .put("name", "tobekept")
                                                        ))))
                                )
                        ));

        FilterResult filterResult = formFilter.filter(formFields, sample, true, false);

        JsonArray path2tobekept = JsonArray.of("nested", "partofmewillbewasted", "tobekept");
        assertThat(filterResult.filtrateAsJsonValue().get(path2tobekept), is(sample.get(path2tobekept)));
        JsonArray path2towasted = JsonArray.of("nested", "partofmewillbewasted", "tobewasted");
        assertThat(filterResult.filtrateAsJsonValue().get(path2towasted, JsonValue.NULL), is(JsonValue.NULL));
    }

    @Test
    void exclusiveTest() {

        JsonArray formFields = JsonArray.of(
                JsonObject.EMPTY
                        .put("name", "root")
                        .put("type", "object")
                        .put("form", JsonObject.EMPTY
                                .put("exclusive", false)
                                .put("value", JsonArray.EMPTY
                                        .add(JsonObject.EMPTY
                                                .put("name", "a"))
                                )),
                JsonObject.EMPTY
                        .put("name", "myArray")
                        .put("type", "array")
                        .put("eform", JsonObject.EMPTY
                                .put("value", JsonArray.EMPTY)
                                .put("exclusive", false))
        );

        FormFilter formFilter = new FormFilter();
        FilterResult<JsonObjectWrapper> filterResult = formFilter.filter(Form.EMPTY.builder().value(formFields.as(FormFields.class)).build(), JsonObject.EMPTY.put("myArray", JsonArray.EMPTY.add(JsonObject.EMPTY.put("this", "is ok by the non exclusive form"))).put("root", JsonObject.EMPTY.put("a", "Value of a").put("b", "Should be ok because not exclusive to a")), true, true);

        JsonObject expected = JsonObject.EMPTY.put("waste", JsonObject.EMPTY.put("paths", JsonArray.EMPTY)).put("filtrate", JsonObject.EMPTY.put("myArray", JsonArray.EMPTY.add(JsonObject.EMPTY.put("this", "is ok by the non exclusive form"))).put("root", JsonObject.EMPTY.put("a", "Value of a").put("b", "Should be ok because not exclusive to a")));

        assertThat(filterResult.asJsonObject(), is(expected));
    }
}
