/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.filter;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;

import com.zuunr.forms.Form;
import com.zuunr.forms.FormField;
import com.zuunr.forms.FormFields;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValueTestUtil;
import com.zuunr.json.util.JsonObjectWrapper;

/**
 * @author Niklas Eldberger
 */
class StringFormTest {

    private static FormFilter formFilter = new FormFilter();

    //@Test
    void test() {
        Form form = Form.EMPTY.builder().value(FormFields.EMPTY.builder()
                .add(FormField.builder("stringField").pattern(".* .*").minlength(5).maxlength(20).build())
                .add(FormField.builder("stringField2").pattern(".* .*").minlength(5).maxlength(20).build())
                .add(FormField.builder("stringField3").pattern(".* .*").minlength(5).maxlength(20).build())
                .add(FormField.builder("stringField4").pattern(".* .*").minlength(5).maxlength(20).build())
                .add(FormField.builder("stringField5").pattern(".* .*").minlength(5).maxlength(20).build())
                .add(FormField.builder("stringField6").pattern(".* .*").minlength(5).maxlength(20).build())
                .add(FormField.builder("stringField7").pattern(".* .*").minlength(5).maxlength(20).build())
                .add(FormField.builder("stringField8").pattern(".* .*").minlength(5).maxlength(20).build())
                .add(FormField.builder("stringField9").pattern(".* .*").minlength(5).maxlength(20).build())
                .add(FormField.builder("stringField10").pattern(".* .*").minlength(5).maxlength(20).build())
                .build()).build();

        JsonObject data = JsonObject.EMPTY
                .put("stringField", "Hello World!")
                .put("stringField2", "Hello World!")
                .put("stringField3", "Hello World!")
                .put("stringField4", "Hello World!")
                .put("stringField5", "Hello World!")
                .put("stringField6", "Hello World!")
                .put("stringField7", "Hello World!")
                .put("stringField8", "Hello World!")
                .put("stringField9", "Hello World!")
                .put("stringField10", "Hello World!");

        // One warm-up validation step to cache forms
        FilterResult<JsonObjectWrapper> result = formFilter.filter(form, data, true, false);

        long instancesFromStart = JsonValueTestUtil.jsonValueInstances();
        long startTime = System.nanoTime();

        long iterations = 1000000;
        for (long i = 0; i < iterations; i++) {
            result = formFilter.filter(form, data, true, true);
        }
        long endTime = System.nanoTime();
        long execTime = endTime - startTime;

        long instancesAtEnd = JsonValueTestUtil.jsonValueInstances();

        long diff = (instancesAtEnd - instancesFromStart) / iterations;


        assertThat(result.filtrate.asJsonValue().getJsonObject(), is(data));
        assertThat(execTime / iterations, lessThan(1000L));
        System.out.println("Exectime per call: " + execTime/iterations);
        assertThat("instances per call to formFilter.filter(): " + diff, diff <= 2, is(true));

    }
}
