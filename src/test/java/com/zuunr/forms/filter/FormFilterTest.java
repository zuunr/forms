/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.filter;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

import com.zuunr.forms.formfield.Type;
import org.junit.jupiter.api.Test;

import com.zuunr.forms.Form;
import com.zuunr.forms.FormField;
import com.zuunr.forms.FormFields;
import com.zuunr.forms.formfield.Options;
import com.zuunr.forms.formfield.options.Option;
import com.zuunr.forms.formfield.options.Value;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.tool.JsonUtil;
import com.zuunr.json.util.JsonObjectWrapper;

/**
 * @author Niklas Eldberger
 */
class FormFilterTest {

    private static FormFilter formFilter = new FormFilter();

    @Test
    void mustBeNull() {

        FormField.Builder b = FormField.builder("a");

        Form form = Form.EMPTY.builder().value(FormFields.EMPTY.builder()
                .add(FormField
                        .builder("iHaveToBeNull")
                        .nullable(true)
                        .mustBeNull(true)
                        .build())
                .build()
        ).build();

        FilterResult<JsonObjectWrapper> filterResultOk = new FormFilter().filter(form, JsonObject.EMPTY.builder().put("iHaveToBeNull", JsonValue.NULL).build(), true, true);

        assertThat(filterResultOk.waste.isEmpty(), is(true));

        FilterResult<JsonObjectWrapper> filterResultError = new FormFilter().filter(form, JsonObject.EMPTY.builder().put("iHaveToBeNull", "Some value").build(), true, true);

        assertThat(filterResultError.waste.isEmpty(), is(false));
        assertThat(filterResultError.waste.getWasteItem(0).violations.get("mustBeNull"), is(JsonValue.TRUE));
    }

    @Test
    void eFormTest() {
        Form form = JsonUtil.create(Form.class, "{'value':[{'name':'elements', 'type':'array', 'eform':{'value':[]}}]}");
        JsonObject data = JsonUtil.create("{'elements': [{},{'key':'value'},{}]}");

        FilterResult<JsonObjectWrapper> filterResult = formFilter.filter(form, data, true, true);
        assertThat(filterResult.waste.isEmpty(), is(false));
        assertThat(filterResult.waste.wasteItemsSize(), is(1));
        assertThat(filterResult.waste.getWasteItem(0).path, is(JsonArray.of("elements", 1)));

        filterResult = formFilter.filter(form, data, true, false);
        assertThat(filterResult.waste, nullValue());

        filterResult = formFilter.filter(form, data, false, true);
        assertThat(filterResult.filtrate, nullValue());
    }

    @Test
    void elementFormTest() {
        Form form = JsonUtil.create(Form.class, "{'value':[{'name':'theelements', 'type':'array', 'element': {'type':'object','form':{'value':[]}}}]}");
        JsonObject data = JsonUtil.create("{'theelements': [{},{'key':'value'},{}]}");

        FilterResult<JsonObjectWrapper> filterResult = formFilter.filter(form, data, true, true);
        assertThat(filterResult.waste.isEmpty(), is(false));
        assertThat(filterResult.waste.wasteItemsSize(), is(1));
        assertThat(filterResult.waste.getWasteItem(0).path, is(JsonArray.of("theelements", 1)));

        filterResult = formFilter.filter(form, data, true, false);
        assertThat(filterResult.waste, nullValue());

        filterResult = formFilter.filter(form, data, false, true);
        assertThat(filterResult.filtrate, nullValue());
    }

    @Test
    void elementStringTest() {
        Form form = JsonUtil.create(Form.class, "{'value':[{'name':'theelements', 'type':'array', 'element': {'type':'string','pattern':'a|c'}}]}");
        JsonObject data = JsonUtil.create("{'theelements': ['a','b','c']}");

        FilterResult<JsonObjectWrapper> filterResult = formFilter.filter(form, data, true, true);
        assertThat(filterResult.waste.isEmpty(), is(false));
        assertThat(filterResult.waste.wasteItemsSize(), is(1));
        assertThat(filterResult.waste.getWasteItem(0).path, is(JsonArray.of("theelements", 1)));

        filterResult = formFilter.filter(form, data, true, false);
        assertThat(filterResult.waste, nullValue());

        filterResult = formFilter.filter(form, data, false, true);
        assertThat(filterResult.filtrate, nullValue());
    }

    @Test
    void optionsTest() {

        Options redGreenOptions = Options.builder()
                .value(Value.EMPTY.builder()
                        .add(Option.builder("Red").value(JsonValue.of("RED")).build())
                        .add(Option.builder("Green").value(JsonValue.of("GREEN")).build())
                        .build()).build();

        FormField formField = FormField.builder("color")
                .options(redGreenOptions).build();

        Form formWithOptions = Form.EMPTY.builder()
                .value(FormFields.EMPTY.builder()
                        .add(formField)
                        .build())
                .build();

        FilterResult<JsonObjectWrapper> result1 = formFilter.filter(formWithOptions, JsonObject.EMPTY.put("color", "RED"), true, true);

        assertThat(result1.waste.isEmpty(), is(true));
        assertThat(result1.filtrate.asJsonObject(), is(JsonObject.EMPTY.put("color", "RED")));

        FilterResult<JsonObjectWrapper> result2 = formFilter.filter(formWithOptions, JsonObject.EMPTY.put("color", "BLUE"), true, true);

        assertThat(result2.waste.isEmpty(), is(false));
        assertThat(result2.waste.getWasteItem(0).violations.get("options"), is(redGreenOptions.asJsonValue()));
        assertThat(result2.filtrate.asJsonObject(), is(JsonObject.EMPTY));
    }

    @Test
    void wrongMinTest(){
        Form form = JsonUtil.create(Form.class,
        "{'value':[{'name':'person.age','type':'integer', 'min':10}]}");

        FilterResult<JsonObjectWrapper> filterResult = formFilter.filter(form, JsonObject.EMPTY.put("person.age", "10"), true, true);
        assertThat(filterResult.waste.isEmpty(), is(false));
        assertThat(filterResult.waste.getWasteItem(0).violations.get("type").getValue(), is("integer"));
    }


    @Test
    void dateTest(){
        Form form = JsonUtil.create(Form.class,
                "{'value':[{'name':'birthDate','type':'date', 'max':'1999-01-06'}]}");

        FilterResult<JsonObjectWrapper> filterResult = formFilter.filter(form, JsonObject.EMPTY.put("birthDate", 20000531), true, true);
        assertThat(filterResult.waste.isEmpty(), is(false));
        assertThat(filterResult.waste.getWasteItem(0).violations.get("type").getValue(), is("date"));

        filterResult = formFilter.filter(form, JsonObject.EMPTY.put("birthDate", "2000-05-31"), true, true);
        assertThat(filterResult.waste.isEmpty(), is(false));
        assertThat(filterResult.waste.getWasteItem(0).violations.get("max").getValue(), is("1999-01-06"));

        filterResult = formFilter.filter(form, JsonObject.EMPTY.put("birthDate", "1980-02-02"), true, true);
        assertThat(filterResult.waste.isEmpty(), is(true));
    }


    @Test
    public void required1(){
        Form form = Form.EMPTY.builder()
                .value(FormFields.EMPTY.builder()
                        .add(FormField
                                .builder("required1")
                                .required(true)
                                .pattern("OK")
                                .build())
                        .build())
                .build();



        FilterResult result1 = formFilter.filter(form, JsonObject.EMPTY.put("required1", "NOT_OK").put("nonRequired", "Just ok!"), true, true);
        assertThat(result1.filtrate, nullValue());

        FilterResult result2 = formFilter.filter(form, JsonObject.EMPTY.put("required1", "OK"), true, true);
        assertThat(result2.filtrate.asJsonObject(), is(JsonObject.EMPTY.put("required1", "OK")));
    }

    @Test
    public void required2(){
        Form form = Form.EMPTY.builder()
                .value(FormFields.EMPTY.builder()
                        .add(FormField
                                .builder("required1")
                                .required(true)
                                .type(Type.OBJECT)
                                .form(Form.EMPTY.builder()
                                        .value(FormFields.EMPTY.builder()
                                                .add(FormField.builder("required2").required(true).type(Type.STRING))
                                                .build()).build()).build()).build()).build();

        FilterResult result1 = formFilter.filter(form, JsonObject.EMPTY.put("required1", JsonObject.EMPTY.put("required2", JsonValue.NULL)), true, true);
        assertThat(result1.filtrate, nullValue());

        FilterResult result2 = formFilter.filter(form, JsonObject.EMPTY.put("required1", JsonObject.EMPTY.put("otherField", "some value")), true, true);
        assertThat(result2.filtrate, nullValue());

        FilterResult result3 = formFilter.filter(form, JsonObject.EMPTY.put("required1", JsonObject.EMPTY.put("required2", "some value")), true, true);
        assertThat(result3.filtrate.asJsonObject(), is(JsonObject.EMPTY.put("required1", JsonObject.EMPTY.put("required2", "some value"))));

    }

    @Test
    public void requiredArray(){
        Form form = Form.EMPTY.builder()
                .value(FormFields.EMPTY.builder()
                        .add(FormField
                                .builder("required1")
                                .required(true)
                                .type(Type.ARRAY)
                                .eform(Form.EMPTY.builder()
                                        .value(FormFields.EMPTY.builder()
                                                .add(FormField.builder("required2").required(true).type(Type.STRING))
                                                .build()).build()).build()).build()).build();

        FilterResult result1 = formFilter.filter(form, JsonObject.EMPTY.put("required1", JsonArray.of(JsonObject.EMPTY.put("required2", JsonValue.NULL))), true, true);
        assertThat(result1.filtrate, nullValue());

        FilterResult result2 = formFilter.filter(form, JsonObject.EMPTY.put("required1", JsonArray.of(JsonObject.EMPTY.put("otherField", "some value"))), true, true);
        assertThat(result2.filtrate, nullValue());

        FilterResult result3 = formFilter.filter(form, JsonObject.EMPTY.put("required1", JsonArray.of(JsonObject.EMPTY.put("required2", "some value"))), true, true);
        assertThat(result3.filtrate.asJsonObject(), is(JsonObject.EMPTY.put("required1", JsonArray.of(JsonObject.EMPTY.put("required2", "some value")))));
    }
}
