/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.filter;

import com.zuunr.forms.Form;
import com.zuunr.forms.FormField;
import com.zuunr.forms.FormFields;
import com.zuunr.forms.ValueFormat;
import com.zuunr.forms.formfield.Options;
import com.zuunr.forms.formfield.Type;
import com.zuunr.forms.formfield.options.Option;
import com.zuunr.forms.formfield.options.Value;
import com.zuunr.json.*;
import com.zuunr.json.util.JsonObjectWrapper;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Niklas Eldberger
 */
class OptionsFormsTest {

    @Test
    void valueAndFormOptionsCombined() {
        Form form = Form.EMPTY.builder()
                .value(FormFields.EMPTY.builder()
                        .add(FormField
                                .builder("offer")
                                .type(Type.OBJECT)
                                .required(false)
                                .options(Options.builder()
                                        .value(Value.EMPTY.builder()
                                                .add(Option.builder("small")
                                                        .format(ValueFormat.EMPTY.builder().type(Type.OBJECT).form(Form.EMPTY.builder()
                                                                .value(FormFields.EMPTY.builder()
                                                                        .add(FormField.builder("price")
                                                                                .type(Type.INTEGER).min(0).max(5)
                                                                                .build())
                                                                        .build())
                                                                .build()).build()).build())
                                                .add(Option.builder("a value in the middle")
                                                        .value(JsonObject.EMPTY.put("price", 7).jsonValue())
                                                        .build())
                                                .add(Option.builder("large")
                                                        .format(ValueFormat.EMPTY.builder().type(Type.OBJECT).form(Form.EMPTY.builder()
                                                                .value(FormFields.EMPTY.builder()
                                                                        .add(FormField.builder("price")
                                                                                .type(Type.INTEGER).min(10)
                                                                                .build())
                                                                        .build()).build())
                                                                .build())
                                                        .build())
                                                .build())
                                        .build())).build()).build();

        FormFilter formFilter = new FormFilter();

        FilterResult<JsonObjectWrapper> filterResult0 = formFilter.filter(form, JsonObject.EMPTY.put("offer", JsonObject.EMPTY.put("price", 7)), true, true);
        assertThat(filterResult0.waste.isEmpty(), is(true));

        FilterResult<JsonObjectWrapper> filterResult1 = formFilter.filter(form, JsonObject.EMPTY.put("offer", JsonObject.EMPTY.put("price", 1)), true, true);
        assertThat(filterResult1.waste.isEmpty(), is(true));

        FilterResult<JsonObjectWrapper> filterResult2 = formFilter.filter(form, JsonObject.EMPTY.put("offer", JsonObject.EMPTY.put("price", 6)), true, true);
        assertThat(filterResult2.waste.isEmpty(), is(false));

        assertThat(filterResult2.waste.getWasteItem(0).violations.get(JsonArray.of("options", "value", 0, "label")).getValue(String.class), is("small"));
        assertThat(filterResult2.waste.getWasteItem(0).violations.get(JsonArray.of("options", "value", 0, "error", "paths", 0, "violations", "max")).asInteger(), is(5));

        assertThat(filterResult2.waste.getWasteItem(0).violations.get(JsonArray.of("options", "value", 1, "label")).getValue(String.class), is("a value in the middle"));
        assertThat(filterResult2.waste.getWasteItem(0).violations.get(JsonArray.of("options", "value", 1, "value")), is(JsonObject.EMPTY.put("price", 7).jsonValue()));

        assertThat(filterResult2.waste.getWasteItem(0).violations.get(JsonArray.of("options", "value", 2, "label")).getValue(String.class), is("large"));
        assertThat(filterResult2.waste.getWasteItem(0).violations.get(JsonArray.of("options", "value", 2, "error", "paths", 0, "violations", "min")).asInteger(), is(10));

        FilterResult<JsonObjectWrapper> filterResult3 = formFilter.filter(form, JsonObject.EMPTY.put("offer", JsonObject.EMPTY.put("price", 10)), true, true);
        assertThat(filterResult3.waste.isEmpty(), is(true));
    }

    @Test
    void testOptionsInArray() {

        FormField formField = FormField
                .builder("array")
                .type(Type.ARRAY)
                .element(ValueFormat.EMPTY.builder()
                        .type(Type.STRING)
                        .options(Options.builder()
                                .value(Value.EMPTY.builder()
                                        .add(Option.builder("label1")
                                                .value(JsonValue.of("value1"))
                                                .build())
                                        .add(Option.builder("label2")
                                                .value(JsonValue.of("value2"))
                                                .build())
                                        .build())
                                .build())
                        .build())
                .build();

        Form form = Form.EMPTY.builder()
                .value(JsonArray.EMPTY
                        .add(formField.asJsonObject()).as(FormFields.class)).build();

        FormFilter formFilter = new FormFilter();

        JsonObject data1 = JsonObject.EMPTY.put("array", JsonArray.EMPTY.add("value1"));
        FilterResult<JsonObjectWrapper> filterResult1 = formFilter.filter(form, data1, true, true);
        assertThat(filterResult1.waste.isEmpty(), is(true));

        JsonObject data2 = JsonObject.EMPTY.put("array", JsonArray.EMPTY.add("value3"));
        FilterResult<JsonObjectWrapper> filterResult2 = formFilter.filter(form, data2, true, true);
        assertThat(filterResult2.waste.isEmpty(), is(false));
    }
}
