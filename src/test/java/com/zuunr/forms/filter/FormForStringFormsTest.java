/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.filter;

import com.zuunr.forms.Form;
import com.zuunr.forms.FormField;
import com.zuunr.forms.FormFields;
import com.zuunr.forms.ValueFormat;
import com.zuunr.forms.filter.result.Waste;
import com.zuunr.forms.formfield.Options;
import com.zuunr.forms.formfield.Type;
import com.zuunr.forms.formfield.options.Option;
import com.zuunr.forms.formfield.options.Value;
import com.zuunr.forms.generation.StringPropertyEnumMerger;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.tool.JsonUtil;
import com.zuunr.json.util.JsonObjectWrapper;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

/**
 * @author Niklas Eldberger
 */
class FormForStringFormsTest {

    FormFilter formFilter = new FormFilter();

    private static String formUrl = "https://zuunr.com/form";
    private static String stringUrl = "https://zuunr.com/string";
    private static String booleanUrl = "https://zuunr.com/boolean";
    private static String objectUrl = "https://zuunr.com/object";
    private static String arrayUrl = "https://zuunr.com/array";
    private static String integerUrl = "https://zuunr.com/integer";
    private static String decimalUrl = "https://zuunr.com/decimal";

    private static String stringOptionsUrl = "https://zuunr.com/formfield/string/options";
    private static String booleanOptionsUrl = "https://zuunr.com/formfield/boolean/options";
    private static String objectOptionsUrl = "https://zuunr.com/formfield/object/options";
    private static String arrayOptionsUrl = "https://zuunr.com/formfield/array/options";
    private static String integerOptionsUrl = "https://zuunr.com/formfield/integer/options";
    private static String decimalOptionsUrl = "https://zuunr.com/formfield/decimal/options";

    private static String stringFormFieldUrl = "https://zuunr.com/formfield/string";
    private static String booleanFormFieldUrl = "https://zuunr.com/formfield/boolean";
    private static String objectFormFieldUrl = "https://zuunr.com/formfield/object";
    private static String arrayFormFieldUrl = "https://zuunr.com/formfield/array";
    private static String integerFormFieldUrl = "https://zuunr.com/formfield/integer";
    private static String decimalFormFieldUrl = "https://zuunr.com/formfield/decimal";


    private static Form baseOptionsForm = Form.EMPTY.builder()
            .href(stringOptionsUrl)
            .value(FormFields.EMPTY.builder()
                    .add(FormField.builder("href").build())
                    .add(FormField.builder("value")
                            .required(true)
                            .type(Type.ARRAY)
                            .eform(Form.EMPTY.builder()
                                    .value(FormFields.EMPTY.builder()
                                            .add(FormField.builder("label").required(true).build())
                                            .add(FormField.builder("value").type(Type.STRING).build())
                                            .add(FormField.builder("format")
                                                    .type(Type.OBJECT)
                                                    .form(Form.EMPTY
                                                            .builder()
                                                            .value(FormFields.EMPTY)
                                                            .build())
                                                    .build())
                                            .build())
                                    .build())
                            .build())
                    .build()
            ).build();


    private static JsonArray PATH_TO_TYPE_FORMAT_IN_BASE_OPTION_FORM = JsonArray.of("value", 1, "eform", "value", 2, "format", "form", "value");

    // type:string

    private static FormFields stringFormat = FormFields.EMPTY.builder()
            .add(FormField.builder("type").pattern("string").build())
            .add(FormField.builder("pattern").build())
            .add(FormField.builder("required").type(Type.BOOLEAN).build())
            .add(FormField.builder("nullable").type(Type.BOOLEAN).build())
            .add(FormField.builder("mustBeNull").type(Type.BOOLEAN))
            .add(FormField.builder("minlength").type(Type.INTEGER))
            .add(FormField.builder("maxlength").type(Type.INTEGER))
            .add(FormField.builder("desc").type(Type.STRING))
            .build();

    private static Form stringOptionsForm = baseOptionsForm.asJsonObject()
            .put(PATH_TO_TYPE_FORMAT_IN_BASE_OPTION_FORM, stringFormat.asJsonValue()).as(Form.class);

    private static FormFields stringFormField = stringFormat.asJsonArray()
            .add(0, FormField.builder("name").required(true).build().asJsonValue())
            .add(FormField.builder("options").type(Type.OBJECT)
                    .form(stringOptionsForm)
                    .build().asJsonValue()).as(FormFields.class);


    // type:object

    private static FormFields objectFormat = FormFields.EMPTY
            .builder()
            .add(FormField.builder("type").pattern("object").build())
            .add(FormField.builder("required").type(Type.BOOLEAN).build())
            .add(FormField.builder("nullable").type(Type.BOOLEAN).build())
            .add(FormField.builder("mustBeNull").type(Type.BOOLEAN))
            .add(FormField.builder("desc").type(Type.STRING))
            .add(FormField.builder("options")
                    .type(Type.OBJECT)
                    .form(Form.EMPTY.builder()
                            .href(objectOptionsUrl).build()))
            .add(FormField.builder("form")
                    .type(Type.OBJECT)
                    .form(Form.EMPTY.builder().href(formUrl).build())).build();

    private static Form objectOptionsForm = baseOptionsForm.asJsonObject()
            .put(PATH_TO_TYPE_FORMAT_IN_BASE_OPTION_FORM, objectFormat.asJsonValue()).as(Form.class);

    private static FormFields objectFormField = objectFormat.asJsonArray()
            .add(0, FormField.builder("name").required(true).build().asJsonValue())
            .add(FormField.builder("options").type(Type.OBJECT)
                    .form(objectOptionsForm)
                    .build().asJsonValue()).as(FormFields.class);



    private static final StringPropertyEnumMerger enumUtil = new StringPropertyEnumMerger();
    private static final String ALL_TYPES_PATTERN = enumUtil.regexOf(Type.ALL_TYPES);
    // type:array

    private static FormFields arrayFormat = FormFields.EMPTY
            .builder()
            .add(FormField.builder("type").pattern("array").build())
            .add(FormField.builder("required").type(Type.BOOLEAN).build())
            .add(FormField.builder("nullable").type(Type.BOOLEAN).build())
            .add(FormField.builder("mustBeNull").type(Type.BOOLEAN))
            .add(FormField.builder("maxsize").type(Type.INTEGER))
            .add(FormField.builder("minsize").type(Type.INTEGER))
            .add(FormField.builder("desc").type(Type.STRING))
            .add(FormField.builder("eform")
                    .type(Type.OBJECT)
                    .form(Form.EMPTY.builder().href(formUrl).build()))
            .add(FormField.builder("options")
                    .type(Type.OBJECT)
                    .form(Form.EMPTY.builder()
                            .href(arrayOptionsUrl).build()))
            .add(FormField.builder("etype").pattern(ALL_TYPES_PATTERN).build())
            .add(FormField.builder("eform").type(Type.OBJECT).form(Form.EMPTY.builder().href(formUrl).build()))
            //.add(FormField.builder("element").type(Type.OBJECT).options(elementOptions))


            .build();

    private static Options elementOptions = Options.builder().value(Value.EMPTY.builder()
            .add(Option.builder("type:string")
                    .format(ValueFormat.EMPTY.builder()
                            .type(Type.OBJECT)
                            .form(Form.EMPTY.builder().value(stringFormat).build())
                            .build())
                    .build())
            .add(Option.builder("type:object")
                    .format(ValueFormat.EMPTY.builder()
                            .type(Type.OBJECT)
                            .form(Form.EMPTY.builder().value(objectFormat).build())
                            .build())
                    .build())
            .add(Option.builder("type:array")
                    .format(ValueFormat.EMPTY.builder()
                            .type(Type.OBJECT)
                            .form(Form.EMPTY.builder().value(arrayFormat).build())
                            .build())
                    .build())


            .build()).build();


    private static Form arrayOptionsForm = baseOptionsForm.asJsonObject()
            .put(PATH_TO_TYPE_FORMAT_IN_BASE_OPTION_FORM, arrayFormat.asJsonValue()).as(Form.class).builder().href(arrayOptionsUrl).build();


    private static FormFields arrayFormField = FormFields.EMPTY.builder()
            .add(FormField.builder("name").required(true).build())
            .add(FormField.builder("type").pattern("array").build())
            .add(FormField.builder("required").type(Type.BOOLEAN).build())
            .add(FormField.builder("nullable").type(Type.BOOLEAN).build())
            .add(FormField.builder("mustBeNull").type(Type.BOOLEAN))
            .add(FormField.builder("maxsize").type(Type.INTEGER))
            .add(FormField.builder("minsize").type(Type.INTEGER))
            .add(FormField.builder("options")
                    .type(Type.OBJECT)
                    .form(arrayOptionsForm))
            .add(FormField.builder("etype").pattern("array|boolean|decimal|integer|object|set|string").build())
            .add(FormField.builder("eform").type(Type.OBJECT).form(Form.EMPTY.builder().href(formUrl).build()))
            .add(FormField.builder("element").type(Type.OBJECT).options(elementOptions))
            .build();



    private static Form formsForm = Form.EMPTY.builder()
            .href(formUrl)
            .value(FormFields.EMPTY.builder()
                    .add(FormField.builder("href").type(Type.STRING))
                    .add(FormField.builder("exclusive").type(Type.BOOLEAN))
                    .add(FormField
                            .builder("value")
                            .type(Type.ARRAY)
                            .element(ValueFormat.EMPTY.builder().type(Type.OBJECT)
                                    .options(Options.builder()
                                            .value(Value.EMPTY.builder()
                                                    .add(Option.builder("type:string")
                                                            .format(ValueFormat.EMPTY.builder()
                                                                    .href(stringFormFieldUrl)
                                                                    .type(Type.OBJECT)
                                                                    .form(Form.EMPTY.builder()
                                                                            .value(stringFormField)
                                                                            .build())
                                                                    .build())
                                                            .build())
                                                    .add(Option.builder("type:object")
                                                            .format(ValueFormat.EMPTY.builder()
                                                                    .type(Type.OBJECT)
                                                                    .href(objectFormFieldUrl)
                                                                    .form(Form.EMPTY.builder()
                                                                            .value(objectFormField)
                                                                            .build())
                                                                    .build())
                                                            .build())
                                                    .add(Option.builder("type:array")
                                                            .format(ValueFormat.EMPTY.builder()
                                                                    .type(Type.OBJECT)
                                                                    .href(arrayFormFieldUrl)
                                                                    .form(Form.EMPTY.builder()
                                                                            .value(arrayFormField)
                                                                            .build())
                                                                    .build())
                                                            .build())
                                                    .build())
                                            .build()).build()).build()).build()).build();

    private static Form stringFormsForm = Form.EMPTY.builder()
            .href(formUrl)
            .value(FormFields.EMPTY.builder()
                    .add(FormField.builder("href").type(Type.STRING))
                    .add(FormField.builder("exclusive").type(Type.BOOLEAN))
                    .add(FormField
                            .builder("value")
                            .type(Type.ARRAY)
                            .element(ValueFormat.EMPTY.builder().type(Type.OBJECT)
                                    .options(Options.builder()
                                            .value(Value.EMPTY.builder()
                                                    .add(Option.builder("type:string")
                                                            .format(ValueFormat.EMPTY.builder()
                                                                    .href(stringFormFieldUrl)
                                                                    .type(Type.OBJECT)
                                                                    .form(Form.EMPTY.builder()
                                                                            .value(stringFormField)
                                                                            .build())
                                                                    .build())
                                                            .build())

                                                    .build())
                                            .build()).build()).build()).build()).build();

    @Test
    void testStringFormsForm() {

        Form form1 = Form.EMPTY.builder()
                .value(FormFields.EMPTY.builder()
                        .add(FormField.builder("mystring").build())
                        .build())
                .build();

        FilterResult<JsonObjectWrapper> result1 = formFilter.filter(formsForm, form1.asJsonObject(), true, true);
        assertThat(result1.waste.isEmpty(), is(true));

        Form form2 = Form.EMPTY.builder()
                .value(FormFields.EMPTY.builder()
                        .add(FormField.builder("mystring").build().asExplicitFormField())
                        .build())
                .build();

        FilterResult<JsonObjectWrapper> result2 = formFilter.filter(formsForm, form2.asJsonObject(), true, true);
        assertThat(result2.waste.isEmpty(), is(true));

        Form form3 = Form.EMPTY.builder()
                .value(FormFields.EMPTY.builder()
                        .add(FormField.builder("myobject")
                                .type(Type.OBJECT)
                                .form(form1)
                                .build())
                        .build())
                .build();

        FilterResult<JsonObjectWrapper> result3 = formFilter.filter(formsForm, form3.asJsonObject(), true, true);
        assertThat(result3.waste.isEmpty(), is(true));

        Form form4 = Form.EMPTY.builder()
                .value(FormFields.EMPTY.builder()
                        .add(FormField.builder("myobject")
                                .type(Type.OBJECT)
                                .form(form2)
                                .build().asExplicitFormField())
                        .build())
                .build();

        FilterResult<JsonObjectWrapper> result4 = formFilter.filter(formsForm, form4.asJsonObject(), true, true);
        assertThat(result4.waste.isEmpty(), is(true));

        Form form5 = Form.EMPTY.builder()
                .value(FormFields.EMPTY.builder()
                        .add(FormField.builder("myarray").type(Type.ARRAY).eform(form2))
                        .build())
                .build();

        form5 = Form.EMPTY.builder()
                .value(FormFields.EMPTY.builder()
                        .add(FormField.builder("myarray")
                                .type(Type.ARRAY)
                                .element(ValueFormat.EMPTY.builder()
                                        .type(Type.OBJECT)
                                        .form(Form.EMPTY.builder().value(FormFields.EMPTY.builder()
                                                .add(FormField.builder("array2")
                                                        .type(Type.ARRAY)
                                                        .element(ValueFormat.EMPTY.builder()
                                                                .type(Type.STRING)
                                                                .pattern("Kalle")
                                                                .build())
                                                        .build()))
                                                .build())
                                        .build()))
                        .build())
                .build();


        FilterResult<JsonObjectWrapper> result5 = formFilter.filter(formsForm, form5.asJsonObject(), true, true);
        assertThat(result5.waste.isEmpty(), is(true));

        /*
        FilterResult<JsonObjectWrapper> result = formFilter.filter(formsForm, stringFormsForm.asJsonObject(), true, true);
        assertThat(result.waste.isEmpty(), is(true));
        */
    }

    //@Test
    void testFilterSelf() {
        FilterResult<JsonObjectWrapper> result1 = formFilter.filter(formsForm, formsForm.asJsonObject(), true, true);
        assertThat(result1.waste.isEmpty(), is(true));
    }

    @Test
    void filterFormWithFormTest() {

        JsonObject customForm = JsonObject.EMPTY
                .put("value", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("name", "fullName"))
                        .add(JsonObject.EMPTY
                                        .put("name", "mother")
                                        .put("type", "object")
                                        .put("form", JsonObject.EMPTY
                                                .put("value", JsonArray.EMPTY
                                                    .add(JsonObject.EMPTY.put("name", "mothersName")))
                                        ))
                        .add(JsonObject.EMPTY
                                .put("name", "friends")
                                .put("type", "array")
                                .put("element", JsonObject.EMPTY
                                        .put("type", "object"))
                        ));
        FormFilter formFilter = new FormFilter();

        FilterResult<JsonObjectWrapper> filterResult = formFilter.filter(formsForm, customForm, true, true);

        assertThat(customForm, is(filterResult.filtrate.asJsonValue().getValue(JsonObject.class)));
    }


    //@Test
    void formFilterSelf() {
        FilterResult<JsonObjectWrapper> filterYourself = new FormFilter().filter(formsForm, formsForm.asJsonObject(), true, true);
        assertThat(filterYourself.waste.asJsonObject(), is(Waste.builder().build()));
        assertThat(filterYourself.filtrate.asJsonObject(), is(formsForm.asJsonObject()));
    }

    @Test
    void emptyFormValidationTest() {

        JsonObject arrayInArrayForm = JsonObject.EMPTY
                .put("value", JsonArray.EMPTY);

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(formsForm, arrayInArrayForm, true, true);
        assertThat(result.waste.asJsonObject().toString(), result.waste.isEmpty(), is(true));
    }

    @Test
    void singleStringFormValidationTest() {

        JsonObject arrayInArrayForm = JsonObject.EMPTY
                .put("value", JsonArray.EMPTY.add(JsonObject.EMPTY
                        .put("name", "firstName")));

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(formsForm, arrayInArrayForm, true, true);
        assertThat(result.waste.asJsonObject().toString(), result.waste.isEmpty(), is(true));
    }

    @Test
    void singleArrayFieldFormValidationTest() {

        JsonObject formToBeValidated = JsonUtil.create("{'value':[{'name':'contacts','type':'array'}]}");

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(formsForm, formToBeValidated, true, true);
        assertThat(result.waste.asJsonObject().toString(), result.waste.isEmpty(), is(true));
    }

    @Test
    void singleArrayFieldWithElementFormValidationTest() {

        JsonObject formToBeValidated = JsonUtil.create("{'value':[{'name':'contacts','type':'array','element':{'type':'string'}}]}");

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(formsForm, formToBeValidated, true, true);
        assertThat(result.waste.asJsonObject().toString(), result.waste.isEmpty(), is(true));
    }

    @Test
    void multiArrayFieldWithElementFormValidationTest() {

        JsonObject formToBeValidated = JsonUtil.create("{'value':[{'name':'contacts','type':'array','element':{'type':'string'}},{'name':'friends','type':'array','element':{'type':'string'}}]}");

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(formsForm, formToBeValidated, true, true);
        assertThat(result.waste.asJsonObject().toString(), result.waste.isEmpty(), is(true));
    }

    @Test
    void singleArrayFieldsWithObjectElementFormValidationTest() {

        JsonObject formToBeValidated = JsonUtil.create("{'value':[{'name':'contacts','type':'array','element':{'type':'object','form':{'value':[{'name':'firstName'}]}}}]}");

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(formsForm, formToBeValidated, true, true);
        assertThat(result.waste.asJsonObject().toString(), result.waste.isEmpty(), is(true));
    }

    // @Test - integer type not implemented yet
    void singleArrayFieldsWithIntegerElementFormValidationTest() {

        JsonObject formToBeValidated = JsonUtil.create("{'value':[{'name':'contacts','type':'array','element':{'type':'integer'}}]}");

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(formsForm, formToBeValidated, true, true);
        assertThat(result.waste.asJsonObject().toString(), result.waste.isEmpty(), is(true));
    }

    //@Test - decimal type not implemented yet
    void singleArrayFieldsWithDecimalElementFormValidationTest() {

        JsonObject formToBeValidated = JsonUtil.create("{'value':[{'name':'contacts','type':'array','element':{'type':'decimal'}}]}");

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(formsForm, formToBeValidated, true, true);
        assertThat(result.waste.asJsonObject().toString(), result.waste.isEmpty(), is(true));
    }

    //@Test - boolean type not implemented yet
    void singleArrayFieldsWithBooleanElementFormValidationTest() {

        JsonObject formToBeValidatedOk = JsonUtil.create("{'value':[{'name':'contacts','type':'array','element':{'type':'boolean'}}]}");

        FilterResult<JsonObjectWrapper> okResult = new FormFilter().filter(formsForm, formToBeValidatedOk, true, true);
        assertThat(okResult.waste.asJsonObject().toString(), okResult.waste.isEmpty(), is(true));
    }

    @Test
    void arrayInArray() {

        JsonObject arrayInArrayForm = JsonObject.EMPTY
                .put("value", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("name", "contacts")
                                .put("type", Type.ARRAY)
                                .put("element", JsonObject.EMPTY
                                        .put("type", Type.OBJECT)
                                )));

        Form form = arrayInArrayForm.as(Form.class);

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(formsForm, arrayInArrayForm, true, true);
        assertThat(result.waste.asJsonObject().toString(), result.waste.isEmpty(), is(true));
    }

    @Test
    void arrayInArray2() {

        JsonObject formJsonObject = JsonObject.EMPTY
                .put("value", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("name", "contacts")
                                .put("type", Type.ARRAY)
                                .put("element", JsonObject.EMPTY
                                        .put("type", Type.OBJECT)
                                )));

        Form form = formJsonObject.as(Form.class);

        JsonObject data = JsonObject.EMPTY
                .put("contacts", JsonArray.EMPTY
                        .add(JsonObject.EMPTY.put("a", "aval").put("b", "bval")));

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(form, data, true, true);
        assertThat(result.waste.asJsonObject().toString(), result.waste.isEmpty(), is(true));
        assertThat(result.filtrate.asJsonValue(), is(data.jsonValue()));
    }


    @Test
    void arrayInArrayFiltrateTest() {
        Form form = JsonUtil.create(Form.class, "{'value': [{'name': 'myarray','type': 'array','element': {'type': 'array','element': {'type': 'string'}}}]}");

        JsonObject data = JsonUtil.create("{'myarray':[['lastName',{},4],['1','2']]}");
        JsonObject expectedFiltrate = JsonUtil.create("{'myarray':[['lastName',null,null],['1','2']]}");

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(form, data, true, true);

        assertThat(result.waste.getWasteItem(0).path, is(JsonArray.of("myarray", 0, 1)));
        assertThat(result.waste.getWasteItem(1).path, is(JsonArray.of("myarray", 0, 2)));
        assertThat(result.filtrate, nullValue());
    }

    @Test
    void arrayInArraySimpleFiltrateTest() {
        Form form = JsonUtil.create(Form.class, "{'value': [{'name': 'myarray','type': 'array','element': {'type': 'array','element': {'type': 'string'}}}]}");

        JsonObject data = JsonUtil.create("{'myarray':[[4]]}");
        JsonObject expectedFiltrate = JsonUtil.create("{'myarray':[[null]]}");

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(form, data, true, true);

        assertThat(result.filtrate, nullValue());
    }

    //@Test
    void objectOptionsTest() {
        JsonObject form = JsonUtil.create("{'value':[{'name':'rating', 'type':'object', 'options':{'value':[{'label':'Yes', 'value':{'key':'true'}},{'label':'No', 'value':{'key':'false'}}]}}]}");
        FilterResult<JsonObjectWrapper> result = formFilter.filter(formsForm, form, true, true);
        assertThat(result.waste.isEmpty(), is(true));
    }

    @Test
    void stringOptionsTest() {
        JsonObject form = JsonUtil.create("{'value':[{'name':'rating', 'options':{'value':[{'label':'Yes', 'value':'true'},{'label':'No', 'value':'false'}]}}]}");
        FilterResult<JsonObjectWrapper> result = formFilter.filter(formsForm, form, true, true);
        assertThat(result.waste.isEmpty(), is(true));
    }

    @Test
    void arrayWithElementFieldMemberTest() {
        JsonObject form = JsonUtil.create("{'value':[{'name':'arrayField', 'type':'array', 'element':{'type':'string'}}]}");
        FilterResult<JsonObjectWrapper> result = formFilter.filter(formsForm, form, true, true);
        assertThat(result.waste.isEmpty(), is(true));
    }

    @Test
    void arrayWithElementFieldMemberObjectTest() {
        JsonObject form = JsonUtil.create("{'value':[{'name':'arrayField', 'type':'array', 'element':{'type':'object'}}]}");
        FilterResult<JsonObjectWrapper> result = formFilter.filter(formsForm, form, true, true);
        assertThat(result.waste.isEmpty(), is(true));
    }
}
