/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

import org.junit.jupiter.api.Test;

import com.zuunr.forms.formfield.Type;
import com.zuunr.json.JsonObject;

/**
 * @author Niklas Eldberger
 */
class ValueFormatTest {

    @Test
    void valueFormatWithImplicitStringTypeTest(){
        assertThat(JsonObject.EMPTY.put("name", "field1").as(ValueFormat.class).type(), nullValue());
    }

    @Test
    void valueFormatWithExplicitStringTypeTest(){
        assertThat(JsonObject.EMPTY.put("name", "field1").put("type", "string").as(ValueFormat.class).asCompactValueFormat().type(), nullValue());
    }

    @Test
    void valueFormatWithFormTest(){
        ValueFormat valueFormat = JsonObject.EMPTY.put("type", "object").put("form", Form.EMPTY.builder().value(FormFields.EMPTY.builder().add(FormField.builder("someField").type(Type.STRING).build())).build()).as(ValueFormat.class).asCompactValueFormat();
        assertThat(valueFormat.asJsonObject().asJson().contains("string"), is(false) );
    }
}
