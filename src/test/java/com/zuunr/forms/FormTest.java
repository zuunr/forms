/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms;

import com.zuunr.forms.formfield.Enum;
import com.zuunr.forms.formfield.Options;
import com.zuunr.forms.formfield.Type;
import com.zuunr.forms.formfield.options.Option;
import com.zuunr.forms.formfield.options.ValidationStep;
import com.zuunr.forms.formfield.options.ValidationSteps;
import com.zuunr.forms.formfield.options.Value;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * @author Niklas Eldberger
 */
class FormTest {

    @Test
    void formFromJsonValue() {

        FormFields formFields = new FormFields(JsonArray.EMPTY.jsonValue());

        formFields = JsonArray.EMPTY.as(FormFields.class);

        JsonObject jsonObject = JsonObject.EMPTY.put("value", JsonArray.EMPTY);

        Form form0 = new Form(jsonObject.jsonValue());

        Form form = jsonObject.jsonValue().as(Form.class);
        assertThat(form, notNullValue());
    }

    @Test
    void removeExclusiveInCompactFormTest() {
        assertThat(Form.EMPTY.builder().exclusive(true).build().asCompactForm().exclusive(), nullValue());
    }

    @Test
    void optionsOfFormAreCompactTest() {
        Form form = Form.EMPTY.builder().exclusive(true).value(FormFields.EMPTY.builder()
                .add(FormField.builder("queryParams")
                        .type(Type.OBJECT)
                        .options(Options.builder().value(Value.EMPTY.builder()
                                .add(Option.builder("firstName option").format(ValueFormat.EMPTY.builder()
                                        .type(Type.OBJECT)
                                        .form(Form.EMPTY.builder()
                                                .exclusive(true)
                                                .value(FormFields.EMPTY.builder()
                                                        .add(FormField.builder("value._.firstName")
                                                                .type(Type.STRING).build())
                                                        .build())
                                                .build()).build()).build())
                                .add(Option.builder("lastName option")
                                        .validationSteps(ValidationSteps.EMPTY.builder()
                                                .add(ValidationStep.builder(ValueFormat.EMPTY.builder()
                                                        .type(Type.OBJECT).form(Form.EMPTY.builder()
                                                                .exclusive(true)
                                                                .value(FormFields.EMPTY.builder()
                                                                        .add(FormField.builder("value._.lastName")
                                                                                .type(Type.STRING).build())
                                                                        .build())
                                                                .build()).build())
                                                        .build())
                                                .build())
                                        .format(ValueFormat.EMPTY.builder()
                                                .type(Type.OBJECT)
                                                .form(Form.EMPTY.builder().value(FormFields.EMPTY.builder()
                                                        .add(FormField.builder("value._.lastName")
                                                                .type(Type.STRING).build())
                                                        .build())
                                                        .build()).build()).build())
                                .build()).build())
                        .build())
                .build()).build();

        Form compactForm = form.asCompactForm().asCompactForm().asCompactForm();
        assertThat(compactForm.formField("queryParams").schema().options().value().asList().size(), is(2));
        assertThat(compactForm.asJsonObject().asJson().contains("string"), is(false));
        assertThat(compactForm.formField("queryParams").schema().options().getOption("lastName option").format().form().exclusive(), nullValue());
        assertThat(compactForm.asJsonObject().asJson().contains("exclusive"), is(false));
    }

    @Test
    void alwaysFails1(){
        assertThat(Form.EMPTY.alwaysFails(), is(false));
    }

    @Test
    void alwaysFails2(){
        assertThat(Form.EMPTY.builder().value(FormFields.EMPTY.builder().add(FormField.builder("field1").enumeration(Enum.EMPTY)).build()).build().alwaysFails(), is(false));
    }

    @Test
    void alwaysFails3(){
        assertThat(Form.EMPTY.builder().value(FormFields.EMPTY.builder().add(FormField.builder("field1").required(true).enumeration(Enum.EMPTY)).build()).build().alwaysFails(), is(true));
    }

    @Test
    public void testType() {
        assertThat(
                JsonObject.EMPTY
                        .put("name", "fieldName")
                        .put("type", Type.STRING)
                        .jsonValue()
                        .as(FormField.class)
                        .schema()
                        .type()     ,
                is(Type.STRING));
    }
}
