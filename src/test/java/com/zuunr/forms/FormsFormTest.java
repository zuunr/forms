/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms;

import com.zuunr.forms.filter.FilterResult;
import com.zuunr.forms.filter.FormFilter;
import com.zuunr.forms.filter.result.Waste;
import com.zuunr.forms.formfield.Type;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.tool.JsonUtil;
import com.zuunr.json.util.JsonObjectWrapper;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

/**
 * @author Niklas Eldberger
 */
class FormsFormTest {

    FormFilter formFilter = new FormFilter();

    Form formsForm = FormsForm.FORM;

    @Test
    void validateNestedStringField() {
        JsonObject form1JsonObject = JsonObject.EMPTY.builder()
                .put("value", JsonArray.of(
                        JsonObject.EMPTY.builder()
                                .put("name", "myObject")
                                .put("type", "object")
                                .put("form", JsonObject.EMPTY.builder()
                                        .put("value", JsonArray.of(
                                                JsonObject.EMPTY.builder()
                                                        .put("name", "myString")
                                                        .put("form", JsonObject.EMPTY.builder().put("value", JsonArray.EMPTY).build())
                                                        .build()
                                        ))
                                        .build())
                                .build()
                ))
                .build();

        //FilterResult<JsonObjectWrapper> result1 = formFilter.filter(formsForm, form1.asJsonObject(), true, true);

        FilterResult<JsonObjectWrapper> result1 = formFilter.filter(formsForm, form1JsonObject, true, true);
        assertThat(result1.waste.isEmpty(), is(false));

        JsonObject expectedResult = JsonUtil.create("{'waste':[{'path':['value',0],'badValue':{'form':{'value':[{'form':{'value':[]},'name':'myString'}]},'name':'myObject','type':'object'},'description':'','violations':{'options':{'value':[{'label':'type:object','error':{'paths':[{'path':['form','value',0],'badValue':{'form':{'value':[]},'name':'myString'},'description':'','violations':{'options':{'value':[{'label':'type:string','error':{'paths':[{'path':[],'badValue':{'form':{'value':[]},'name':'myString'},'violations':{'supportedParams':['name','type','pattern','required','nullable','mustBeNull','minlength','maxlength','desc','enum','const','options']}}]}}]}}}]}}]}}}]}");

        JsonValue expectedWaste = expectedResult.get("waste");
        JsonValue resultingWaste = result1.waste.asJsonObject().get("paths");

        assertThat(resultingWaste, is(expectedWaste));
    }

    @Test
    void validateStringFieldWithInvalidFormFieldForm() {

        JsonObject form1JsonObject = JsonObject.EMPTY.builder()
                .put("value", JsonArray.of(
                        JsonObject.EMPTY.builder()
                                .put("name", "myString")
                                .put("form", JsonObject.EMPTY.builder().put("value", JsonArray.EMPTY).build())
                                .build()
                ))
                .build();

        FilterResult<JsonObjectWrapper> result1 = formFilter.filter(formsForm, form1JsonObject, true, true);
        assertThat(result1.waste.isEmpty(), is(false));

        assertThat(
                result1.waste.asJsonObject().get(JsonArray.of("paths", 0, "violations", "options", "value", 0, "label")).getString(),
                is("type:string"));
        assertThat(
                result1.waste.asJsonObject().get(JsonArray.of("paths", 0, "violations", "options", "value", 0, "error", "paths", 0, "badValue", "form")).getValue(),
                is(JsonObject.EMPTY.put("value", JsonArray.EMPTY)));
    }

    @Test
    void testStringFormsForm() {

        Form form1 = Form.EMPTY.builder()
                .value(FormFields.EMPTY.builder()
                        .add(FormField.builder("mystring").build())
                        .build())
                .build();

        FilterResult<JsonObjectWrapper> result1 = formFilter.filter(formsForm, form1.asJsonObject(), true, true);
        assertThat(result1.waste.isEmpty(), is(true));

        Form form2 = Form.EMPTY.builder()
                .value(FormFields.EMPTY.builder()
                        .add(FormField.builder("mystring").build().asExplicitFormField())
                        .build())
                .build();

        FilterResult<JsonObjectWrapper> result2 = formFilter.filter(formsForm, form2.asJsonObject(), true, true);
        assertThat(result2.waste.isEmpty(), is(true));

        Form form3 = Form.EMPTY.builder()
                .value(FormFields.EMPTY.builder()
                        .add(FormField.builder("myobject")
                                .type(Type.OBJECT)
                                .form(form1)
                                .build())
                        .build())
                .build();

        FilterResult<JsonObjectWrapper> result3 = formFilter.filter(formsForm, form3.asJsonObject(), true, true);
        assertThat(result3.waste.isEmpty(), is(true));

        Form form4 = Form.EMPTY.builder()
                .value(FormFields.EMPTY.builder()
                        .add(FormField.builder("myobject")
                                .type(Type.OBJECT)
                                .form(form2)
                                .build().asExplicitFormField())
                        .build())
                .build();

        FilterResult<JsonObjectWrapper> result4 = formFilter.filter(formsForm, form4.asJsonObject(), true, true);
        assertThat(result4.waste.isEmpty(), is(true));

        Form form5 = Form.EMPTY.builder()
                .value(FormFields.EMPTY.builder()
                        .add(FormField.builder("myarray").type(Type.ARRAY).eform(form2))
                        .build())
                .build();

        form5 = Form.EMPTY.builder()
                .value(FormFields.EMPTY.builder()
                        .add(FormField.builder("myarray")
                                .type(Type.ARRAY)
                                .element(ValueFormat.EMPTY.builder()
                                        .type(Type.OBJECT)
                                        .form(Form.EMPTY.builder().value(FormFields.EMPTY.builder()
                                                .add(FormField.builder("array2")
                                                        .type(Type.ARRAY)
                                                        .element(ValueFormat.EMPTY.builder()
                                                                .type(Type.STRING)
                                                                .pattern("Kalle")
                                                                .build())
                                                        .build()))
                                                .build())
                                        .build()))
                        .build())
                .build();

        FilterResult<JsonObjectWrapper> result5 = formFilter.filter(formsForm, form5.asJsonObject(), true, true);
        assertThat(result5.waste.isEmpty(), is(true));

        /*
        FilterResult<JsonObjectWrapper> result = formFilter.filter(formsForm, stringFormsForm.asJsonObject(), true, true);
        assertThat(result.waste.isEmpty(), is(true));
        */
    }

    //@Test
    void testFilterSelf() {
        FilterResult<JsonObjectWrapper> result1 = formFilter.filter(formsForm, formsForm.asJsonObject(), true, true);
        assertThat(result1.waste.isEmpty(), is(true));
    }

    @Test
    void filterFormWithFormTest() {

        JsonObject customForm = JsonObject.EMPTY
                .put("value", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("name", "fullName"))
                        .add(JsonObject.EMPTY
                                .put("name", "mother")
                                .put("type", "object")
                                .put("form", JsonObject.EMPTY
                                        .put("value", JsonArray.EMPTY
                                                .add(JsonObject.EMPTY.put("name", "mothersName")))
                                ))
                        .add(JsonObject.EMPTY
                                .put("name", "friends")
                                .put("type", "array")
                                .put("element", JsonObject.EMPTY
                                        .put("type", "object"))
                        ));
        FormFilter formFilter = new FormFilter();

        FilterResult<JsonObjectWrapper> filterResult = formFilter.filter(formsForm, customForm, true, true);

        assertThat(customForm, is(filterResult.filtrate.asJsonValue().getValue(JsonObject.class)));
    }

    //@Test
    void formFilterSelf() {
        FilterResult<JsonObjectWrapper> filterYourself = new FormFilter().filter(formsForm, formsForm.asJsonObject(), true, true);
        assertThat(filterYourself.waste.asJsonObject(), is(Waste.builder().build()));
        assertThat(filterYourself.filtrate.asJsonObject(), is(formsForm.asJsonObject()));
    }



    @Test
    void emptyFormValidationTest() {

        JsonObject arrayInArrayForm = JsonObject.EMPTY
                .put("value", JsonArray.EMPTY);

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(formsForm, arrayInArrayForm, true, true);
        assertThat(result.waste.asJsonObject().toString(), result.waste.isEmpty(), is(true));
    }

    @Test
    void singleStringFormValidationTest() {

        JsonObject arrayInArrayForm = JsonObject.EMPTY
                .put("value", JsonArray.EMPTY.add(JsonObject.EMPTY
                        .put("name", "firstName")));

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(formsForm, arrayInArrayForm, true, true);
        assertThat(result.waste.asJsonObject().toString(), result.waste.isEmpty(), is(true));
    }

    @Test
    void singleArrayFieldFormValidationTest() {

        JsonObject formToBeValidated = JsonUtil.create("{'value':[{'name':'contacts','type':'array'}]}");

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(formsForm, formToBeValidated, true, true);
        assertThat(result.waste.asJsonObject().toString(), result.waste.isEmpty(), is(true));
    }

    @Test
    void singleArrayFieldWithElementFormValidationTest() {

        JsonObject formToBeValidated = JsonUtil.create("{'value':[{'name':'contacts','type':'array','element':{'type':'string'}}]}");

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(formsForm, formToBeValidated, true, true);
        assertThat(result.waste.asJsonObject().toString(), result.waste.isEmpty(), is(true));
    }

    @Test
    void multiArrayFieldWithElementFormValidationTest() {

        JsonObject formToBeValidated = JsonUtil.create("{'value':[{'name':'contacts','type':'array','element':{'type':'string'}},{'name':'friends','type':'array','element':{'type':'string'}}]}");

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(formsForm, formToBeValidated, true, true);
        assertThat(result.waste.asJsonObject().toString(), result.waste.isEmpty(), is(true));
    }

    @Test
    void singleArrayFieldsWithObjectElementFormValidationTest() {

        JsonObject formToBeValidated = JsonUtil.create("{'value':[{'name':'contacts','type':'array','element':{'type':'object','form':{'value':[{'name':'firstName'}]}}}]}");

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(formsForm, formToBeValidated, true, true);
        assertThat(result.waste.asJsonObject().toString(), result.waste.isEmpty(), is(true));
    }

    // @Test - integer type not implemented yet
    void singleArrayFieldsWithIntegerElementFormValidationTest() {

        JsonObject formToBeValidated = JsonUtil.create("{'value':[{'name':'contacts','type':'array','element':{'type':'integer'}}]}");

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(formsForm, formToBeValidated, true, true);
        assertThat(result.waste.asJsonObject().toString(), result.waste.isEmpty(), is(true));
    }

    //@Test - decimal type not implemented yet
    void singleArrayFieldsWithDecimalElementFormValidationTest() {

        JsonObject formToBeValidated = JsonUtil.create("{'value':[{'name':'contacts','type':'array','element':{'type':'decimal'}}]}");

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(formsForm, formToBeValidated, true, true);
        assertThat(result.waste.asJsonObject().toString(), result.waste.isEmpty(), is(true));
    }

    //@Test - boolean type not implemented yet
    void singleArrayFieldsWithBooleanElementFormValidationTest() {

        JsonObject formToBeValidatedOk = JsonUtil.create("{'value':[{'name':'contacts','type':'array','element':{'type':'boolean'}}]}");

        FilterResult<JsonObjectWrapper> okResult = new FormFilter().filter(formsForm, formToBeValidatedOk, true, true);
        assertThat(okResult.waste.asJsonObject().toString(), okResult.waste.isEmpty(), is(true));
    }

    @Test
    void arrayInArray() {

        JsonObject arrayInArrayForm = JsonObject.EMPTY
                .put("value", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("name", "contacts")
                                .put("type", Type.ARRAY)
                                .put("element", JsonObject.EMPTY
                                        .put("type", Type.OBJECT)
                                )));

        Form form = arrayInArrayForm.as(Form.class);

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(formsForm, arrayInArrayForm, true, true);
        assertThat(result.waste.asJsonObject().toString(), result.waste.isEmpty(), is(true));
    }

    @Test
    void arrayInArray2() {

        JsonObject formJsonObject = JsonObject.EMPTY
                .put("value", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("name", "contacts")
                                .put("type", Type.ARRAY)
                                .put("element", JsonObject.EMPTY
                                        .put("type", Type.OBJECT)
                                )));

        Form form = formJsonObject.as(Form.class);

        JsonObject data = JsonObject.EMPTY
                .put("contacts", JsonArray.EMPTY
                        .add(JsonObject.EMPTY.put("a", "aval").put("b", "bval")));

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(form, data, true, true);
        assertThat(result.waste.asJsonObject().toString(), result.waste.isEmpty(), is(true));
        assertThat(result.filtrate.asJsonValue(), is(data.jsonValue()));
    }

    @Test
    void arrayInArrayFiltrateTest() {
        Form form = JsonUtil.create(Form.class, "{'value': [{'name': 'myarray','type': 'array','element': {'type': 'array','element': {'type': 'string'}}}]}");

        JsonObject data = JsonUtil.create("{'myarray':[['lastName',{},4],['1','2']]}");
        JsonObject expectedFiltrate = JsonUtil.create("{'myarray':[['lastName',null,null],['1','2']]}");

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(form, data, true, true);

        assertThat(result.waste.getWasteItem(0).path, is(JsonArray.of("myarray", 0, 1)));
        assertThat(result.waste.getWasteItem(1).path, is(JsonArray.of("myarray", 0, 2)));
        assertThat(result.filtrate, nullValue());
    }

    @Test
    void arrayInArraySimpleFiltrateTest() {
        Form form = JsonUtil.create(Form.class, "{'value': [{'name': 'myarray','type': 'array','element': {'type': 'array','element': {'type': 'string'}}}]}");

        JsonObject data = JsonUtil.create("{'myarray':[['4']]}");
        JsonObject expectedFiltrate = JsonUtil.create("{'myarray':[['4']]}");

        FilterResult<JsonObjectWrapper> result = new FormFilter().filter(form, data, true, true);

        assertThat(result.filtrate.asJsonObject(), is(expectedFiltrate));
    }

    //@Test
    void objectOptionsTest() {
        JsonObject form = JsonUtil.create("{'value':[{'name':'rating', 'type':'object', 'options':{'value':[{'label':'Yes', 'value':{'key':'true'}},{'label':'No', 'value':{'key':'false'}}]}}]}");
        FilterResult<JsonObjectWrapper> result = formFilter.filter(formsForm, form, true, true);
        assertThat(result.waste.isEmpty(), is(true));
    }

    @Test
    void stringOptionsTest() {
        JsonObject form = JsonUtil.create("{'value':[{'name':'rating', 'options':{'value':[{'label':'Yes', 'value':'true'},{'label':'No', 'value':'false'}]}}]}");
        FilterResult<JsonObjectWrapper> result = formFilter.filter(formsForm, form, true, true);
        assertThat(result.waste.isEmpty(), is(true));
    }

    @Test
    void arrayWithElementFieldMemberTest() {
        JsonObject form = JsonUtil.create("{'value':[{'name':'arrayField', 'type':'array', 'element':{'type':'string'}}]}");
        FilterResult<JsonObjectWrapper> result = formFilter.filter(formsForm, form, true, true);
        assertThat(result.waste.isEmpty(), is(true));
    }

    @Test
    @Disabled // There is a bug that prevents arrays in arrays!
    void arrayWithElementFieldMemberArrayTest() {
        JsonObject form = JsonUtil.create("{'value':[{'name':'arrayField', 'type':'array', 'element':{'type':'array', 'element': {'type': 'integer'}}}]}");
        FilterResult<JsonObjectWrapper> result = formFilter.filter(formsForm, form, true, true);
        assertThat(result.waste.isEmpty(), is(true));
    }

    @Test
    void arrayWithElementFieldMemberObjectTest() {
        JsonObject form = JsonUtil.create("{'value':[{'name':'arrayField', 'type':'array', 'element':{'type':'object'}}]}");
        FilterResult<JsonObjectWrapper> result = formFilter.filter(formsForm, form, true, true);
        assertThat(result.waste.isEmpty(), is(true));
    }

    @Test
    void formsFormInOtherForm() {
        Form configForm = Form.EMPTY.builder().value(FormFields.EMPTY.builder().add(FormField.builder("config").type(Type.OBJECT).form(formsForm)).build()).build();

        FilterResult<JsonObjectWrapper> filterResult = formFilter.filter(configForm, JsonObject.EMPTY.put("config", Form.EMPTY.asJsonObject()), true, true);
        assertThat(filterResult.waste.isEmpty(), is(true));
    }
}