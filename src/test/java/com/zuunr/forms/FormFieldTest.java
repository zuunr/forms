/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms;

import com.zuunr.forms.formfield.Type;
import com.zuunr.json.JsonObject;
import com.zuunr.json.tool.JsonUtil;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

/**
 * @author Niklas Eldberger
 */
class FormFieldTest {

    @Test
    void stringFormFieldTest() {
        FormField formField = FormField.builder("someField").build();

        // As given when FormField is created
        JsonObject expectedFormFieldAsGiven = JsonObject.EMPTY.put("name", "someField");
        assertThat(formField.asJsonObject(), is(expectedFormFieldAsGiven));

        // Explicit
        JsonObject expectedExplicit = JsonObject.EMPTY
                .put("name", "someField")
                .put("type", "string")
                .put("minlength", 0)
                .put("maxlength", Integer.MAX_VALUE)
                .put("required", false)
                .put("nullable", false)
                .put("mustBeNull", false);
        assertThat(formField.asExplicitFormField().asJsonObject(), is(expectedExplicit));

        // Compact
        JsonObject expectedCompact = JsonObject.EMPTY
                .put("name", "someField");
        assertThat(formField.asCompactFormField().asJsonObject(), is(expectedCompact));
    }

    @Test
    void integerFormFieldTest() {

        FormField.Builder formFieldBuilder = FormField.builder("someField");

        FormField formField = formFieldBuilder.type(Type.INTEGER).build();

        // As given when FormField is created
        JsonObject expectedFormFieldAsGiven = JsonObject.EMPTY
                .put("name", "someField")
                .put("type", Type.INTEGER);
        assertThat(formField.asJsonObject(), is(expectedFormFieldAsGiven));

        // Explicit
        JsonObject expectedExplicit = JsonObject.EMPTY
                .put("name", "someField")
                .put("type", "integer")
                .put("required", false)
                .put("nullable", false)
                .put("mustBeNull", false)
                .put("min", Long.MIN_VALUE)
                .put("max", Long.MAX_VALUE);

        FormField explicit = formField.asExplicitFormField();

        assertThat(explicit.asJsonObject(), is(expectedExplicit));

        // Compact
        JsonObject expectedCompact = JsonObject.EMPTY
                .put("name", "someField")
                .put("type", "integer");

        assertThat(formField.asCompactFormField().asJsonObject(), is(expectedCompact));
    }

    @Test
    void minTest() {

        FormField formField = JsonObject.EMPTY.builder()
                .put("name", "temp")
                .put("type", Type.DECIMAL)
                .put("min", new BigDecimal(37))
                .build()
                .as(FormField.class);

        assertThat(formField.schema().min().asBigDecimal(), is(new BigDecimal(37)));

        formField = JsonObject.EMPTY.builder()
                .put("name", "temp")
                .put("type", Type.DECIMAL)
                .build()
                .as(FormField.class);

        assertThat(formField.schema().min(), nullValue());
    }

    @Test
    void createFormFieldFromJsonObjectTest() {
        FormField formField = JsonUtil.create(FormField.class,
                "{'min':'2012-01-01','max':'2016-01-30','name':'birthDate','nullable':false,'required':false,'mustBeNull':false,'type':'date'}");
        assertThat(formField.schema().type(), is(Type.DATE));
        assertThat(formField.builder().build().schema().type(), is(Type.DATE));

    }

    @Test
    void explicitEformTest() {
        FormField formField = FormField.builder("persons").type(Type.ARRAY).eform(Form.EMPTY.builder().value(FormFields.EMPTY.builder().add(FormField.builder("name").required(true)).build()).build()).build();
        assertThat(formField.asExplicitFormField().schema().eform(), is(formField.asExplicitFormField().schema().element().form()));
    }

    @Test
    void explicitElementFormTest() {
        FormField formField = FormField.builder("persons").type(Type.ARRAY).element(Form.EMPTY.builder().value(FormFields.EMPTY.builder().add(FormField.builder("name").required(true)).build()).build().asObjectValueFormat()).build();
        assertThat(formField.asExplicitFormField().schema().eform(), is(formField.asExplicitFormField().schema().element().form()));
    }
}
