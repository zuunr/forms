/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.generation;

import com.zuunr.forms.Form;
import com.zuunr.forms.FormField;
import com.zuunr.forms.FormFields;
import com.zuunr.forms.ValueFormat;
import com.zuunr.forms.filter.FilterResult;
import com.zuunr.forms.filter.FormFilter;
import com.zuunr.forms.formfield.Enum;
import com.zuunr.forms.formfield.Options;
import com.zuunr.forms.formfield.Type;
import com.zuunr.forms.formfield.options.Option;
import com.zuunr.forms.formfield.options.Value;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectMerger;
import com.zuunr.json.JsonValue;
import com.zuunr.json.tool.JsonUtil;
import com.zuunr.json.util.JsonObjectWrapper;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

class FormMergerTest {

    private FormMerger formMerger = new FormMerger();

    @Test
    void mergeForm_HARDEN_required_and_non_exclusive() {
        Form form1 = Form.EMPTY.builder()
                .exclusive(false)
                .value(FormFields.EMPTY.builder()
                        .add(FormField.builder("param1").pattern(".*").required(true).build()).build())
                .build();
        Form form2 = Form.EMPTY.builder()
                .exclusive(true)
                .value(FormFields.EMPTY.builder()
                        .add(FormField.builder("param1").pattern(".*").required(false).build())
                        .add(FormField.builder("param2").pattern(".*").required(false).build())
                        .build())
                .build();
        Form resultingForm1 = formMerger.intersectionOf(form1, form2).asExplicitForm();
        assertThat(resultingForm1.formField("param1").required(), is(true));
        assertThat(resultingForm1.formField("param2").required(), is(false));
        assertThat(resultingForm1.exclusive(), is(true));

        Form resultingForm2 = formMerger.intersectionOf(form2, form1).asExplicitForm();
        assertThat(resultingForm2.formField("param1").required(), is(true));
        assertThat(resultingForm2.formField("param2").required(), is(false));
        assertThat(resultingForm2.exclusive(), is(true));
    }

    @Test
    void mergeForm_HARDEN_required_and_exclusive() {
        Form form1 = Form.EMPTY.builder()
                .exclusive(true)
                .value(FormFields.EMPTY.builder()
                        .add(FormField.builder("param1").required(true).build()).build())
                .build();
        Form form2 = Form.EMPTY.builder()
                .exclusive(false)
                .value(FormFields.EMPTY.builder()
                        .add(FormField.builder("param1").required(false).build())
                        .add(FormField.builder("param2").required(false).build())
                        .build())
                .build();
        Form resultingForm1 = formMerger.intersectionOf(form1, form2).asExplicitForm();

        assertThat(resultingForm1.formField("param1").required(), is(true));
        assertThat(resultingForm1.formField("param2"), nullValue());
        assertThat(resultingForm1.exclusive(), is(true));

        Form resultingForm2 = formMerger.intersectionOf(form2, form1).asExplicitForm();

        assertThat(resultingForm2.formField("param1").required(), is(true));
        assertThat(resultingForm2.formField("param2"), nullValue());
        assertThat(resultingForm2.exclusive(), is(true));
    }

    //@Test
    void mergeForm_HARDEN_required2() {
        Form form1 = Form.EMPTY.builder().value(FormFields.EMPTY.builder()
                .add(FormField.builder("param1").required(true).build()).build()).build();
        Form form2 = Form.EMPTY.builder().value(FormFields.EMPTY.builder()
                .add(FormField.builder("param1").required(false).build())
                .add(FormField.builder("param2").required(true).build())
                .build()).build();
        Form result = formMerger.intersectionOf(form1, form2);

        assertThat(result.formField("param1").required(), is(true));
        assertThat("form1 is exclusive and cannot allow any more params", result.formField("param2"), nullValue());
    }

    @Test
    void maxFormGeneratorTest() {

        FormMerger formMerger = new FormMerger();

        Form form1 = JsonUtil.create(Form.class, "{'value':[{'name':'param','mustBeNull':false,'max':'a'}]}");
        Form form2 = JsonUtil.create(Form.class, "{'value':[{'name':'param','mustBeNull':false,'max':'c'}]}");

        Form softenedForm1 = formMerger.unionOf(form1, form2);
        assertThat(softenedForm1.formField("param").schema().max().asString(), is("c"));

        Form softenedForm2 = formMerger.unionOf(form2, form1);
        assertThat(softenedForm2.formField("param").schema().max().asString(), is("c"));

        Form hardenedForm1 = formMerger.intersectionOf(form1, form2);
        assertThat(hardenedForm1.formField("param").schema().max().asString(), is("a"));

        Form hardenedForm2 = formMerger.intersectionOf(form2, form1);
        assertThat(hardenedForm2.formField("param").schema().max().asString(), is("a"));
    }

    @Test
    void minStringFormGeneratorTest() {

        FormMerger formMerger = new FormMerger();

        Form form1 = JsonUtil.create(Form.class, "{'value':[{'name':'param','mustBeNull':false,'min':'a'}]}");
        Form form2 = JsonUtil.create(Form.class, "{'value':[{'name':'param','mustBeNull':false,'min':'c'}]}");

        Form softenedForm1 = formMerger.unionOf(form1, form2);
        assertThat(softenedForm1.formField("param").schema().min().asString(), is("a"));

        Form softenedForm2 = formMerger.unionOf(form2, form1);
        assertThat(softenedForm2.formField("param").schema().min().asString(), is("a"));

        Form hardenedForm1 = formMerger.intersectionOf(form1, form2);
        assertThat(hardenedForm1.formField("param").schema().min().asString(), is("c"));

        Form hardenedForm2 = formMerger.intersectionOf(form2, form1);
        assertThat(hardenedForm2.formField("param").schema().min().asString(), is("c"));
    }

    @Test
    void minIntegerFormGeneratorTest() {

        FormMerger formMerger = new FormMerger();

        Form form1 = JsonUtil.create(Form.class, "{'value':[{'name':'param','type':'integer','mustBeNull':false,'min':1}]}");
        Form form2 = JsonUtil.create(Form.class, "{'value':[{'name':'param','type':'integer','mustBeNull':false,'min':2}]}");

        Form softenedForm1 = formMerger.unionOf(form1, form2);
        assertThat(softenedForm1.formField("param").schema().min().asLong(), is(1L));

        Form softenedForm2 = formMerger.unionOf(form2, form1);
        assertThat(softenedForm2.formField("param").schema().min().asLong(), is(1L));

        Form hardenedForm1 = formMerger.intersectionOf(form1, form2);
        assertThat(hardenedForm1.formField("param").schema().min().asLong(), is(2L));

        Form hardenedForm2 = formMerger.intersectionOf(form2, form1);
        assertThat(hardenedForm2.formField("param").schema().min().asLong(), is(2L));
    }

    @Test
    void hardenMinMaxInteger() {
        Form form1 = JsonUtil.create(Form.class, "{'exclusive':false,'value':[{'form':{'exclusive':false,'value':[{'min':18,'name':'age','type':'integer'}]},'name':'person','type':'object'}]}");

        Form form2 = JsonUtil.create(Form.class, "{'exclusive':false,'value':[{'form':{'exclusive':false,'value':[{'name':'gender','pattern':'MALE'},{'min':20,'max':25,'name':'age','type':'integer'}]},'name':'person','type':'object'}]}");

        Form mergedForm = formMerger.intersectionOf(form1, form2);

        assertThat(mergedForm.formField("person").schema().form().formField("age").schema().min().asLong(), is(20L));
        assertThat(mergedForm.formField("person").schema().form().formField("age").schema().max().asLong(), is(25L));
        assertThat(mergedForm.formField("person").schema().form().formField("gender").schema().pattern().asString(), is("MALE"));
    }

    @Test
    void hardenExclusive() {
        Form form1 = JsonUtil.create(Form.class, "{'exclusive':false,'value':[{'name':'firstName'}]}");
        Form form2 = JsonUtil.create(Form.class, "{'exclusive':true,'value':[]}");

        Form mergedForm = formMerger.intersectionOf(form1, form2);
        assertThat(mergedForm.formFields().asJsonArray().isEmpty(), is(true));
        assertThat(mergedForm.asExplicitForm().exclusive(), is(true));

        mergedForm = formMerger.intersectionOf(form2, form1);
        assertThat(mergedForm.formFields().asJsonArray().isEmpty(), is(true));
        assertThat(mergedForm.asExplicitForm().exclusive(), is(true));
    }

    @Test
    void hardenExclusive2() {
        Form form1 = JsonUtil.create(Form.class, "{'exclusive':true,'value':[{'name':'firstName'}]}");
        Form form2 = JsonUtil.create(Form.class, "{'exclusive':false,'value':[]}");

        Form mergedForm = formMerger.intersectionOf(form1, form2).asExplicitForm();
        assertThat(mergedForm.formFields().asJsonArray().isEmpty(), is(false));
        assertThat(mergedForm.exclusive(), is(true));

        mergedForm = formMerger.intersectionOf(form2, form1).asExplicitForm();
        assertThat(mergedForm.formFields().asJsonArray().isEmpty(), is(false));
        assertThat(mergedForm.exclusive(), is(true));
    }

    @Test
    void mergeOptions() {

        Option green = Option.builder("Green").value(JsonValue.of("GREEN")).build();
        Option red = Option.builder("Red").value(JsonValue.of("RED")).build();

        Options redGreenOptions = Options.builder()
                .value(Value.EMPTY.builder()
                        .add(red)
                        .add(green)
                        .build()).build();

        Form formWithRedGreenOptions = Form.EMPTY.builder().value(FormFields.EMPTY.builder().add(
                FormField.builder("color")
                        .options(redGreenOptions).build()).build()).build();

        Options blackWhiteOptions = Options.builder()
                .value(Value.EMPTY.builder()
                        .add(Option.builder("Black").value(JsonValue.of("BLACK")).build())
                        .add(Option.builder("White").value(JsonValue.of("WHITE")).build())
                        .build()).build();

        Form formWithBlackWhiteOptions = Form.EMPTY.builder().value(FormFields.EMPTY.builder().add(
                FormField.builder("color")
                        .options(blackWhiteOptions).build()).build()).build();

        Form hardenMergedForm = formMerger.intersectionOf(formWithRedGreenOptions, formWithBlackWhiteOptions);
        assertThat(hardenMergedForm.formField("color").schema().options().value().asList().isEmpty(), is(true));

        Form softenMergedForm = formMerger.unionOf(formWithRedGreenOptions, formWithBlackWhiteOptions);
        assertThat(softenMergedForm.formField("color").schema().options().value().asList().size(), is(4));

        Form formWithoutOptions = Form.EMPTY.builder().value(FormFields.EMPTY.builder().add(
                FormField.builder("color").build()).build()).build();

        Form hardenMergedForm1 = formMerger.intersectionOf(formWithRedGreenOptions, formWithoutOptions);
        assertThat(hardenMergedForm1.formField("color").asExplicitFormField().asJsonObject(), is(formWithRedGreenOptions.formField("color").asExplicitFormField().asJsonObject()));

        Form softenMergedForm1 = formMerger.unionOf(formWithoutOptions, formWithRedGreenOptions);
        assertThat(softenMergedForm1.formField("color").schema().options().perLabel().get("Green").as(Option.class), is(green));
        assertThat(softenMergedForm1.formField("color").schema().options().perLabel().get("Red").as(Option.class), is(red));
    }


    @Test
    void patchForm() {

        // For the BR model as a whole
        Form fullForm = JsonUtil.create(
                Form.class,
                "{'exclusive':false, 'value':[{'name':'person','type':'object','form':{'exclusive':false, 'value':[{'name':'age', 'type':'integer', 'min': 0},{'name':'gender', 'type':'string'}]}}]}");

        // For a specific rule - set in advance
        Form genderFormFieldTemplate = JsonUtil.create(Form.class, "{'value':[{'name':'person','type':'object','form':{'value':[{'name':'gender', 'type':'string'}]}}]}");
        Form ageFormFieldTemplate = JsonUtil.create(Form.class, "{'value':[{'name':'person','type':'object','form':{'value':[{'name':'age', 'type':'integer'}]}}]}");

        // For a specific attribute - set i
        Form renderedGenderForm = genderFormFieldTemplate.asJsonObject().put(JsonArray.of("value", 0, "form", "value", 0, "pattern"), "MALE|FEMALE").as(Form.class);
        Form renderedAgeForm = ageFormFieldTemplate.asJsonObject().put(JsonArray.of("value", 0, "form", "value", 0, "min"), 18).as(Form.class);

        FormMerger formMerger = new FormMerger();
        Form mergedGender = formMerger.mergeForms(fullForm, renderedGenderForm, MergeStrategy.PATCH_BY_OVERWRITE);
        Form mergedAge = formMerger.mergeForms(fullForm, renderedAgeForm, MergeStrategy.PATCH_BY_OVERWRITE);

        Form businessRulesApplied = formMerger.intersectionOf(mergedAge, mergedGender);

        JsonObject data = JsonObject.EMPTY.builder()
                .put("person", JsonObject.EMPTY.builder()
                        .put("age", 15)
                        .put("gender", "MALE")
                        .build())
                .put("customerSegments", JsonObject.EMPTY
                        .put("vip", true))
                .build();

        FormFilter formFilter = new FormFilter();
        FilterResult<JsonObjectWrapper> result = formFilter.filter(businessRulesApplied, data, true, true);

        assertThat(result.waste.wasteItemsSize(), is(1));
    }

    @Test
    void businessRulesForm() {
        JsonObject createBusinessRuleRequestBody = JsonUtil.create("{'person.age': {'min':21}}");
        Form createBusinessRulesForm = JsonUtil.create(Form.class, "{'value':[{'name':'person.age', 'type':'object', 'form': {'value':[{'name':'min', 'type':'integer'}]}}]}");

        // Validation in service of service request
        FilterResult<JsonObjectWrapper> filterResult = new FormFilter().filter(createBusinessRulesForm, createBusinessRuleRequestBody, true, true);
        assertThat(filterResult.waste.isEmpty(), is(true));

        // Apply types (needed for merge to work)
        JsonObject types = JsonObject.EMPTY.builder()
                .put("person.age", JsonObject.EMPTY
                        .put("type", "integer"))
                .build();
        JsonObjectMerger merger = new JsonObjectMerger();
        JsonObject typesApplied = merger.merge(createBusinessRuleRequestBody, types);

        // Create a form which  can be applied as business rule on the full business rule model
        Form businessRule = createBusinessRule(typesApplied);

        assertThat(businessRule.exclusive(), is(false));
    }

    @Test
    void mergeEnumTest() {

        FormMerger formMerger = new FormMerger();

        Form form1 = JsonUtil.create(Form.class, "{'value':[{'name':'param','mustBeNull':false,'enum':['ENUM1','ENUM2']}]}]}");
        Form form2 = JsonUtil.create(Form.class, "{'value':[{'name':'param','mustBeNull':false,'enum':['ENUM2','ENUM3']}]}]}");
        Form form3 = JsonUtil.create(Form.class, "{'value':[{'name':'param','mustBeNull':false}]}");

        Form softenedForm1 = formMerger.unionOf(form1, form2);
        assertThat(softenedForm1.formField("param").schema().enumeration().asJsonArray().sort(), is(JsonArray.of("ENUM1", "ENUM2", "ENUM3")));

        Form softenedForm2 = formMerger.unionOf(form2, form1);
        assertThat(softenedForm2.formField("param").schema().enumeration().asJsonArray().sort(), is(JsonArray.of("ENUM1", "ENUM2", "ENUM3")));

        Form hardenedForm1 = formMerger.intersectionOf(form1, form2);
        assertThat(hardenedForm1.formField("param").schema().enumeration().asJsonArray().sort(), is(JsonArray.of("ENUM2")));

        Form hardenedForm2 = formMerger.intersectionOf(form2, form1);
        assertThat(hardenedForm2.formField("param").schema().enumeration().asJsonArray().sort(), is(JsonArray.of("ENUM2")));

        Form hardenedForm3 = formMerger.intersectionOf(form1, form3);
        assertThat(hardenedForm3.formField("param").schema().enumeration().asJsonArray().sort(), is(JsonArray.of("ENUM1", "ENUM2")));

        Form hardenedForm4 = formMerger.intersectionOf(form3, form1);
        assertThat(hardenedForm4.formField("param").schema().enumeration().asJsonArray().sort(), is(JsonArray.of("ENUM1", "ENUM2")));

    }


    private Form fullModelForm() {
        Form fullForm = JsonUtil.create(
                Form.class,
                "{'exclusive':false, 'value':[{'name':'person','type':'object','form':{'exclusive':false, 'value':[{'name':'age', 'type':'integer', 'min': 0},{'name':'gender', 'type':'string'}]}}]}");
        return fullForm;
    }

    public Form createBusinessRule(JsonObject createBusinessRuleRequest) {

        Form result = fullModelForm();
        for (JsonArray keyValueTuple : createBusinessRuleRequest.asKeyValueTuples().asList(JsonArray.class)) {
            String key = keyValueTuple.get(0).getValue(String.class);
            JsonObject constraints = keyValueTuple.get(1).getValue(JsonObject.class);
            result = formMerger.mergeForms(result, createForm(key, constraints), MergeStrategy.PATCH_BY_OVERWRITE);
        }
        return result;
    }


    private Form createForm(String modelPath, JsonObject constraints) {

        JsonArray path = JsonArray.of((Object[]) modelPath.split("[.]"));
        return createForm(path, constraints);
    }

    private Form createForm(JsonArray modelPath, JsonObject constraints) {

        Form result;
        if (modelPath.size() == 1) {
            result = JsonObject.EMPTY.put("value", JsonArray.EMPTY.add(constraints.builder().put("name", modelPath.get(0)).build())).as(Form.class);
        } else {
            result = JsonObject.EMPTY.put("value", JsonArray.EMPTY
                    .add(constraints.builder()
                            .put("name", modelPath.get(0))
                            .put("type", "object")
                            .put("form", createForm(modelPath.tail(), constraints)).build())).as(Form.class);
        }
        return result;
    }

    @Test
    void intersectionTest1() {

        Form form1 = JsonUtil.create(Form.class, "{'value':[{'name':'firstName','pattern':'kalle'}]}");
        Form form2 = JsonUtil.create(Form.class, "{'value':[{'name':'firstName','enum':['kalle','olle'], 'pattern':'kalle|olle'}]}");
        Form expectedResult = JsonUtil.create(Form.class, "{'value':[{'name':'firstName','pattern':'kalle','enum':['kalle','olle']}]}");

        Form result1 = formMerger.intersectionOf(form1, form2);
        assertThat(result1, is(expectedResult));

        Form result2 = formMerger.intersectionOf(form2, form1);
        assertThat(result2, is(expectedResult));
    }

    @Test
    void intersectionTest2() {

        Form form1 = JsonUtil.create(Form.class, "{'value':[{'name':'firstName','pattern':'kalle?ui+','minlength':6,'maxlength':8}]}");
        Form form2 = JsonUtil.create(Form.class, "{'value':[{'name':'firstName','pattern':'kalle?ui+|olle','minlength':7,'maxlength':22}]}");
        Form expectedResult = JsonUtil.create(Form.class, "{'value':[{'name':'firstName','pattern':'kalle?ui+','minlength':7,'maxlength':8}]}");

        Form result1 = formMerger.intersectionOf(form1, form2);
        assertThat(result1, is(expectedResult));

        Form result2 = formMerger.intersectionOf(form2, form1);
        assertThat(result2, is(expectedResult));
    }

    @Test
    void intersectionTest3() {

        Form form1 = Form.EMPTY.builder().value(FormFields.EMPTY.builder()
                .add(FormField.builder("arrayField")
                        .type(Type.ARRAY)
                        .element(ValueFormat.EMPTY.builder()
                                .type(Type.UNDEFINED)
                                .build())
                        .build())
                .build()).build();

        Form form2 = Form.EMPTY.builder().value(FormFields.EMPTY.builder()
                .add(FormField.builder("arrayField")
                        .type(Type.ARRAY)
                        .element(ValueFormat.EMPTY.builder()
                                .type(Type.INTEGER)
                                .build())
                        .build())
                .build()).build();


        Form expectedResult = form2;

        Form result1 = formMerger.intersectionOf(form1, form2);
        assertThat(result1, is(expectedResult));

        Form result2 = formMerger.intersectionOf(form2, form1);
        assertThat(result2, is(expectedResult));
    }


    @Test
    void enum1() {
        Form form1 = Form.EMPTY.builder()
                .value(FormFields.EMPTY.builder()
                        .add(FormField.builder("age")
                                .enumeration(Enum.EMPTY.builder()
                                        .add(16)
                                        .build())
                                .build())
                        .build())
                .build();
        Form form2 = Form.EMPTY.builder()
                .value(FormFields.EMPTY.builder()
                        .add(FormField.builder("age")
                                .enumeration(Enum.EMPTY.builder()
                                        .add(15)
                                        .build())
                                .build())
                        .build())
                .build();
        Form expectedForm = Form.EMPTY.builder()
                .value(FormFields.EMPTY.builder()
                        .add(FormField.builder("age")
                                .enumeration(Enum.EMPTY.builder()
                                        .add(15)
                                        .add(16)
                                        .build())
                                .build())
                        .build())
                .build();
        Form result = formMerger.unionOf(form1, form2);
        assertThat(result, is(expectedForm));
    }

    @Test
    void testNew() {

        JsonObject b = JsonObject.EMPTY
                .put("form", JsonObject.EMPTY
                        .put("exclusive", true)
                        .put("value", JsonArray.EMPTY.add(JsonObject.EMPTY
                                .put("element", JsonObject.EMPTY
                                        .put("form", JsonObject.EMPTY
                                                .put("exclusive", true)
                                                .put("value", JsonArray.EMPTY.add(JsonObject.EMPTY
                                                        .put("form", JsonObject.EMPTY
                                                                .put("exclusive", true)
                                                                .put("value", JsonArray.EMPTY.add(JsonObject.EMPTY
                                                                        .put("name", "orgNumber")
                                                                        .put("required", false)
                                                                        .put("type", "string")).add(JsonObject.EMPTY
                                                                        .put("name", "name")
                                                                        .put("required", false)
                                                                        .put("type", "string"))))
                                                        .put("name", "monitoredOrganisation")
                                                        .put("required", false)
                                                        .put("type", "object"))))
                                        .put("type", "object"))
                                .put("name", "items")
                                .put("required", false)
                                .put("type", "array")).add(JsonObject.EMPTY
                                .put("name", "size")
                                .put("required", false)
                                .put("type", "integer"))))
                .put("type", "object");
    }
}
