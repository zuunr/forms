/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.generation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

import com.zuunr.forms.FormField;
import com.zuunr.json.tool.JsonUtil;

/**
 * @author Niklas Eldberger
 */
class MaxMergerTest {

    @Test
    void longTest(){

        MaxMerger maxMerger = new MaxMerger();

        FormField formField1 = JsonUtil
                .create(FormField.class, "{'name':'param','type':'integer','mustBeNull':false,'max':1}");
        FormField formField2 = JsonUtil
                .create(FormField.class, "{'name':'param','type':'integer','mustBeNull':false,'max':2}");

        assertThat(maxMerger.soften(formField2, formField1).asLong(), is(2L));
        assertThat(maxMerger.soften(formField1, formField2).asLong(), is(2L));

        assertThat(maxMerger.harden(formField2, formField1).asLong(), is(1L));
        assertThat(maxMerger.harden(formField1, formField2).asLong(), is(1L));
    }

    @Test
    void bigDecimalTest(){

        MaxMerger maxMerger = new MaxMerger();

        FormField formField1 = JsonUtil
                .create(FormField.class, "{'name':'param','type':'decimal','mustBeNull':false,'max':1.1}");
        FormField formField2 = JsonUtil
                .create(FormField.class, "{'name':'param','type':'decimal','mustBeNull':false,'max':2.1}");

        assertThat(maxMerger.soften(formField2, formField1).asBigDecimal(), is(new BigDecimal("2.1")));
        assertThat(maxMerger.soften(formField1, formField2).asBigDecimal(), is(new BigDecimal("2.1")));
        assertThat(maxMerger.harden(formField2, formField1).asBigDecimal(), is(new BigDecimal("1.1")));
        assertThat(maxMerger.harden(formField1, formField2).asBigDecimal(), is(new BigDecimal("1.1")));
    }

    @Test
    void stringTest(){

        MaxMerger maxMerger = new MaxMerger();

        FormField formField1 = JsonUtil
                .create(FormField.class, "{'name':'param','type':'string','mustBeNull':false,'max':'a'}");
        FormField formField2 = JsonUtil
                .create(FormField.class, "{'name':'param','type':'string','mustBeNull':false,'max':'b'}");

        assertThat(maxMerger.soften(formField2, formField1).asString(), is("b"));
        assertThat(maxMerger.soften(formField1, formField2).asString(), is("b"));
        assertThat(maxMerger.harden(formField2, formField1).asString(), is("a"));
        assertThat(maxMerger.harden(formField1, formField2).asString(), is("a"));
    }
}
