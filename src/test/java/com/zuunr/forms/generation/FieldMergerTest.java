/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.generation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

import com.zuunr.forms.formfield.Enum;
import com.zuunr.json.*;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import com.zuunr.forms.Form;
import com.zuunr.forms.FormField;
import com.zuunr.forms.formfield.Type;
import com.zuunr.json.tool.JsonUtil;

class FieldMergerTest {

    private FieldMerger fieldMerger = new FieldMerger();

    @Test
    void mergeFormField_SOFTEN_required() {
        FormField formField1 = FormField.builder("operator").required(true).build();
        FormField formField2 = FormField.builder("operator").required(true).build();
        FormField resultingFormField = fieldMerger.mergeFormField(formField1, formField2, MergeStrategy.SOFTEN);
        assertThat(resultingFormField.asExplicitFormField(), is(FormField.builder("operator").required(true).build().asExplicitFormField()));
    }

    @Test
    void mergeFormField_HARDEN_required() {
        FormField formField1 = FormField.builder("operator").required(true).build();
        FormField formField2 = FormField.builder("operator").required(false).build();

        FormField resultingFormField1 = fieldMerger.intersectionOf(formField1, formField2);
        assertThat(resultingFormField1.asExplicitFormField(), is(FormField.builder("operator").required(true).build().asExplicitFormField()));

        FormField resultingFormField2 = fieldMerger.intersectionOf(formField2, formField1);
        assertThat(resultingFormField2.asExplicitFormField(), is(FormField.builder("operator").required(true).build().asExplicitFormField()));
    }

    @Test
    void maxFormGeneratorTest() {

        FormMerger formMerger = new FormMerger();

        Form form1 = JsonUtil.create(Form.class, "{'value':[{'name':'param','mustBeNull':false,'max':'a'}]}");
        Form form2 = JsonUtil.create(Form.class, "{'value':[{'name':'param','mustBeNull':false,'max':'c'}]}");

        Form softenedForm1 = formMerger.unionOf(form1, form2);
        assertThat(softenedForm1.formField("param").schema().max().asString(), is("c"));

        Form softenedForm2 = formMerger.unionOf(form2, form1);
        assertThat(softenedForm2.formField("param").schema().max().asString(), is("c"));

        Form hardenedForm1 = formMerger.intersectionOf(form1, form2);
        assertThat(hardenedForm1.formField("param").schema().max().asString(), is("a"));

        Form hardenedForm2 = formMerger.intersectionOf(form2, form1);
        assertThat(hardenedForm2.formField("param").schema().max().asString(), is("a"));
    }

    @Test
    void minStringFormGeneratorTest() {

        FormMerger formMerger = new FormMerger();

        Form form1 = JsonUtil.create(Form.class, "{'value':[{'name':'param','mustBeNull':false,'min':'a'}]}");
        Form form2 = JsonUtil.create(Form.class, "{'value':[{'name':'param','mustBeNull':false,'min':'c'}]}");

        Form softenedForm1 = formMerger.unionOf(form1, form2);
        assertThat(softenedForm1.formField("param").schema().min().asString(), is("a"));

        Form softenedForm2 = formMerger.unionOf(form2, form1);
        assertThat(softenedForm2.formField("param").schema().min().asString(), is("a"));

        Form hardenedForm1 = formMerger.intersectionOf(form1, form2);
        assertThat(hardenedForm1.formField("param").schema().min().asString(), is("c"));

        Form hardenedForm2 = formMerger.intersectionOf(form2, form1);
        assertThat(hardenedForm2.formField("param").schema().min().asString(), is("c"));
    }

    @Test
    void minIntegerFormGeneratorTest() {

        FormMerger formMerger = new FormMerger();

        Form form1 = JsonUtil.create(Form.class, "{'value':[{'name':'param','type':'integer','mustBeNull':false,'min':1}]}");
        Form form2 = JsonUtil.create(Form.class, "{'value':[{'name':'param','type':'integer','mustBeNull':false,'min':2}]}");

        Form softenedForm1 = formMerger.unionOf(form1, form2);
        assertThat(softenedForm1.formField("param").schema().min().asLong(), is(1L));

        Form softenedForm2 = formMerger.unionOf(form2, form1);
        assertThat(softenedForm2.formField("param").schema().min().asLong(), is(1L));

        Form hardenedForm1 = formMerger.intersectionOf(form1, form2);
        assertThat(hardenedForm1.formField("param").schema().min().asLong(), is(2L));

        Form hardenedForm2 = formMerger.intersectionOf(form2, form1);
        assertThat(hardenedForm2.formField("param").schema().min().asLong(), is(2L));
    }

    @Test
    void mergeIonFormFieldTest() {

        JsonObject result = fieldMerger.mergeFormField(
                JsonObject.EMPTY
                        .put("name", "paramName")
                        .put("required", true)
                        .put("type", "set")
                        .put("eform", JsonObject.EMPTY
                                .put("value", JsonArray.EMPTY))
                        .put("equals", JsonObject.EMPTY.put("paths", JsonArray.EMPTY)).as(FormField.class)
                ,
                JsonObject.EMPTY
                        .put("name", "paramName")
                        .put("required", false)
                        .put("type", "set")
                        .put("equals", JsonObject.EMPTY.put("paths", JsonArray.EMPTY))
                        .put("eform", JsonObject.EMPTY
                                .put("value", JsonArray.EMPTY)).as(FormField.class),
                MergeStrategy.SOFTEN
        ).asJsonObject();

        JsonObject expected = JsonObject.EMPTY
                .put("nullable", false)
                .put("eform", JsonObject.EMPTY
                        .put("value", JsonArray.EMPTY))
                .put("equals", JsonObject.EMPTY
                        .put("paths", JsonArray.EMPTY))
                .put("name", "paramName")
                .put("mustBeNull", false)
                .put("type", "set")
                .put("required", false);

        assertThat(result, is(expected));
    }

    @Test
    void mergeIonFormFieldTest2() {

        JsonObject result = fieldMerger.mergeFormField(
                JsonObject.EMPTY
                        .put("name", "paramName")
                        .put("required", true)
                        .put("type", "set")
                        .put("eform", JsonObject.EMPTY.put("value", JsonArray.EMPTY)).as(FormField.class)
                ,
                JsonObject.EMPTY
                        .put("name", "paramName")
                        .put("required", false)
                        .put("type", "array")
                        .put("eform", JsonObject.EMPTY.put("value", JsonArray.EMPTY)).as(FormField.class),
                MergeStrategy.SOFTEN).asJsonObject();

        JsonObject expected = JsonObject.EMPTY
                .put("name", "paramName")
                .put("mustBeNull", false)
                .put("nullable", false)
                .put("type", "array")
                .put("required", false)
                .put("eform", JsonObject.EMPTY.put("value", JsonArray.EMPTY));


        expected = new JsonObjectMerger().merge(result, expected);

        assertThat(result, is(expected));
    }

    @Test
    void mergeDate() {
        FormField formField1 = JsonUtil.create(FormField.class, "{'name':'birthDate','type':'date', 'min':'2000-01-01', 'max':'2016-01-30'}");
        FormField formField2 = JsonUtil.create(FormField.class, "{'name':'birthDate','type':'date', 'min':'2012-01-01', 'max':'2018-01-30'}");
        FormField mergedFormField = fieldMerger.intersectionOf(formField1, formField2);

        assertThat(mergedFormField.schema().type(), is(Type.DATE));
        assertThat(mergedFormField.schema().min().asString(), is("2012-01-01"));
        assertThat(mergedFormField.schema().max().asString(), is("2016-01-30"));
    }

    @Test
    void mergeDatetime() {
        String pattern = "....-..-..T..:..:..[.]...Z";
        FormField formField1 = JsonUtil.create(FormField.class, "{'name':'start','type':'datetime', 'min':'2000-01-01T17:17:17.876Z', 'max':'2015-01-01T17:17:17.876Z', 'pattern':'" + pattern + "'}");
        FormField formField2 = JsonUtil.create(FormField.class, "{'name':'start','type':'datetime', 'min':'1995-01-01T17:17:17.876Z', 'max':'2015-01-01T17:17:17.876Z', 'pattern':'" + pattern + "'}}");
        FormField mergedFormField = fieldMerger.mergeFormField(formField1, formField2, MergeStrategy.SOFTEN);

        assertThat(mergedFormField.schema().type(), is(Type.DATETIME));
        assertThat(mergedFormField.schema().min().asString(), is("1995-01-01T17:17:17.876Z"));
        assertThat(mergedFormField.schema().max().asString(), is("2015-01-01T17:17:17.876Z"));

        assertThat(mergedFormField.schema().pattern().asString(), is(pattern));
    }

    @Test
    @Disabled
    void intersectionOfDesc(){
        FormField formField1 = FormField.builder("field").desc("A").build();
        FormField formField2 = FormField.builder("field").desc("A").build();
        assertThat(fieldMerger.intersectionOf(formField1, formField2).schema().desc(), is(formField1.schema().desc()));
    }

    @Test
    @Disabled
    void intersectionOfDefault(){
        FormField formField1 = FormField.builder("field").defaultValue(JsonValue.of("A")).build();
        FormField formField2 = FormField.builder("field").defaultValue(JsonValue.of("A")).build();
        assertThat(fieldMerger.intersectionOf(formField1, formField2).schema().defaultValue(), is(formField1.schema().defaultValue()));
    }

    @Test
    void patchStringWithEnum1(){
        FormField formField1 = FormField.builder("status").constant(JsonValue.of("STATUS1")).build();
        FormField formField2 = FormField.builder("status").enumeration(JsonArray.of("STATUS1","STATUS2").as(Enum.class)).build();
        FormField merged = fieldMerger.mergeFormField(formField1, formField2, MergeStrategy.PATCH_BY_OVERWRITE);
        assertThat(merged.asJsonObject().get("const"), nullValue());
        assertThat(merged.asJsonObject().get("pattern"), nullValue());
        assertThat(merged.asJsonObject().get("enum"), is(JsonArray.of("STATUS1","STATUS2").jsonValue()));
    }

    @Test
    void patchStringWithEnum2(){
        FormField formField1 = JsonObject.EMPTY.put("name", "status").put("enum", JsonArray.of("STATUS1", "STATUS2")).put("pattern", "STATUS1|STATUS2").as(FormField.class);
        FormField formField2 = JsonObject.EMPTY.put("name", "status").put("const", JsonValue.of("STATUS1")).as(FormField.class);
        FormField merged = fieldMerger.mergeFormField(formField1, formField2, MergeStrategy.PATCH_BY_OVERWRITE);
        assertThat(merged.asJsonObject().get("enum"), nullValue());
        assertThat(merged.asJsonObject().get("pattern"), nullValue());
        assertThat(merged.asJsonObject().get("const"), is(JsonValue.of("STATUS1")));
    }
}
