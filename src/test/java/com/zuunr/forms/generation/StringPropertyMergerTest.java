/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.generation;

import com.zuunr.forms.formfield.Pattern;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Niklas Eldberger
 */
class StringPropertyMergerTest {

    private static final StringPropertyMerger stringPropertyMerger = new StringPropertyMerger();

    Pattern pattern = JsonValue.of("A|A(B|C)D|B").as(Pattern.class);

    @Test
    void testPattern() {
        assertThat(pattern.compiled().matcher("ABD").matches(), is(true));
    }

    @Test
    void unionTest1() {
        JsonObject property1 = JsonObject.EMPTY
                .put("const", "VALUE1");
        JsonObject property2 = JsonObject.EMPTY
                .put("const", "VALUE1");
        JsonObject expectedResult = JsonObject.EMPTY
                .put("const", "VALUE1");
        JsonObject result = stringPropertyMerger.unionOf(property1, property2);
        assertThat(result, is(expectedResult));
        JsonObject resultOfReversedOrder = stringPropertyMerger.unionOf(property2, property1);
        assertThat(resultOfReversedOrder, is(expectedResult));
    }

    @Test
    void unionTest2() {
        JsonObject property1 = JsonObject.EMPTY
                .put("const", "VALUE1");
        JsonObject property2 = JsonObject.EMPTY
                .put("const", "VALUE2");
        JsonObject expectedResult = JsonObject.EMPTY
                .put("enum", JsonArray.of("VALUE1", "VALUE2"));
        JsonObject result = stringPropertyMerger.unionOf(property1, property2);
        assertThat(result, is(expectedResult));
        JsonObject resultOfReversedOrder = stringPropertyMerger.unionOf(property2, property1);
        assertThat(resultOfReversedOrder, is(expectedResult));
    }

    @Test
    void unionTest3() {
        JsonObject property1 = JsonObject.EMPTY
                .put("enum", JsonArray.of("VALUE1"));
        JsonObject property2 = JsonObject.EMPTY
                .put("const", "VALUE2");
        JsonObject expectedResult = JsonObject.EMPTY
                .put("enum", JsonArray.of("VALUE1", "VALUE2"));
        JsonObject result = stringPropertyMerger.unionOf(property1, property2);
        assertThat(result, is(expectedResult));
        JsonObject resultOfReversedOrder = stringPropertyMerger.unionOf(property2, property1);
        assertThat(resultOfReversedOrder, is(expectedResult));
    }

    @Test
    void unionTest4() {
        JsonObject property1 = JsonObject.EMPTY
                .put("pattern", "Filter.firstName asc");
        JsonObject property2 = JsonObject.EMPTY
                .put("const", "VALUE2");
        JsonObject expectedResult = JsonObject.EMPTY
                .put("pattern", "Filter.firstName asc|VALUE2");
        JsonObject result = stringPropertyMerger.unionOf(property1, property2);
        assertThat(result, is(expectedResult));
        JsonObject resultOfReversedOrder = stringPropertyMerger.unionOf(property2, property1);
        assertThat(resultOfReversedOrder, is(expectedResult));
    }

    @Test
    void unionTest5() {
        JsonObject property1 = JsonObject.EMPTY
                .put("pattern", "VALUE1");
        JsonObject property2 = JsonObject.EMPTY
                .put("pattern", "VALUE2|VALUE1");
        JsonObject expectedResult = JsonObject.EMPTY
                .put("pattern", "VALUE1|VALUE2");
        JsonObject result = stringPropertyMerger.unionOf(property1, property2);
        assertThat(result, is(expectedResult));
        JsonObject resultOfReversedOrder = stringPropertyMerger.unionOf(property2, property1);
        assertThat(resultOfReversedOrder, is(expectedResult));
    }

    @Test
    void unionTest6() {
        JsonObject property1 = JsonObject.EMPTY;
        JsonObject property2 = JsonObject.EMPTY
                .put("pattern", "VALUE2|VALUE1");
        JsonObject expectedResult = JsonObject.EMPTY;
        JsonObject result = stringPropertyMerger.unionOf(property1, property2);
        assertThat(result, is(expectedResult));
        JsonObject resultOfReversedOrder = stringPropertyMerger.unionOf(property2, property1);
        assertThat(resultOfReversedOrder, is(expectedResult));
    }

    @Test
    void unionTest7() {
        JsonObject property1 = JsonObject.EMPTY
                .put("enum", JsonArray.of("A", "B"));
        JsonObject property2 = JsonObject.EMPTY
                .put("pattern", "A(B|C)D");
        JsonObject expectedResult = JsonObject.EMPTY.put("pattern", "A|A(B|C)D|B");
        JsonObject result = stringPropertyMerger.unionOf(property1, property2);
        assertThat(result, is(expectedResult));
        JsonObject resultOfReversedOrder = stringPropertyMerger.unionOf(property2, property1);
        assertThat(resultOfReversedOrder, is(expectedResult));
    }

    @Test
    void unionTest8() {
        JsonObject property1 = JsonObject.EMPTY
                .put("enum", JsonArray.of("A|B"));
        JsonObject property2 = JsonObject.EMPTY
                .put("pattern", "A|B");
        JsonObject expectedResult = JsonObject.EMPTY.put("pattern", "A|A[|]B|B");
        JsonObject result = stringPropertyMerger.unionOf(property1, property2);
        assertThat(result, is(expectedResult));
        JsonObject resultOfReversedOrder = stringPropertyMerger.unionOf(property2, property1);
        assertThat(resultOfReversedOrder, is(expectedResult));
    }

    @Test
    void unionTest9() {
        JsonObject property1 = JsonObject.EMPTY
                .put("enum", JsonArray.of(".*"));
        JsonObject property2 = JsonObject.EMPTY
                .put("enum", JsonArray.of("_?"));
        JsonObject expectedResult = JsonObject.EMPTY.put("enum", JsonArray.of(".*", "_?"));
        JsonObject result = stringPropertyMerger.unionOf(property1, property2);
        assertThat(result, is(expectedResult));
        JsonObject resultOfReversedOrder = stringPropertyMerger.unionOf(property2, property1);
        assertThat(resultOfReversedOrder, is(expectedResult));
    }

    @Test
    void intersectionTest1() {
        JsonObject property1 = JsonObject.EMPTY
                .put("enum", JsonArray.of(".*"));
        JsonObject property2 = JsonObject.EMPTY
                .put("enum", JsonArray.of("_?"));
        JsonObject expectedResult = JsonObject.EMPTY.put("enum", JsonArray.EMPTY);
        JsonObject result = stringPropertyMerger.intersectionOf(property1, property2);
        assertThat(result, is(expectedResult));
        JsonObject resultOfReversedOrder = stringPropertyMerger.intersectionOf(property2, property1);
        assertThat(resultOfReversedOrder, is(expectedResult));
    }

    @Test
    void intersectionTest2() {
        JsonObject property1 = JsonObject.EMPTY
                .put("pattern", ".*");
        JsonObject property2 = JsonObject.EMPTY
                .put("enum", JsonArray.of("ENUM1"));
        JsonObject expectedResult = JsonObject.EMPTY
                .put("pattern", ".*")
                .put("enum", JsonArray.of("ENUM1"));
        JsonObject result = stringPropertyMerger.intersectionOf(property1, property2);
        assertThat(result, is(expectedResult));
        JsonObject resultOfReversedOrder = stringPropertyMerger.intersectionOf(property2, property1);
        assertThat(resultOfReversedOrder, is(expectedResult));
    }

    @Test
    void intersectionTest3() {
        JsonObject property1 = JsonObject.EMPTY
                .put("enum", JsonArray.of("ENUM3", "ENUM2"));
        JsonObject property2 = JsonObject.EMPTY
                .put("enum", JsonArray.of("ENUM1", "ENUM2"));
        JsonObject expectedResult = JsonObject.EMPTY
                .put("enum", JsonArray.of("ENUM2"));
        JsonObject result = stringPropertyMerger.intersectionOf(property1, property2);
        assertThat(result, is(expectedResult));
        JsonObject resultOfReversedOrder = stringPropertyMerger.intersectionOf(property2, property1);
        assertThat(resultOfReversedOrder, is(expectedResult));
    }

    @Test
    void intersectionTest4() {
        JsonObject property1 = JsonObject.EMPTY
                .put("const", "ENUM1");
        JsonObject property2 = JsonObject.EMPTY
                .put("const", "ENUM2");
        JsonObject expectedResult = JsonObject.EMPTY
                .put("enum", JsonArray.EMPTY);
        JsonObject result = stringPropertyMerger.intersectionOf(property1, property2);
        assertThat(result, is(expectedResult));
        JsonObject resultOfReversedOrder = stringPropertyMerger.intersectionOf(property2, property1);
        assertThat(resultOfReversedOrder, is(expectedResult));
    }

    @Test
    void intersectionTest5() {
        JsonObject property1 = JsonObject.EMPTY
                .put("const", "ENUM1");
        JsonObject property2 = JsonObject.EMPTY;
        JsonObject expectedResult = JsonObject.EMPTY
                .put("const", "ENUM1");
        JsonObject result = stringPropertyMerger.intersectionOf(property1, property2);
        assertThat(result, is(expectedResult));
        JsonObject resultOfReversedOrder = stringPropertyMerger.intersectionOf(property2, property1);
        assertThat(resultOfReversedOrder, is(expectedResult));
    }


    @Test
    void intersectionTest6() {
        JsonObject property1 = JsonObject.EMPTY
                .put("pattern", "ENUM1");
        JsonObject property2 = JsonObject.EMPTY
                .put("pattern", "ENUM2");
        JsonObject expectedResult = JsonObject.EMPTY
                .put("pattern", "(?=ENUM1)(?=ENUM2)");
        JsonObject result = stringPropertyMerger.intersectionOf(property1, property2);
        assertThat(result, is(expectedResult));
        JsonObject resultOfReversedOrder = stringPropertyMerger.intersectionOf(property2, property1);
        assertThat(resultOfReversedOrder, is(expectedResult));
    }

    @Test
    void intersectionTest7() {
        JsonObject property1 = JsonObject.EMPTY
                .put("pattern", "(?=ENUM1)(?=ENUM2)");
        JsonObject property2 = JsonObject.EMPTY
                .put("pattern", "ENUM3");
        JsonObject expectedResult = JsonObject.EMPTY
                .put("pattern", "(?=ENUM1)(?=ENUM2)(?=ENUM3)");
        JsonObject result = stringPropertyMerger.intersectionOf(property1, property2);
        assertThat(result, is(expectedResult));
        JsonObject resultOfReversedOrder = stringPropertyMerger.intersectionOf(property2, property1);
        assertThat(resultOfReversedOrder, is(expectedResult));
    }

    @Test
    void intersectionTest8() {
        JsonObject property1 = JsonObject.EMPTY
                .put("pattern", "(?=ENUM1)(?=ENUM2)");
        JsonObject property2 = JsonObject.EMPTY
                .put("pattern", "(?=ENUM3)");
        JsonObject expectedResult = JsonObject.EMPTY
                .put("pattern", "(?=ENUM1)(?=ENUM2)(?=ENUM3)");
        JsonObject result = stringPropertyMerger.intersectionOf(property1, property2);
        assertThat(result, is(expectedResult));
        JsonObject resultOfReversedOrder = stringPropertyMerger.intersectionOf(property2, property1);
        assertThat(resultOfReversedOrder, is(expectedResult));
    }

    @Test
    void intersectionTest9() {
        JsonObject property1 = JsonObject.EMPTY
                .put("pattern", "(?=A)(?=B)");
        JsonObject property2 = JsonObject.EMPTY
                .put("pattern", "(?=A|B)");
        JsonObject expectedResult = JsonObject.EMPTY
                .put("pattern", "(?=A)(?=B)");
        JsonObject result = stringPropertyMerger.intersectionOf(property1, property2);
        assertThat(result, is(expectedResult));
        JsonObject resultOfReversedOrder = stringPropertyMerger.intersectionOf(property2, property1);
        assertThat(resultOfReversedOrder, is(expectedResult));
    }

    @Test
    void intersectionTest10() {
        JsonObject property1 = JsonObject.EMPTY
                .put("pattern", "(?=A)(?=B)");
        JsonObject property2 = JsonObject.EMPTY
                .put("pattern", "(?=A|B|C)");
        JsonObject expectedResult = JsonObject.EMPTY
                .put("pattern", "(?=A)(?=B)");
        JsonObject result = stringPropertyMerger.intersectionOf(property1, property2);
        assertThat(result, is(expectedResult));
        JsonObject resultOfReversedOrder = stringPropertyMerger.intersectionOf(property2, property1);
        assertThat(resultOfReversedOrder, is(expectedResult));
    }

    @Test
    void intersectionTest11() {
        JsonObject property1 = JsonObject.EMPTY
                .put("pattern", ".*");
        JsonObject property2 = JsonObject.EMPTY
                .put("pattern", ".?[A-F].*");
        JsonObject expectedResult = JsonObject.EMPTY
                .put("pattern", "(?=.*)(?=.?[A-F].*)");
        JsonObject result = stringPropertyMerger.intersectionOf(property1, property2);
        assertThat(result, is(expectedResult));
        JsonObject resultOfReversedOrder = stringPropertyMerger.intersectionOf(property2, property1);
        assertThat(resultOfReversedOrder, is(expectedResult));
    }

    @Test
    void intersectionTest12() {
        JsonObject property1 = JsonObject.EMPTY
                .put("pattern", "A");
        JsonObject property2 = JsonObject.EMPTY
                .put("pattern", "A|C");
        JsonObject expectedResult = JsonObject.EMPTY
                .put("pattern", "A");
        JsonObject result = stringPropertyMerger.intersectionOf(property1, property2);
        assertThat(result, is(expectedResult));
        JsonObject resultOfReversedOrder = stringPropertyMerger.intersectionOf(property2, property1);
        assertThat(resultOfReversedOrder, is(expectedResult));
    }

    @Test
    void intersectionTest13() {
        JsonObject property1 = JsonObject.EMPTY
                .put("pattern", "peter");
        JsonObject property2 = JsonObject.EMPTY
                .put("enum", JsonArray.of("laura", "peter"));
        JsonObject expectedResult = JsonObject.EMPTY
                .put("pattern", "peter")
                .put("enum", JsonArray.of("laura", "peter"));
        JsonObject result = stringPropertyMerger.intersectionOf(property1, property2);
        assertThat(result, is(expectedResult));
        JsonObject resultOfReversedOrder = stringPropertyMerger.intersectionOf(property2, property1);
        assertThat(resultOfReversedOrder, is(expectedResult));
    }

    @Test
    void intersectionsShouldBeLikeThis() {
        JsonObject.EMPTY
                .put("A|C", JsonObject.EMPTY
                        .put("A", "A"))
                .put("A|B", JsonObject.EMPTY
                        .put("A", "A")
                        .put("B", "B")
                );
    }

    @Test
    void unionOfExtendPattern() {

        JsonObject property1 = JsonObject.EMPTY
                .put("pattern", "(value[.]_[.]games|value[.]_[.]spreadsheet)(,value[.]_[.]games|,value[.]_[.]spreadsheet)*");

        JsonObject property2 = JsonObject.EMPTY
                .put("pattern", "(value[.]_[.]other|value[.]_[.]spreadsheet)(,value[.]_[.]other|,value[.]_[.]spreadsheet)*");

        JsonObject expectedResult = JsonObject.EMPTY
                .put("pattern", "(value[.]_[.]games|value[.]_[.]other|value[.]_[.]spreadsheet)(,value[.]_[.]games|,value[.]_[.]other|,value[.]_[.]spreadsheet)*");

        JsonObject result = stringPropertyMerger.unionOf(property1, property2);

        assertThat(result, is(expectedResult));
    }

    @Test
    void aOrB() {
        JsonObject mergedProperty = stringPropertyMerger.unionOf(
                JsonObject.EMPTY.put("type", JsonArray.of("string")).put("pattern", "a"),
                JsonObject.EMPTY.put("type", JsonArray.of("string")).put("const", "B"));
        assertThat(mergedProperty.get("pattern").getString(), is("B|a"));
    }

    @Test
    void _AAndBOrC() {
        JsonObject mergedProperty = stringPropertyMerger.unionOf(
                JsonObject.EMPTY.put("type", JsonArray.of("string")).put("pattern", "(A|C)(,A|,C)*"),
                JsonObject.EMPTY.put("type", JsonArray.of("string")).put("pattern", "(B|C)(,B|,C)*"));
        assertThat(mergedProperty.get("pattern").getString(), is("(A|B|C)(,A|,B|,C)*"));
    }
}
