/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.generation;

import com.zuunr.forms.formfield.Pattern;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonValue;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class PatternMergerTest {

    private PatternMerger patternMerger = new PatternMerger();

    @Test
    void hardenPatternTest1() {
        String pattern1 = ".*";
        String pattern2 = "ABC";
        String resultingPattern1 = patternMerger.hardenPattern(JsonValue.of(pattern1).as(Pattern.class), JsonValue.of(pattern2).as(Pattern.class)).asString();
        assertThat(resultingPattern1, is("ABC"));

        // The other way around
        String resultingPattern2 = patternMerger.hardenPattern(JsonValue.of(pattern2).as(Pattern.class), JsonValue.of(pattern1).as(Pattern.class)).asString();
        assertThat(resultingPattern2, is("ABC"));
    }

    @Test
    void hardenPatternTest2() {
        String pattern1 = "DEF";
        String pattern2 = "ABC";
        String resultingPattern = patternMerger.hardenPattern(JsonValue.of(pattern1).as(Pattern.class), JsonValue.of(pattern2).as(Pattern.class)).asString();
        assertThat(resultingPattern, is("(?=ABC)(?=DEF)"));
    }

    @Test
    @Disabled
    void hardenPatternTest2b() {
        String pattern1 = "ENUM_1|ENUM_2|ENUM_3";
        String pattern2 = "ENUM_2|ENUM_3|ENUM_4";
        String resultingPattern = patternMerger.hardenPattern(JsonValue.of(pattern1).as(Pattern.class), JsonValue.of(pattern2).as(Pattern.class)).asString();
        assertThat(resultingPattern, is("ENUM_2|ENUM_3"));
    }

    @Test
    @Disabled
    void hardenPatternTest2c() {
        String pattern1 = "ENUM_1|ENUM_3|ENUM_2";
        String pattern2 = "ENUM_3|ENUM_2|ENUM_4";
        String resultingPattern = patternMerger.hardenPattern(JsonValue.of(pattern1).as(Pattern.class), JsonValue.of(pattern2).as(Pattern.class)).asString();
        assertThat(resultingPattern, is("ENUM_2|ENUM_3"));
    }

    @Test
    void hardenPatternTest3() {
        String pattern1 = "(?=ENUM_1)(?=ENUM_2)";
        String pattern2 = "ENUM_1";
        String resultingPattern = patternMerger.hardenPattern(JsonValue.of(pattern1).as(Pattern.class), JsonValue.of(pattern2).as(Pattern.class)).asString();
        assertThat(resultingPattern, is("(?=ENUM_1)(?=ENUM_2)"));
    }

    @Test
    void compactPattern() {
        String pattern1 = "(?=ENUM1|ENUM2)(?=ENUM3)";
        String pattern2 = "(?=ENUM2|ENUM4)";
        String expectedResultingPattern = "(?=ENUM2)(?=ENUM3)";
        String resultingPattern = patternMerger.hardenPattern(JsonValue.of(pattern1).as(Pattern.class), JsonValue.of(pattern2).as(Pattern.class)).asString();
        assertThat(resultingPattern, is("(?=ENUM1|ENUM2)(?=ENUM2|ENUM4)(?=ENUM3)"));
    }

    @Test
    void hardenPatternTest3b() {
        String pattern1 = "(?=A|B)(?=B|C)";
        String pattern2 = "C";
        String resultingPattern = patternMerger.hardenPattern(JsonValue.of(pattern1).as(Pattern.class), JsonValue.of(pattern2).as(Pattern.class)).asString();
        assertThat(resultingPattern, is("(?=A|B)(?=B|C)(?=C)")); // Should give (?=A)(?=[^A])
    }

    @Test
    void hardenPatternTest3c() {
        String pattern1 = "(?=A|B)(?=B|C)";
        String pattern2 = "(?=A|B)";
        String resultingPattern = patternMerger.hardenPattern(JsonValue.of(pattern1).as(Pattern.class), JsonValue.of(pattern2).as(Pattern.class)).asString();
        assertThat(resultingPattern, is("(?=A|B)(?=B|C)")); // Should give B
    }

    @Test
    void softenPattern() {
        String pattern1 = "firstName|firstName desc,lastName";
        String pattern2 = "firstName,lastName desc";
        String resultingPattern = patternMerger.softenPattern(pattern1, pattern2);
        assertThat(resultingPattern, is("firstName|firstName desc,lastName|firstName,lastName desc"));
    }

    @Test
    void softenPatternWithExisting() {
        String pattern1 = "firstName|firstName desc,lastName|firstName,lastName desc";
        String pattern2 = "firstName,lastName desc";
        String resultingPattern = patternMerger.softenPattern(pattern1, pattern2);
        assertThat(resultingPattern, is("firstName|firstName desc,lastName|firstName,lastName desc"));
    }

    @Test
    void softenExtendPatternWithExisting() {
        String pattern1 = "(value[.]_[.]games|value[.]_[.]spreadsheet)(,value[.]_[.]games|,value[.]_[.]spreadsheet)*";
        String pattern2 = "value[.]_[.]games";
        String resultingPattern = patternMerger.softenPattern(pattern1, pattern2);
        assertThat(resultingPattern, is("(value[.]_[.]games|value[.]_[.]spreadsheet)(,value[.]_[.]games|,value[.]_[.]spreadsheet)*"));
    }

    @Test
    void intersectionSet() {
        JsonArray array1 = JsonArray.of("a","b");
        JsonArray array2 = JsonArray.of("b","c");

        assertThat(patternMerger.intersectionSet(array1, array2), is(JsonArray.of("b")));
    }

    @Test
    void unionSet() {
        JsonArray array1 = JsonArray.of("a","b");
        JsonArray array2 = JsonArray.of("b","c");

        assertThat(patternMerger.unionSet(array1, array2).sort(), is(JsonArray.of("a", "b", "c").sort()));
    }

    @Test
    void simpifyIntersectionSetTest(){
        // ["A|B","B|C"] -> [["A","B"],["B","C"]];

        JsonArray intersectionSet = JsonArray.of("A|B|D", "B|C|D|E");
        String result = patternMerger.simplifyIntersectionSet(intersectionSet);
        assertThat(result, is("B|D"));
    }
}
