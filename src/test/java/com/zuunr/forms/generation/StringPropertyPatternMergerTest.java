/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.generation;

import com.zuunr.json.JsonObject;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Niklas Eldberger
 */
class StringPropertyPatternMergerTest {

    private static StringPropertyPatternMerger stringPropertyPatternMerger = new StringPropertyPatternMerger();

    @Test
    void test1() {
        String pattern1 = "A";
        String pattern2 = "B";
        String expectedResult = "A|B";

        String result = stringPropertyPatternMerger.unionOf(pattern1, pattern2);
        assertThat(result, is(expectedResult));
        String resultOfReversedOrder = stringPropertyPatternMerger.unionOf(pattern2, pattern1);
        assertThat(resultOfReversedOrder, is(expectedResult));
    }

    @Test
    void test2() {
        String pattern1 = "something";
        String pattern2 = "something else|yet something else";
        String expectedResult = "something|something else|yet something else";

        String result = stringPropertyPatternMerger.unionOf(pattern1, pattern2);
        assertThat(result, is(expectedResult));
        String resultOfReversedOrder = stringPropertyPatternMerger.unionOf(pattern2, pattern1);
        assertThat(resultOfReversedOrder, is(expectedResult));
    }

    @Test
    void test3() {
        String pattern1 = "(A|B)C";
        String pattern2 = "AC|BC";
        String expectedResult = "(A|B)C|AC|BC";

        String result = stringPropertyPatternMerger.unionOf(pattern1, pattern2);
        assertThat(result, is(expectedResult));
        String resultOfReversedOrder = stringPropertyPatternMerger.unionOf(pattern2, pattern1);
        assertThat(resultOfReversedOrder, is(expectedResult));
    }

    @Test
    void test4() {
        String pattern1 = "^STRING_1$";
        String pattern2 = "STRING_1";
        String expectedResult = "STRING_1|^STRING_1$";

        String result = stringPropertyPatternMerger.unionOf(pattern1, pattern2);
        assertThat(result, is(expectedResult));
        String resultOfReversedOrder = stringPropertyPatternMerger.unionOf(pattern2, pattern1);
        assertThat(resultOfReversedOrder, is(expectedResult));
    }

    @Test
    void test5() {
        String pattern1 = "^STRING_1$|STRING_1";
        String pattern2 = "STRING_2";
        String expectedResult = "STRING_1|STRING_2|^STRING_1$";

        String result = stringPropertyPatternMerger.unionOf(pattern1, pattern2);
        assertThat(result, is(expectedResult));
        String resultOfReversedOrder = stringPropertyPatternMerger.unionOf(pattern2, pattern1);
        assertThat(resultOfReversedOrder, is(expectedResult));
    }

    @Test
    void test6() {
        String pattern = "STRING_2|^STRING_1$|STRING_1";
        String expectedResult = "STRING_1|STRING_2";
        String result = stringPropertyPatternMerger.normalizePattern(pattern);
        assertThat(result, is(expectedResult));
    }

    @Test
    void test7() {
        String pattern = "^A_2$|A_(2[|]3)";
        String expectedResult = "A_(2[|]3)|A_2";
        JsonObject unionSubsetResult = stringPropertyPatternMerger.unionSubsetsOf(pattern);
        JsonObject expectedUnionSubset = JsonObject.EMPTY
                .put("^A_2$", "^A_2$")
                .put("A_(2[|]3)", "A_(2[|]3)");
        assertThat(unionSubsetResult, is(expectedUnionSubset));

        String result = stringPropertyPatternMerger.normalizePattern(pattern);
        assertThat(result, is(expectedResult));
    }

    @Test
    void test8() {
        String pattern = "STRING_(2[|]3)|STRING_2";
        JsonObject expectedResult = JsonObject.EMPTY
                .put("STRING_(2[|]3)", "STRING_(2[|]3)")
                .put("STRING_2", "STRING_2");
        JsonObject result = stringPropertyPatternMerger.unionSubsetsOf(pattern);
        assertThat(result, is(expectedResult));
    }

    @Test
    void test9() {
        String pattern = "A|B|C|(D|E)|F|C";
        String expectedResult = "(D|E)|A|B|C|F";
        String result = stringPropertyPatternMerger.normalizePattern(pattern);
        assertThat(result, is(expectedResult));
    }

    @Test
    void test10() {
        String pattern = "A|B[B|C]|C|(D|E)|F|C";
        String expectedResult = "(D|E)|A|B[B|C]|C|F";
        String result = stringPropertyPatternMerger.normalizePattern(pattern);
        assertThat(result, is(expectedResult));
    }

    @Test
    void test11() {
        String pattern = "A\\|B|C";
        JsonObject expectedResult = JsonObject.EMPTY.put("A\\|B", "A\\|B").put("C","C");
        JsonObject result = stringPropertyPatternMerger.unionSubsetsOf(pattern);
        assertThat(result, is(expectedResult));
    }

    @Test
    void test12() {
        String pattern = "^STRING_2$";
        String expectedResult = "STRING_2";
        String result = stringPropertyPatternMerger.normalizePattern(pattern);
        assertThat(result, is(expectedResult));
    }

    @Test
    void test13() {
        String pattern = "(({[[{()}]]})))|A";
        String expectedResult = "(({[[{()}]]})))|A";
        String result = stringPropertyPatternMerger.normalizePattern(pattern);
        assertThat(result, is(expectedResult));
    }

    @Test
    void test14() {
        String pattern = ")|A";
        String expectedResult = ")|A";
        String result = stringPropertyPatternMerger.normalizePattern(pattern);
        assertThat(result, is(expectedResult));
    }

    @Test
    void test15() {
        String pattern = "(A|B|C)(,A|,B|,C)*";
        JsonObject expectedResult = JsonObject.EMPTY
                .put("A", "A")
                .put("B", "B")
                .put("C", "C");
        JsonObject result = stringPropertyPatternMerger.unionSubsetsOfCommaSeparatedPattern(pattern);
        assertThat(result, is(expectedResult));
    }
}
