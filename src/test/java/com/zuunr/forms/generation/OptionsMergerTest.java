/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.generation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.jupiter.api.Test;

import com.zuunr.forms.FormField;
import com.zuunr.forms.ValueFormat;
import com.zuunr.forms.formfield.Options;
import com.zuunr.forms.formfield.options.Option;
import com.zuunr.forms.formfield.options.Value;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;

/**
 * @author Niklas Eldberger
 */
class OptionsMergerTest {

    private OptionsMerger optionsMerger = new OptionsMerger();

    @Test
    void mergeOptionsTest() {

        FormField formField1 = FormField.builder("color")
                .options(Options.builder().value(Value.EMPTY.builder()
                        .add(Option.builder("warm").format(ValueFormat.EMPTY.builder().pattern("RED").build()).build())
                        .build()).build()).build();

        FormField formField2 = FormField.builder("color")
                .options(Options.builder().value(Value.EMPTY.builder()
                        .add(Option.builder("warm").format(ValueFormat.EMPTY.builder().pattern("YELLOW").build()).build())
                        .build()).build()).build();


        JsonObject result = optionsMerger.soften(formField1, formField2, JsonObject.EMPTY.put("name", "color"));
        FormField formFieldResult = result.as(FormField.class);

        assertThat(formFieldResult.schema().options().perLabel().get("warm").as(Option.class).format().pattern().asString(), is("RED|YELLOW"));
    }

    @Test
    void mergeOptionsValueTest() {

        FormField formField1 = FormField.builder("someField")
                .options(Options.builder()
                        .value(Value.EMPTY.builder()
                                .add(Option.builder("a")
                                        .value(JsonValue.of("A"))
                                        .build())
                                .build())
                        .build()).build();

        FormField formField2 = FormField.builder("someField")
                .options(Options.builder()
                        .value(Value.EMPTY.builder()
                                .add(Option.builder("b")
                                        .value(JsonValue.of("B"))
                                        .build())
                                .build())
                        .build()).build();

        FormField expected = FormField.builder("someField")
                .options(Options.builder()
                        .value(Value.EMPTY.builder()
                                .add(Option.builder("b")
                                        .value(JsonValue.of("B"))
                                        .build())
                                .add(Option.builder("a")
                                        .value(JsonValue.of("A"))
                                        .build())
                                .build())
                        .build()).build();


        JsonObject mergedFormField = optionsMerger.merge(formField1, formField2, MergeStrategy.SOFTEN, JsonObject.EMPTY.put("name", "someField"));

        assertThat(mergedFormField.as(FormField.class), is(expected));
    }

    @Test
    void mergeOptionsFormatTest() {

        FormField formField1 = FormField.builder("someField")
                .options(Options.builder()
                        .value(Value.EMPTY.builder()
                                .add(Option.builder("b")
                                        .format(ValueFormat.EMPTY.builder().pattern("B1").build())
                                        .build())

                                .add(Option.builder("a")
                                        .format(ValueFormat.EMPTY.builder().pattern("A").build())
                                        .build())

                                .build())
                        .build()).build();

        FormField formField2 = FormField.builder("someField")
                .options(Options.builder()
                        .value(Value.EMPTY.builder()
                                .add(Option.builder("b")
                                        .format(ValueFormat.EMPTY.builder().pattern("B2").build())
                                        .build())
                                .add(Option.builder("c")
                                        .format(ValueFormat.EMPTY.builder().pattern("C").build())
                                        .build())
                                .build())
                        .build()).build();

        FormField expected = FormField.builder("someField")
                .options(Options.builder()
                        .value(Value.EMPTY.builder()
                                .add(Option.builder("b")
                                        .format(ValueFormat.EMPTY.builder().pattern("B1|B2").build())
                                        .build())
                                .add(Option.builder("c")
                                        .format(ValueFormat.EMPTY.builder().pattern("C").build())
                                        .build())
                                .add(Option.builder("a")
                                        .format(ValueFormat.EMPTY.builder().pattern("A").build())
                                        .build())
                                .build())
                        .build()).build();

        JsonObject mergedFormField = optionsMerger.merge(formField1.asCompactFormField(), formField2, MergeStrategy.SOFTEN, FormField.builder("someField").build().asJsonObject());

        assertThat(mergedFormField.as(FormField.class), is(expected.asCompactFormField()));
    }
}
