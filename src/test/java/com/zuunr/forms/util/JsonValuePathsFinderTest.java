/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.util;

import com.zuunr.forms.Form;
import com.zuunr.forms.FormField;
import com.zuunr.forms.FormFields;
import com.zuunr.forms.formfield.Type;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Niklas Eldberger
 */
class JsonValuePathsFinderTest {

    private final JsonObject testData = JsonObject.EMPTY
            .put("firstName", "Peter")
            .put("lastName", "Andersson")
            .put("contacts", JsonArray.EMPTY
                    .add(JsonObject.EMPTY
                            .put("employer", JsonObject.EMPTY
                                    .put("companyName", "The Small Startup"))
                            .put("firstName", "Richard")
                            .put("lastName", "Wood"))

                    .add(JsonObject.EMPTY
                            .put("employer", JsonObject.EMPTY
                                    .put("companyName", "BigCo Inc"))
                            .put("firstName", "Laura")
                            .put("lastName", "Green"))
                    .add(JsonObject.EMPTY
                            .put("employer", JsonObject.EMPTY
                                    .put("companyName", "BigCo Inc"))
                            .put("firstName", "Rob")
                            .put("lastName", "Red")));

    private static JsonArray companySearchPattern(int contactIndex, String companyName){
        return JsonArray
                .of("contacts",
                        contactIndex,
                        JsonObject.EMPTY
                                .put("fieldNamePattern", "firstName")
                                .put("objectFormat", Form.EMPTY.builder()
                                        .exclusive(false)
                                        .value(FormFields.EMPTY.builder()
                                                .add(FormField.builder("employer")
                                                        .type(Type.OBJECT).form(Form.EMPTY.builder()
                                                                .value(JsonArray.of(
                                                                        FormField.builder("companyName")
                                                                                .pattern(companyName).build().asJsonObject()).as(FormFields.class))
                                                                .build()
                                                        )
                                                ).build())
                                        .build()
                                )
                );

    }

    private static JsonArray badCompanySearchPattern(int contactIndex, String companyName){
        return JsonArray
                .of("contacts",
                        contactIndex,
                        JsonObject.EMPTY
                                .put("fieldNamePattern", "firstName")
                                .put("objectFormat wrong spelling", Form.EMPTY.builder()
                                        .exclusive(false)
                                        .value(FormFields.EMPTY.builder()
                                                .add(FormField.builder("employer")
                                                        .type(Type.OBJECT).form(Form.EMPTY.builder()
                                                                .value(JsonArray.of(
                                                                        FormField.builder("companyName")
                                                                                .pattern(companyName).build().asJsonObject()).as(FormFields.class))
                                                                .build()
                                                        )
                                                ).build())
                                        .build()
                                )
                );

    }

    @Test
    void when_companyNameIsTheSmallStartup_then_returnRichard() {

        JsonArray patternPath = companySearchPattern(-1, "The Small Startup");
        JsonArray paths = new JsonValuePathsFinder().getPaths(testData.jsonValue(), patternPath, true);
        assertThat(paths.size(), is(1));
        assertThat(paths.last().getValue(JsonArray.class).last(), is(JsonValue.of("Richard")));
    }

    @Test
    void when_companyNameIsTheSmallStartup_then_returnTwoPaths() {

        JsonArray patternPath = companySearchPattern(-1, "BigCo Inc");
        JsonArray paths = new JsonValuePathsFinder().getPaths(testData.jsonValue(), patternPath, true);
        assertThat(paths.size(), is(2));
        assertThat(paths.get(0), is(JsonArray.of("contacts", 1, "firstName", "Laura").jsonValue()));
        assertThat(paths.get(1), is(JsonArray.of("contacts", 2, "firstName", "Rob").jsonValue()));
    }

    @Test
    void when_badSearchPattern_then_returnNoPaths() {

        JsonArray patternPath = badCompanySearchPattern(-1, "BigCo Inc");
        JsonArray paths = new JsonValuePathsFinder().getPaths(testData.jsonValue(), patternPath, true);
        assertThat(paths.size(), is(0));
    }

    @Test
    void when_companyNameIsTheSmallStartupInArrayIndex1_then_returnOnePath() {

        JsonArray patternPath = companySearchPattern(1, "BigCo Inc");
        JsonArray paths = new JsonValuePathsFinder().getPaths(testData.jsonValue(), patternPath, true);
        assertThat(paths.size(), is(1));
        assertThat(paths.get(0), is(JsonArray.of("contacts", 1, "firstName", "Laura").jsonValue()));
    }

    @Test
    void when_companyNameIsTheSmallStartupInArrayIndex1_then_returnOnePathWithoutValueLast() {

        JsonArray patternPath = companySearchPattern(1, "BigCo Inc");
        JsonArray paths = new JsonValuePathsFinder().getPaths(testData.jsonValue(), patternPath, false);
        assertThat(paths.size(), is(1));
        assertThat(paths.get(0), is(JsonArray.of("contacts", 1, "firstName").jsonValue()));
    }
}
