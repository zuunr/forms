/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.util;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;

import java.util.List;

import org.junit.jupiter.api.Test;

import com.zuunr.forms.Form;
import com.zuunr.forms.FormField;
import com.zuunr.forms.FormFields;
import com.zuunr.forms.ValueFormat;
import com.zuunr.forms.filter.FilterResult;
import com.zuunr.forms.filter.FormFilter;
import com.zuunr.forms.formfield.Options;
import com.zuunr.forms.formfield.Type;
import com.zuunr.forms.formfield.options.Option;
import com.zuunr.forms.formfield.options.ValidationStep;
import com.zuunr.forms.formfield.options.ValidationSteps;
import com.zuunr.forms.formfield.options.Value;
import com.zuunr.forms.generation.FormMerger;
import com.zuunr.forms.generation.MergeStrategy;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonArrayBuilder;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.tool.JsonUtil;
import com.zuunr.json.util.JsonObjectWrapper;

class FormUtilTest {

    FormMerger formMerger = new FormMerger();
    FormFilter formFilter = new FormFilter();

    @Test
    void dynamicFormGenerationTestScenario() {

        JsonArray aggregates = JsonArray.of(JsonObject.EMPTY
                        .put("in", JsonObject.EMPTY
                                .put("form", JsonObject.EMPTY
                                        .put("value", JsonArray.EMPTY
                                                .add(JsonObject.EMPTY
                                                        .put("name", "age")
                                                        .put("type", "integer")
                                                        .put("min", 12)
                                                        .put("max", 18)
                                                        .put("required", true)
                                                )
                                                .add(JsonObject.EMPTY
                                                        .put("name", "gender")
                                                        .put("type", "string")
                                                        .put("pattern", "FEMALE|MALE"))
                                        ))
                        )
                        .put("out", JsonObject.EMPTY
                                .put("form", JsonObject.EMPTY
                                        .put("value", JsonArray.EMPTY
                                                .add(JsonObject.EMPTY
                                                        .put("name", "validBoysClubMember")
                                                        .put("type", "boolean")
                                                )
                                                .add(JsonObject.EMPTY
                                                        .put("name", "validGirlsClubMember")
                                                        .put("type", "boolean")
                                                )
                                        ))),
                JsonObject.EMPTY
                        .put("in", JsonObject.EMPTY
                                .put("form", JsonObject.EMPTY
                                        .put("value", JsonArray.EMPTY
                                                .add(JsonObject.EMPTY
                                                        .put("name", "firstName")
                                                        .put("type", "string")
                                                        .put("pattern", "Kalle")
                                                        .put("required", true)

                                                )
                                                .add(JsonObject.EMPTY
                                                        .put("name", "lastName")
                                                        .put("type", "string")
                                                        .put("required", true)
                                                )

                                        ))
                        )
                        .put("out", JsonObject.EMPTY
                                .put("form", JsonObject.EMPTY
                                        .put("value", JsonArray.EMPTY
                                                .add(JsonObject.EMPTY
                                                        .put("name", "name")
                                                        .put("type", "string")
                                                )
                                        )))
        );

        JsonArrayBuilder aggregatesUpdatedWithDefaults = JsonArray.EMPTY.builder();
        JsonArray inForm = JsonArray.of("in", "form");
        JsonArray outForm = JsonArray.of("out", "form");
        for (JsonObject jsonObject : aggregates.asList(JsonObject.class)) {
            aggregatesUpdatedWithDefaults.add(JsonObject.EMPTY
                    .put(inForm, formMerger.merge(jsonObject.get(inForm).as(Form.class), jsonObject.get(inForm).as(Form.class), MergeStrategy.SOFTEN).asJsonArray())
                    .put(outForm, formMerger.merge(jsonObject.get(outForm).as(Form.class), jsonObject.get(outForm).as(Form.class), MergeStrategy.SOFTEN).asJsonArray()));

        }

        JsonArray requestedValues = JsonArray.of(
                JsonArray.of("validBoysClubMember"),
                JsonArray.of("name")
        );

        JsonObject resource = JsonObject.EMPTY.builder()
                .put("gender", "MALE")
                .build();

        JsonObject fullResourceForm = JsonObject.EMPTY
                .put("value", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("name", "name")
                                .put("type", "string")
                        )
                        .add(JsonObject.EMPTY
                                .put("name", "firstName")
                                .put("type", "string")
                        )
                        .add(JsonObject.EMPTY
                                .put("name", "lastName")
                                .put("type", "string")
                        )
                        .add(JsonObject.EMPTY
                                .put("name", "validBoysClubMember")
                                .put("type", "boolean")
                        )
                        .add(JsonObject.EMPTY
                                .put("name", "validGirlsClubMember")
                                .put("type", "boolean")
                        )
                        .add(JsonObject.EMPTY
                                .put("name", "age")
                                .put("type", "integer")
                                .put("min", 0)
                        )
                        .add(JsonObject.EMPTY
                                .put("name", "gender")
                                .put("type", "string")
                                .put("pattern", "FEMALE|MALE")
                        )
                );


        Form form = getForm(requestedValues, resource, aggregatesUpdatedWithDefaults.build(), fullResourceForm.jsonValue().as(Form.class));

        Form expectedForm = JsonObject.EMPTY
                .put("value", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("min", 12).put("nullable", false).put("max", 18).put("name", "age").put("mustBeNull", false).put("type", "integer").put("required", true))
                        .add(JsonObject.EMPTY
                                .put("nullable", false).put("name", "firstName").put("pattern", "Kalle").put("mustBeNull", false).put("type", "string").put("required", true))
                        .add(JsonObject.EMPTY.put("nullable", false).put("name", "lastName").put("mustBeNull", false).put("type", "string").put("required", true))).jsonValue().as(Form.class);
        assertThat("", form.value().sort(), is(expectedForm.value().sort()));
    }

    @Test
    void getSubFormsTest() {

        Form form = Form.EMPTY.builder().value(FormFields.EMPTY.builder()
                .add(FormField
                        .builder("name")
                        .type(Type.STRING)
                        .build())
                .add(FormField
                        .builder("age")
                        .type(Type.INTEGER)
                        .build())
                .add(FormField
                        .builder("contact")
                        .type(Type.OBJECT)
                        .form(Form.EMPTY.builder().value(FormFields.EMPTY.builder()
                                .add(FormField
                                        .builder("email")
                                        .type(Type.STRING)
                                        .pattern(".*")
                                        .build())
                                .add(FormField
                                        .builder("phone")
                                        .type(Type.STRING)
                                        .pattern(".*")
                                        .build())

                                .build()).build()

                        ).build()
                ).build()).build();

        JsonArray subFormsArray = getSubForms(form);
        List<Form> subForms = subFormsArray.asList(Form.class);


        assertThat("There are 4 form fields which don't contains forms themselves", subForms.size(), is(4));

        JsonArray subFormsArray2 = getSubForms(JsonArray.EMPTY.add(JsonArray.EMPTY.add("name")).add(JsonArray.EMPTY.add("contact").add("email")), form);
        assertThat(subFormsArray2.size(), is(2));
    }

    private Form getForm(JsonArray requestedData, JsonObject currentData, JsonArray aggregates, Form fullResourceForm) {

        JsonObject form = JsonObject.EMPTY;

        // Remove existing data from requested data
        requestedData = updateRequestedData(requestedData, currentData);

        JsonArray targetForms = getSubForms(requestedData, fullResourceForm);

        JsonArray forms = getFormsFromTargetForms(targetForms, currentData, aggregates, fullResourceForm);

        return forms
                .asList(Form.class).stream().reduce((form1, form2) ->
                        formMerger.mergeSubforms(form1, form2)).get();

    }

    private JsonArray getFormsFromTargetForms(JsonArray targetForms, JsonObject currentData, JsonArray aggregates, Form fullResourceForm) {

        JsonArrayBuilder resultingSubForms = JsonArray.EMPTY.builder();

        // Remove existing data from target forms
        targetForms = updateTargetForms(targetForms, currentData);

        boolean targetFormsUpdated = true;
        while (targetFormsUpdated) {
            targetFormsUpdated = false;

            JsonArrayBuilder newTargetForms = JsonArray.EMPTY.builder();
            for (Form targetForm : targetForms.asList(Form.class)) {
                JsonArray derivedTargetSubforms = derivedTargetSubforms(targetForm, aggregates, currentData);

                if (derivedTargetSubforms != null) {
                    newTargetForms.addAll(derivedTargetSubforms);
                    targetFormsUpdated = true;
                } else {
                    newTargetForms.add(targetForm);
                }
            }
            targetForms = newTargetForms.build();
        }
        return targetForms;

    }

    public JsonArray derivedTargetSubforms(Form targetForm, JsonArray aggregates, JsonObject currentData) {
        JsonArray derivedTargetsubforms = null;
        for (JsonObject aggregate : aggregates.asList(JsonObject.class)) {
            // Get "in"-form for the "out" matching path
            FormFields outForm = aggregate.get(JsonArray.of("out", "form")).as(FormFields.class);

            JsonArray mergedFormFields = formMerger.merge(Form.EMPTY.builder().value(outForm).build(), targetForm, MergeStrategy.SOFTEN).asJsonArray();
            if (outForm.asJsonArray().equals(mergedFormFields)) {
                // Aggregate matched the out-form and this means the in-form should be choosen
                FormFields inForm = aggregate.get(JsonArray.of("in", "form")).as(FormFields.class);

                JsonArray subFormsOfIn = getSubForms(Form.EMPTY.builder().value(inForm).build());
                JsonArray updatedSubFormsOfArray = updateTargetForms(subFormsOfIn, currentData);
                derivedTargetsubforms = updatedSubFormsOfArray;
            }
        }
        return derivedTargetsubforms;
    }

    private JsonArray updateTargetForms(JsonArray targetSubForms, JsonObject currentData) {

        JsonArrayBuilder updatedTargetForms = JsonArray.EMPTY.builder();

        for (Form form : targetSubForms.asList(Form.class)) {
            FilterResult<JsonObjectWrapper> filterResult = formFilter.filter(form, currentData, true, false);
            if (filterResult.filtrate == null || filterResult.filtrate.asJsonObject().isEmpty()) {
                updatedTargetForms.add(form);
            }
        }

        return updatedTargetForms.build();
        // remove all parts of target form which is already covered by the current data
        // Only keep the forms which are needed to complete the current data
    }

    private JsonArray getSubForms(JsonArray paths, Form resourceForm) {
        JsonArrayBuilder subFormsBuilder = JsonArray.EMPTY.builder();
        for (JsonArray path : paths.asList(JsonArray.class)) {

            Form subForm = getSubForm(path, resourceForm);
            if (subForm != null) {
                subFormsBuilder.add(subForm);
            }
        }
        return subFormsBuilder.build();
    }

    @Test
    void getSubForm() {
        JsonObject fullResourceForm = JsonObject.EMPTY
                .put("value", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("name", "firstName")
                                .put("type", "string")
                        )
                        .add(JsonObject.EMPTY
                                .put("name", "lastName")
                                .put("type", "string")
                        )
                        .add(JsonObject.EMPTY
                                .put("name", "validBoysClubMember")
                                .put("type", "boolean")
                        )
                        .add(JsonObject.EMPTY
                                .put("name", "validGirlsClubMember")
                                .put("type", "boolean")
                        )
                        .add(JsonObject.EMPTY
                                .put("name", "age")
                                .put("type", "integer")
                                .put("min", 0)
                        )
                        .add(JsonObject.EMPTY
                                .put("name", "gender")
                                .put("type", "string")
                                .put("pattern", "FEMALE|MALE")
                        )
                );

        Form form = getSubForm(JsonArray.of("lastName"), fullResourceForm.jsonValue().as(Form.class));
        JsonObject expectedResult = JsonObject.EMPTY
                .put("value", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("name", "lastName")
                                .put("type", "string")
                                .put("required", false)
                                .put("nullable", false)
                                .put("mustBeNull", false)
                        ));
        assertThat(form.value().size(), is(1));
        assertThat(form.value().get(0).as(FormField.class).asExplicitFormField(), is(expectedResult.as(Form.class).value().get(0).as(FormField.class).asExplicitFormField()));
    }


    private Form getSubForm(JsonArray path, Form form) {
        Form result = form;
        List<FormField> formFields = form.formFields().asList();
        if (!path.isEmpty()) {

            if (path.head().is(String.class)) {

                FormField formField = form.formField(path.head().getValue(String.class));

                if (formField == null) {
                    result = null;
                } else {

                    Type type = formField.schema().type();
                    if (type.isObject()) {

                        Form subForm = getSubForm(path.tail(), formField.schema().form());
                        FormField newFormField = formField.builder().form(subForm).build();
                        FormFields newFormFields = FormFields.EMPTY.builder().add(newFormField).build();

                        result = form.builder().value(newFormFields).build();

                    } else if (type.isArray() || type.isSet()) {
                        Form subForm = getSubForm(path.tail(), formField.schema().eform());
                        FormField newFormField = formField.builder().eform(subForm).build();
                        FormFields newFormFields = FormFields.EMPTY.builder().add(newFormField).build();

                        result = form.builder().value(newFormFields).build();
                    } else {
                        result = form.builder().value(FormFields.EMPTY.builder().add(formField).build()).build();
                    }
                }

            } else if (path.head().isInteger() && path.size() > 1 && path.get(1).is(String.class)) {
                result = getSubForm(path.tail(), form);
            }
        }
        return result;
    }

    private JsonArray getSubForms(Form form) {
        JsonArrayBuilder formsBuilder = JsonArray.EMPTY.builder();
        List<FormField> formFields = form.formFields().asList();

        for (FormField formField : formFields) {


            if (formField.schema().type().isObject() && formField.schema().form() != null) {

                List<Form> subForms = getSubForms(formField.schema().form()).asList(Form.class);
                for (Form subForm : subForms) {
                    Form.FormBuilder formBuilder = Form.EMPTY.builder();
                    FormField.Builder formFieldBuilder = formField.builder();
                    formsBuilder.add(formBuilder.value(FormFields.EMPTY.builder().add(formFieldBuilder.form(subForm).build()).build()).build());
                }
            } else if (formField.schema().type().isArray() && formField.schema().type().isSet() && formField.schema().eform() != null) {
                List<Form> subForms = getSubForms(formField.schema().eform()).asList(Form.class);
                for (Form subForm : subForms) {
                    Form.FormBuilder formBuilder = Form.EMPTY.builder();
                    FormField.Builder formFieldBuilder = formField.builder();
                    formsBuilder.add(formBuilder.value(FormFields.EMPTY.builder().add(formFieldBuilder.eform(subForm).build()).build()).build());
                }

            } else {
                Form.FormBuilder formBuilder = Form.EMPTY.builder();
                formsBuilder.add(formBuilder.value(FormFields.EMPTY.builder().add(formField).build()).build());
            }

        }
        return formsBuilder.build();
    }

    private JsonArray updateRequestedData(JsonArray requestedData, JsonObject currentData) {
        JsonArrayBuilder builder = JsonArray.EMPTY.builder();
        for (JsonArray path : requestedData.asList(JsonArray.class)) {
            if (currentData.get(path, JsonValue.NULL).getValue() == null) {
                builder.add(path);
            }
        }
        return builder.build();
    }

    @Test
    void requestParameterValidationCreationTest() {

        Form formWithOnlyA = Form.EMPTY.builder()
                .value(FormFields.EMPTY.builder()
                        .add(FormField.builder("a"))
                        .build())
                .build();

        Form formWithAandBandC = Form.EMPTY.builder()
                .value(FormFields.EMPTY.builder()
                        .add(FormField.builder("c"))
                        .add(FormField.builder("a"))
                        .add(FormField.builder("b"))
                        .build())
                .build();

        Form requestParamsFormWithAandBandC = Form.EMPTY.builder()
                .value(FormFields.EMPTY.builder()
                        .add(FormField
                                .builder("requestParams")
                                .type(Type.OBJECT)
                                .form(formWithAandBandC)
                                .build())
                        .build())
                .build();

        Form requestParamsFormWithOptions = Form.EMPTY.builder()
                .value(FormFields.EMPTY.builder()
                        .add(FormField
                                .builder("requestParams")
                                .type(Type.OBJECT)
                                .options(Options.builder()
                                        .value(Value.EMPTY.builder()
                                                .add(Option
                                                        .builder("a and b")
                                                        .format(formWithAandBandC.asObjectValueFormat())
                                                        .build())
                                                .build())
                                        .build())
                                .build())
                        .build())
                .build();

        Option option = FormUtil.asOptionWithFormFieldNamesAsLabel(requestParamsFormWithAandBandC
                .formFields()
                .formFieldsByName()
                .get("requestParams")
                .as(FormField.class)
                .schema()
                .form());

        assertThat(option.label(), is("a,b,c"));


    }

    @Test
    void putValueFormatTest() {

        Form formWithAandBandC = Form.EMPTY.builder()
                .value(FormFields.EMPTY.builder()
                        .add(FormField.builder("c"))
                        .add(FormField.builder("a"))
                        .add(FormField.builder("b"))
                        .build())
                .build();

        Form requestParamsFormWithAandBandC = Form.EMPTY.builder()
                .value(FormFields.EMPTY.builder()
                        .add(FormField
                                .builder("requestParams")
                                .type(Type.OBJECT)
                                .form(formWithAandBandC)
                                .build())
                        .build())
                .build();

        ValueFormat resultingValueFormat0 = FormUtil.putValueFormat(JsonArray.of("requestParams"), ValueFormat.EMPTY.builder().pattern("abc123").build(), ValueFormat.EMPTY);
        ValueFormat expected0 = JsonUtil.create(ValueFormat.class, "{'form':{'value':[{'name':'requestParams','pattern':'abc123'}]},'type':'object'}");
        assertThat(resultingValueFormat0, is(expected0));
        ValueFormat resultingValueFormat1 = FormUtil.putValueFormat(JsonArray.of("requestParams"), ValueFormat.EMPTY.builder().pattern("abc123").build(), requestParamsFormWithAandBandC.asObjectValueFormat());
        ValueFormat expected1 = JsonUtil.create(ValueFormat.class, "{'form':{'value':[{'name':'requestParams','pattern':'abc123'}]},'type':'object'}");
        assertThat(resultingValueFormat1, is(expected1));
        ValueFormat resultingValueFormat2 = FormUtil.putValueFormat(JsonArray.of("requestParams", "d"), ValueFormat.EMPTY.builder().pattern("abc123").build(), requestParamsFormWithAandBandC.asObjectValueFormat());
        ValueFormat expected2 = JsonUtil.create(ValueFormat.class, "{'form':{'value':[{'form':{'value':[{'name':'c'},{'name':'a'},{'name':'b'},{'name':'d','pattern':'abc123'}]},'name':'requestParams','type':'object'}]},'type':'object'}");
        assertThat(resultingValueFormat2, is(expected2));
        ValueFormat resultingValueFormat3 = FormUtil.putValueFormat(JsonArray.of(3), ValueFormat.EMPTY.builder().pattern("abc123").build(), ValueFormat.EMPTY);
        ValueFormat expected3 = JsonUtil.create(ValueFormat.class, "{'element':{'pattern':'abc123'}}");
        assertThat(resultingValueFormat3, is(expected3));
        ValueFormat resultingValueFormat4 = FormUtil.putValueFormat(JsonArray.of("requestParamsArray", 3), ValueFormat.EMPTY.builder().pattern("abc123").build(), ValueFormat.EMPTY);
        ValueFormat expected4 = ValueFormat.EMPTY.builder().type(Type.OBJECT).form(Form.EMPTY.builder().value(FormFields.EMPTY.builder().add(FormField.builder("requestParamsArray").type(Type.ARRAY).element(ValueFormat.EMPTY.builder().pattern("abc123").build()).build()).build()).build()).build();
        assertThat(resultingValueFormat4, is(expected4));
        ValueFormat resultingValueFormat5 = FormUtil.putValueFormat(JsonArray.of("requestParams", "d", 3), ValueFormat.EMPTY.builder().pattern("abc123").build(), requestParamsFormWithAandBandC.asObjectValueFormat());
        ValueFormat expected5 = JsonUtil.create(ValueFormat.class, "{'form':{'value':[{'form':{'value':[{'name':'c'},{'name':'a'},{'name':'b'},{'element':{'pattern':'abc123'},'name':'d','type':'array'}]},'name':'requestParams','type':'object'}]},'type':'object'}");
        assertThat(resultingValueFormat5, is(expected5));
    }

    @Test
    void queryParamsAsOptionTest(){

        Form form0 = Form.EMPTY.builder()
                .value(FormFields.EMPTY.builder()
                        .add(FormField.builder("queryParams")
                                .type(Type.OBJECT)
                                .form(Form.EMPTY.builder().value(FormFields.EMPTY.builder()
                                        .add(FormField.builder("value._.firstName").build())
                                        .add(FormField.builder("value._.lastName").build())
                                        .build()).build())
                                .build())
                        .build())
                .build();
        ValueFormat valueFormatAsOption0 = FormUtil.replaceFormWithOptionForm(form0.formField("queryParams").schema().form().asObjectValueFormat());
        assertThat(valueFormatAsOption0.options().perLabel().get("value._.firstName,value._.lastName"), notNullValue());

        ValueFormat queryParamsOptions0 = FormUtil.putValueFormat(JsonArray.of("queryParams"), valueFormatAsOption0, form0.asObjectValueFormat());
        assertThat(queryParamsOptions0.form().formField("queryParams").schema().options().getOption("value._.firstName,value._.lastName"), notNullValue());
        assertThat(queryParamsOptions0.form().formField("queryParams").schema().form(), nullValue());
        assertThat(queryParamsOptions0.form().formField("queryParams").schema().options().getOption("value._.firstName,value._.lastName").format().form(), is(form0.formField("queryParams").schema().form()));


        Form form1 = Form.EMPTY.builder()
                .value(FormFields.EMPTY.builder()
                        .add(FormField.builder("queryParams")
                                .type(Type.OBJECT)
                                .form(Form.EMPTY.builder().value(FormFields.EMPTY.builder()
                                        .add(FormField.builder("value._.firstName").build())
                                        .add(FormField.builder("value._.age").build())
                                        .build()).build())
                                .build())
                        .build())
                .build();

        ValueFormat valueFormatAsOption1 = FormUtil.replaceFormWithOptionForm(form1.formField("queryParams").schema().form().asObjectValueFormat());
        assertThat(valueFormatAsOption1.options().perLabel().get("value._.age,value._.firstName"), notNullValue());

        ValueFormat queryParamsOptions1 = FormUtil.putValueFormat(JsonArray.of("queryParams"), valueFormatAsOption1, form1.asObjectValueFormat());
        assertThat(queryParamsOptions1.form().formField("queryParams").schema().options().getOption("value._.age,value._.firstName"), notNullValue());
        assertThat(queryParamsOptions1.form().formField("queryParams").schema().form(), nullValue());
        assertThat(queryParamsOptions1.form().formField("queryParams").schema().options().getOption("value._.age,value._.firstName").format().form(), is(form1.formField("queryParams").schema().form()));

        Form resultingForm = formMerger.mergeForms(queryParamsOptions0.form(), queryParamsOptions1.form(), MergeStrategy.SOFTEN);

        assertThat(resultingForm.formField("queryParams").asValueFormat().options().getOption("value._.age,value._.firstName"), notNullValue());
    }

    @Test
    void replaceAllValidationStepsOfIndexTest(){

        ValueFormat toBeupdated = ValueFormat.EMPTY.builder().type(Type.OBJECT).options(Options.builder().value(Value.EMPTY.builder()
                .add(Option.builder("Option1")
                        .validationSteps(ValidationSteps.EMPTY.builder()
                                .add(ValidationStep.builder(ValueFormat.EMPTY.builder().type(Type.OBJECT)
                                        .form(Form.EMPTY.builder().value(FormFields.EMPTY.builder()
                                                .add(FormField.builder("firstName").build())
                                                .build()).build())
                                        .build()).build())
                                .add(ValidationStep.builder(ValueFormat.EMPTY.builder().type(Type.OBJECT)
                                        .form(Form.EMPTY.builder().value(FormFields.EMPTY.builder()
                                                .add(FormField.builder("firstName").build())
                                                .build()).build())
                                        .build()).build())
                                .build()).build())
                .add(Option.builder("Option2")
                        .validationSteps(ValidationSteps.EMPTY.builder()
                                .add(ValidationStep.builder(ValueFormat.EMPTY.builder().type(Type.OBJECT)
                                        .form(Form.EMPTY.builder().value(FormFields.EMPTY.builder()
                                                .add(FormField.builder("firstName").build())
                                                .build()).build())
                                        .build()).build())
                                .add(ValidationStep.builder(ValueFormat.EMPTY.builder().type(Type.OBJECT)
                                        .form(Form.EMPTY.builder().value(FormFields.EMPTY.builder()
                                                .add(FormField.builder("firstName").build())
                                                .build()).build())
                                        .build()).build())
                                .build()).build())


                .build()).build()).build();


        ValidationStep validationStep = ValidationStep.builder(ValueFormat.EMPTY.builder().type(Type.OBJECT)
                .form(Form.EMPTY.builder().value(FormFields.EMPTY.builder()
                        .add(FormField.builder("firstName").pattern("Peter").build())
                        .build()).build())
                .build()).build();

        ValueFormat result = FormUtil.replaceAllValidationStepsOfIndex(toBeupdated, 1, validationStep);
        ValueFormat expected = ValueFormat.EMPTY.builder().type(Type.OBJECT).options(Options.builder().value(Value.EMPTY.builder()
                .add(Option.builder("Option1")
                        .validationSteps(ValidationSteps.EMPTY.builder()
                                .add(ValidationStep.builder(ValueFormat.EMPTY.builder().type(Type.OBJECT)
                                        .form(Form.EMPTY.builder().value(FormFields.EMPTY.builder()
                                                .add(FormField.builder("firstName").build())
                                                .build()).build())
                                        .build()).build())
                                .add(ValidationStep.builder(ValueFormat.EMPTY.builder().type(Type.OBJECT)
                                        .form(Form.EMPTY.builder().value(FormFields.EMPTY.builder()
                                                .add(FormField.builder("firstName").pattern("Peter").build())
                                                .build()).build())
                                        .build()).build())
                                .build()).build())
                .add(Option.builder("Option2")
                        .validationSteps(ValidationSteps.EMPTY.builder()
                                .add(ValidationStep.builder(ValueFormat.EMPTY.builder().type(Type.OBJECT)
                                        .form(Form.EMPTY.builder().value(FormFields.EMPTY.builder()
                                                .add(FormField.builder("firstName").build())
                                                .build()).build())
                                        .build()).build())
                                .add(ValidationStep.builder(ValueFormat.EMPTY.builder().type(Type.OBJECT)
                                        .form(Form.EMPTY.builder().value(FormFields.EMPTY.builder()
                                                .add(FormField.builder("firstName").pattern("Peter").build())
                                                .build()).build())
                                        .build()).build())
                                .build()).build())
                .build()).build()).build();

        assertThat(result, is(expected));

    }
}
