/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.util;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.function.Predicate;
import java.util.function.UnaryOperator;

import org.junit.jupiter.api.Test;

import com.zuunr.forms.Form;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.tool.JsonUtil;

/**
 * @author Niklas Eldberger
 */
class DecorationTemplateTest {

    private Form resourceBodyForm = JsonUtil.create(Form.class, "{" +
            "    'value': [" +
            "      {'name': 'href', 'pattern': 'https://example[.]com/v1/sys/api/people/\\\\w+)'}," +
            "      {'name': 'name'}," +
            "      {" +
            "        'name': 'friends'," +
            "        'type': 'array'," +
            "        'eform': {" +
            "          'value': [" +
            "            {'name': 'href', 'pattern': 'https://example[.]com/v1/sys/api/people/\\\\w+'}," +
            "            {'name': 'name'}" +
            "          ]" +
            "        }" +
            "      }," +
            "      {" +
            "        'name': 'mother'," +
            "        'type': 'object'," +
            "        'form': {" +
            "          'value': [" +
            "            {'name': 'href', 'pattern': 'https://example[.]com/v1/sys/api/people/\\\\w+'}," +
            "            {'name': 'name'}," +
            "            {'name': 'image', 'type': 'object', 'form': " +
            "               {'value': " +
            "                  [" +
            "                     {'name': 'href', 'pattern': 'https://example.com/pictures/\\\\w+'}" +
            "                  ]" +
            "               }" +
            "            }," +
            "            {'name': 'status', 'pattern': 'TODO|DOING'}" +
            "          ]" +
            "        }" +
            "      }" +
            "    ]" +
            "  }");

    @Test
    void endToEndTest() {

        JsonObject resource = JsonObject.EMPTY
                .put("href", "https://example.com/v1/sys/api/people/1")
                .put("name", "Peter Andersson")
                .put("friends", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("href", "https://example.com/v1/sys/api/people/2")
                                .put("name", "Rob Robson")
                        ))
                .put("mother", JsonObject.EMPTY
                        .put("href", "https://example.com/v1/sys/api/people/3")
                        .put("name", "Laura Andersson")
                        .put("image", JsonObject.EMPTY.put("href", "https://example.com/pictures/4"))
                );
        UnaryOperator<JsonObject> decorator = obj -> {
            String href = obj.get("href").getString();
            char lastChar = href.charAt(href.length() - 1);
            return obj.put("id", "" + lastChar);
        };

        JsonObject expectedResult = JsonObject.EMPTY
                .put("href", "https://example.com/v1/sys/api/people/1")
                .put("name", "Peter Andersson")
                .put("friends", JsonArray.EMPTY
                        .add(JsonObject.EMPTY
                                .put("href", "https://example.com/v1/sys/api/people/2")
                                .put("id", "2")
                                .put("name", "Rob Robson")
                        ))
                .put("mother", JsonObject.EMPTY
                        .put("href", "https://example.com/v1/sys/api/people/3")
                        .put("id", "3")
                        .put("name", "Laura Andersson")
                        .put("image", JsonObject.EMPTY.put("href", "https://example.com/pictures/4"))
                );

        JsonObject result = resourceBodyForm.asJsonObject().as(DecorationTemplate.class).decorateLinkedResourcesOfSameApiUri(resource, decorator);

        assertThat(result.asJsonSorted(), is(expectedResult.asJsonSorted()));
    }

    public JsonObject decorateHrefs(JsonObject resource, Predicate<String> hrefMatcher, UnaryOperator<JsonObject> decorator) {
        return new LinkExtender().decorateAllPaths(resource, resourceBodyForm.asJsonObject().as(DecorationTemplate.class).getHrefPaths(hrefMatcher), decorator);
    }

    @Test
    void testApiUriPattern() {
        String apiUriPattern = DecorationTemplate.getApiUriPattern(JsonValue.of("https://example[.]com/v1/sys/api/people/984755"));

        assertThat(apiUriPattern, is("https://example[.]com/v1/sys/api/"));
    }

    @Test
    void test() {

        String hrefPatternStart = "https://example[.]com/v1/sys/api";
        JsonArray hrefPaths = resourceBodyForm.asJsonObject().as(DecorationTemplate.class).getHrefPaths();

        JsonArray expected = JsonArray.of(JsonArray.of("friends", -1, "href"), JsonArray.of("href"), JsonArray.of("mother", "href"));
        assertThat(hrefPaths, is(expected));

        JsonArray hrefPaths2 = resourceBodyForm.asJsonObject().as(DecorationTemplate.class).getHrefPaths(hrefPatternStart);
        assertThat(hrefPaths2, is(expected));
    }
}
