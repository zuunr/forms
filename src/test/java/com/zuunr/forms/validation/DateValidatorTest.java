/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.validation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.jupiter.api.Test;

import com.zuunr.forms.FormField;
import com.zuunr.json.JsonValue;
import com.zuunr.json.tool.JsonUtil;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * @author Niklas Eldberger
 */
class DateValidatorTest {
    
    @Test
    void test(){

        FormField formField = JsonUtil.create("{'name':'birthDate','type':'date'}").as(FormField.class);

        DateValidator dateValidator = new DateValidator();

        assertThat(
                dateValidator.validate(formField, JsonValue.of("2012-01-01"), true, FormFieldValidationResult.builder()).isValid(),
                is(true));

        FormFieldValidationResult result = dateValidator.validate(formField, JsonValue.of("2012-01-45"), true, FormFieldValidationResult.builder()).build();
        assertThat(
                result.violations.isEmpty(),
                is(false));
    }

    @Test
    void dateTimeTest(){
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME;

        assertThat(isDateTime(JsonValue.of("2000-01-01T00:01:13.454Z"), dateTimeFormatter), is(true));
        assertThat(isDateTime(JsonValue.of("1889-01-01T00:01:13.45Z"), dateTimeFormatter), is(true));
        assertThat(isDateTime(JsonValue.of("1889-01-01T00:01:13Z"), dateTimeFormatter), is(true));
        assertThat(isDateTime(JsonValue.of("1889-01-01T00:01Z"), dateTimeFormatter), is(true));
        assertThat(isDateTime(JsonValue.of("1889-13-01T00:01:13.454Z"), dateTimeFormatter), is(false));
    }

    private boolean isDateTime(JsonValue jsonValue, DateTimeFormatter formatter){
        try {
            LocalDate.parse(jsonValue.getValue(String.class), formatter);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
