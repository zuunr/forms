/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.validation;

import com.zuunr.forms.FormField;
import com.zuunr.json.JsonValue;
import com.zuunr.json.tool.JsonUtil;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Niklas Eldberger
 */
class DatetimeValidatorTest {

    @Test
    void test() {

        FormField formField = JsonUtil.create("{'name':'birthDate','type':'datetime'}").as(FormField.class);

        DatetimeValidator datetimeValidator = new DatetimeValidator();

        assertThat(
                datetimeValidator.validate(formField, JsonValue.of("2012-01-01T03:03:03Z"), true, FormFieldValidationResult.builder()).isValid(),
                is(true));

        assertThat(
                datetimeValidator.validate(formField, JsonValue.of("2012-01-01T03:03Z"), true, FormFieldValidationResult.builder()).isValid(),
                is(true));

        assertThat(
                datetimeValidator.validate(formField, JsonValue.of("2012-01-01T03:03:03.123Z"), true, FormFieldValidationResult.builder()).isValid(),
                is(true));


        assertThat(
                datetimeValidator.validate(formField, JsonValue.of("2012-01-01T03:03:03:123Z"), true, FormFieldValidationResult.builder()).isValid(),
                is(false));

        assertThat(
                datetimeValidator.validate(formField, JsonValue.of("2012-01-01T03.03.03.123Z"), true, FormFieldValidationResult.builder()).isValid(),
                is(false));


        FormFieldValidationResult result = datetimeValidator.validate(formField, JsonValue.of("2012-01-45"), true, FormFieldValidationResult.builder()).build();
        assertThat(
                result.violations.isEmpty(),
                is(false));
    }
}
