/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.validation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

/**
 * @author Niklas Eldberger
 */
class DecimalValidatorTest {

    @Test
    void greaterThan(){
        DecimalValidator decimalValidator = new DecimalValidator();
        BigDecimal smaller = new BigDecimal(9.0);
        BigDecimal bigger = new BigDecimal(10.0);
        assertThat(decimalValidator.greaterThan(bigger, smaller), is(true));
        assertThat(decimalValidator.greaterThan(smaller, bigger), is(false));
    }

    @Test
    void lessThan(){
        DecimalValidator decimalValidator = new DecimalValidator();
        BigDecimal smaller = new BigDecimal(9.0);
        BigDecimal bigger = new BigDecimal(10.0);
        assertThat(decimalValidator.lessThan(bigger, smaller), is(false));
        assertThat(decimalValidator.lessThan(smaller, bigger), is(true));
    }
}
