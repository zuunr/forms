/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.jsonpointer;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.jsonpointer.JsonPointer;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Niklas Eldberger
 */
class JsonPointerTest {

    @Test
    void test1(){

        JsonObject jsonObject = JsonObject.EMPTY
                .put("foo", JsonArray.of("bar", "baz"))
                .put("", 0)
                .put("a/b", 1)
                .put("c%d", 2)
                .put("e^f", 3)
                .put("g|h", 4)
                .put("i\\j", 5)
                .put("k\"l", 6)
                .put(" ", 7)
                .put("m~n", 8);

        assertThat(jsonObject.get(JsonPointer.of("#").asJsonArray()), is(jsonObject.jsonValue()));  // the whole document
        assertThat(jsonObject.get(JsonPointer.of("#/foo").asJsonArray()), is(JsonArray.of("bar", "baz").jsonValue()));
        assertThat(jsonObject.get(JsonPointer.of("#/foo/0").asJsonArray()), is(JsonValue.of("bar")));
        assertThat(jsonObject.get(JsonPointer.of("#/").asJsonArray()), is(JsonValue.of(0)));
        assertThat(jsonObject.get(JsonPointer.of("#/a~1b").asJsonArray()), is(JsonValue.of(1)));
        assertThat(jsonObject.get(JsonPointer.of("#/c%25d").asJsonArray()), is(JsonValue.of(2)));
        assertThat(jsonObject.get(JsonPointer.of("#/e%5Ef").asJsonArray()), is(JsonValue.of(3)));
        assertThat(jsonObject.get(JsonPointer.of("#/g%7Ch").asJsonArray()), is(JsonValue.of(4)));
        assertThat(jsonObject.get(JsonPointer.of("#/i%5Cj").asJsonArray()), is(JsonValue.of(5)));
        assertThat(jsonObject.get(JsonPointer.of("#/k%22l").asJsonArray()), is(JsonValue.of(6)));
        assertThat(jsonObject.get(JsonPointer.of("#/%20").asJsonArray()), is(JsonValue.of(7)));
        assertThat(jsonObject.get(JsonPointer.of("#/m~0n").asJsonArray()), is(JsonValue.of(8)));
    }

    @Test
    void decodeHex1(){
        assertThat(JsonPointer.decodeHex("%25"), is("%"));
    }

    @Test
    void decodeHex2(){
        assertThat(JsonPointer.decodeHex("%255"), is("%5"));
    }
}
