/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.uritemplate;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectBuilder;

/**
 * @author Niklas Eldberger
 */
public class UriTemplate {

    private String[] templateArray;
    private String[] templateParameterNames;

    public static UriTemplate of(String template) {
        return new UriTemplate(template);
    }

    public UriTemplate(String template){
        templateArray = template.split("/");
        templateParameterNames = createTemplateParameterNames(templateArray.clone());
    }

    private static String[] createTemplateParameterNames(String[] templateArray) {

        for (int i = 0; i < templateArray.length; i++) {

            String level = templateArray[i];
            if (level.startsWith("{") && level.endsWith("}")) {
                templateArray[i] = level.substring(1, level.length() - 1);
            } else {
                templateArray[i] = null;
            }
        }
        return templateArray;
    }

    public JsonObject pathParameters(String path){
        return pathParameters(templateArray, templateParameterNames, path.split("/"));
    }

    private JsonObject pathParameters(String[] templateArray, String[] templateParametersArray, String[] path) {
        String[] pathArray = path;

        if (templateParametersArray.length != pathArray.length) {
            return null;
        }

        JsonObjectBuilder builder = null;

        for (int i = 0; i < pathArray.length; i++) {
            String name = templateParametersArray[i];
            if (name == null){
                if (!pathArray[i].equals(templateArray[i])){
                    return null;
                }
            } else {
                if (builder == null) {
                    builder = JsonObject.EMPTY.builder();
                }
                builder.put(name, pathArray[i]);
            }
        }
        return builder.build();
    }

    public boolean semanticallyEquals(UriTemplate uriTemplate) {
        if (uriTemplate.templateArray.length != templateArray.length) {
            return false;
        }
        for (int i = 0; i < templateArray.length; i++) {
            String parameter = templateParameterNames[i];
            if (parameter == null) {
                if (!templateArray[i].equals(uriTemplate.templateArray[i])) {
                    return false;
                }
            } else {
                if (uriTemplate.templateArray[i] == null) {
                    return false;
                }
            }
        }
        return true;
    }
}
