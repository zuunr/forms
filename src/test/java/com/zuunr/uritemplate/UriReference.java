/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.uritemplate;

import com.zuunr.json.JsonValue;

/**
 * @author Niklas Eldberger
 */
public class UriReference {

    private String rid;
    private JsonValue scheme;
    private JsonValue userInfo;
    private JsonValue host;
    private JsonValue port;
    private JsonValue path;
    private JsonValue query;
    private JsonValue fragment;

    private JsonValue userInfoAndHostAndPort;
    private String pathAndQuery;

    private UriReference(String absoluteOrRelativeId) {
        this.rid = absoluteOrRelativeId;
    }

    private UriReference(String rid, String scheme, String userInfo, String host, Integer port, String path, String query, String fragment) {
        this.rid = rid;
        this.scheme = JsonValue.of(scheme);
        this.userInfo = JsonValue.of(userInfo);
        this.host = JsonValue.of(host);
        this.port = JsonValue.of(port);
        this.path = JsonValue.of(path);
        this.query = JsonValue.of(query);
        this.fragment = JsonValue.of(fragment);
    }

    private static UriReference create(String scheme, String userInfo, String host, Integer port, String path, String query, String fragment) {
        StringBuilder ridBuilder = new StringBuilder();
        if (scheme != null) {
            ridBuilder.append(scheme);
            if (host != null) {
                ridBuilder.append("://");
                if (userInfo != null) {
                    ridBuilder.append(userInfo).append("@");
                }
                ridBuilder.append(host);
                if (port != null) {
                    ridBuilder.append(":");
                    ridBuilder.append(port);
                }
            }
        }
        if (path == null) {
            throw new NullPointerException("path must not be null");
        } else {
            ridBuilder.append(path);
        }
        if (query != null) {
            ridBuilder.append("?");
            ridBuilder.append(query);
        }
        return new UriReference(ridBuilder.toString());
    }


    public static UriReference ofPath(String path) {

        String temp;

        // Make sure path starts with slash
        if (path.startsWith("/")) {
            temp = path;
        } else {
            temp = "/" + path;
        }

        // Remove ending slash
        if (temp.endsWith("/")) {
            temp.substring(0, temp.length() - 1);
        }

        return new UriReference(temp);

    }

    public String getScheme() {
        if (scheme == null) {
            scheme = JsonValue.NULL;
            for (int i = 0; i < rid.length(); i++) {
                if (rid.charAt(i) == ':') {
                    scheme = JsonValue.of(pathAndQuery = rid.substring(0, i));
                }
            }
        }
        return scheme.getString();
    }

    public String getUserInfoAndHostAndPort() {


        if (userInfoAndHostAndPort == null) {
            int end = rid.length();

            if (getScheme() == null) {
                userInfoAndHostAndPort = JsonValue.NULL;
            } else if (getScheme() != null &&
                    rid.length() > getScheme().length() + 3 &&
                    rid.charAt(getScheme().length() + 2) == ':' &&
                    rid.charAt(getScheme().length() + 2) == '/' &&
                    rid.charAt(getScheme().length() + 3) == '/') {

                for (int i = 0; i < rid.length(); i++) {
                    if (rid.charAt(i) == '/') {
                        end = i;
                        break;
                    }
                }
                userInfoAndHostAndPort = JsonValue.of(rid.substring(0, end));
            }
        }
        return userInfoAndHostAndPort.getString();
    }

    public String getPath() {
        if (path == null) {
            // parse lazily
        }
        return path.getString();

    }
}
