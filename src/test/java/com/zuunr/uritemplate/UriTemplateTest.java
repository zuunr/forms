/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.uritemplate;

import com.zuunr.json.JsonObject;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author Niklas Eldberger
 */
class UriTemplateTest {

    @Test
    void test1() {

        JsonObject expected = JsonObject.EMPTY.builder()
                .put("customer", "customerA")
                .put("department", "depB")
                .put("user", "userC")
                .build();

        assertThat(UriTemplate.of("/start/{customer}/{department}/{user}").pathParameters("/start/customerA/depB/userC"), is(expected));
    }

    @Test
    void semanticallyEquals1() {
        assertThat(UriTemplate.of("/start/{customer}/{department}/{user}").semanticallyEquals(UriTemplate.of("/start/{cust}/{dep}/{userId}")), is(true));
    }

    @Test
    void semanticallyEquals2() {
        assertThat(UriTemplate.of("/start/{customer}/department/{user}").semanticallyEquals(UriTemplate.of("/start/{cust}/{dep}/{userId}")), is(false));
    }
}
