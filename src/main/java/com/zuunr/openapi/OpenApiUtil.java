/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.openapi;

import com.zuunr.json.*;

/**
 * @author Niklas Eldberger
 */
public class OpenApiUtil {

    public JsonObject openApiParametersAsJsonObjectSchema(JsonArray openApiParameters) {

        JsonObjectBuilder propertiesBuilder = JsonObject.EMPTY.builder();
        JsonArrayBuilder requiredBuilder = JsonArray.EMPTY.builder();

        appendJsonSchemaOfParameterType("path", openApiParameters, propertiesBuilder, requiredBuilder);
        appendJsonSchemaOfParameterType("query", openApiParameters, propertiesBuilder, requiredBuilder);
        appendJsonSchemaOfParameterType("header", openApiParameters, propertiesBuilder, requiredBuilder);

        return JsonObject.EMPTY
                .put("type", "object")
                .put("properties", propertiesBuilder.build())
                .put("required", requiredBuilder.build());
    }

    private void appendJsonSchemaOfParameterType(String parameterType, JsonArray parameters, JsonObjectBuilder propertiesBuilder, JsonArrayBuilder requiredBuilder) {
        JsonObject pathParametersJsonSchema = openApiParametersAsJsonObjectSchema(parameterType, parameters);
        propertiesBuilder.put(parameterType, pathParametersJsonSchema);
        if (!pathParametersJsonSchema.get("required", JsonArray.EMPTY).getValue(JsonArray.class).isEmpty()) {
            requiredBuilder.add(parameterType);
        }
    }

    private JsonObject openApiParametersAsJsonObjectSchema(String parameterType, JsonArray parameters) {

        JsonArrayBuilder requiredBuilder = JsonArray.EMPTY.builder();
        JsonObjectBuilder propertiesBuilder = JsonObject.EMPTY.builder();
        for (JsonValue parameterJsonValue : parameters) {

            JsonObject parameter = parameterJsonValue.getValue(JsonObject.class);

            if (parameterType.equals(parameter.get("in", JsonValue.NULL).getString())) {
                String parameterName = parameter.get("name", JsonValue.NULL).getString();
                boolean requiredParam = parameter.get("required", JsonValue.FALSE).getValue(Boolean.class).booleanValue();
                if (requiredParam) {
                    requiredBuilder.add(parameterName);
                }
                JsonObject parameterSchema = parameter.get("schema", JsonObject.EMPTY).getValue(JsonObject.class);
                propertiesBuilder.put(parameterName, parameterSchema);
            }
        }
        return JsonObject.EMPTY.put("type", JsonArray.of("object")).put("required", requiredBuilder.build()).put("properties", propertiesBuilder.build());
    }
}
