/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms;

import com.zuunr.forms.formfield.Enum;
import com.zuunr.forms.formfield.*;
import com.zuunr.forms.formfield.options.ValidationStep;
import com.zuunr.json.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

/**
 * <p>This class represents the form field in its raw format without applying any default values.</p>
 *
 * @author Niklas Eldberger
 */
public abstract class AbstractValueFormat implements JsonObjectSupport {

    private static final Logger logger = LoggerFactory.getLogger(AbstractValueFormat.class);

    private ValueFormatDefaultsUtil valueFormatDefaultsUtil = new ValueFormatDefaultsUtil();

    private JsonValue me;

    private Min min;
    private ExclusiveMinimum exclusiveMinimum;
    private Max max;
    private ExclusiveMaximum exclusiveMaximum;
    private MinLength minlength;
    private MaxLength maxlength;
    private MinSize minSize;
    private MaxSize maxSize;
    private Pattern pattern;
    private Boolean mustBeNull;
    private Boolean nullable;
    private ValueFormat element;
    private Options options;
    private Type type;
    private Form form;
    private String href;
    private Form eform;
    private Enum enumeration;
    private JsonValue constant;
    private String format;
    private Boolean alwaysFails;
    private Boolean alwaysSucceeds;
    private String desc;
    private JsonValue defaultValue;


    private ValidationStep asValidationStep;
    private JsonValue explicit;
    private JsonValue compact;

    protected AbstractValueFormat(JsonValue jsonValue) {
        me = jsonValue;
    }

    protected AbstractValueFormat(JsonObject jsonObject) {
        me = jsonObject.jsonValue();
    }

    protected static JsonValue init(JsonValue source) {
        if (source.isJsonObject()) {
            return init(source.getJsonObject()).jsonValue();
        } else if(source.isBoolean()) {
            return source;
        }
        throw new UnsupportedTypeException("Only Boolean and JsonObject is supported");
    }

    protected static JsonObject init(JsonObject source) {

        JsonObject in = source;
        JsonObjectBuilder builder = JsonObject.EMPTY.builder();

        Type type = initAnyType(in, builder);

        if (type.isObject()) {
            initObject(in, builder);
        } else if (Type.INTEGER.equals(type)) {
            init(in, builder);
        } else if (Type.DECIMAL.equals(type)) {
            init(in, builder);
        } else if (type.isStringTypeOrSubtypeOfString()) {
            initString(in, builder);
        } else if (type.isArrayOrSet()) {
            initArray(in, builder);
        }
        return builder.build();
    }

    private static Type initAnyType(JsonObject in, JsonObjectBuilder builder) {
        if (in.get("desc", JsonValue.NULL).getValue(String.class) != null) {
            builder.put("desc", in.get("desc"));
        }

        if (in.get("default") != null) {
            builder.put("default", in.get("default"));
        }

        if (in.get("equals", JsonValue.NULL).as(Equals.class) != null) {
            builder.put("equals", in.get("equals"));
        }

        String href = in.get("href", JsonValue.NULL).as(String.class);
        if (href != null) {
            builder.put("href", href);
        }

        Boolean mustBeNull = in.get("mustBeNull", JsonValue.NULL).getValue(Boolean.class);

        if (mustBeNull != null) {
            builder.put("mustBeNull", mustBeNull);
        }

        Boolean nullable = in.get("nullable", JsonValue.NULL).getValue(Boolean.class);

        if (nullable != null) {
            builder.put("nullable", in.get("nullable", nullable).getValue(Boolean.class));
        }

        // Options should be validated to be ok with type? Now it is possible to have options that are impossible
        Options options = in.get("options", JsonValue.NULL).as(Options.class);
        if (options != null) {
            builder.put("options", options);
        }

        String format = in.get("typeFormat", JsonValue.NULL).getString();
        if (format != null) {
            builder.put("typeFormat", in.get("typeFormat"));
        }

        Type type = mustBeNull != null && mustBeNull ? null : in.get("type", JsonValue.NULL).as(Type.class);
        if (type == null) {
            if (in.get("form") != null) {
                logger.warn("form is not a valid value for implicit type 'string', changing type to 'object'. This bahavour is subject to change! Make sure to set type 'object' explicitly to ensure unchanged bahaviour.");
                type = Type.OBJECT; // This is for backward compatibility reasons. Should be removed.
            } else {
                type = Type.STRING; // Type is string but is implicit and should not be set explicitly on FormField
            }
        } else {
            builder.put("type", type);
        }

        if (in.get("enum", JsonValue.NULL).getValue(JsonArray.class) != null) {
            Enum enm = in.get("enum").as(Enum.class);
            builder.put("enum", enm.asJsonValue());
        }
        return type;
    }

    private static void initObject(JsonObject in, JsonObjectBuilder builder) {
        if (in.get("form", JsonValue.NULL).as(Form.class) != null) {
            builder.put("form", in.get("form"));
        }
    }

    private static void init(JsonObject in, JsonObjectBuilder builder) {
        if (in.get("max", JsonValue.NULL).getValue() != null) {
            Max max = new Max(in.get("max"));
            builder.put("max", max.asJsonValue());
        }
        if (in.get("exclusiveMaximum", JsonValue.NULL).getValue() != null) {
            ExclusiveMaximum exclusiveMaximum = new ExclusiveMaximum(in.get("exclusiveMaximum"));
            builder.put("exclusiveMaximum", exclusiveMaximum.asJsonValue());
        }
        if (in.get("min", JsonValue.NULL).getValue() != null) {
            Min min = new Min(in.get("min"));
            builder.put("min", min.asJsonValue());
        }
        if (in.get("exclusiveMinimum", JsonValue.NULL).getValue() != null) {
            ExclusiveMinimum exclusiveMinimum = new ExclusiveMinimum(in.get("exclusiveMinimum"));
            builder.put("exclusiveMinimum", exclusiveMinimum.asJsonValue());
        }
    }


    private static void initString(JsonObject in, JsonObjectBuilder builder) {
        if (in.get("maxlength", JsonValue.NULL).asInteger() != null) {
            MaxLength maxlength = new MaxLength(in.get("maxlength"));
            builder.put("maxlength", maxlength.asJsonValue());
        }
        if (in.get("minlength", JsonValue.NULL).asInteger() != null) {
            MinLength minlength = new MinLength(in.get("minlength"));
            builder.put("minlength", minlength.asJsonValue());
        }
        if (in.get("max", JsonValue.NULL).getValue(String.class) != null) {
            Max max = new Max(in.get("max"));
            builder.put("max", max.asJsonValue());
        }
        if (in.get("min", JsonValue.NULL).getValue(String.class) != null) {
            Min min = new Min(in.get("min"));
            builder.put("min", min.asJsonValue());
        }
        if (in.get("const") != null) {
            builder.put("const", in.get("const"));
        }
        if (in.get("pattern", JsonValue.NULL).getValue(String.class) != null) {
            Pattern pattern = new Pattern(in.get("pattern"));
            builder.put("pattern", pattern.asJsonValue());
        }
    }

    private static void initArray(JsonObject in, JsonObjectBuilder builder) {
        if (in.get("maxsize", JsonValue.NULL).asInteger() != null) {
            MaxSize maxSize = new MaxSize(in.get("maxsize"));
            builder.put("maxsize", maxSize.asJsonValue());
        }
        if (in.get("minsize", JsonValue.NULL).asInteger() != null) {
            MinSize minSize = new MinSize(in.get("minsize"));
            builder.put("minsize", minSize.asJsonValue());
        }
        ValueFormat element = in.get("element", JsonValue.NULL).as(ValueFormat.class);
        if (element != null) {
            builder.put("element", element);
        }
        Form eform = in.get("eform", JsonValue.NULL).as(Form.class);
        if (eform != null) {
            builder.put("eform", eform);
            if (element != null) {
                ValueFormat explicitElement = element.asExplicitValueFormat();
                if (explicitElement.form().equals(eform.asExplicitForm()) && explicitElement.type().isObject()) {
                    // All is good. eform and element have same semantics
                } else {
                    throw new InvalidFormatException("element and eform mismatch");
                }
            }
        }
    }

    public String desc() {
        if (desc == null) {
            desc = asJsonValue().get("desc", JsonValue.NULL).getString();
        }
        return desc;
    }

    public MinLength minlength() {
        if (minlength == null) {
            minlength = asJsonValue().get("minlength", JsonValue.NULL).as(MinLength.class);
        }
        return minlength;
    }

    public MaxLength maxlength() {
        if (maxlength == null) {
            maxlength = asJsonValue().get("maxlength", JsonValue.NULL).as(MaxLength.class);
        }
        return maxlength;
    }

    public MaxSize maxSize() {
        if (maxSize == null) {
            maxSize = asJsonValue().get("maxsize", JsonValue.NULL).as(MaxSize.class);
        }
        return maxSize;
    }

    public MinSize minSize() {
        if (minSize == null) {
            minSize = asJsonValue().get("minsize", JsonValue.NULL).as(MinSize.class);
        }
        return minSize;
    }

    public Boolean mustBeNull() {
        if (mustBeNull == null) {
            mustBeNull = asJsonValue().get("mustBeNull", JsonValue.NULL).getValue(Boolean.class);
        }
        return mustBeNull;
    }

    public Max max() {
        if (max == null) {
            max = asJsonValue().get("max", JsonValue.NULL).as(Max.class);
        }
        return max;
    }

    public ExclusiveMaximum exclusiveMaximum() {
        if (exclusiveMaximum == null) {
            exclusiveMaximum = asJsonValue().get("exclusiveMaximum", JsonValue.NULL).as(ExclusiveMaximum.class);
        }
        return exclusiveMaximum;
    }

    public Min min() {
        if (min == null) {
            min = asJsonValue().get("min", JsonValue.NULL).as(Min.class);
        }
        return min;
    }

    public ExclusiveMinimum exclusiveMinimum() {
        if (exclusiveMinimum == null) {
            exclusiveMinimum = asJsonValue().get("exclusiveMinimum", JsonValue.NULL).as(ExclusiveMinimum.class);
        }
        return exclusiveMinimum;
    }

    public Boolean nullable() {
        if (nullable == null) {
            nullable = asJsonValue().get("nullable", JsonValue.NULL).getValue(Boolean.class);
        }
        return nullable;
    }

    public ValueFormat element() {
        if (element == null) {
            element = asJsonValue().get("element", JsonValue.NULL).as(ValueFormat.class);
        }
        return element;
    }

    public Options options() {
        if (options == null) {
            options = asJsonValue().get("options", JsonValue.NULL).as(Options.class);
        }
        return options;
    }

    public Type type() {
        if (type == null) {
            type = asJsonValue().get("type", JsonValue.NULL).as(Type.class);
        }
        return type;
    }

    public Pattern pattern() {
        if (pattern == null) {
            pattern = asJsonValue().get("pattern", JsonValue.NULL).as(Pattern.class);
        }
        return pattern;
    }

    public Form form() {
        if (form == null) {
            form = asJsonValue().get("form", JsonValue.NULL).as(Form.class);
        }
        return form;
    }

    public String href() {
        if (href == null) {
            href = asJsonValue().get("href", JsonValue.NULL).getString();
        }
        return href;
    }

    public Form eform() {
        if (eform == null) {
            eform = asJsonValue().get("eform", JsonValue.NULL).as(Form.class);
        }
        return eform;
    }

    public Enum enumeration() {
        if (enumeration == null) {
            enumeration = asJsonValue().get("enum", JsonValue.NULL).as(Enum.class);
        }
        return enumeration;
    }

    public JsonValue constant() {
        if (constant == null) {
            constant = asJsonValue().get("const");
        }
        return constant;
    }

    public ValidationStep asValidationStep() {
        if (asValidationStep == null) {
            asValidationStep = JsonObject.EMPTY.put("format", this).as(ValidationStep.class);
        }
        return asValidationStep;
    }

    public String format() {
        if (format == null) {
            format = asJsonValue().get("typeFormat").getString();
        }
        return format;
    }

    public JsonValue defaultValue() {
        if (defaultValue == null) {
            defaultValue = asJsonValue().get("default");
        }
        return defaultValue;
    }

    public boolean alwaysFails() {
        if (alwaysFails == null) {
            alwaysFails = alwaysFails(asJsonValue().as(ValueFormat.class));
        }
        return alwaysFails;
    }

    public boolean alwaysSucceeds() {
        if (alwaysSucceeds == null) {
            alwaysSucceeds = me.isBoolean() ? me.getBoolean() : false;
        }
        return alwaysSucceeds;
    }

    protected static boolean alwaysFails(ValueFormat valueFormat) {

        if (valueFormat.asJsonValue().isBoolean()) {
            return !valueFormat.asJsonValue().getBoolean();
        }

        valueFormat = valueFormat.asExplicitValueFormat();
        if (valueFormat.enumeration() != null && valueFormat.enumeration().isEmpty()) {
            return true;
        }

        if (valueFormat.form() != null && valueFormat.form().alwaysFails()) {
            return true;
        }

        if (valueFormat.element() != null && valueFormat.element().alwaysFails()) {
            return true;
        }
        return false;
    }




    /**
     * @return where all defaults are applied
     */
    protected JsonValue asExplicit() {
        if (explicit == null) {
            if (me.isBoolean()) {
                explicit = me;
            } else if (me.isJsonObject()) {
                explicit = asExplicitJsonObject().jsonValue();
            }
        }
        return explicit;
    }

    private JsonObject asExplicitJsonObject(){
        JsonObject defaultApplied = valueFormatDefaultsUtil.putDefaultsInValueFormat(asJsonObject());
        JsonObjectBuilder explicitBuilder = defaultApplied.builder();

        if (form() != null) {
            explicitBuilder.put("form", form().asExplicitForm());
        }

        if (element() == null && eform() == null) {
            explicitBuilder.put("element", JsonObject.EMPTY.put("type", Type.UNDEFINED));
        } else if (element() != null) {
            explicitBuilder.put("element", element().asExplicitValueFormat());
            if (element().asExplicitValueFormat().type().isObject() && element().asExplicitValueFormat().form() != null) {
                explicitBuilder.put("eform", element().asExplicitValueFormat().form());
            } else {
                explicitBuilder.remove("eform");
            }
        } else if (eform() != null) {
            explicitBuilder.put("eform", eform().asExplicitForm());
            explicitBuilder.put("element", eform().asExplicitForm().asObjectValueFormat());
        }

        if (options() != null) {
            explicitBuilder.put("options", options().asExplicitOptions());
        }

        return explicitBuilder.build();

    }

    protected JsonValue asCompact() {
        if (compact == null) {
            if (me.isBoolean()) {
                compact = me;
            } else if (me.isJsonObject()){
                compact = asCompactJsonObject().jsonValue();
            } else {
                throw new UnsupportedTypeException("Only Boolean and JsonObject supported");
            }
        }
        return compact;
    }

    private JsonObject asCompactJsonObject(){
        JsonObject defaultsremoved = valueFormatDefaultsUtil.removeDefaultsOfValueFormat(asExplicit().getJsonObject());
        JsonObjectBuilder compactBuilder = defaultsremoved.builder();

        if (form() != null) {
            compactBuilder.put("form", form().asCompactForm());
        }

        if (element() != null) {
            compactBuilder.put("element", element().asCompactValueFormat());
            compactBuilder.remove("eform");
        } else if (eform() != null) {
            compactBuilder.put("eform", eform().asCompactForm());
        }

        if (options() != null) {
            compactBuilder.put("options", options().asCompactOptions());
        }

        return compactBuilder.build();
    }

    @Override
    public boolean equals(Object o) {
        return o.getClass() == getClass() && me.equals(((AbstractValueFormat)o).asJsonValue());
    }

    @Override
    public String toString() {
        return me.toString();
    }

    @Override
    public int hashCode() {
        return me.hashCode();
    }

    @Override
    public JsonValue asJsonValue() {
        return me;
    }

    @Deprecated
    @Override
    public JsonObject asJsonObject() {
        return me.getJsonObject();
    }
}
