/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.formfield;

import com.zuunr.json.*;

public final class Equals extends FormFieldMember {

    public static final Equals DEFAULT = new Equals(JsonObject.EMPTY.builder().put("paths", JsonArray.EMPTY).build());

    public Builder builder() {
        return new Builder(jsonValue.getValue(JsonObject.class));
    }

    public Equals(JsonValue source) {
        JsonObject jsonObject = JsonObject.EMPTY;

        if (source.get("paths", JsonValue.NULL).getValue(JsonArray.class) != null) {
            jsonObject.put("paths", source.get("paths", JsonValue.NULL).getValue(JsonArray.class));
        }

        this.jsonValue = jsonObject.jsonValue();
    }

    private Equals(JsonObject jsonObject) {
        this(jsonObject.jsonValue());
    }

    public static final class Builder {

        JsonObjectBuilder jsonObjectBuilder;

        private Builder(JsonObject jsonObject) {
            this.jsonObjectBuilder = jsonObject.builder();
        }

        public Equals build() {
            return new Equals(jsonObjectBuilder.build());
        }

        public Builder paths(JsonArray paths) {
            jsonObjectBuilder.put("paths", paths);
            return this;
        }
    }
}
