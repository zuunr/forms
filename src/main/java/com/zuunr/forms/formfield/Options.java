/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.formfield;

import com.zuunr.forms.formfield.options.AggregatedValidationSteps;
import com.zuunr.forms.formfield.options.Option;
import com.zuunr.forms.formfield.options.Value;
import com.zuunr.json.AbstractJsonObjectSupport;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonArrayBuilder;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectBuilder;
import com.zuunr.json.JsonValue;

/**
 * @author Niklas Eldberger
 */
public class Options extends AbstractJsonObjectSupport {

    private Value value;

    private boolean aggregatedValidationStepIsSet = false;
    private AggregatedValidationSteps aggregatedValidationSteps;
    private JsonObject perLabel;

    private Options asCompactOptions;
    private Options asExplicitOptions;

    private Options(JsonValue in) {
        super(init(in.getValue(JsonObject.class)));
    }

    public static JsonObject init(JsonObject jsonObject){

        Type type = jsonObject.get("type", JsonValue.NULL).as(Type.class);
        Value value = jsonObject.get("value", JsonValue.NULL).as(Value.class);

        JsonObjectBuilder jsonObjectBuilder = JsonObject.EMPTY.builder();

        if (type != null) {
            jsonObjectBuilder.put("type", type);
        }

        if (value != null) {
            jsonObjectBuilder.put("value", value);
        }
        return jsonObjectBuilder.build();
    }

    public Type type() {
        return asJsonObject().get("type", JsonValue.NULL).as(Type.class);
    }

    public Value value() {
        if (value == null) {
            value = asJsonObject().get("value", JsonValue.NULL).as(Value.class);
        }
        return value;
    }

    public Options asCompactOptions() {

        if (asCompactOptions == null) {
            JsonArrayBuilder jsonArrayBuilder = JsonArray.EMPTY.builder();
            for (Option option : this.value().asList()) {
                jsonArrayBuilder.add(option.asCompactOption());
            }
            asCompactOptions = builder().value(jsonArrayBuilder.build()).build();
        }
        return asCompactOptions;
    }

    private static JsonArray aggregateValidationSteps(JsonObject options) {
        JsonArray steps = null;

        for (Option option : options.get("value").asList(Option.class)) {
            if (option.validationSteps() != null) {

                if (steps == null) {
                    steps = JsonArray.EMPTY;
                }

                for (int validationStepIndex = 0; validationStepIndex < option.validationSteps().asJsonArray().size(); validationStepIndex++) {

                    String label = option.label();
                    steps = steps.put(
                            JsonArray.of(validationStepIndex, label),
                            option.validationSteps()
                                    .asList().get(validationStepIndex).asJsonObject().put("label", option.label()));
                }
                // TODO: Chack if format can be added here instead of i validationSteps directly

                if (option.format() != null) {
                    steps = steps.put(
                            JsonArray.of(option.validationSteps().asJsonArray().size(), option.label()),
                            option.format()
                                    .asValidationStep()
                                    .asJsonObject().put("label", option.label()));
                }
            }
        }
        return steps;
    }

    public AggregatedValidationSteps aggregatedValidationSteps() {
        if (!aggregatedValidationStepIsSet) {
            JsonArray steps =  aggregateValidationSteps(asJsonObject());
            if (steps == null) {
                aggregatedValidationSteps = null;
            } else {
                aggregatedValidationSteps = aggregateValidationSteps(asJsonObject()).as(AggregatedValidationSteps.class);
            }
            aggregatedValidationStepIsSet = true;
        }
        return aggregatedValidationSteps;
    }

    public Option getOption(String label) {
        return perLabel().get(label, JsonValue.NULL).as(Option.class);
    }

    public JsonObject perLabel() {
        if (perLabel == null) {
            int index = 0;
            JsonObjectBuilder perLabelBuilder = JsonObject.EMPTY.builder();
            for (Option option : value().asList()) {
                perLabelBuilder.put(option.label(), option.asJsonObject().put("index", index++));
            }
            perLabel = perLabelBuilder.build();
        }

        return perLabel;
    }

    public Options asExplicitOptions() {
        if (asExplicitOptions == null) {
            asExplicitOptions = builder().value(value().asExplicitValue()).build();
        }
        return asExplicitOptions;
    }

    public static Builder builder() {
        return new Builder(JsonObject.EMPTY);
    }

    public static final class Builder {
        protected JsonObjectBuilder jsonObjectBuilder;

        private Builder(JsonObject jsonObject) {
            this.jsonObjectBuilder = jsonObject.builder();
        }


        public Builder value(JsonArray value) {
            jsonObjectBuilder.put("value", value);
            return this;
        }

        public Builder value(Value value) {
            jsonObjectBuilder.put("value", value);
            return this;
        }

        public Options build() {
            return new Options(jsonObjectBuilder.build().jsonValue());
        }
    }
}
