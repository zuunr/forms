/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.formfield;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonValue;

public final class Type extends FormFieldMember {

    public static final JsonArray ALL_TYPES = JsonArray.of(
            "array",
            "boolean",
            "date",
            "datetime",
            "decimal",
            "integer",
            "object",
            "set",
            "string",
            "undefined"
    );


    public static final Type STRING = JsonValue.of("string").as(Type.class);
    public static final Type INTEGER = JsonValue.of("integer").as(Type.class);
    public static final Type BOOLEAN = JsonValue.of("boolean").as(Type.class);
    public static final Type DECIMAL = JsonValue.of("decimal").as(Type.class);
    public static final Type ARRAY = JsonValue.of("array").as(Type.class);
    public static final Type SET = JsonValue.of("set").as(Type.class);
    public static final Type OBJECT = JsonValue.of("object").as(Type.class);
    public static final Type DATE = JsonValue.of("date").as(Type.class);
    public static final Type DATETIME = JsonValue.of("datetime").as(Type.class);
    public static final Type UNDEFINED = JsonValue.of("undefined").as(Type.class);

    private String type;

    private Type(JsonValue jsonValue) {
        this.jsonValue = jsonValue;
        this.type = jsonValue.getValue(String.class);
    }

    public boolean isInteger() {
        return INTEGER.type.equals(type);
    }

    public boolean isString() {
        return STRING.type.equals(type);
    }

    public boolean isBoolean() {
        return BOOLEAN.type.equals(type);
    }

    public boolean isObject() {
        return OBJECT.type.equals(type);
    }

    public boolean isArray() {
        return ARRAY.type.equals(type);
    }

    public boolean isSet() {
        return SET.type.equals(type);
    }

    public boolean isArrayOrSet() {
        return ARRAY.type.equals(type) || SET.type.equals(type);
    }

    public boolean isDecimal() {
        return DECIMAL.type.equals(type);
    }

    public boolean isDate() {
        return DATE.type.equals(type);
    }

    public boolean isDatetime() {
        return DATETIME.type.equals(type);
    }

    public boolean isStringTypeOrSubtypeOfString() {
        return isString() || isDate() || isDatetime();
    }

    public boolean isUndefined() {
        return UNDEFINED.type.equals(type);
    }

    @Override
    public String toString() {
        return type;
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && getClass() == obj.getClass() && type.equals(((Type) obj).type);
    }
}
