/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.formfield;

import com.zuunr.json.*;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Niklas Eldberger
 */
public class Enum extends AbstractJsonArraySupport {

    public static final Enum EMPTY = new Enum(JsonArray.EMPTY.jsonValue());

    private HashSet<JsonValue> hashSet;


    protected Enum(JsonValue jsonValue) {
        super(jsonValue.getValue(JsonArray.class));
    }

    private Set set(){
        if (hashSet == null) {
            HashSet tmp = new HashSet<>();
            for (JsonValue jsonValue: this.asJsonArray()) {
                tmp.add(jsonValue);
            }
            hashSet = tmp;
        }
        return hashSet;
    }

    public boolean matches(JsonValue valueToTest) {
        return set().contains(valueToTest);
    }

    public Builder builder() {
        return new Builder(this.asJsonArray());
    }

    public static class Builder {
        JsonArrayBuilder jsonArrayBuilder;

        private Builder(JsonArray jsonArray) {
            jsonArrayBuilder = jsonArray.builder();
        }

        public Builder add(JsonValue enm) {
            if (enm == null) {
                throw new NullPointerException("String must not be null");
            }
            jsonArrayBuilder.add(enm);
            return this;
        }

        public Builder add(String enm) {
            return add(JsonValue.of(enm));
        }

        public Builder add(Integer enm) {
            return add(JsonValue.of(enm));
        }

        public Builder add(Long enm) {
            return add(JsonValue.of(enm));
        }

        public Builder add(Boolean enm) {
            return add(JsonValue.of(enm));
        }

        public Builder add(BigDecimal enm) {
            return add(JsonValue.of(enm));
        }

        public Builder add(JsonArray enm) {
            return add(JsonValue.of(enm));
        }

        public Builder add(JsonObject enm) {
            return add(JsonValue.of(enm));
        }

        public Enum build(){
            return new Enum(jsonArrayBuilder.build().jsonValue());
        }
    }

    public boolean isEmpty() {
        return set().isEmpty();
    }
}
