/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.formfield;

import com.zuunr.json.JsonValue;
import com.zuunr.json.JsonValueSupport;

/**
 * @author Niklas Eldberger
 */
public abstract class FormFieldMember implements JsonValueSupport {

    protected JsonValue jsonValue;


    @Override
    public JsonValue asJsonValue() {
        return jsonValue;
    }

    @Override
    public String toString() {
        return jsonValue.toString();
    }

    @Override
    public int hashCode() {
        return jsonValue.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return
                this == obj ||
                        obj != null &&
                                obj.getClass() == getClass() &&
                                super.equals(obj);
    }
}
