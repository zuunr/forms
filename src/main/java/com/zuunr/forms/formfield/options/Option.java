/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.formfield.options;

import com.zuunr.forms.FormatVerbosity;
import com.zuunr.forms.ValueFormat;
import com.zuunr.json.AbstractJsonObjectSupport;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectBuilder;
import com.zuunr.json.JsonValue;

/**
 * @author Niklas Eldberger
 */
public final class Option extends AbstractJsonObjectSupport {

    private String label;
    private ValueFormat format;
    private JsonValue value;
    private ValidationSteps validationSteps;

    private Option asCompactOption;
    private Option asExplicitOption;

    private Option(JsonValue jsonValue) {
        this(init(jsonValue.getValue(JsonObject.class)), FormatVerbosity.UNSPECIFIED);
    }

    public Option asCompactOption() {
        if (asCompactOption == null) {

            Option.Builder builder = Option.builder(label());

            if (validationSteps() != null) {
                ValidationSteps compactValidationSteps = validationSteps().asCompactValidationSteps();
                if (compactValidationSteps != null) {
                    builder.validationSteps(compactValidationSteps);
                }
            }
            if (format() != null) {
                builder.format(format().asCompactValueFormat());
            }
            if (value() != null) {
                builder.value(value());
            }
            asCompactOption = builder.buildCompact();
        }
        return asCompactOption;
    }

    public Option asExplicitOption() {
        if (asExplicitOption == null) {
            Option.Builder builder = Option.builder(label());
            if (validationSteps() != null) {
                ValidationSteps explicitValidationSteps = validationSteps().asExplicitValidationSteps();
                if (explicitValidationSteps != null) {
                    builder.validationSteps(explicitValidationSteps);
                }
            }
            if (format() != null) {
                builder.format(format().asExplicitValueFormat());
            }
            if (value() != null) {
                builder.value(value());
            }
            asExplicitOption = builder.buildExplicit();
        }
        return asExplicitOption;
    }

    public Builder builder() {
        return new Builder(asJsonObject());
    }

    private Option(JsonObject jsonObject, FormatVerbosity formatVerbosity) {
        super(init(jsonObject));
        switch (formatVerbosity) {
            case COMPACT: {
                asCompactOption = this;
                break;
            }
            case EXPLICIT: {
                asExplicitOption = this;
                break;
            }
        }
    }

    private static JsonObject init(JsonObject source) {

        JsonValue value = source.get("value");
        String label = source.get("label", JsonValue.NULL).getValue(String.class);
        ValidationSteps validationSteps = source.get("validationSteps", JsonValue.NULL).as(ValidationSteps.class);

        JsonObjectBuilder builder = JsonObject.EMPTY.builder();

        ValueFormat format = source.get("format", JsonValue.NULL).as(ValueFormat.class);

        if (format != null) {
            builder.put("format", format);
        }

        if (value != null) {
            builder.put("value", value);

        }

        if (label != null) {
            builder.put("label", label);
        }

        if (validationSteps != null) {
            builder.put("validationSteps", validationSteps);
        }

        return builder.build();
    }

    public String label() {
        if (label == null) {
            label = asJsonObject().get("label", JsonValue.NULL).getString();
        }
        return label;
    }

    public ValueFormat format() {
        if (format == null) {
            format = asJsonObject().get("format", JsonValue.NULL).as(ValueFormat.class);
        }
        return format;
    }

    public JsonValue value() {
        if (value == null) {
            value = asJsonObject().get("value");
        }
        return value;
    }

    @Override
    public boolean equals(Object object) {
        return object != null && object.getClass() == getClass() && ((Option) object).asJsonObject().equals(asJsonObject());
    }

    public ValidationSteps validationSteps() {

        if (validationSteps == null) {
            validationSteps = asJsonObject().get("validationSteps", JsonValue.NULL).as(ValidationSteps.class);
        }
        return validationSteps;
    }

    public static Builder builder(String label) {
        return new Builder(JsonObject.EMPTY).label(label);
    }

    public static final class Builder {

        JsonObjectBuilder jsonObjectBuilder;

        private Builder(JsonObject jsonObject) {
            jsonObjectBuilder = jsonObject.builder();
        }

        public Builder label(String label) {
            jsonObjectBuilder.put("label", label);
            return this;
        }

        public Builder format(ValueFormat valueFormat) {
            jsonObjectBuilder.put("format", valueFormat);
            return this;
        }

        public Builder validationSteps(ValidationSteps validationSteps) {
            jsonObjectBuilder.put("validationSteps", validationSteps);
            return this;
        }

        public Builder value(JsonValue value) {
            jsonObjectBuilder.put("value", value);
            return this;
        }

        public Option build() {
            return new Option(jsonObjectBuilder.build(), FormatVerbosity.UNSPECIFIED);
        }

        public Option buildCompact() {
            return new Option(jsonObjectBuilder.build(), FormatVerbosity.COMPACT);
        }

        public Option buildExplicit() {
            return new Option(jsonObjectBuilder.build(), FormatVerbosity.EXPLICIT);
        }
    }
}
