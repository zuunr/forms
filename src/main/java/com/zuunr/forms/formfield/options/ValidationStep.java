/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.formfield.options;

import com.zuunr.forms.FormatVerbosity;
import com.zuunr.forms.ValueFormat;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectBuilder;
import com.zuunr.json.JsonObjectSupport;
import com.zuunr.json.JsonValue;

/**
 * @author Niklas Eldberger
 */
public class ValidationStep implements JsonObjectSupport {

    private JsonObject jsonObject;
    private ValueFormat format;

    private ValidationStep asCompactValidationStep;
    private ValidationStep asExplicitValidationStep;

    private ValidationStep(JsonValue jsonValue) {
        this(jsonValue.getValue(JsonObject.class), FormatVerbosity.UNSPECIFIED);
    }

    private ValidationStep(JsonObject jsonObject, FormatVerbosity formatVerbosity) {
        init(jsonObject);
        switch (formatVerbosity){
            case COMPACT: {
                asCompactValidationStep = this;
                break;
            }
            case EXPLICIT: {
                asExplicitValidationStep = this;
                break;
            }
        }
    }

    private void init(JsonObject source) {
        format = source.get("format", JsonValue.NULL).as(ValueFormat.class);

        if (format == null) {
            throw new IllegalStateException("format is required");
        }

        jsonObject = JsonObject.EMPTY.put("format", format);
    }

    @Override
    public JsonObject asJsonObject() {
        return jsonObject;
    }

    @Override
    public JsonValue asJsonValue() {
        return jsonObject.jsonValue();
    }

    public ValueFormat format() {
        return format;
    }

    public static Builder builder(ValueFormat valueFormat) {
        return (new Builder(JsonObject.EMPTY)).format(valueFormat);
    }

    public ValidationStep asCompactValidationStep() {
        if (asCompactValidationStep == null) {
            asCompactValidationStep = new Builder(jsonObject).format(format().asCompactValueFormat()).build();
        }
        return asCompactValidationStep;
    }

    public ValidationStep asExplicitValidationStep() {
        if (asExplicitValidationStep == null) {
            asExplicitValidationStep = new Builder(jsonObject).format(format().asExplicitValueFormat()).build();
        }
        return asExplicitValidationStep;
    }


    public static final class Builder {

        JsonObjectBuilder jsonObjectBuilder;

        private Builder(JsonObject jsonObject) {
            jsonObjectBuilder = jsonObject.builder();
        }

        public Builder format(ValueFormat valueFormat){
            jsonObjectBuilder.put("format", valueFormat);
            return this;
        }

        public ValidationStep build(){
            return new ValidationStep(jsonObjectBuilder.build(), FormatVerbosity.UNSPECIFIED);
        }

        private ValidationStep buildAsCompact(){
            return new ValidationStep(jsonObjectBuilder.build(), FormatVerbosity.COMPACT);
        }

        private ValidationStep buildAsExplicit(){
            return new ValidationStep(jsonObjectBuilder.build(), FormatVerbosity.EXPLICIT);
        }
    }
}