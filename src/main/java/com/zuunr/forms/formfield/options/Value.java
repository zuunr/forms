/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.formfield.options;

import com.zuunr.forms.FormatVerbosity;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonArrayBuilder;
import com.zuunr.json.JsonArraySupport;
import com.zuunr.json.JsonValue;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Niklas Eldberger
 */
public class Value implements JsonArraySupport {

    public static final Value EMPTY = new Value(JsonArray.EMPTY.jsonValue());
    private JsonArray jsonArray;
    private HashSet<JsonValue> optionValueSet;

    private Value asCompactValue;
    private Value asExplicitValue;

    private Value(JsonValue jsonValue) {
        this(jsonValue.getValue(JsonArray.class), FormatVerbosity.UNSPECIFIED);
    }

    private Value(JsonArray jsonArray, FormatVerbosity formatVerbosity) {
        this.jsonArray = jsonArray;
        switch (formatVerbosity) {
            case COMPACT: {
                asCompactValue = this;
                break;
            }
            case EXPLICIT: {
                asExplicitValue = this;
                break;
            }
        }
    }

    public Value asCompactValue() {
        if (asCompactValue == null) {

            Builder builder = Value.EMPTY.builder();
            for (Option option : this.asList()) {
                builder.add(option.asCompactOption());
            }
            asCompactValue = builder.build();
        }
        return asCompactValue;
    }

    public Value asExplicitValue() {
        if (asExplicitValue == null) {

            Builder builder = Value.EMPTY.builder();
            for (Option option : this.asList()) {
                builder.add(option.asExplicitOption());
            }
            asExplicitValue = builder.build();
        }
        return asExplicitValue;
    }

    @Override
    public JsonArray asJsonArray() {
        return jsonArray;
    }

    @Override
    public JsonValue asJsonValue() {
        return jsonArray.jsonValue();
    }

    public List<Option> asList() {
        return jsonArray.asList(Option.class);
    }

    private Set<JsonValue> asOptionValueSet() {
        if (optionValueSet == null) {
            optionValueSet = new HashSet<>();
            for (Option option : asList()) {
                optionValueSet.add(option.value());
            }
        }
        return optionValueSet;
    }

    public boolean containsOptionValue(JsonValue optionValue) {
        return asOptionValueSet().contains(optionValue);
    }

    public Builder builder() {
        return new Builder(jsonArray);
    }

    public static final class Builder {
        private JsonArrayBuilder jsonArrayBuilder;

        private Builder(JsonArray jsonArray) {
            jsonArrayBuilder = jsonArray.builder();
        }

        public Builder add(Option option) {
            jsonArrayBuilder.add(option);
            return this;
        }

        public Value build() {
            return new Value(jsonArrayBuilder.build(), FormatVerbosity.UNSPECIFIED);
        }

        public Value buildCompact() {
            return new Value(jsonArrayBuilder.build(), FormatVerbosity.COMPACT);
        }

        public Value buildExplicit() {
            return new Value(jsonArrayBuilder.build(), FormatVerbosity.EXPLICIT);
        }
    }
}
