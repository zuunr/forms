/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.formfield.options;

import com.zuunr.forms.FormatVerbosity;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonArrayBuilder;
import com.zuunr.json.JsonArraySupport;
import com.zuunr.json.JsonValue;

import java.util.List;

/**
 * @author Niklas Eldberger
 */
public class ValidationSteps implements JsonArraySupport {

    public static final ValidationSteps EMPTY = new ValidationSteps(JsonArray.EMPTY);

    private JsonArray jsonArray;

    private ValidationSteps asCompactValidationSteps;
    private ValidationSteps asExplicitValidationSteps;

    private ValidationSteps(JsonArray jsonArray, FormatVerbosity formatVerbosity) {
        this(jsonArray);
        switch (formatVerbosity) {
            case COMPACT: {
                asCompactValidationSteps = this;
                break;
            }
            case EXPLICIT: {
                asExplicitValidationSteps = this;
                break;
            }
        }
    }

    public ValidationSteps(JsonValue in) {
        this(in.getValue(JsonArray.class));
    }

    public ValidationSteps(JsonArray in) {
        for (ValidationStep validationStep : in.asList(ValidationStep.class)) {
            // Implicit validation of each element
            validationStep.asJsonObject();
        }
        jsonArray = in;
    }

    @Override
    public JsonArray asJsonArray() {
        return jsonArray;
    }

    @Override
    public JsonValue asJsonValue() {
        return jsonArray.jsonValue();
    }

    public List<ValidationStep> asList() {
        return jsonArray.asList(ValidationStep.class);
    }

    public Builder builder() {
        return new Builder(jsonArray);
    }

    public ValidationSteps asCompactValidationSteps() {
        if (asCompactValidationSteps == null) {
            ValidationSteps.Builder builder = ValidationSteps.EMPTY.builder();
            for (ValidationStep validationStep : asList()) {
                builder.add(validationStep.asCompactValidationStep());
            }
            asCompactValidationSteps = builder.build();
        }
        return asCompactValidationSteps;
    }

    public ValidationSteps asExplicitValidationSteps() {
        if (asExplicitValidationSteps == null) {
            ValidationSteps.Builder builder = ValidationSteps.EMPTY.builder();
            for (ValidationStep validationStep : asList()) {
                builder.add(validationStep.asExplicitValidationStep());
            }
            asExplicitValidationSteps = builder.build();
        }
        return asExplicitValidationSteps;
    }

    public ValidationStep get(int stepIndex) {
        return asCompactValidationSteps().asJsonArray().get(stepIndex).as(ValidationStep.class);
    }

    public static final class Builder {
        JsonArrayBuilder jsonArrayBuilder;

        private Builder(JsonArray jsonArray) {
            jsonArrayBuilder = jsonArray.builder();
        }

        public Builder add(ValidationStep validationStep) {
            jsonArrayBuilder.add(validationStep);
            return this;
        }

        public ValidationSteps build() {
            return new ValidationSteps(jsonArrayBuilder.build(), FormatVerbosity.UNSPECIFIED);
        }

        public ValidationSteps buildCompact() {
            return new ValidationSteps(jsonArrayBuilder.build(), FormatVerbosity.COMPACT);
        }

        public ValidationSteps buildExplicit() {
            return new ValidationSteps(jsonArrayBuilder.build(), FormatVerbosity.EXPLICIT);
        }
    }
}
