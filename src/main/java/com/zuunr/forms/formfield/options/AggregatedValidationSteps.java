/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.formfield.options;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonArraySupport;
import com.zuunr.json.JsonValue;

import java.util.List;

/**
 * @author Niklas Eldberger
 */
public class AggregatedValidationSteps implements JsonArraySupport {

    private JsonArray jsonArray;

    public AggregatedValidationSteps(JsonValue in) {
        for (AggregatedValidationStep aggregatedValidationStep : in.asList(AggregatedValidationStep.class)){
            aggregatedValidationStep.asJsonObject(); // Validation of each element
        }
        jsonArray = in.getValue(JsonArray.class);
    }

    @Override
    public JsonArray asJsonArray() {
        return jsonArray;
    }

    @Override
    public JsonValue asJsonValue() {
        return jsonArray.jsonValue();
    }

    public List<AggregatedValidationStep> asList() {
        return jsonArray.asList(AggregatedValidationStep.class);
    }
}
