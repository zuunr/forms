/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.formfield;

import com.zuunr.json.JsonValue;

/**
 * @author Niklas Eldberger
 */
public class MinLength extends FormFieldMember {

    public static final MinLength DEFAULT = new MinLength(JsonValue.ZERO);

    public MinLength(JsonValue jsonValue) {
        this.jsonValue = jsonValue;
    }

    public Integer asInteger() {
        return jsonValue.asInteger();
    }
}
