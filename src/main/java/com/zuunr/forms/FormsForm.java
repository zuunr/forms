/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms;

import com.zuunr.forms.formfield.Options;
import com.zuunr.forms.formfield.Type;
import com.zuunr.forms.formfield.options.Option;
import com.zuunr.forms.formfield.options.ValidationStep;
import com.zuunr.forms.formfield.options.ValidationSteps;
import com.zuunr.forms.formfield.options.Value;
import com.zuunr.json.JsonArray;

/**
 * @author Niklas Eldberger
 */
public class FormsForm {

    private static final String FORM_URL = "https://zuunr.com/form";
    /*
    private static final String stringUrl = "https://zuunr.com/string";
    private static final String booleanUrl = "https://zuunr.com/boolean";
    private static final String objectUrl = "https://zuunr.com/object";
    private static final String arrayUrl = "https://zuunr.com/array";
    private static final String integerUrl = "https://zuunr.com/integer";
    private static final String decimalUrl = "https://zuunr.com/decimal";
    */

    private static final String STRING_OPTIONS_URL = "https://zuunr.com/formfield/string/options";
    // private static final String booleanOptionsUrl = "https://zuunr.com/formfield/boolean/options";
    private static final String OBJECT_OPTIONS_URL = "https://zuunr.com/formfield/object/options";
    private static final String ARRAY_OPTIONS_URL = "https://zuunr.com/formfield/array/options";
    // private static final String integerOptionsUrl = "https://zuunr.com/formfield/integer/options";
    // private static final String decimalOptionsUrl = "https://zuunr.com/formfield/decimal/options";

    private static final String STRING_FORM_FIELD_URL = "https://zuunr.com/formfield/string";
    // private static final String booleanFormFieldUrl = "https://zuunr.com/formfield/boolean";
    private static final String OBJECT_FORM_FIELD_URL = "https://zuunr.com/formfield/object";
    private static final String ARRAY_FORM_FIELD_URL = "https://zuunr.com/formfield/array";
    // private static final String integerFormFieldUrl = "https://zuunr.com/formfield/integer";
    // private static final String decimalFormFieldUrl = "https://zuunr.com/formfield/decimal";


    private static final Form baseOptionsForm = Form.EMPTY.builder()
            .href(STRING_OPTIONS_URL)
            .value(FormFields.EMPTY.builder()
                    .add(FormField.builder("href").build())
                    .add(FormField.builder("value")
                            .required(true)
                            .type(Type.ARRAY)
                            .eform(Form.EMPTY.builder()
                                    .value(FormFields.EMPTY.builder()
                                            .add(FormField.builder("label").required(true).build())
                                            .add(FormField.builder("value").type(Type.STRING).build())
                                            .add(FormField.builder("format")
                                                    .type(Type.OBJECT)
                                                    .form(Form.EMPTY
                                                            .builder()
                                                            .value(FormFields.EMPTY)
                                                            .build())
                                                    .build())
                                            .build())
                                    .build())
                            .build())
                    .build()
            ).build();


    private static final JsonArray PATH_TO_TYPE_FORMAT_IN_BASE_OPTION_FORM = JsonArray.of("value", 1, "eform", "value", 2, "format", "form", "value");

    // type:string

    private static final FormFields stringFormat = FormFields.EMPTY.builder()
            .add(FormField.builder("type").pattern("string").build())
            .add(FormField.builder("pattern").build())
            .add(FormField.builder("required").type(Type.BOOLEAN).build())
            .add(FormField.builder("nullable").type(Type.BOOLEAN).build())
            .add(FormField.builder("mustBeNull").type(Type.BOOLEAN).build())
            .add(FormField.builder("minlength").type(Type.INTEGER).build())
            .add(FormField.builder("maxlength").type(Type.INTEGER).build())
            .add(FormField.builder("desc").type(Type.STRING).build())
            .add(FormField.builder("enum").type(Type.ARRAY).build())
            .add(FormField.builder("const").type(Type.UNDEFINED).build())
            .build();

    private static final Form stringOptionsForm = baseOptionsForm.asJsonObject()
            .put(PATH_TO_TYPE_FORMAT_IN_BASE_OPTION_FORM, stringFormat.asJsonValue()).as(Form.class);

    private static final FormFields stringFormField = stringFormat.asJsonArray()
            .add(0, FormField.builder("name").required(true).build().asJsonValue())
            .add(FormField.builder("options").type(Type.OBJECT)
                    .form(stringOptionsForm)
                    .build().asJsonValue()).as(FormFields.class);

    // type:object

    private static final FormFields objectFormat = FormFields.EMPTY
            .builder()
            .add(FormField.builder("type").pattern("object").build())
            .add(FormField.builder("required").type(Type.BOOLEAN).build())
            .add(FormField.builder("nullable").type(Type.BOOLEAN).build())
            .add(FormField.builder("mustBeNull").type(Type.BOOLEAN))
            .add(FormField.builder("desc").type(Type.STRING))
            .add(FormField.builder("enum").type(Type.ARRAY).build())
            .add(FormField.builder("const").type(Type.UNDEFINED).build())
            .add(FormField.builder("options")
                    .type(Type.OBJECT)
                    .form(Form.EMPTY.builder()
                            .href(OBJECT_OPTIONS_URL).build()))
            .add(FormField.builder("form")
                    .type(Type.OBJECT)
                    .form(Form.EMPTY.builder().href(FORM_URL).build())).build();

    private static final Form objectOptionsForm = baseOptionsForm.asJsonObject()
            .put(PATH_TO_TYPE_FORMAT_IN_BASE_OPTION_FORM, objectFormat.asJsonValue()).as(Form.class);

    private static final FormFields objectFormField = objectFormat.asJsonArray()
            .add(0, FormField.builder("name").required(true).build().asJsonValue())
            .add(FormField.builder("options").type(Type.OBJECT)
                    .form(objectOptionsForm)
                    .build().asJsonValue()).as(FormFields.class);

    // type:array

    private static final FormFields arrayFormat = FormFields.EMPTY
            .builder()
            .add(FormField.builder("type").pattern("array|set").build())
            .add(FormField.builder("required").type(Type.BOOLEAN).build())
            .add(FormField.builder("maxsize").type(Type.INTEGER).minsize(0).build())
            .add(FormField.builder("minsize").type(Type.INTEGER).minsize(0).build())
            .add(FormField.builder("nullable").type(Type.BOOLEAN).build())
            .add(FormField.builder("mustBeNull").type(Type.BOOLEAN))
            .add(FormField.builder("desc").type(Type.STRING))
            .add(FormField.builder("eform").type(Type.OBJECT).form(Form.EMPTY.builder().href(FORM_URL).build()))
            .add(FormField.builder("enum").type(Type.ARRAY).build())
            .add(FormField.builder("const").type(Type.UNDEFINED).build())
            .add(FormField.builder("options")
                    .type(Type.OBJECT)
                    .form(Form.EMPTY.builder()
                            .href(ARRAY_OPTIONS_URL).build())).build();

    private static final Options elementOptions = Options.builder().value(Value.EMPTY.builder()
            .add(Option.builder("type:string")
                    .format(ValueFormat.EMPTY.builder()
                            .type(Type.OBJECT)
                            .form(Form.EMPTY.builder().value(stringFormat).build())
                            .build())
                    .build())
            .add(Option.builder("type:object")
                    .format(ValueFormat.EMPTY.builder()
                            .type(Type.OBJECT)
                            .form(Form.EMPTY.builder().value(objectFormat).build())
                            .build())
                    .build())
            .add(Option.builder("type:array|set")
                    .format(ValueFormat.EMPTY.builder()
                            .type(Type.OBJECT)
                            .form(Form.EMPTY.builder().value(arrayFormat).build())
                            .build())
                    .build())
            .build()).build();

    private static final Form arrayOptionsForm = baseOptionsForm.asJsonObject()
            .put(PATH_TO_TYPE_FORMAT_IN_BASE_OPTION_FORM, arrayFormat.asJsonValue()).as(Form.class).builder().href(ARRAY_OPTIONS_URL).build();

    private static final FormFields arrayFormField = FormFields.EMPTY.builder()
            .add(FormField.builder("name").required(true).build())
            .add(FormField.builder("type").pattern("array|set").build())
            .add(FormField.builder("required").type(Type.BOOLEAN).build())
            .add(FormField.builder("maxsize").type(Type.INTEGER).minsize(0).build())
            .add(FormField.builder("minsize").type(Type.INTEGER).minsize(0).build())
            .add(FormField.builder("nullable").type(Type.BOOLEAN).build())
            .add(FormField.builder("mustBeNull").type(Type.BOOLEAN))
            .add(FormField.builder("enum").type(Type.ARRAY).build())
            .add(FormField.builder("const").type(Type.UNDEFINED).build())
            .add(FormField.builder("options")
                    .type(Type.OBJECT)
                    .form(arrayOptionsForm))
            .add(FormField.builder("etype").pattern("array|boolean|date|datetime|decimal|integer|object|set|string").build())
            .add(FormField.builder("eform").type(Type.OBJECT).form(Form.EMPTY.builder().href(FORM_URL).build()))
            .add(FormField.builder("element").type(Type.OBJECT).options(elementOptions))
            .build();

    public static final Form FORM = Form.EMPTY.builder()
            .href(FORM_URL)
            .value(FormFields.EMPTY.builder()
                    .add(FormField.builder("href").type(Type.STRING))
                    .add(FormField.builder("exclusive").type(Type.BOOLEAN))
                    .add(FormField
                            .builder("value")
                            .type(Type.ARRAY)
                            .element(ValueFormat.EMPTY.builder().type(Type.OBJECT)
                                    .options(Options.builder()
                                            .value(Value.EMPTY.builder()
                                                    .add(Option.builder("type:string")
                                                            .validationSteps(ValidationSteps.EMPTY.builder()
                                                                    .add(ValidationStep.builder(ValueFormat.EMPTY.builder()
                                                                            .type(Type.OBJECT).form(Form.EMPTY.builder().exclusive(false).value(FormFields.EMPTY.builder()
                                                                                    .add(FormField.builder("type").pattern("string").required(false))
                                                                            ).build()).build()).build())
                                                                    .build())
                                                            .format(ValueFormat.EMPTY.builder()
                                                                    .href(STRING_FORM_FIELD_URL)
                                                                    .type(Type.OBJECT)
                                                                    .form(Form.EMPTY.builder()
                                                                            .value(stringFormField)
                                                                            .build())
                                                                    .build())
                                                            .build())
                                                    .add(Option.builder("type:object")
                                                            .validationSteps(ValidationSteps.EMPTY.builder()
                                                                    .add(ValidationStep.builder(ValueFormat.EMPTY.builder()
                                                                            .type(Type.OBJECT).form(Form.EMPTY.builder().exclusive(false).value(FormFields.EMPTY.builder()
                                                                                    .add(FormField.builder("type").pattern("object").required(true))
                                                                            ).build()).build()).build())
                                                                    .build())
                                                            .format(ValueFormat.EMPTY.builder()
                                                                    .type(Type.OBJECT)
                                                                    .href(OBJECT_FORM_FIELD_URL)
                                                                    .form(Form.EMPTY.builder()
                                                                            .value(objectFormField)
                                                                            .build())
                                                                    .build())
                                                            .build())
                                                    .add(Option.builder("type:array|set")
                                                            .validationSteps(ValidationSteps.EMPTY.builder()
                                                                    .add(ValidationStep.builder(ValueFormat.EMPTY.builder()
                                                                            .type(Type.OBJECT).form(Form.EMPTY.builder().exclusive(false).value(FormFields.EMPTY.builder()
                                                                                    .add(FormField.builder("type").pattern("array|set").required(true))
                                                                            ).build()).build()).build())
                                                                    .build())
                                                            .format(ValueFormat.EMPTY.builder()
                                                                    .type(Type.OBJECT)
                                                                    .href(ARRAY_FORM_FIELD_URL)
                                                                    .form(Form.EMPTY.builder()
                                                                            .value(arrayFormField)
                                                                            .build())
                                                                    .build())
                                                            .build())
                                                    .add(Option.builder("type:boolean|date|datetime|decimal|integer")
                                                            .validationSteps(ValidationSteps.EMPTY.builder()
                                                                    .add(ValidationStep.builder(ValueFormat.EMPTY.builder()
                                                                            .type(Type.OBJECT).form(Form.EMPTY.builder().exclusive(false).value(FormFields.EMPTY.builder()
                                                                                    .add(FormField.builder("type").pattern("boolean|date|datetime|decimal|integer").required(true))
                                                                            ).build()).build()).build())
                                                                    .build())
                                                            .format(ValueFormat.EMPTY.builder()
                                                                    .type(Type.OBJECT)
                                                                    .build())
                                                            .build())
                                                    .build())
                                            .build()).build()).build()).build()).build();

}
