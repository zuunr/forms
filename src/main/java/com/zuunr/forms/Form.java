/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms;

import com.zuunr.forms.formfield.Type;
import com.zuunr.json.*;

public class Form extends AbstractJsonObjectSupport {

    public static final Form EMPTY = JsonObject.EMPTY
            .put("value", JsonArray.EMPTY)
            .jsonValue()
            .as(Form.class);

    private Form asCompactForm;
    private Form asExplicitForm;
    private ValueFormat asObjectValueFormat;

    private JsonObject formFieldsByName;
    private JsonObject requiredFormFields;

    private static final JsonObject fromSource(JsonObject source) {
        JsonObject sourceJsonObject = source;
        JsonObjectBuilder builder = sourceJsonObject.builder();
        String href = source.get("href", JsonValue.NULL).as(String.class);
        if (href != null) {
            builder.put("href", href);
        }

        JsonValue exclusive = sourceJsonObject.get("exclusive");
        if (exclusive != null) {
            builder.put("exclusive", exclusive);
        }

        JsonValue value = sourceJsonObject.get("value", JsonArray.EMPTY);
        builder.put("value", value.as(FormFields.class));
        return builder.build();
    }

    public Form(JsonValue jsonValue) {
        this(fromSource(jsonValue.getValue(JsonObject.class)), FormatVerbosity.UNSPECIFIED);
    }

    private Form(JsonObject jsonObject, FormatVerbosity formatVerbosity) {
        super(jsonObject);
        switch (formatVerbosity){
            case COMPACT: {
                asCompactForm = this;
                break;
            }
            case EXPLICIT: {
                asExplicitForm = this;
                break;
            }
            default:{}
        }
    }

    public FormBuilder builder() {
        return new FormBuilder(asJsonObject());
    }

    public String href() {
        return asJsonObject().get("href", JsonValue.NULL).as(String.class);
    }

    public FormFields formFields() {
        return asJsonObject().get("value").as(FormFields.class);
    }

    public JsonArray value() {
        return asJsonObject().get("value", JsonValue.NULL).getValue(JsonArray.class);
    }

    public Boolean required() {
        return asJsonObject().get("required", JsonValue.FALSE).getValue(Boolean.class);
    }

    public Boolean mustBeNull() {
        return asJsonObject().get("mustBeNull", JsonValue.FALSE).getValue(Boolean.class);
    }

    public Boolean nullable() {
        return asJsonObject().get("nullable", JsonValue.FALSE).getValue(Boolean.class);
    }

    public Boolean exclusive() {
        return asJsonObject().get("exclusive", JsonValue.NULL).getValue(Boolean.class);
    }

    public FormField formField(String fieldName) {
        return formFieldsByName().get(fieldName, JsonValue.NULL).as(FormField.class);
    }

    public JsonObject formFieldsByName() {
        if (formFieldsByName == null) {
            JsonObjectBuilder formFieldsCacheBuilder = JsonObject.EMPTY.builder();
            for (FormField formField : formFields().asList()) {
                formFieldsCacheBuilder = formFieldsCacheBuilder.put(formField.name(), formField);
            }
            formFieldsByName = formFieldsCacheBuilder.build();
        }
        return formFieldsByName;
    }

    public JsonObject requiredFormFields() {
        if (requiredFormFields == null) {
            JsonObjectBuilder formFieldsCacheBuilder = JsonObject.EMPTY.builder();
            for (FormField formField : formFields().asList()) {
                if (formField.asExplicitFormField().required()) {
                    formFieldsCacheBuilder = formFieldsCacheBuilder.put(formField.name(), formField);
                }
            }
            requiredFormFields = formFieldsCacheBuilder.build();
        }
        return requiredFormFields;
    }

    public Form asCompactForm(){

        if (asCompactForm == null) {

            JsonObject compactForm = asJsonObject();

            if (exclusive() != null && exclusive()){
                compactForm = compactForm.remove("exclusive");
            }

            FormFields.FormFieldsBuilder builder = FormFields.EMPTY.builder();
            for (FormField formField: formFields().asList()){
                builder.add(formField.asCompactFormField());
            }

            compactForm = compactForm.put("value", builder.build());

            asCompactForm = compactForm.as(Form.class);
        }
        return asCompactForm;
    }

    public Form asExplicitForm(){
        if (asExplicitForm == null) {

            Form.FormBuilder formBuilder = builder();
            if (exclusive() == null) {
                formBuilder.exclusive(true);
            }

            FormFields.FormFieldsBuilder fieldsBuilder = FormFields.EMPTY.builder();
            for (FormField formField: asJsonObject().get("value", JsonArray.EMPTY).asList(FormField.class)) {
                fieldsBuilder.add(formField.asExplicitFormField());
            }

            asExplicitForm = formBuilder.value(fieldsBuilder.build()).buildExplicit();
        }
        return asExplicitForm;
    }

    public ValueFormat asObjectValueFormat(){
        if (asObjectValueFormat == null) {
            asObjectValueFormat = ValueFormat.EMPTY.builder().type(Type.OBJECT).form(this).build();
        }
        return asObjectValueFormat;
    }

    public boolean alwaysFails() {
        if (formFields() != null) {
            for (FormField formField: formFields().asExplicitFormFields().asList()){
                if (formField.required() && formField.schema().alwaysFails()){
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public static class FormBuilder {

        JsonObjectBuilder jsonObjectBuilder;

        private FormBuilder(JsonObject jsonObject) {
            jsonObjectBuilder = jsonObject.builder();
        }

        public FormBuilder href(String href) {
            jsonObjectBuilder.put("href", href);
            return this;
        }

        public FormBuilder value(FormFields formFields) {
            jsonObjectBuilder.put("value", formFields);
            return this;
        }

        public FormBuilder value(FormFields.FormFieldsBuilder formFieldsBuilder) {
            jsonObjectBuilder.put("value", formFieldsBuilder.build());
            return this;
        }

        public FormBuilder exclusive(Boolean exclusive) {
            jsonObjectBuilder.put("exclusive", exclusive);
            return this;
        }

        public Form build() {
            return new Form(jsonObjectBuilder.build().jsonValue());
        }

        public Form buildCompact(){
            return new Form(jsonObjectBuilder.build(), FormatVerbosity.COMPACT);
        }

        public Form buildExplicit() {
            return new Form(jsonObjectBuilder.build(), FormatVerbosity.EXPLICIT);
        }
    }
}
