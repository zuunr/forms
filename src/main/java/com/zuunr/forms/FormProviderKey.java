/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;

public class FormProviderKey {

    public final String apiName;
    public final String collectionName;
    public final FormType formType;
    public final String fromStatus;
    public final JsonValue toStatus;
    public final JsonObject data;

    public final JsonArray asJsonArray;

    private FormProviderKey(String apiName, String collectionName, FormType formType, String fromStatus, JsonValue toStatus, JsonObject data) {
        this.apiName = apiName;
        this.collectionName = collectionName;
        this.formType = formType;
        this.fromStatus = fromStatus;
        this.toStatus = toStatus;
        this.data = data;

        asJsonArray = JsonArray.of(
                apiName,
                collectionName,
                formType.name(),
                fromStatus == null ? JsonValue.NULL : fromStatus,
                toStatus == null ? JsonValue.NULL : toStatus);
    }

    public Builder builder() {
        return new Builder()
                .apiName(apiName)
                .collectionName(collectionName)
                .data(data)
                .formType(formType)
                .fromStatus(fromStatus)
                .toStatus(toStatus);
    }

    public static class Builder {

        private String apiName;
        private String collectionName;
        private FormType formType;
        private String fromStatus;
        private JsonValue toStatus;
        private JsonObject data;

        public Builder() {
        }

        public Builder data(JsonObject data) {
            this.data = data;
            return this;
        }

        public Builder formType(FormType formType) {
            this.formType = formType;
            return this;
        }

        public Builder fromStatus(String fromStatus) {
            this.fromStatus = fromStatus;
            return this;
        }

        public Builder toStatus(JsonValue toStatus) {
            this.toStatus = toStatus;
            return this;
        }

        public Builder apiName(String apiName) {
            this.apiName = apiName;
            return this;
        }

        public Builder collectionName(String collectionName) {
            this.collectionName = collectionName;
            return this;
        }

        public FormProviderKey build() {
            return new FormProviderKey(apiName, collectionName, formType, fromStatus, toStatus, data);
        }

    }

    public enum FormType {
        REQUEST_BODY(false, JsonArray.of("writeItem", "validation", "requestBody", "schema")),
        RESOURCE_BODY(false, JsonArray.of("writeItem", "validation", "resourceState", "schema")),
        RESOURCE_BODY_RESTRICTED(false, null), // Configured extra validations which are done before RESOURCE_BODY is validated
        ITEM_QUERY_PARAMS(true, JsonArray.of("queryItem", "requestValidation", "request", "schema")),
        COLLECTION_QUERY_PARAMS(true, JsonArray.of("queryItem", "requestValidation", "request", "schema")),
        RESOURCE_MODEL(false, null);

        private boolean isQueryParams;
        private JsonArray collectionValidationProperty;

        private FormType(boolean isQueryParams, JsonArray collectionValidationProperty) {
            this.isQueryParams = isQueryParams;
            this.collectionValidationProperty = collectionValidationProperty;
        }

        public boolean isQueryParams() {
            return isQueryParams;
        }

        public JsonArray getCollectionValidationProperty() {
            return collectionValidationProperty;
        }
    }
}
