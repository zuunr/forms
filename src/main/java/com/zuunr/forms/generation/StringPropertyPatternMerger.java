/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.generation;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectBuilder;
import com.zuunr.json.JsonValue;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * @author Niklas Eldberger
 */
public class StringPropertyPatternMerger {

    public static final String ANY_STRING_PATTERN = "[\\s\\S]*";

    public String unionOf(String pattern1, String pattern2) {
        if (pattern1 == null || pattern2 == null) {
            return null;
        }
        return unionOf(JsonValue.of(pattern1), JsonValue.of(pattern2)).getString();
    }

    public JsonValue unionOf(JsonValue pattern1, JsonValue pattern2) {
        if (pattern1 == null || pattern2 == null) {
            return null;
        }
        if (semanticallyEqual(pattern1.getString(), pattern2.getString())) {
            return pattern1.getString().length() < pattern2.getString().length() ? pattern1 : pattern2;
        }

        JsonValue commaSeparatedPattern = unionOfCommaSeparatedPatterns(pattern1, pattern2);

        if (commaSeparatedPattern != null) {
            return commaSeparatedPattern;
        } else {
            JsonObject subdomains = unionSubsetsOf(pattern1);
            subdomains = subdomains.putAll(unionSubsetsOf(pattern2));
            if (subdomains.containsKey(ANY_STRING_PATTERN)) {
                return JsonValue.NULL;
            }
            return JsonValue.of(unionPatternOf(subdomains));
        }
    }

    private JsonValue unionOfCommaSeparatedPatterns(JsonValue pattern1, JsonValue pattern2) {
        if (!pattern1.getString().contains(",") && !pattern2.getString().contains(",")) {
            return null;
        }
        JsonObject subsets1 = unionSubsetsOfCommaSeparatedPattern(pattern1.getString());
        JsonObject subsets2 = unionSubsetsOfCommaSeparatedPattern(pattern2.getString());
        if (subsets1.isEmpty() || subsets2.isEmpty()) {
            return null;
        }
        JsonObject allSubsets = subsets1.putAll(subsets2);
        return JsonValue.of(commaSeparatedPatternOf(allSubsets.values()));
    }

    public String intersectionOf(String pattern1, String pattern2) {
        return intersectionOf(JsonValue.of(pattern1), JsonValue.of(pattern2)).getString();
    }

    public JsonValue intersectionOf(JsonValue pattern1, JsonValue pattern2) {
        JsonObject intersectionSet = intersectionSubsetsOf(pattern1);
        intersectionSet = intersectionSet.putAll(intersectionSubsetsOf(pattern2));
        JsonObject simplifiedIntersection = simplifyIntersection(intersectionSet);
        return JsonValue.of(intersectionPatternOf(simplifiedIntersection));
    }

    protected boolean semanticallyEqual(String pattern1, String pattern2) {
        return pattern1.equals(pattern2);
    }

    protected String normalizePattern(String pattern) {

        JsonObject intersectionSubsets = intersectionSubsetsOf(JsonValue.of(pattern));
        JsonObjectBuilder normalizedIntersectionSubsets = JsonObject.EMPTY.builder();
        for (JsonValue intersectionSubset : intersectionSubsets.values()) {
            JsonObject unionSubsets = unionSubsetsOf(intersectionSubset);
            String normalizedUnionSubsets = normalizeUnionSubsets(unionSubsets);
            normalizedIntersectionSubsets.put(normalizedUnionSubsets, normalizedUnionSubsets);
        }
        return intersectionPatternOf(normalizedIntersectionSubsets.build());
    }

    protected String normalizeUnionSubsets(JsonObject unionSubsets) {
        JsonObjectBuilder builder = JsonObject.EMPTY.builder();
        for (JsonValue unionSubsetPattern : unionSubsets.values()) {
            JsonValue normalizedUnionSubsetPattern = normalizeUnionSubsetPattern(unionSubsetPattern);
            builder.put(normalizedUnionSubsetPattern.getString(), normalizedUnionSubsetPattern);
        }
        return unionPatternOf(builder.build());
    }

    protected JsonValue normalizeUnionSubsetPattern(JsonValue unionSubsetPattern) {
        String pattern = unionSubsetPattern.getString();
        int start = 0;
        int end = pattern.length();
        if (pattern.startsWith("^")) {
            start = 1;
        }
        if (pattern.charAt(pattern.length() - 1) == '$') {
            end = pattern.length() - 1;
        }
        return JsonValue.of(pattern.substring(start, end));
    }

    private String intersectionPatternOf(JsonObject intersectionSet) {

        if (intersectionSet.size() == 1) {
            return intersectionSet.values().get(0).getString();
        }

        StringBuilder builder = null;
        for (JsonValue subsetPattern : intersectionSet.keys().sort()) {
            if (builder == null) {
                builder = new StringBuilder("(?=");
            } else {
                builder.append(")(?=");
            }
            builder.append(subsetPattern.getString());
        }
        return builder == null ? null : builder.append(")").toString();
    }

    private JsonObject simplifyIntersection(JsonObject intersectionSet) {

        JsonObjectBuilder simplified = JsonObject.EMPTY.builder();
        for (JsonValue patternJsonValue : intersectionSet.keys()) {
            if (!patternIsRedundant(patternJsonValue, intersectionSet.remove(patternJsonValue.getString()))) {
                simplified.put(patternJsonValue.getString(), patternJsonValue);
            }
        }
        return simplified.build();
    }

    private boolean patternIsRedundant(JsonValue pattern, JsonObject intersectionSet) {
        JsonObject patternSubsets = unionSubsetsOf(pattern);

        for (JsonValue patternSubset : patternSubsets.values()) {
            if (intersectionSet.containsKey(patternSubset.getString())) {
                return true;
            }
        }
        return false;
    }

    protected String unionPatternOf(JsonObject subsetPattern) {
        StringBuilder patternBuilder = null;
        for (JsonValue pattern : subsetPattern.values().sort()) {
            if (patternBuilder == null) {
                patternBuilder = new StringBuilder();
            } else {
                patternBuilder.append("|");
            }
            patternBuilder.append(pattern.getString());
        }
        return patternBuilder == null ? null : patternBuilder.toString();
    }

    protected JsonObject unionSubsetsOf(JsonValue pattern) {
        return unionSubsetsOf(pattern.getString());
    }

    protected JsonObject unionSubsetsOf(String pattern) {
        Deque<Character> charStack = new ArrayDeque<>();

        int start = 0;
        JsonObjectBuilder builder = JsonObject.EMPTY.builder();
        int i = 0;
        boolean oneCharEscape = false;
        boolean blockEscape = false;


        try {
            for (; i < pattern.length(); i++) {
                char currentChar = pattern.charAt(i);

                if (oneCharEscape) {
                    oneCharEscape = false;
                } else if (blockEscape) {
                    if (currentChar == ']') {
                        pushOrPopStack(currentChar, charStack);
                        blockEscape = false;
                    }
                } else if (isSubsetDelimiter(currentChar, charStack)) {
                    addSubset(pattern, start, i, builder);
                    start = i + 1;
                } else {
                    pushOrPopStack(currentChar, charStack);
                    blockEscape = currentChar == '[';
                    oneCharEscape = currentChar == '\\';
                }
            }
        } catch (NaiveRegexParsingException e) {
            return JsonObject.EMPTY.put(pattern, pattern);
        }

        if (charStack.isEmpty()) {
            String substring = pattern.substring(start, i);
            return builder.put(substring, substring).build();
        } else {
            return JsonObject.EMPTY.put(pattern, pattern);
        }
    }

    private void addSubset(String pattern, int start, int end, JsonObjectBuilder builder) {
        String substring = pattern.substring(start, end);
        builder.put(substring, substring);
    }

    private boolean isSubsetDelimiter(char currentChar, Deque<Character> charStack) {
        return currentChar == '|' && charStack.isEmpty();
    }

    private void pushOrPopStack(char currentChar, Deque<Character> charStack) throws NaiveRegexParsingException {
        if (currentChar == ']') {
            if (!charStack.isEmpty() && charStack.peek() == '[') {
                charStack.pop();
            } else {
                throw new NaiveRegexParsingException();
            }
        } else if (currentChar == '[' || currentChar == '(' || currentChar == '{') {
            charStack.push(currentChar);
        } else if (currentChar == ')') {
            if (!charStack.isEmpty() && charStack.peek() == '(') {
                charStack.pop();
            } else {
                throw new NaiveRegexParsingException();
            }
        } else if (currentChar == '}') {
            if (!charStack.isEmpty() && charStack.peek() == '{') {
                charStack.pop();
            } else {
                throw new NaiveRegexParsingException();
            }
        }
    }

    protected JsonObject intersectionSubsetsOf(JsonValue pattern) {
        String[] intersectionSubsets = null;
        if (pattern.getString().startsWith("(?=") && pattern.getString().endsWith(")")) {
            intersectionSubsets = pattern.getString().substring(3, pattern.getString().length() - 1).split("[)][(][?][=]");
        } else {
            intersectionSubsets = new String[1];
            intersectionSubsets[0] = pattern.getString();
        }

        JsonObjectBuilder builder = JsonObject.EMPTY.builder();
        for (String intersectionSubsetPattern : intersectionSubsets) {
            builder.put(intersectionSubsetPattern, intersectionSubsetPattern);
        }
        return builder.build();
    }

    /**
     * Returns the union subset if regex is defining explicit strings that may be comma separated. Eg. (BREAD|BUTTER)(,BREAD|,BUTTER)*
     * Else returns null
     *
     * @param pattern
     * @return
     */
    protected JsonObject unionSubsetsOfCommaSeparatedPattern(String pattern) {

        String[] firstAndRest = pattern.split("[)][(]");

        String first = null;
        if (firstAndRest.length == 1) {
            first = firstAndRest[0];
        } else if (firstAndRest.length == 2) {
            first = firstAndRest[0].substring(1); // No parenteces after this

        }

        if (first != null) {
            JsonObject firstUnionSubSet = unionSubsetsOf(first);
            String commaSeparatedPatternCreatedFromFirst = commaSeparatedPatternOf(firstUnionSubSet.values());
            if (commaSeparatedPatternCreatedFromFirst != null && commaSeparatedPatternCreatedFromFirst.equals(pattern)) {
                return firstUnionSubSet;
            }
        }


        return JsonObject.EMPTY;
    }

    public String commaSeparatedPatternOf(JsonArray unionSubSet) {

        if (unionSubSet.isEmpty()) {
            return null;
        } else if (unionSubSet.size() == 1) {
            return unionSubSet.get(0).getString();
        }
        StringBuilder first = null; // friends.1.team,team -> (friends[.]1[.]team|team)(,
        StringBuilder rest = null;
        for (JsonValue subset : unionSubSet.sort()) {
            if (first == null) {
                first = new StringBuilder("(");
            } else {
                first.append("|");
            }
            first.append(subset.getString());
            if (rest == null) {
                rest = new StringBuilder("(,");
            } else {
                rest.append("|,");
            }
            rest.append(subset.getString());

        }

        if (first == null) {
            // FIXME Should first be nullable?
            throw new IllegalStateException("first is null");
        }

        return first.append(")").append(rest.append(")*").toString()).toString();
    }


    private class NaiveRegexParsingException extends Exception {

        private static final long serialVersionUID = 1L;
    }
}
