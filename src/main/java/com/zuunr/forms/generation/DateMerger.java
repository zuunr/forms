/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.generation;

import com.zuunr.forms.FormField;
import com.zuunr.forms.formfield.Max;
import com.zuunr.forms.formfield.Min;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectMerger;
import com.zuunr.json.JsonValue;

/**
 * @author Niklas Eldberger
 */
public class DateMerger {
    private static final JsonObjectMerger jsonObjectMerger = new JsonObjectMerger();
    private static final PatternMerger patternMerger = new PatternMerger();

    private static final MinMerger minMerger = new MinMerger();
    private static final MaxMerger maxMerger = new MaxMerger();

    public JsonObject mergeDate(FormField formField1, FormField formField2, JsonObject formFieldSoFar, MergeStrategy mergeStrategy) {

        JsonObject formFieldJsonObject1 = formField1.asJsonObject();
        JsonObject formFieldJsonObject2 = formField2.asJsonObject();

        JsonObject mergedFormField = formFieldSoFar;
        if (mergeStrategy.patchFormField) {
            mergedFormField = jsonObjectMerger.merge(formFieldJsonObject1, formFieldJsonObject2);
        } else if (mergeStrategy == MergeStrategy.SOFTEN) {

            Boolean formField1mustBeNull = formFieldJsonObject1.get("mustBeNull", JsonValue.FALSE).getValue(Boolean.class);
            Boolean formField2mustBeNull = formFieldJsonObject2.get("mustBeNull", JsonValue.FALSE).getValue(Boolean.class);

            boolean restrictionApplicable = !(formField1mustBeNull && formField2mustBeNull);

            if (restrictionApplicable) {
                Max max = maxMerger.soften(formField1, formField2);
                if (max != null) {
                    mergedFormField = mergedFormField.put("max", max);
                }

                Min min = minMerger.soften(formField1, formField2);
                if (min != null) {
                    mergedFormField = mergedFormField.put("min", min);
                }
            }
        } else if (mergeStrategy.hardenFormField) {

            Boolean formField1mustBeNull = formFieldJsonObject1.get("mustBeNull", JsonValue.FALSE).getValue(Boolean.class);
            Boolean formField2mustBeNull = formFieldJsonObject2.get("mustBeNull", JsonValue.FALSE).getValue(Boolean.class);

            boolean restrictionApplicable = !(formField1mustBeNull && formField2mustBeNull);

            if (restrictionApplicable) {
                Max max = maxMerger.harden(formField1, formField2);
                if (max != null) {
                    mergedFormField = mergedFormField.put("max", max);
                }

                Min min = minMerger.harden(formField1, formField2);
                if (min != null) {
                    mergedFormField = mergedFormField.put("min", min);
                }
            }
        } else {
            throw new RuntimeException("Merge strategy is not implemented yet");
        }

        mergedFormField = patternMerger.mergePattern(formField1, formField2, mergeStrategy, mergedFormField);

        return mergedFormField;
    }
}
