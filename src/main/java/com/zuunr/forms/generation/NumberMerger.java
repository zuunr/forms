/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.generation;

import com.zuunr.forms.FormField;
import com.zuunr.forms.formfield.Max;
import com.zuunr.forms.formfield.Min;
import com.zuunr.json.JsonObject;

/**
 * @author Niklas Eldberger
 */
public class NumberMerger {

    MaxMerger maxMerger = new MaxMerger();
    MinMerger minMerger = new MinMerger();

    public JsonObject merge(FormField formField1, FormField formField2, JsonObject result, MergeStrategy mergeStrategy) {

        if (mergeStrategy.softenFormField) {

            Max max = maxMerger.soften(formField1, formField2);
            if (max != null) {
                result = result.put("max", max);
            }

            Min min = minMerger.soften(formField1, formField2);
            if (min != null) {
                result = result.put("min", min);
            }
        } else if (mergeStrategy.hardenFormField) {

            Max max = maxMerger.harden(formField1, formField2);
            if (max != null) {
                result = result.put("max", max);
            }

            Min min = minMerger.harden(formField1, formField2);
            if (min != null) {
                result = result.put("min", min);
            }
        } else {
            throw new RuntimeException("soften or harden required");
        }

        return result;
    }
}
