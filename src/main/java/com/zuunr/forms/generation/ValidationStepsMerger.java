/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.generation;

import com.zuunr.forms.ValueFormat;
import com.zuunr.forms.formfield.options.ValidationStep;
import com.zuunr.forms.formfield.options.ValidationSteps;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonArrayBuilder;

import java.util.Iterator;

/**
 * @author Niklas Eldberger
 */
public class ValidationStepsMerger {

    private FormMerger formMerger = new FormMerger();

    public ValidationSteps merge(ValidationSteps validationSteps1, ValidationSteps validationSteps2, MergeStrategy mergeStrategy) {
        if (MergeStrategy.SOFTEN == mergeStrategy) {
            return soften(validationSteps1, validationSteps2);
        }
        throw new RuntimeException("Only soften is supported");
    }

    private ValidationSteps soften(ValidationSteps validationSteps1, ValidationSteps validationSteps2) {

        validationSteps2 = validationSteps2 == null ? ValidationSteps.EMPTY : validationSteps2;
        validationSteps1 = validationSteps1 == null ? ValidationSteps.EMPTY : validationSteps1;

        if (validationSteps1.asJsonArray().size() < validationSteps2.asJsonArray().size()) {
            validationSteps1 = validationSteps1.asJsonArray().builder()
                    .addAll(validationSteps2.asJsonArray().subArray(validationSteps1.asJsonArray().size()))
                    .build()
                    .as(ValidationSteps.class);
        }

        JsonArrayBuilder builder = JsonArray.EMPTY.builder();
        Iterator<ValidationStep> validationSteps2Iter = validationSteps2.asList().iterator();

        for (ValidationStep validationStep1: validationSteps1.asList()) {
            ValidationStep validationStep2 = validationSteps2Iter.hasNext() ? validationSteps2Iter.next() : ValidationStep.builder(ValueFormat.EMPTY).build();
            ValidationStep mergedStep = soften(validationStep1, validationStep2);
            builder.add(mergedStep);
        }
        ValidationSteps result = builder.build().as(ValidationSteps.class);
        return result;
    }

    private ValidationStep soften(ValidationStep validationStep1, ValidationStep validationStep2) {
        ValidationStep result = ValidationStep.builder(
                formMerger.unionOf(
                        validationStep1.format().form(),
                        validationStep2.format().form())
                        .asObjectValueFormat())
                .build();
        return result;
    }
}
