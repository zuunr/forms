/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.generation;

import com.zuunr.forms.formfield.Max;
import com.zuunr.forms.formfield.MaxSize;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.JsonValueUtil;

/**
 * @author Niklas Eldberger
 */
public class MaximumMerger {

    private JsonValueUtil jsonValueUtil = new JsonValueUtil();

    public JsonObject mergeMaximum(String maximumName, JsonObject formField1, JsonObject formField2, JsonObject result, MergeStrategy mergeStrategy) {
        Boolean formField1mustBeNull = formField1.get("mustBeNull", JsonValue.FALSE).getValue(Boolean.class);
        Boolean formField2mustBeNull = formField2.get("mustBeNull", JsonValue.FALSE).getValue(Boolean.class);

        boolean restrictionApplicable = !(formField1mustBeNull && formField2mustBeNull);

        if (restrictionApplicable) {
            // max
            Integer formField1max = formField1.get(maximumName, JsonValue.NULL).asInteger();
            Integer formField2max = formField2.get(maximumName, JsonValue.NULL).asInteger();


            if (formField1max == null && !formField1mustBeNull || formField2max == null && !formField2mustBeNull) {
                result = result.remove(maximumName);
            } else {
                if (formField1max == null) {
                    result = result.put(maximumName, formField2max);
                } else if (formField2max == null) {
                    result = result.put(maximumName, formField1max);
                } else if (formField1max > formField2max) {
                    result = result.put(maximumName, formField1max);
                } else {
                    result = result.put(maximumName, formField2max);
                }
            }
        }
        return result;
    }


    public MaxSize intersect(MaxSize maxSize1, MaxSize maxSize2) {
        if (maxSize1 == null) {
            return maxSize2;
        } else if (maxSize2 == null) {
            return maxSize1;
        }
        JsonValue result = intersect(maxSize1.asJsonValue(), maxSize2.asJsonValue());
        return result == null? null : result.as(MaxSize.class);
    }

    public Max intersect(Max max1, Max max2) {
        if (max1 == null) {
            return max2;
        } else if (max2 == null) {
            return max1;
        }
        JsonValue result = intersect(max1.asJsonValue(), max2.asJsonValue());
        return result == null? null : result.as(Max.class);
    }

    public JsonValue intersect(JsonValue max1, JsonValue max2) {
        if (max1 == null) {
            return max2;
        } else if (max2 == null) {
            return max1;
        }

        if (max1.equals(JsonValue.NULL) || max2.equals(JsonValue.NULL)) {
            throw new NullPointerException("JsonValue.NULL is not allowed");
        }

        return jsonValueUtil.minOf(max1, max2);
    }
}
