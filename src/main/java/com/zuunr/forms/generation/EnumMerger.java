/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.generation;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonArrayBuilder;
import com.zuunr.json.JsonValue;

/**
 * @author Niklas Eldberger
 */
public class EnumMerger {

    /**
     * @param enum1
     * @param enum2 order of elements in enum2 is kept
     * @return
     */
    public JsonArray intersectionOf(JsonArray enum1, JsonArray enum2) {
        JsonArrayBuilder intersection = JsonArray.EMPTY.builder();
        for (JsonValue enm : enum2) {
            if (enum1.contains(enm)) {
                intersection.add(enm);
            }
        }
        return intersection.build().sort();
    }

    /**
     * @param enum1
     * @param enum2
     * @return
     */
    public JsonArray unionOf(JsonArray enum1, JsonArray enum2) {

        JsonArrayBuilder union = enum2.builder();
        for (JsonValue enm : enum1) {
            if (!enum2.contains(enm)) {
                union.add(enm);
            }
        }
        return union.build().sort();
    }
}
