/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.generation;

import com.zuunr.forms.Form;
import com.zuunr.forms.FormField;
import com.zuunr.forms.formfield.Options;
import com.zuunr.forms.formfield.options.Option;
import com.zuunr.forms.formfield.options.ValidationSteps;
import com.zuunr.forms.formfield.options.Value;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonArrayBuilder;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;

/**
 * @author Niklas Eldberger
 */
public class OptionsMerger {

    private static FormMerger formMerger = new FormMerger();
    private static ValidationStepsMerger validationStepsMerger = new ValidationStepsMerger();

    public JsonObject merge(FormField formField1, FormField formField2, MergeStrategy mergeStrategy, JsonObject result) {

        Options options1 = formField1.schema().options() == null ? formField2.schema().options() : formField1.schema().options();
        Options options2 = formField2.schema().options() == null ? formField1.schema().options() : formField2.schema().options();

        if (options1 == null || options2 == null) {
            return result;
        }

        JsonObject perOptions1Label = JsonObject.EMPTY;
        int options1Index = 0;
        for (JsonObject option1 : options1.value().asJsonArray().asList(JsonObject.class)) {
            perOptions1Label = perOptions1Label.put(option1.get("label").getString(), option1.put("prio", 2).put("index", options1Index));
        }

        JsonObject resultingOptionsPerLabel = perOptions1Label;

        int options2Index = 0;
        for (JsonObject option2 : options2.value().asJsonArray().asList(JsonObject.class)) {
            String label2 = option2.get("label").getString();
            JsonObject option1 = options1.perLabel().get(label2, JsonValue.NULL).getValue(JsonObject.class);

            if (option1 == null) {
                resultingOptionsPerLabel = resultingOptionsPerLabel.put(label2, option2
                        .put("prio", 1).put("index", options2Index));
            } else {
                resultingOptionsPerLabel = resultingOptionsPerLabel.put(
                        labelOf(option2),
                        softenOption(option1.as(Option.class), option2.as(Option.class))
                                .asJsonObject().put("prio", 1).put("index", options2Index)
                );
            }
            options2Index++;
        }

        JsonArray array = resultingOptionsPerLabel.values().sort((JsonValue a, JsonValue b) -> {
            Integer prioA = a.get("prio").asInteger();
            Integer prioB = b.get("prio").asInteger();

            int c = prioA.compareTo(prioB);

            if (c == 0) {
                Integer inta = ((JsonValue) a).get("index").asInteger();
                Integer intb = ((JsonValue) b).get("index").asInteger();

                c = inta.compareTo(intb);
            }
            return c;
        });

        JsonArrayBuilder washedFromPrioAndIndex = JsonArray.EMPTY.builder();
        for (JsonObject option : array.asList(JsonObject.class)) {

            JsonObject optionWashed = option.remove("index").remove("prio");
            washedFromPrioAndIndex.add(optionWashed);
        }

        JsonArray washedFromPrioAndIndexArray = washedFromPrioAndIndex.build();

        result = result.put("options", options2.asJsonObject().put("value", washedFromPrioAndIndexArray));

        return result;
    }

    private Option softenOption(Option option1, Option option2) {

        Option result;

        if (option1 == null) {
            result = option2;
        } else if (option2 == null) {
            result = option1;
        } else if (option1.format() != null && option2.format() == null) {
            result = option1;
        } else if (option2.format() != null && option1.format() == null) {
            result = option2;
        } else if (option1.format() != null && option2.format() != null) {

            Form elementForm1 = option1.format().asFormatOfTypeObjectWithElementField().form();
            Form elementForm2 = option2.format().asFormatOfTypeObjectWithElementField().form();

            Form mergedElementForm = formMerger.mergeForms(elementForm1, elementForm2, MergeStrategy.SOFTEN);

            result = option2.asJsonObject()
                    .put("format", mergedElementForm.formField("element").asJsonObject().remove("name")).as(Option.class);
        } else if (option1.validationSteps() != null && option2.validationSteps() != null) {

            ValidationSteps mergedValidationSteps = validationStepsMerger.merge(option1.validationSteps(), option2.validationSteps(), MergeStrategy.SOFTEN);

            result = option2.asJsonObject()
                    .put("validationSteps", mergedValidationSteps).as(Option.class);
        } else if (option1.value().equals(option2.value())) {
            result = option2;
        } else {
            throw new RuntimeException("format or value must be set on all options");
        }
        return result;
    }

    private String labelOf(JsonObject option) {
        return option.get("label").getValue(String.class);
    }

    public JsonObject soften(FormField formField1, FormField formField2, JsonObject result) {
        return merge(formField1, formField2, MergeStrategy.SOFTEN, result);
    }


    public JsonObject harden(FormField formField1, FormField formField2, JsonObject result) {
        Options options1 = formField1.schema().options();
        Options options2 = formField2.schema().options();

        if (options1 == null && options2 != null) {
            result = result.put("options", options2);
        } else if (options2 == null && options1 != null) {
            result = result.put("options", options1);
        } else if (options1 != null && options2 != null) {
            Value.Builder valueBuilder = Value.EMPTY.builder();

            for (Option option2 : formField2.schema().options().value().asList()) {

                boolean duplicate = false;
                for (Option option1 : formField1.schema().options().value().asList()) {
                    if (option1.equals(option2)) {
                        duplicate = true;
                        break;
                    }
                }

                if (duplicate) {
                    valueBuilder.add(option2);
                }
            }
            result = result.put("options", JsonObject.EMPTY.put("value", valueBuilder.build()));
        }
        return result;
    }
}
