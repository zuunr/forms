/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.generation;

import com.zuunr.forms.FormField;
import com.zuunr.forms.formfield.Enum;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectMerger;

/**
 * @author Niklas Eldberger
 */
public class FieldMerger {

    private static final JsonObjectMerger jsonObjectMerger = new JsonObjectMerger();
    private static final ObjectMerger objectMerger = new ObjectMerger();
    private static final StringMerger stringMerger = new StringMerger();
    private static final ArrayMerger arrayMerger = new ArrayMerger();
    private static final EqualsMerger equalsMerger = new EqualsMerger();
    private static final TypeMerger typeMerger = new TypeMerger();
    private static final IntegerMerger integerMerger = new IntegerMerger();
    private static final DecimalMerger decimalMerger = new DecimalMerger();
    private static final DateMerger dateMerger = new DateMerger();
    private static final OptionsMerger optionsMerger = new OptionsMerger();
    private static final EnumMerger enumMerger = new EnumMerger();

    public FormField mergeFormField(FormField formField1, FormField formField2, MergeStrategy mergeStrategy) {

        FormField result;
        if (mergeStrategy.hardenFormField) {
            if (formField1 == null) {
                result = formField2;
            } else if (formField2 == null) {
                result = formField1;
            } else {
                JsonObject mergedFormField = mergeFormFieldInternal(formField1, formField2, mergeStrategy);
                result = mergedFormField.as(FormField.class).builder()
                        .required((
                                formField1 != null && formField1.required() != null && formField1.required()) ||
                                (formField2 != null && formField2.required() != null && formField2.required())
                        ).build();
            }
        } else if (mergeStrategy.patchFormField) {
            if (formField1 == null) {
                result = null;
            } else if (formField2 == null) {
                result = formField1;
            } else {
                result = mergeFormFieldInternal(formField1, formField2, mergeStrategy).as(FormField.class).builder().type(formField1.asExplicitFormField().schema().type()).required(formField1.required() != null && formField1.required()).build();
            }
        } else if (mergeStrategy == MergeStrategy.SOFTEN) {
            Boolean required = formField1 != null && formField2 != null && formField1.required() != null && formField1.required() && formField2.required() != null && formField2.required();
            JsonObject mergedFormField = mergeFormFieldInternal(
                    formField1,
                    formField2,
                    mergeStrategy);
            result = mergedFormField.as(FormField.class).builder().required(required).build();
        } else {
            throw new RuntimeException("Only HARDEN, PATCH and SOFTEN is supported");
        }
        return result;
    }

    public FormField unionOf(FormField formField1, FormField formField2) {

        FormField result;
        Boolean required = formField1 != null && formField2 != null && formField1.required() != null && formField1.required() && formField2.required() != null && formField2.required();
        JsonObject mergedFormField = unionInternal(
                formField1,
                formField2);
        result = mergedFormField.as(FormField.class).builder().required(required).build();

        return result;
    }

    private JsonObject mergeFormFieldInternal(FormField formField1, FormField formField2, MergeStrategy mergeStrategy) {

        JsonObject result;
        if (mergeStrategy.patchFormField) {

            String type = formField2.schema().type() == null ? formField1.asExplicitFormField().schema().type().toString() : formField2.schema().type().toString();

            JsonObject updatedFormField1 = formField1.asJsonObject();
            if (formField2.schema().constant() != null || formField2.schema().enumeration() != null || formField2.schema().pattern() != null) {
                updatedFormField1 = updatedFormField1.remove("const").remove("enum").remove("pattern");
            }

            result = jsonObjectMerger.merge(updatedFormField1, formField2.asJsonObject());

            if ("object".equals(type)) {
                result = objectMerger.merge(formField1, formField2, result, mergeStrategy);
            } else if ("array".equals(type) || "set".equals(type)) {
                result = arrayMerger.mergeArray(formField1, formField2, result, mergeStrategy);
            }
        } else if (mergeStrategy.softenFormField) {

            if (formField1 == null) {
                formField1 = formField2;
            } else if (formField2 == null) {
                formField2 = formField1;
            }

            String name = formField1.name() != null ? formField1.name() : formField2.name();

            result = JsonObject.EMPTY.put("name", name);

            // required
            result = result.put("required", formField1.required() != null && formField1.required() && formField2.required() != null && formField2.required());

            // nullable
            result = result.put("nullable", (formField1.schema().nullable() != null && formField1.schema().nullable()) || (formField2.schema().nullable() != null && formField2.schema().nullable()));

            // mustBeNull
            result = result.put("mustBeNull", formField1.schema().mustBeNull() != null && formField1.schema().mustBeNull() && formField2.schema().mustBeNull() != null && formField2.schema().mustBeNull());

            result = equalsMerger.merge(formField1.asJsonObject(), formField2.asJsonObject(), result);

            result = enumUnion(formField1, formField2, result);

            // type
            result = typeMerger.merge(formField1.asJsonObject(), formField2.asJsonObject(), result, mergeStrategy);

            // make sure mustBeNull contraints are not used when merging other types' constraints
            formField1 = formField1.schema().mustBeNull() != null && formField1.schema().mustBeNull() ? formField2 : formField1;
            formField2 = formField2.schema().mustBeNull() != null && formField2.schema().mustBeNull() ? formField1 : formField2;

            String type = result.get("type", "string").getValue(String.class);
            if ("string".equals(type)) {
                result = stringMerger.mergeString(formField1, formField2, mergeStrategy, result);
            } else if ("integer".equals(type)) {
                result = integerMerger.merge(formField1, formField2, result, mergeStrategy);
            } else if ("decimal".equals(type)) {
                result = decimalMerger.merge(formField1, formField2, result, mergeStrategy);
            } else if ("object".equals(type)) {
                result = objectMerger.merge(formField1, formField2, result, mergeStrategy);
            } else if ("array".equals(type) || "set".equals(type)) {
                result = arrayMerger.mergeArray(formField1, formField2, result, mergeStrategy);
            } else if ("date".equals(type)) {
                result = dateMerger.mergeDate(formField1, formField2, result, mergeStrategy);
            } else if ("datetime".equals(type)) {
                result = dateMerger.mergeDate(formField1, formField2, result, mergeStrategy);
            }

            result = optionsMerger.soften(formField1, formField2, result);

        } else if (mergeStrategy.hardenFormField) {
            throw new RuntimeException("Implementation moved! Use method intersectInternal()");
        } else {
            throw new RuntimeException("Merge strategy is not implemented");
        }
        return result;
    }

    private JsonObject enumUnion(FormField formField1, FormField formField2, JsonObject result) {

        Enum enum1 = formField1.schema().enumeration();
        Enum enum2 = formField2.schema().enumeration();

        if (enum1 != null || enum2 != null) {
            JsonArray enumArray1 = formField1.schema().enumeration() == null ? formField2.schema().enumeration().asJsonArray() : formField1.schema().enumeration().asJsonArray();
            JsonArray enumArray2 = formField2.schema().enumeration() == null ? formField1.schema().enumeration().asJsonArray() : formField2.schema().enumeration().asJsonArray();
            result = result.put("enum", enumMerger.unionOf(enumArray1, enumArray2));
        }
        return result;
    }

    private JsonObject unionInternal(FormField formField1, FormField formField2) {

        JsonObject result;

        if (formField1 == null) {
            formField1 = formField2;
        } else if (formField2 == null) {
            formField2 = formField1;
        }

        String name = formField1.name() != null ? formField1.name() : formField2.name();

        result = JsonObject.EMPTY.put("name", name);

        // required
        result = result.put("required", formField1.required() != null && formField1.required() && formField2.required() != null && formField2.required());

        // nullable
        result = result.put("nullable", (formField1.schema().nullable() != null && formField1.schema().nullable()) || (formField2.schema().nullable() != null && formField2.schema().nullable()));

        // mustBeNull
        result = result.put("mustBeNull", formField1.schema().mustBeNull() != null && formField1.schema().mustBeNull() && formField2.schema().mustBeNull() != null && formField2.schema().mustBeNull());

        result = equalsMerger.merge(formField1.asJsonObject(), formField2.asJsonObject(), result);

        result = enumUnion(formField1, formField2, result);

        // type
        result = typeMerger.unionOf(formField1.asJsonObject(), formField2.asJsonObject(), result);

        // make sure mustBeNull contraints are not used when merging other types' constraints
        formField1 = formField1.schema().mustBeNull() != null && formField1.schema().mustBeNull() ? formField2 : formField1;
        formField2 = formField2.schema().mustBeNull() != null && formField2.schema().mustBeNull() ? formField1 : formField2;

        String type = result.get("type", "string").getValue(String.class);
        if ("string".equals(type)) {
            result = stringMerger.unionOf(formField1, formField2, result);
        } else if ("integer".equals(type)) {
            result = integerMerger.merge(formField1, formField2, result, MergeStrategy.SOFTEN);
        } else if ("decimal".equals(type)) {
            result = decimalMerger.merge(formField1, formField2, result, MergeStrategy.SOFTEN);
        } else if ("object".equals(type)) {
            result = objectMerger.merge(formField1, formField2, result, MergeStrategy.SOFTEN);
        } else if ("array".equals(type) || "set".equals(type)) {
            result = arrayMerger.mergeArray(formField1, formField2, result, MergeStrategy.SOFTEN);
        } else if ("date".equals(type)) {
            result = dateMerger.mergeDate(formField1, formField2, result, MergeStrategy.SOFTEN);
        } else if ("datetime".equals(type)) {
            result = dateMerger.mergeDate(formField1, formField2, result, MergeStrategy.SOFTEN);
        }

        result = optionsMerger.soften(formField1, formField2, result);

        return result;
    }

    public FormField intersectionOf(FormField formField1, FormField formField2) {
        FormField result;

        if (formField1 == null) {
            result = formField2;
        } else if (formField2 == null) {
            result = formField1;
        } else {
            JsonObject mergedFormField = intersectionOfInternal(formField1, formField2);
            result = mergedFormField.as(FormField.class).builder()
                    .required((
                            formField1 != null && formField1.required() != null && formField1.required()) ||
                            (formField2 != null && formField2.required() != null && formField2.required())
                    ).build();
        }

        return result;
    }

    private JsonObject intersectionOfInternal(FormField formField1, FormField formField2) {
        JsonObject result;
        if (formField1 == null) {
            formField1 = formField2;
        } else if (formField2 == null) {
            formField2 = formField1;
        }

        String name = formField1.name() != null ? formField1.name() : formField2.name();

        result = JsonObject.EMPTY.put("name", name);

        // required
        Boolean required = formField1.required() != null && formField1.required() || formField2.required() != null && formField2.required();
        result = result.put("required", required);

        // nullable

        Boolean nullable = formField1 != null && formField1.schema().nullable() != null && formField1.schema().nullable() && formField2 != null && formField2.schema().nullable() != null && formField2.schema().nullable();
        result = result.put("nullable", nullable);

        // mustBeNull
        Boolean mustBeNull = (formField1.schema().mustBeNull() != null && formField1.schema().mustBeNull()) || (formField2.schema().mustBeNull() != null && formField2.schema().mustBeNull());
        result = result.put("mustBeNull", mustBeNull);

        if (mustBeNull && !nullable && required) {
            result = null; // Maybe throw exception here?
        } else {

            result = equalsMerger.merge(formField1.asJsonObject(), formField2.asJsonObject(), result);

            // type
            result = typeMerger.intersectionOf(formField1.asJsonObject(), formField2.asJsonObject(), result);

            String type = result.get("type", "string").getValue(String.class);
            if ("string".equals(type)) {
                result = stringMerger.mergeString(formField1, formField2, MergeStrategy.HARDEN, result);
            } else if ("integer".equals(type)) {
                result = integerMerger.merge(formField1, formField2, result, MergeStrategy.HARDEN);
            } else if ("decimal".equals(type)) {
                result = decimalMerger.merge(formField1, formField2, result, MergeStrategy.HARDEN);
            } else if ("object".equals(type)) {
                result = objectMerger.merge(formField1, formField2, result, MergeStrategy.HARDEN);
            } else if ("array".equals(type) || "set".equals(type)) {

                result = arrayMerger.intersect(formField1, formField2, result.put("type", type));
            } else if ("date".equals(type)) {
                result = dateMerger.mergeDate(formField1, formField2, result, MergeStrategy.HARDEN);
            }
        }

        result = optionsMerger.harden(formField1, formField2, result);
        return result;
    }
}
