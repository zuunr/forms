/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.generation;

import com.zuunr.forms.Form;
import com.zuunr.forms.FormField;
import com.zuunr.forms.FormFields;
import com.zuunr.forms.ValueFormat;
import com.zuunr.json.*;

import java.util.Comparator;


public class FormMerger {

    protected static final JsonArray eform_value = JsonArray.of("eform").add("value");

    protected static final JsonObjectMerger jsonObjectMerger = new JsonObjectMerger();

    protected static final Comparator formFieldComparator = (o1, o2) -> {

        JsonObject formField1 = ((JsonValue) o1).getValue(JsonObject.class);
        JsonObject formField2 = ((JsonValue) o2).getValue(JsonObject.class);

        int result = formField1.get("name").getValue(String.class).compareTo(formField2.get("name").getValue(String.class));
        if (result == 0) {
            throw new RuntimeException("There can never be more than one form field per builder");
        }
        return result;
    };

    private FieldMerger fieldMerger = new FieldMerger();

    public FormFields patch(Form form1, Form form2) {
        return merge(form1, form2, MergeStrategy.PATCH_BY_OVERWRITE);
    }

    /**
     * Use union, intersection or patch instead
     *
     * @param form1
     * @param form2
     * @param mergeStrategy
     * @return
     */
    @Deprecated
    public FormFields merge(Form form1, Form form2, MergeStrategy mergeStrategy) {

        FormFields formFields1 = form1.formFields();
        FormFields formFields2 = form2.formFields();

        FormFields.FormFieldsBuilder resultBuilder = FormFields.EMPTY.builder();
        boolean continueMergeFromForm2 = true;

        JsonObject formFields2ByName = formFields2.formFieldsByName();

        for (FormField formField1 : formFields1.asList()) {

            FormField mergedFormField = null;
            if (mergeStrategy.patchForm) {
                FormField formField2 = formFields2.of(formField1.name());
                if (formField2 == null) {
                    mergedFormField = formField1;
                } else {
                    mergedFormField = fieldMerger.mergeFormField(formField1, formField2, mergeStrategy);
                }
            } else if (mergeStrategy.softenForm) {
                FormField formField2 = formFields2.of(formField1.name());
                mergedFormField = fieldMerger.mergeFormField(formField1, formField2, mergeStrategy);
            } else if (mergeStrategy.hardenForm) {
                FormField formField2 = formFields2.of(formField1.name());

                if (formField2 == null) {
                    if (form2.exclusive()) {
                        if (formField1.required() != null && formField1.required()) {
                            resultBuilder = FormFields.EMPTY.builder();
                            continueMergeFromForm2 = false;
                            break;
                        }
                    } else {
                        mergedFormField = formField1;
                    }
                } else if (formField2 != null) {
                    mergedFormField = fieldMerger.mergeFormField(formField1, formField2, mergeStrategy);
                }
            } else {
                throw new RuntimeException("Strategy not supported");
            }

            if (mergedFormField != null) {
                resultBuilder.add(mergedFormField);
            }
            formFields2ByName = formFields2ByName.remove(formField1.name());
        }

        if (continueMergeFromForm2) {

            if (mergeStrategy.patchForm) {
                // Do nothing - existing form fields should not be extended with formfields of patching form fields
            } else if (mergeStrategy == MergeStrategy.SOFTEN) {

                for (FormField formField2 : formFields2ByName.values().asList(FormField.class)) {
                    if (formField2.builder() != null) {
                        resultBuilder.add(
                                fieldMerger.mergeFormField(formFields1.of(formField2.name()), formField2, mergeStrategy));
                    }
                }
            } else if (mergeStrategy.hardenForm) {

                for (FormField formField2 : formFields2ByName.values().asList(FormField.class)) {

                    if (formField2.required() != null && formField2.required() && (form2.exclusive() || form1.exclusive())) {
                        // formField2 was not part of form1 but is required - it is impossible to findx least denominator
                        resultBuilder = FormFields.EMPTY.builder();
                        break;
                    }

                    if (!form1.exclusive()) {
                        if (formField2.builder() != null) {
                            resultBuilder.add(
                                    fieldMerger.mergeFormField(formFields1.of(formField2.name()), formField2, mergeStrategy));
                        }
                    }
                }
            } else {
                throw new RuntimeException("strategy not supported");
            }
        }
        return resultBuilder == null ? null : resultBuilder.buildSortedByName(); // TODO: Sorting should be supported in JsonArrayBuilder and FormFieldBuilder
    }

    public FormFields unionFormFields(Form form1, Form form2) {

        FormFields formFields1 = form1.formFields();
        FormFields formFields2 = form2.formFields();

        FormFields.FormFieldsBuilder resultBuilder = FormFields.EMPTY.builder();
        boolean continueMergeFromForm2 = true;

        JsonObject formFields2ByName = formFields2.formFieldsByName();

        for (FormField formField1 : formFields1.asList()) {

            FormField mergedFormField = null;
            FormField formField2 = formFields2.of(formField1.name());
            mergedFormField = fieldMerger.mergeFormField(formField1, formField2, MergeStrategy.SOFTEN);

            if (mergedFormField != null) {
                resultBuilder.add(mergedFormField);
            }
            formFields2ByName = formFields2ByName.remove(formField1.name());
        }

        if (continueMergeFromForm2) {

            for (FormField formField2 : formFields2ByName.values().asList(FormField.class)) {
                if (formField2.builder() != null) {
                    resultBuilder.add(
                            fieldMerger.mergeFormField(formFields1.of(formField2.name()), formField2, MergeStrategy.SOFTEN));
                }
            }
        }
        return resultBuilder == null ? null : resultBuilder.buildSortedByName(); // TODO: Sorting should be supported in JsonArrayBuilder and FormFieldBuilder
    }

    protected JsonObject formFieldsAsJsonObject(JsonArray formFields) {
        return formFieldsAsJsonObject(formFields, false);
    }

    public JsonObject formFieldsAsJsonObject(JsonArray formFields, boolean recursive) {

        JsonObjectBuilder builder = JsonObject.EMPTY.builder();
        for (int i = 0; i < formFields.size(); i++) {
            JsonObject formField = formFields.get(i).getValue(JsonObject.class);

            formField.toString();
            if (recursive) {
                String type = formField.get("type", JsonValue.NULL).getValue(String.class);

                if ("object".equals(type)) {
                    JsonObject form = formField.get("form", JsonValue.NULL).getValue(JsonObject.class);
                    if (form != null) {
                        JsonArray value = form.get("value", JsonValue.NULL).getValue(JsonArray.class);
                        if (value != null) {
                            JsonObject valueAsJsonObject = formFieldsAsJsonObject(value, recursive);
                            form = form.put("value", valueAsJsonObject);
                            formField = formField.put("form", form);
                            formField.toString();
                        }
                    }
                } else if ("array".equals(type) || "set".equals(type)) {
                    JsonObject eform = formField.get("eform", JsonValue.NULL).getValue(JsonObject.class);
                    if (eform != null) {
                        JsonArray value = eform.get("value", JsonValue.NULL).getValue(JsonArray.class);
                        if (value != null) {
                            eform = eform.put("value", formFieldsAsJsonObject(value, recursive));
                            formField = formField.put("eform", eform);
                            formField.toString();
                        }
                    }
                }
            }
            builder.put(formField.get("name").getValue(String.class), formField);
        }
        return builder.build();
    }

    /*
        A subform of a form is a form which only handles one specific form field. Subforms which are to merged must be distict. Form fields which are leaves (ie end-nodes) must be present in only one of the subforms. When merging subforms - all form field members should stay as they are.
     */
    public Form mergeSubforms(Form subform1, Form subform2) {

        JsonObjectBuilder formFieldsByName = subform1.formFieldsByName().builder();
        for (FormField formField2 : subform2.formFields().asList()) {
            FormField formField1 = subform1.formField(formField2.name());

            if (formField1 == null) {
                formFieldsByName.put(formField2.name(), formField2);
            } else {
                if (formField1.schema().type().isObject() && formField1.schema().form() != null) {
                    Form mergedForm = mergeSubforms(formField1.schema().form(), formField2.schema().form());
                    formFieldsByName.put(formField2.name(), formField1.builder().form(mergedForm).build());
                } else if (formField1.schema().type().isArrayOrSet() && formField1.schema().eform() != null) {
                    Form mergedForm = mergeSubforms(formField1.schema().eform(), formField2.schema().eform());
                    formFieldsByName.put(formField2.name(), formField1.builder().eform(mergedForm).build());

                } else {
                    throw new RuntimeException("Only distict subsets are supported (ie leaves must be present in only one of the subforms). formfield1 and formfield2 of same builder must be non-leaf if both are present.");
                }
            }
        }
        return subform1.builder().value(new FormFields.FormFieldsBuilder(formFieldsByName.build().values()).build()).build();
    }

    @Deprecated
    public Form mergeForms(Form form1, Form form2, MergeStrategy mergeStrategy) {

        form1 = form1.asExplicitForm();
        form2 = form2.asExplicitForm();
        Form.FormBuilder formBuilder = Form.EMPTY.builder();

        formBuilder.value(merge(form1, form2, mergeStrategy));

        if (mergeStrategy.hardenForm) {
            formBuilder.exclusive(form1.exclusive() || form2.exclusive());
        } else if (mergeStrategy.softenForm) {
            formBuilder.exclusive(form1.exclusive() && form2.exclusive());
        } else if (mergeStrategy.patchForm) {
            formBuilder.exclusive(form1.exclusive());
        } else {
            throw new RuntimeException("Strategy not supported");
        }
        return formBuilder.build().asCompactForm();
    }

    public ValueFormat intersectionOf(ValueFormat valueFormat1, ValueFormat valueFormat2) {
        return intersectionOf(
                Form.EMPTY.builder()
                        .value(FormFields.EMPTY.builder()
                                .add(valueFormat1.asFormField("valueFormat")).build()).build(),
                Form.EMPTY.builder()
                        .value(FormFields.EMPTY.builder()
                                .add(valueFormat2.asFormField("valueFormat")).build()).build()
        ).formField("valueFormat").asValueFormat();
    }

    public Form intersectionOf(Form form1, Form form2) {

        form1 = form1.asExplicitForm();
        form2 = form2.asExplicitForm();
        Form.FormBuilder formBuilder = Form.EMPTY.builder();

        formBuilder.value(intersectFormFields(form1, form2));
        formBuilder.exclusive(form1.exclusive() || form2.exclusive());
        return formBuilder.build().asCompactForm();
    }

    public Form unionOf(Form form1, Form form2) {
        form1 = form1.asExplicitForm();
        form2 = form2.asExplicitForm();
        Form.FormBuilder formBuilder = Form.EMPTY.builder();

        formBuilder.value(unionFormFields(form1, form2));
        formBuilder.exclusive(form1.exclusive() && form2.exclusive());
        return formBuilder.build().asCompactForm();
    }

    private FormFields intersectFormFields(Form form1, Form form2) {

        FormFields formFields1 = form1.formFields();
        FormFields formFields2 = form2.formFields();

        FormFields.FormFieldsBuilder resultBuilder = FormFields.EMPTY.builder();
        boolean continueMergeFromForm2 = true;

        JsonObject formFields2ByName = formFields2.formFieldsByName();

        for (FormField formField1 : formFields1.asList()) {

            FormField mergedFormField = null;
                FormField formField2 = formFields2.of(formField1.name());

                if (formField2 == null) {
                    if (form2.exclusive()) {

                        if (formField1.required() != null && formField1.required()) {
                            resultBuilder = FormFields.EMPTY.builder();
                            continueMergeFromForm2 = false;
                            break;
                        }
                    } else {
                        mergedFormField = formField1;
                    }
                } else if (formField2 != null) {
                    mergedFormField = fieldMerger.intersectionOf(formField1, formField2);
                }


            if (mergedFormField != null) {
                resultBuilder.add(mergedFormField);
            }
            formFields2ByName = formFields2ByName.remove(formField1.name());
        }

        if (continueMergeFromForm2) {


                for (FormField formField2 : formFields2ByName.values().asList(FormField.class)) {

                    if (formField2.required() != null && formField2.required() && (form2.exclusive() || form1.exclusive())) {
                        // formField2 was not part of form1 but is required - it is impossible to findx least denominator
                        resultBuilder = FormFields.EMPTY.builder();
                        break;
                    }

                    if (!form1.exclusive()) {
                        if (formField2.builder() != null) {
                            resultBuilder.add(
                                    fieldMerger.mergeFormField(formFields1.of(formField2.name()), formField2, MergeStrategy.HARDEN));
                        }
                    }
                }

        }
        return resultBuilder == null ? null : resultBuilder.buildSortedByName();

    }
}
