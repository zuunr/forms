/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.generation;

import com.zuunr.forms.FormField;
import com.zuunr.forms.ValueFormat;

/**
 * @author Niklas Eldberger
 */
public class FormatMerger {

    FieldMerger fieldMerger = new FieldMerger();

    public ValueFormat soften(ValueFormat valueFormat1, ValueFormat valueFormat2) {
        FormField formField1 = valueFormat1.asExplicitValueFormat().asJsonObject().put("name", "name").as(FormField.class);
        FormField formField2 = valueFormat2.asExplicitValueFormat().asJsonObject().put("name", "name").as(FormField.class);
        return fieldMerger.mergeFormField(formField1, formField2, MergeStrategy.SOFTEN).asJsonObject().remove("name").as(ValueFormat.class);
    }

    public ValueFormat harden(ValueFormat valueFormat1, ValueFormat valueFormat2) {
        FormField formField1 = valueFormat1.asExplicitValueFormat().asJsonObject().put("name", "name").as(FormField.class);
        FormField formField2 = valueFormat2.asExplicitValueFormat().asJsonObject().put("name", "name").as(FormField.class);
        return fieldMerger.mergeFormField(formField1, formField2, MergeStrategy.HARDEN).asJsonObject().remove("name").as(ValueFormat.class);
    }
}
