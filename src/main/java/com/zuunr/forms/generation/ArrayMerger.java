/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.generation;

import com.zuunr.forms.FormField;
import com.zuunr.forms.ValueFormat;
import com.zuunr.forms.formfield.MaxSize;
import com.zuunr.forms.formfield.MinSize;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;

/**
 * @author Niklas Eldberger
 */
public class ArrayMerger {

    private static final MaximumMerger maximumMerger = new MaximumMerger();
    private static final MinimumMerger minimumMerger = new MinimumMerger();

    protected static final JsonArray eform_value = JsonArray.of("eform").add("value");

    private FormMerger formMerger = new FormMerger();


    public JsonObject mergeArray(FormField formField1, FormField formField2, JsonObject result, MergeStrategy mergeStrategy) {

        JsonArray eformValue1 = formField1.schema().eform() == null ? null : formField1.schema().eform().value();
        JsonArray eformValue2 = formField2.schema().eform() == null ? null : formField2.schema().eform().value();

        if (mergeStrategy.patchFormField) {
            if (eformValue1 == null && eformValue2 == null) {
                throw new RuntimeException("Only merging of eform is implemented. If arrays should be able contain values other than JSON Objects then this needs to be implemented");
            } else if (eformValue1 != null && eformValue2 != null) {
                result = result.put(eform_value, formMerger.merge(formField1.schema().eform(), formField2.schema().eform(), mergeStrategy).asJsonArray());
            } else if (eformValue1 == null) {
                result = result.put(eform_value, eformValue2);
            } else if (eformValue2 == null) {
                result = result.put(eform_value, eformValue1);
            }
        } else if (mergeStrategy.softenFormField) {

            JsonArray mergedFormFields;
            if (eformValue1 == null && eformValue2 == null) {
                // Cannot do any merge of something that does not exist
            } else {
                if (eformValue1 == null || eformValue2 == null) {
                    mergedFormFields = eformValue1 == null ? eformValue2 : eformValue1;

                } else {
                    mergedFormFields = formMerger
                            .merge(
                                    formField1.schema().eform(),
                                    formField2.schema().eform(),
                                    mergeStrategy)
                            .asJsonArray();
                }

                if (mergedFormFields == null) {
                    result = result.remove("eform").put("etype", "object");
                } else {
                    result = result.put(eform_value, mergedFormFields);
                }
            }

            result = minimumMerger.mergeMinimum("minsize", formField1.schema().asJsonObject(), formField2.schema().asJsonObject(), result, mergeStrategy);
            result = maximumMerger.mergeMaximum("maxsize", formField1.schema().asJsonObject(), formField2.schema().asJsonObject(), result, mergeStrategy);
        }
        return result;
    }

    public JsonObject intersect(FormField formField1, FormField formField2, JsonObject resultSoFar) {

        FormField.Builder builder = resultSoFar.as(FormField.class).builder();

        ValueFormat valueFormat1 = formField1.asExplicitFormField().schema().element();
        ValueFormat valueFormat2 = formField2.asExplicitFormField().schema().element();

        ValueFormat element;
        if (valueFormat1 != null || valueFormat2 != null) {
            if (valueFormat1 == null) {
                element = valueFormat2;
            } else if (valueFormat2 == null) {
                element = valueFormat1;
            } else {
                element = formMerger.intersectionOf(valueFormat1, valueFormat2);
            }

            if (element != null) {
                builder.element(element);
            }
        }

        MinSize minSize = minimumMerger.intersect(valueFormat1.minSize(), valueFormat2.minSize());
        if (minSize != null) {
            builder.minsize(minSize);
        }

        MaxSize maxSize = maximumMerger.intersect(valueFormat1.maxSize(), valueFormat2.maxSize());
        if (maxSize != null) {
            builder.maxsize(maxSize);
        }

        return builder.build().asJsonObject();
    }
}
