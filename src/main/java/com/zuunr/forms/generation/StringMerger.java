/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.generation;

import com.zuunr.forms.FormField;
import com.zuunr.forms.formfield.Max;
import com.zuunr.forms.formfield.Min;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectMerger;
import com.zuunr.json.JsonValue;

/**
 * @author Niklas Eldberger
 */
public class StringMerger {

    JsonObjectMerger jsonObjectMerger = new JsonObjectMerger();
    PatternMerger patternMerger = new PatternMerger();
    MinMerger minMerger = new MinMerger();
    MaxMerger maxMerger = new MaxMerger();
    StringPropertyMerger stringPropertyMerger = new StringPropertyMerger();

    public JsonObject mergeString(FormField formField1, FormField formField2, MergeStrategy mergeStrategy, JsonObject formFieldSoFar) {

        JsonObject formFieldJsonObject1 = formField1.asJsonObject();
        JsonObject formFieldJsonObject2 = formField2.asJsonObject();

        JsonObject mergedFormField = formFieldSoFar;
        if (mergeStrategy.patchFormField) {
            mergedFormField = jsonObjectMerger.merge(formFieldJsonObject1, formFieldJsonObject2);
        } else if (mergeStrategy == MergeStrategy.SOFTEN) {

            Boolean formField1mustBeNull = formFieldJsonObject1.get("mustBeNull", JsonValue.FALSE).getValue(Boolean.class);
            Boolean formField2mustBeNull = formFieldJsonObject2.get("mustBeNull", JsonValue.FALSE).getValue(Boolean.class);

            boolean restrictionApplicable = !(formField1mustBeNull && formField2mustBeNull);

            if (restrictionApplicable) {
                // minlength
                Integer formField1minlength = formFieldJsonObject1.get("minlength", JsonValue.NULL).asInteger();
                Integer formField2minlength = formFieldJsonObject2.get("minlength", JsonValue.NULL).asInteger();

                if (formField1minlength == null && !formField1mustBeNull || formField2minlength == null && !formField2mustBeNull) {
                    mergedFormField = mergedFormField.remove("minlength");
                } else {
                    if (formField1minlength == null) {
                        mergedFormField = mergedFormField.put("minlength", formField2minlength);
                    } else if (formField2minlength == null) {
                        mergedFormField = mergedFormField.put("minlength", formField1minlength);
                    } else if (formField1minlength < formField2minlength) {
                        mergedFormField = mergedFormField.put("minlength", formField1minlength);
                    } else {
                        mergedFormField = mergedFormField.put("minlength", formField2minlength);
                    }
                }

                // maxlength
                Integer formField1maxlength = formFieldJsonObject1.get("maxlength", JsonValue.NULL).asInteger();
                Integer formField2maxlength = formFieldJsonObject2.get("maxlength", JsonValue.NULL).asInteger();

                if (formField1maxlength == null && !formField1mustBeNull || formField2maxlength == null && !formField2mustBeNull) {
                    mergedFormField = mergedFormField.remove("maxlength");
                } else {
                    if (formField1maxlength == null) {
                        mergedFormField = mergedFormField.put("maxlength", formField2maxlength);
                    } else if (formField2maxlength == null) {
                        mergedFormField = mergedFormField.put("maxlength", formField1maxlength);
                    } else if (formField1maxlength > formField2maxlength) {
                        mergedFormField = mergedFormField.put("maxlength", formField1maxlength);
                    } else {
                        mergedFormField = mergedFormField.put("maxlength", formField2maxlength);
                    }
                }

                Max max = maxMerger.soften(formField1, formField2);
                if (max != null) {
                    mergedFormField = mergedFormField.put("max", max);
                }

                Min min = minMerger.soften(formField1, formField2);
                if (min != null) {
                    mergedFormField = mergedFormField.put("min", min);
                }


                JsonObject constEnumAndPattern = stringPropertyMerger.unionOf(formField1.asJsonObject(), formField2.asJsonObject());
                mergedFormField = applyConstEnumAndPattern(mergedFormField, constEnumAndPattern);

            }
        } else if (mergeStrategy.hardenFormField) {

            Boolean formField1mustBeNull = formFieldJsonObject1.get("mustBeNull", JsonValue.FALSE).getValue(Boolean.class);
            Boolean formField2mustBeNull = formFieldJsonObject2.get("mustBeNull", JsonValue.FALSE).getValue(Boolean.class);

            boolean restrictionApplicable = !(formField1mustBeNull && formField2mustBeNull);

            if (restrictionApplicable) {
                // minlength
                Integer formField1minlength = formFieldJsonObject1.get("minlength", JsonValue.NULL).asInteger();
                Integer formField2minlength = formFieldJsonObject2.get("minlength", JsonValue.NULL).asInteger();

                if (formField1minlength == null && !formField1mustBeNull || formField2minlength == null && !formField2mustBeNull) {
                    mergedFormField = mergedFormField.remove("minlength");
                } else {
                    if (formField1minlength == null) {
                        mergedFormField = mergedFormField.put("minlength", formField2minlength);
                    } else if (formField2minlength == null) {
                        mergedFormField = mergedFormField.put("minlength", formField1minlength);
                    } else if (formField1minlength < formField2minlength) {
                        mergedFormField = mergedFormField.put("minlength", formField2minlength);
                    } else {
                        mergedFormField = mergedFormField.put("minlength", formField1minlength);
                    }
                }

                // maxlength
                Integer formField1maxlength = formFieldJsonObject1.get("maxlength", JsonValue.NULL).asInteger();
                Integer formField2maxlength = formFieldJsonObject2.get("maxlength", JsonValue.NULL).asInteger();

                if (formField1maxlength == null && !formField1mustBeNull || formField2maxlength == null && !formField2mustBeNull) {
                    mergedFormField = mergedFormField.remove("maxlength");
                } else {
                    if (formField1maxlength == null) {
                        mergedFormField = mergedFormField.put("maxlength", formField2maxlength);
                    } else if (formField2maxlength == null) {
                        mergedFormField = mergedFormField.put("maxlength", formField1maxlength);
                    } else if (formField1maxlength > formField2maxlength) {
                        mergedFormField = mergedFormField.put("maxlength", formField2maxlength);
                    } else {
                        mergedFormField = mergedFormField.put("maxlength", formField1maxlength);
                    }
                }

                Max max = maxMerger.harden(formField1, formField2);
                if (max != null) {
                    mergedFormField = mergedFormField.put("max", max);
                }

                Min min = minMerger.harden(formField1, formField2);
                if (min != null) {
                    mergedFormField = mergedFormField.put("min", min);
                }

                JsonObject constEnumAndPattern = stringPropertyMerger.intersectionOf(formField1.asJsonObject(), formField2.asJsonObject());
                mergedFormField = applyConstEnumAndPattern(mergedFormField, constEnumAndPattern);
            }
        } else {
            throw new RuntimeException("Merge strategy is not implemented yet");
        }
        return mergedFormField;
    }

    public JsonObject intersectionOf(FormField formField1, FormField formField2, JsonObject formFieldSoFar) {

        JsonObject formFieldJsonObject1 = formField1.asJsonObject();
        JsonObject formFieldJsonObject2 = formField2.asJsonObject();

        JsonObject mergedFormField = formFieldSoFar;

            Boolean formField1mustBeNull = formFieldJsonObject1.get("mustBeNull", JsonValue.FALSE).getValue(Boolean.class);
            Boolean formField2mustBeNull = formFieldJsonObject2.get("mustBeNull", JsonValue.FALSE).getValue(Boolean.class);

            boolean restrictionApplicable = !(formField1mustBeNull && formField2mustBeNull);

            if (restrictionApplicable) {
                // minlength
                Integer formField1minlength = formFieldJsonObject1.get("minlength", JsonValue.NULL).asInteger();
                Integer formField2minlength = formFieldJsonObject2.get("minlength", JsonValue.NULL).asInteger();

                if (formField1minlength == null && !formField1mustBeNull || formField2minlength == null && !formField2mustBeNull) {
                    mergedFormField = mergedFormField.remove("minlength");
                } else {
                    if (formField1minlength == null) {
                        mergedFormField = mergedFormField.put("minlength", formField2minlength);
                    } else if (formField2minlength == null) {
                        mergedFormField = mergedFormField.put("minlength", formField1minlength);
                    } else if (formField1minlength < formField2minlength) {
                        mergedFormField = mergedFormField.put("minlength", formField2minlength);
                    } else {
                        mergedFormField = mergedFormField.put("minlength", formField1minlength);
                    }
                }

                // maxlength
                Integer formField1maxlength = formFieldJsonObject1.get("maxlength", JsonValue.NULL).asInteger();
                Integer formField2maxlength = formFieldJsonObject2.get("maxlength", JsonValue.NULL).asInteger();

                if (formField1maxlength == null && !formField1mustBeNull || formField2maxlength == null && !formField2mustBeNull) {
                    mergedFormField = mergedFormField.remove("maxlength");
                } else {
                    if (formField1maxlength == null) {
                        mergedFormField = mergedFormField.put("maxlength", formField2maxlength);
                    } else if (formField2maxlength == null) {
                        mergedFormField = mergedFormField.put("maxlength", formField1maxlength);
                    } else if (formField1maxlength > formField2maxlength) {
                        mergedFormField = mergedFormField.put("maxlength", formField2maxlength);
                    } else {
                        mergedFormField = mergedFormField.put("maxlength", formField1maxlength);
                    }
                }

                Max max = maxMerger.harden(formField1, formField2);
                if (max != null) {
                    mergedFormField = mergedFormField.put("max", max);
                }

                Min min = minMerger.harden(formField1, formField2);
                if (min != null) {
                    mergedFormField = mergedFormField.put("min", min);
                }

                JsonObject constEnumAndPattern = stringPropertyMerger.intersectionOf(formField1.asJsonObject(), formField2.asJsonObject());
                mergedFormField = applyConstEnumAndPattern(mergedFormField, constEnumAndPattern);
            }

        return mergedFormField;
    }

    public JsonObject unionOf(FormField formField1, FormField formField2, JsonObject formFieldSoFar) {

        JsonObject formFieldJsonObject1 = formField1.asJsonObject();
        JsonObject formFieldJsonObject2 = formField2.asJsonObject();

        JsonObject mergedFormField = formFieldSoFar;

        Boolean formField1mustBeNull = formFieldJsonObject1.get("mustBeNull", JsonValue.FALSE).getValue(Boolean.class);
        Boolean formField2mustBeNull = formFieldJsonObject2.get("mustBeNull", JsonValue.FALSE).getValue(Boolean.class);

        boolean restrictionApplicable = !(formField1mustBeNull && formField2mustBeNull);

        if (restrictionApplicable) {
            // minlength
            Integer formField1minlength = formFieldJsonObject1.get("minlength", JsonValue.NULL).asInteger();
            Integer formField2minlength = formFieldJsonObject2.get("minlength", JsonValue.NULL).asInteger();

            if (formField1minlength == null && !formField1mustBeNull || formField2minlength == null && !formField2mustBeNull) {
                mergedFormField = mergedFormField.remove("minlength");
            } else {
                if (formField1minlength == null) {
                    mergedFormField = mergedFormField.put("minlength", formField2minlength);
                } else if (formField2minlength == null) {
                    mergedFormField = mergedFormField.put("minlength", formField1minlength);
                } else if (formField1minlength < formField2minlength) {
                    mergedFormField = mergedFormField.put("minlength", formField1minlength);
                } else {
                    mergedFormField = mergedFormField.put("minlength", formField2minlength);
                }
            }

            // maxlength
            Integer formField1maxlength = formFieldJsonObject1.get("maxlength", JsonValue.NULL).asInteger();
            Integer formField2maxlength = formFieldJsonObject2.get("maxlength", JsonValue.NULL).asInteger();

            if (formField1maxlength == null && !formField1mustBeNull || formField2maxlength == null && !formField2mustBeNull) {
                mergedFormField = mergedFormField.remove("maxlength");
            } else {
                if (formField1maxlength == null) {
                    mergedFormField = mergedFormField.put("maxlength", formField2maxlength);
                } else if (formField2maxlength == null) {
                    mergedFormField = mergedFormField.put("maxlength", formField1maxlength);
                } else if (formField1maxlength > formField2maxlength) {
                    mergedFormField = mergedFormField.put("maxlength", formField1maxlength);
                } else {
                    mergedFormField = mergedFormField.put("maxlength", formField2maxlength);
                }
            }

            Max max = maxMerger.soften(formField1, formField2);
            if (max != null) {
                mergedFormField = mergedFormField.put("max", max);
            }

            Min min = minMerger.soften(formField1, formField2);
            if (min != null) {
                mergedFormField = mergedFormField.put("min", min);
            }

            JsonObject constEnumAndPattern = stringPropertyMerger.unionOf(formField1.asJsonObject(), formField2.asJsonObject());
            mergedFormField = applyConstEnumAndPattern(mergedFormField, constEnumAndPattern);
        }

        return mergedFormField;
    }


    private JsonObject applyConstEnumAndPattern(JsonObject mergedFormField, JsonObject constEnumAndPattern) {
        JsonValue constant = constEnumAndPattern.get("const");
        if (constant != null) {
            mergedFormField = mergedFormField.put("const", constant);
        }

        JsonValue enumeration = constEnumAndPattern.get("enum");
        if (enumeration != null) {
            mergedFormField = mergedFormField.put("enum", enumeration);
        }

        JsonValue pattern = constEnumAndPattern.get("pattern");
        if (pattern != null) {
            mergedFormField = mergedFormField.put("pattern", pattern);
        }
        return mergedFormField;
    }
}
