/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.generation;

import com.zuunr.forms.formfield.Type;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;

/**
 * @author Niklas Eldberger
 */
public class TypeMerger {

    public JsonObject merge(JsonObject formField1, JsonObject formField2, JsonObject resultSoFar, MergeStrategy mergeStrategy) {

        Boolean formField1mustBeNull = formField1.get("mustBeNull", JsonValue.FALSE).getValue(Boolean.class);
        Boolean formField2mustBeNull = formField2.get("mustBeNull", JsonValue.FALSE).getValue(Boolean.class);

        boolean restrictionApplicable = !(formField1mustBeNull && formField2mustBeNull);

        if (restrictionApplicable) {
            JsonValue formField1Type = formField1.get("type");
            JsonValue formField2Type = formField2.get("type");

            if (formField1Type != null || formField2Type != null) {
                if (formField1Type == null) {
                    formField1Type = formField2Type;
                } else if (formField2Type == null) {
                    formField2Type = formField1Type;
                }

                if (formField1Type.equals(formField2Type)) {
                    resultSoFar = resultSoFar.put("type", formField1Type);
                } else if (mergeStrategy.hardenFormField && (formField1Type.equals(JsonValue.of("set")) || formField2Type.equals(JsonValue.of("set")))) {
                    resultSoFar = resultSoFar.put("type", "set");
                } else if ((formField1Type.equals(JsonValue.of("array")) || formField1Type.equals(JsonValue.of("set"))) &&
                        (formField2Type.equals(JsonValue.of("array")) || formField2Type.equals(JsonValue.of("set")))) {
                    resultSoFar = resultSoFar.put("type", "array");
                } else if (formField1Type.as(Type.class).isStringTypeOrSubtypeOfString() && formField2Type.as(Type.class).isStringTypeOrSubtypeOfString()) {

                    if (MergeStrategy.HARDEN.equals(mergeStrategy)) {
                        if (Type.STRING.equals(formField1Type.as(Type.class))) {
                            resultSoFar = resultSoFar.put("type", formField2Type);
                        } else {
                            resultSoFar = resultSoFar.put("type", formField1Type);
                        }
                    } else if (MergeStrategy.SOFTEN.equals(mergeStrategy)) {
                        resultSoFar = resultSoFar.put("type", Type.STRING);
                    } else if (MergeStrategy.PATCH_BY_OVERWRITE.equals(mergeStrategy)) {
                        resultSoFar = resultSoFar.put("type", formField2Type);
                    } else {
                        throw new RuntimeException("Strategy not supported: " + mergeStrategy);
                    }
                } else {
                    resultSoFar = resultSoFar.put("type", "undefined");
                }
            }
        }
        return resultSoFar;
    }

    public JsonObject unionOf(JsonObject formField1, JsonObject formField2, JsonObject resultSoFar) {

        Boolean formField1mustBeNull = formField1.get("mustBeNull", JsonValue.FALSE).getValue(Boolean.class);
        Boolean formField2mustBeNull = formField2.get("mustBeNull", JsonValue.FALSE).getValue(Boolean.class);

        boolean restrictionApplicable = !(formField1mustBeNull && formField2mustBeNull);

        if (restrictionApplicable) {
            JsonValue formField1Type = formField1.get("type");
            JsonValue formField2Type = formField2.get("type");

            if (formField1Type != null || formField2Type != null) {
                if (formField1Type == null) {
                    formField1Type = formField2Type;
                } else if (formField2Type == null) {
                    formField2Type = formField1Type;
                }

                if (formField1Type.equals(formField2Type)) {
                    resultSoFar = resultSoFar.put("type", formField1Type);
                } else if ((formField1Type.equals(JsonValue.of("array")) || formField1Type.equals(JsonValue.of("set"))) &&
                        (formField2Type.equals(JsonValue.of("array")) || formField2Type.equals(JsonValue.of("set")))) {
                    resultSoFar = resultSoFar.put("type", "array");
                } else if (formField1Type.as(Type.class).isStringTypeOrSubtypeOfString() && formField2Type.as(Type.class).isStringTypeOrSubtypeOfString()) {

                    resultSoFar = resultSoFar.put("type", Type.STRING);
                } else {
                    resultSoFar = resultSoFar.put("type", "undefined");
                }
            }
        }
        return resultSoFar;
    }

    public JsonObject intersectionOf(JsonObject formField1, JsonObject formField2, JsonObject resultSoFar) {

        Boolean formField1mustBeNull = formField1.get("mustBeNull", JsonValue.FALSE).getValue(Boolean.class);
        Boolean formField2mustBeNull = formField2.get("mustBeNull", JsonValue.FALSE).getValue(Boolean.class);

        boolean restrictionApplicable = !(formField1mustBeNull && formField2mustBeNull);

        if (restrictionApplicable) {
            JsonValue formField1Type = formField1.get("type");
            JsonValue formField2Type = formField2.get("type");

            if (formField1Type != null || formField2Type != null) {
                if (formField1Type == null) {
                    formField1Type = formField2Type;
                } else if (formField2Type == null) {
                    formField2Type = formField1Type;
                } else if (Type.UNDEFINED.asJsonValue().equals(formField1Type)) {
                    formField1Type = formField2Type;
                } else if (Type.UNDEFINED.asJsonValue().equals(formField2Type)) {
                    formField2Type = formField1Type;
                }

                if (formField1Type.equals(formField2Type)) {
                    resultSoFar = resultSoFar.put("type", formField1Type);
                } else if (formField1Type.equals(JsonValue.of("set")) || formField2Type.equals(JsonValue.of("set"))) {
                    resultSoFar = resultSoFar.put("type", "set");
                } else if ((formField1Type.equals(JsonValue.of("array")) || formField1Type.equals(JsonValue.of("set"))) &&
                        (formField2Type.equals(JsonValue.of("array")) || formField2Type.equals(JsonValue.of("set")))) {
                    resultSoFar = resultSoFar.put("type", "array");
                } else if (formField1Type.as(Type.class).isStringTypeOrSubtypeOfString() && formField2Type.as(Type.class).isStringTypeOrSubtypeOfString()) {
                    if (Type.STRING.equals(formField1Type.as(Type.class))) {
                        resultSoFar = resultSoFar.put("type", formField2Type);
                    } else {
                        resultSoFar = resultSoFar.put("type", formField1Type);
                    }
                } else {
                    resultSoFar = resultSoFar.put("type", "undefined");
                }
            }
        }
        return resultSoFar;
    }
}
