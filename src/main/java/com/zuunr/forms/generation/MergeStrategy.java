/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.generation;

public final class MergeStrategy {
    public static final MergeStrategy SOFTEN = new MergeStrategy(false, false, true, false, false,true); // Means all data of form1 as well as data of form2 will be valid after merge (Use case: generation from samples)
    public static final MergeStrategy HARDEN = new MergeStrategy(true, false, false, true, false,false); // Means only data which is valid for both form1 and form2 will be valid after merge (Use case: to build one validation form from two sources - request validation form and access control form - to increase performance

    /**
     * @deprecated
     */
    @Deprecated
    public static final MergeStrategy PATCH_ = new MergeStrategy(false, true, false, false, true,false);  // Means form1 will be patched with all present formFieldsMembers of form2. All formFields and formFieldMember of form1 which are not present in form2 will remain and formFields which are present in form2 but not in form1 will not be part of result. (Use case: Being able to explicitly declare size etc. in config for resourceModels and queryModels

    public static final MergeStrategy PATCH_BY_OVERWRITE = PATCH_; // Means form1 will be patched with all present formFieldsMembers of form2. All formFields and formFieldMember of form1 which are not present in form2 will remain and formFields which are present in form2 but not in form1 will not be part of result. (Use case: Being able to explicitly declare size etc. in config for resourceModels and queryModels

    public final Boolean hardenForm;
    public final Boolean patchForm;
    public final Boolean softenForm;

    public final Boolean hardenFormField;
    public final Boolean patchFormField;
    public final Boolean softenFormField;



    private MergeStrategy(Boolean hardenForm, Boolean patchForm, Boolean softenForm, Boolean hardenFormField, Boolean patchFormField, Boolean softenFormField) {
        this.hardenForm = hardenForm;
        this.patchForm = patchForm;
        this.softenForm = softenForm;
        this.hardenFormField = hardenFormField;
        this.patchFormField = patchFormField;
        this.softenFormField = softenFormField;
    }
}