/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.generation;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonArrayBuilder;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;

/**
 * @author Niklas Eldberger
 */
public class EqualsMerger {

    private static final JsonArray equals_paths = JsonArray.of("equals", "paths");

    public JsonObject merge(JsonObject formField1, JsonObject formField2, JsonObject result) {
        JsonArrayBuilder resultingPaths = JsonArray.EMPTY.builder();

        JsonArray equalsPathsOfFormField1 = formField1.get(equals_paths, JsonValue.NULL).getValue(JsonArray.class);
        JsonArray equalsPathsOfFormField2 = formField2.get(equals_paths, JsonValue.NULL).getValue(JsonArray.class);


        if (equalsPathsOfFormField1 == null) {

            if (equalsPathsOfFormField2 != null) {
                result = result.put(equals_paths, equalsPathsOfFormField2);
            }
        } else if (equalsPathsOfFormField2 == null) {

            result = result.put(equals_paths, equalsPathsOfFormField1);

        } else {

            for (JsonValue path : equalsPathsOfFormField1.asList()) {
                if (equalsPathsOfFormField2.contains(path)) {
                    resultingPaths = resultingPaths.add(path);
                }
            }
            result = result.put(equals_paths, resultingPaths.build().sort((o1, o2) -> ((JsonValue) o1).getValue(JsonArray.class).asDotSeparatedString().compareTo(((JsonValue) o2).getValue(JsonArray.class).asDotSeparatedString())));
        }
        return result;
    }
}
