/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.generation;

import com.zuunr.forms.Form;
import com.zuunr.forms.FormField;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;

/**
 * @author Niklas Eldberger
 */
public class ObjectMerger {

    private static final JsonArray form_value = JsonArray.EMPTY.add("form").add("value");

    private FormMerger formMerger;

    public FormMerger getFormMerger() {
        if (formMerger == null) {
            formMerger = new FormMerger();
        }

        return formMerger;
    }

    public JsonObject merge(FormField formField1, FormField formField2, JsonObject result, MergeStrategy mergeStrategy) {

        Boolean formField1mustBeNull = formField1.schema().mustBeNull() != null && formField1.schema().mustBeNull();
        Boolean formField2mustBeNull = formField2.schema().mustBeNull() != null && formField2.schema().mustBeNull();

        boolean restrictionApplicable = !(formField1mustBeNull && formField2mustBeNull);


        Form form1 = formField1.schema().form();
        Form form2 = formField2.schema().form();

        JsonArray formValue1 = form1 == null ? null : form1.value();
        JsonArray formValue2 = form2 == null ? null : form2.value();

        if (mergeStrategy.patchFormField) {

            if (formValue1 == null) {
                result = result.remove("form");
            } else {
                JsonArray mergedFormFields = getFormMerger().merge(form1, form2, mergeStrategy).asJsonArray();
                result = result.put(form_value, mergedFormFields);
            }
        } else if (restrictionApplicable) {
            {
                if (formValue1 == null && !formField1mustBeNull || formValue2 == null && !formField2mustBeNull) {
                    result = result.remove("form");
                } else if (formValue1 == null) {
                    result = result.put(form_value, formValue2);
                } else if (formValue2 == null) {
                    result = result.put(form_value, formValue1);
                } else {

                    JsonArray mergedFormFields = null;
                    if (mergeStrategy.hardenForm) {
                        mergedFormFields = getFormMerger().intersectionOf(form1, form2).formFields().asJsonArray();
                    } else if (mergeStrategy.softenForm) {
                        mergedFormFields = getFormMerger().unionOf(form1, form2).formFields().asJsonArray();
                    }
                    if (mergedFormFields == null) {
                        result = result.remove("form");
                    } else {
                        result = result.put(form_value, mergedFormFields);
                    }
                }
            }
        }
        return result;
    }
}
