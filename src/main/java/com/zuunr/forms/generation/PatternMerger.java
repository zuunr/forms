/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.generation;

import com.zuunr.forms.FormField;
import com.zuunr.forms.formfield.Pattern;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectBuilder;
import com.zuunr.json.JsonValue;

import java.util.regex.Matcher;

/**
 * @author Niklas Eldberger
 */
public class PatternMerger {

    private static final String PATTERN = "pattern";

    private static final Pattern UNION_PATTERN = new Pattern(JsonValue.of("([^)(|]+)[|](.*)"));
    private static final Pattern LAST_PART_OPTIONAL_PATTERN = new Pattern(JsonValue.of("(.+)([(][^)(]*[)][*])"));
    private static final Pattern AND_OPERATOR_SEPARATOR_PATTERN = new Pattern(JsonValue.of("[)][(][?]="));
    private static final Pattern SIMPLIFICATION_PATTERN = new Pattern(JsonValue.of("[(][?][=]([^)]*)[)]"));

    public JsonObject mergePattern(FormField formField1, FormField formField2, MergeStrategy mergeStrategy, JsonObject formFieldSoFar) {

        JsonObject formFieldJsonObject1 = formField1.asJsonObject();
        JsonObject formFieldJsonObject2 = formField2.asJsonObject();

        JsonObject result = formFieldSoFar;

        Boolean formField1mustBeNull = formFieldJsonObject1.get("mustBeNull", JsonValue.FALSE).getValue(Boolean.class);
        Boolean formField2mustBeNull = formFieldJsonObject2.get("mustBeNull", JsonValue.FALSE).getValue(Boolean.class);

        boolean restrictionApplicable = !(formField1mustBeNull && formField2mustBeNull);

        if (!restrictionApplicable) {
            return result;
        }

        String pattern1 = formFieldJsonObject1.get(PATTERN, JsonValue.NULL).getValue(String.class);
        String pattern2 = formFieldJsonObject2.get(PATTERN, JsonValue.NULL).getValue(String.class);

        if (mergeStrategy == MergeStrategy.SOFTEN) {
            if (pattern1 == null && !formField1mustBeNull || pattern2 == null && !formField2mustBeNull) {
                result = result.remove(PATTERN);
            } else {

                String mergedPattern = softenPattern(pattern1, pattern2);
                result = result.put(PATTERN, mergedPattern);
            }
        } else {

            Pattern hardenedPattern = hardenPattern(formField1.schema().pattern() == null ? formField2.schema().pattern() : formField1.schema().pattern(), formField2.schema().pattern() == null ? formField1.schema().pattern() : formField2.schema().pattern());
            if (hardenedPattern != null) {
                result = result.put(PATTERN, hardenedPattern.asString());
            }
        }

        return result;
    }

    protected boolean isUnion(String regex) {
        return UNION_PATTERN.compiled().asPredicate().test(regex);
    }

    protected boolean isPartOfUnion(String supersetPattern, String subsetPattern) {
        Matcher unionMatcher = UNION_PATTERN.compiled().matcher(supersetPattern);

        boolean isUnion = unionMatcher.matches();

        if (isUnion) {

            String firstPatterninUnion = unionMatcher.group(1);
            if (isSuperSetPattern(firstPatterninUnion, subsetPattern)) {
                return true;
            }
            String allButFirstInUnion = unionMatcher.group(2);

            if (isSuperSetPattern(allButFirstInUnion, subsetPattern)) {
                return true;
            }
        }
        return false;
    }

    protected boolean isPartOfSubsetInsideParenteses(String supersetPattern, String subsetPattern) {
        boolean isSurroundedByParenteses = supersetPattern.startsWith("(") && supersetPattern.endsWith(")");

        if (isSurroundedByParenteses) {

            String supersetRegexInsideParenteses = supersetPattern.substring(1, supersetPattern.length() - 1);

            if (isSuperSetPattern(supersetRegexInsideParenteses, subsetPattern)) {
                return true;
            }
        }
        return false;
    }

    public boolean isSuperSetPattern(String supersetPattern, String subsetPattern) {

        if (supersetPattern.equals(subsetPattern)) {
            return true;
        }

        if (".*".equals(supersetPattern)) {
            return true;
        }

        // superset = "(A)" and subset = "A" -> true
        if (isPartOfSubsetInsideParenteses(supersetPattern, subsetPattern)) {
            return true;
        }

        // superset = "A|B" and subset = "A" -> true
        if (isPartOfUnion(supersetPattern, subsetPattern)) {
            return true;
        }

        // superset = "A(WHATEVER)*" and subset = "A" -> true
        Pattern lastPartIrrelevant = LAST_PART_OPTIONAL_PATTERN;
        Matcher matcher = lastPartIrrelevant.compiled().matcher(supersetPattern);

        if (matcher.matches()) {
            String firstPart = matcher.group(1); // (.+)
            if (isSuperSetPattern(firstPart, subsetPattern)) {
                return true;
            }
        }
        return false;
    }

    public String softenPattern(String pattern1, String pattern2) {

        String mergedPattern;

        if (pattern1 == null) {
            mergedPattern = pattern2;
        } else if (pattern2 == null) {
            mergedPattern = pattern1;
        } else if (pattern1.equals(pattern2)) {
            mergedPattern = pattern1;
        } else if (".*".equals(pattern1) || ".*".equals(pattern2)) {
            mergedPattern = ".*";
        } else if (isSuperSetPattern(pattern1, pattern2)) {
            mergedPattern = pattern1;
        } else if (isSuperSetPattern(pattern2, pattern1)) {
            mergedPattern = pattern2;
        } else {
            String[] statusArray = pattern1.split("[|]");
            JsonObject patterns = JsonObject.EMPTY;
            for (int index = 0; index < statusArray.length; index++) {
                patterns = patterns.put(statusArray[index], statusArray[index]);
            }

            statusArray = pattern2.split("[|]");
            for (int index = 0; index < statusArray.length; index++) {
                patterns = patterns.put(statusArray[index], statusArray[index]);
            }

            StringBuilder builder = new StringBuilder();
            String delimiter = "";
            for (JsonValue jsonValue : patterns.keys().sort().asList()) {
                String pattern = jsonValue.getValue(String.class);
                builder.append(delimiter);
                builder.append(pattern);
                delimiter = "|";
            }
            mergedPattern = builder.toString();
        }
        return mergedPattern;
    }

    public Pattern hardenPattern(Pattern pattern1, Pattern pattern2) {

        // "(?=ENUM_1|ENUM_2)" + "ENUM_2" -> "ENUM_2"
        // "ENUM_1" + "ENUM_2" -> "(?=ENUM_1)(?=ENUM_2)"

        Pattern resultingPattern;

        if (pattern1 == null) {
            resultingPattern = pattern2;
        } else if (pattern2 == null) {
            resultingPattern = pattern1;
        } else if (pattern1.asString().equals(".*")) {
            resultingPattern = pattern2;
        } else if (pattern2.asString().equals(".*")) {
            resultingPattern = pattern1;
        } else if (pattern1.equals(pattern2)) {
            resultingPattern = pattern1;
        } else {
            resultingPattern = hardenPatterns(pattern1, pattern2);
        }
        return resultingPattern;
    }

    private Pattern hardenPatterns(Pattern pattern1, Pattern pattern2) {

        JsonArray andOperators1 = splitAndOperators(pattern1);
        JsonArray andOperators2 = splitAndOperators(pattern2);

        return and(
                unionSet(andOperators1, andOperators2));
    }

    /**
     * Possibly removing parts of OR statement
     *
     * @param pattern1
     * @param pattern2
     * @return
     */
    public Pattern hardenOR(Pattern pattern1, Pattern pattern2) {
        JsonArray array1 = JsonArray.of(pattern1.asString().split("[|]"));
        JsonArray array2 = JsonArray.of(pattern2.asString().split("[|]"));
        JsonArray intersectionSet = intersectionSet(array1, array2).sort();
        StringBuilder builder = null;
        for (String s : intersectionSet.asList(String.class)) {
            if (builder == null) {
                builder = new StringBuilder();
            } else {
                builder.append("|");
            }
            builder.append(s);
        }
        return builder == null ? null : JsonValue.of(builder.toString()).as(Pattern.class);
    }

    public JsonArray intersectionSet(JsonArray jsonArray1, JsonArray jsonArray2) {

        JsonObjectBuilder intersection = JsonObject.EMPTY.builder();
        for (JsonValue jsonValue2 : jsonArray2) {
            if (jsonArray1.contains(jsonValue2)) {
                intersection.put(jsonValue2.asJson(), jsonValue2);
            }
        }

        for (JsonValue jsonValue1 : jsonArray1) {
            if (jsonArray2.contains(jsonValue1)) {
                intersection.put(jsonValue1.asJson(), jsonValue1);
            }
        }
        return intersection.build().values();
    }

    /**
     * @param jsonArray1
     * @param jsonArray2
     * @return
     */
    public JsonArray unionSet(JsonArray jsonArray1, JsonArray jsonArray2) {

        JsonObjectBuilder union = JsonObject.EMPTY.builder();
        for (JsonValue jsonValue2 : jsonArray2) {
            union.put(jsonValue2.asJson(), jsonValue2);
        }

        for (JsonValue jsonValue1 : jsonArray1) {
            union.put(jsonValue1.asJson(), jsonValue1);
        }
        return union.build().values().sort();
    }

    public Pattern hardenAND(Pattern andPattern1, Pattern andPattern2) {
        JsonArray pattern1 = splitAndOperators(andPattern1);
        JsonArray pattern2 = splitAndOperators(andPattern2);
        return and(intersectionSet(pattern1, pattern2));
    }


    public JsonObject arrayToObject(JsonArray jsonArray) {
        JsonObjectBuilder jsonObjectBuilder = JsonObject.EMPTY.builder();
        for (JsonValue jsonValue : jsonArray) {
            jsonObjectBuilder.put(jsonValue.getString(), jsonValue);
        }
        return jsonObjectBuilder.build();
    }

    public JsonObject intersectionOf(JsonObject jsonObject1, JsonObject jsonObject2) {
        JsonObjectBuilder jsonObjectBuilder = JsonObject.EMPTY.builder();
        for (JsonValue key : jsonObject1.keys()) {
            JsonValue jsonValue = jsonObject2.get(key.getString());
            if (jsonValue != null) {
                jsonObjectBuilder.put(jsonValue.getValue(String.class), jsonValue);
            }
        }
        return jsonObjectBuilder.build();
    }

    public String simplifyIntersectionSet(JsonArray intersectionSet) {

        JsonArray originalSetAsArray = JsonArray.of(intersectionSet.stream().map(pattern -> JsonArray.of(pattern.getString().split("[|]"))).toArray());

        JsonArray originalSetWithObjects = JsonArray.of(originalSetAsArray.stream().map(jsonValue -> arrayToObject(jsonValue.getValue(JsonArray.class))).toArray());

        JsonObject candidateSet = originalSetWithObjects.head().getValue(JsonObject.class);
        for (JsonValue otherCandidateJsonValue : originalSetWithObjects.tail()) {
            JsonObject otherCandidateSet = otherCandidateJsonValue.getValue(JsonObject.class);
            candidateSet = intersectionOf(candidateSet, otherCandidateSet);
        }

        JsonArray sorted = candidateSet.keys().sort();
        StringBuilder stringBuilder = null;
        for (JsonValue elem : sorted) {
            if (stringBuilder == null) {
                stringBuilder = new StringBuilder();
            } else {
                stringBuilder.append("|");
            }
            stringBuilder.append(elem.getValue(String.class));
        }
        return stringBuilder == null ? null : stringBuilder.toString();
    }

    private Pattern and(JsonArray intersectionSet) {
        StringBuilder builder = null;
        for (Pattern pattern : intersectionSet.asList(Pattern.class)) {
            if (builder == null) {
                builder = new StringBuilder("(?=");
            } else {
                builder.append(")(?=");
            }
            builder.append(pattern.asString());
        }
        return builder == null ?
                null :
                JsonValue.of(builder.append(")").toString()
                ).as(Pattern.class);
    }

    public JsonArray splitAndOperators(Pattern intersectionPattern) {
        Pattern splitter = AND_OPERATOR_SEPARATOR_PATTERN;

        String[] splitted = splitter.compiled().split(intersectionPattern.asString());

        if (splitted.length > 1) {
            splitted[0] = splitted[0].substring("(?=".length());
            String last = splitted[splitted.length - 1];
            splitted[splitted.length - 1] = last.substring(0, last.length() - ")".length());
        } else {
            splitted[0] = simplify(splitted[0]);
        }
        return JsonArray.of(splitted);
    }

    public String simplify(String pattern) {

        String result = pattern;
        Matcher matcher = SIMPLIFICATION_PATTERN.compiled().matcher(pattern);
        if (matcher.matches()) {
            result = matcher.group(1);
        }
        return result;
    }
}
