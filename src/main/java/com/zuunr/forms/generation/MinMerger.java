/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.generation;

import com.zuunr.forms.FormField;
import com.zuunr.forms.formfield.Min;

/**
 * @author Niklas Eldberger
 */
public class MinMerger {

    public Min soften(FormField formField1, FormField formField2) {

        Min min;

        Min min1 = formField1.schema().min();
        Min min2 = formField2.schema().min();

        if (formField1.schema().mustBeNull() != null && formField1.schema().mustBeNull()) {
            min = min2;
        } else if (formField2.schema().mustBeNull() != null && formField2.schema().mustBeNull()) {
            min = min1;
        } else if (min1 == null || min2 == null) {
            min = null;
        } else {
            if (min1.asJsonValue().compareTo(min2.asJsonValue()) < 0) {
                min = min1;
            } else {
                min = min2;
            }
        }
        return min;
    }

    public Min harden(FormField formField1, FormField formField2) {

        Min min;

        Min min1 = formField1.schema().min();
        Min min2 = formField2.schema().min();

        if (formField1.schema().mustBeNull() != null && formField1.schema().mustBeNull()) {
            min = null;
        } else if (formField2.schema().mustBeNull() != null && formField2.schema().mustBeNull()) {
            min = null;
        } else if (min1 == null && min2 == null) {
            min = null;
        } else {
            if (min1 == null) {
                min = min2;
            } else if (min2 == null) {
                min = min1;
            } else if (min1.asJsonValue().compareTo(min2.asJsonValue()) > 0) {
                min = min1;
            } else {
                min = min2;
            }
        }
        return min;
    }
}
