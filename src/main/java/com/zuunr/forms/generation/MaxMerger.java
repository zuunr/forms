/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.generation;

import com.zuunr.forms.FormField;
import com.zuunr.forms.formfield.Max;

/**
 * @author Niklas Eldberger
 */
public class MaxMerger {

    public Max soften(FormField formField1, FormField formField2) {

        Max max;

        Max max1 = formField1.schema().max();
        Max max2 = formField2.schema().max();


        if (formField1.schema().mustBeNull() != null && formField1.schema().mustBeNull()) {
            max = max2;
        } else if (formField2.schema().mustBeNull() != null && formField2.schema().mustBeNull()) {
            max = max1;
        } else if (max1 == null || max2 == null) {
            max = null;
        } else {
            if (max1.asJsonValue().compareTo(max2.asJsonValue()) > 0) {
                max = max1;
            } else {
                max = max2;
            }
        }
        return max;
    }

    public Max harden(FormField formField1, FormField formField2) {

        Max max;

        Max max1 = formField1.schema().max();
        Max max2 = formField2.schema().max();

        if (formField1.schema().mustBeNull() != null && formField1.schema().mustBeNull()) {
            max = null;
        } else if (formField2.schema().mustBeNull() != null && formField2.schema().mustBeNull()) {
            max = null;
        } else if (max1 == null && max2 == null) {
            max = null;
        } else {
            if (max1 == null) {
                max = max2;
            } else if (max2 == null) {
                max = max1;
            } else if (max1.asJsonValue().compareTo(max2.asJsonValue()) < 0) {
                max = max1;
            } else {
                max = max2;
            }
        }
        return max;
    }
}
