/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.generation;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectBuilder;
import com.zuunr.json.JsonValue;

/**
 * @author Niklas Eldberger
 */
public class StringPropertyMerger {

    private static final StringPropertyEnumMerger STRING_PROPERTY_ENUM_MERGER = new StringPropertyEnumMerger();
    private static final StringPropertyPatternMerger STRING_PROPERTY_PATTERN_MERGER = new StringPropertyPatternMerger();
    private static final EnumMerger ENUM_MERGER = new EnumMerger();

    public JsonObject unionOf(JsonObject property1, JsonObject property2) {
        JsonObjectBuilder result = JsonObject.EMPTY.builder();

        constUnionOf(property1, property2, result);
        enumUnionOf(property1, property2, result);
        patternUnionOf(property1, property2, result);
        return result.build();
    }

    private void constUnionOf(JsonObject property1, JsonObject property2, JsonObjectBuilder result) {
        JsonValue const1 = property1.get("const");
        JsonValue const2 = property2.get("const");

        if (const1 != null && const1.equals(const2)) {
            result.put("const", const1);
        } else if (const1 != null && const2 != null) {
            result.put("enum", ENUM_MERGER.unionOf(JsonArray.of(const1), JsonArray.of(const2)));
        }
    }

    private void enumUnionOf(JsonObject property1, JsonObject property2, JsonObjectBuilder result) {
        if (property1.get("enum") != null || property2.get("enum") != null) {
            JsonArray enum1 = effectiveEnumOf(property1);
            JsonArray enum2 = effectiveEnumOf(property2);

            if (enum1 != null && enum2 != null) {
                result.put("enum", ENUM_MERGER.unionOf(enum1, enum2));
            }
        }
    }

    private void patternUnionOf(JsonObject property1, JsonObject property2, JsonObjectBuilder result) {
        String resultingPattern = null;
        String pattern1 = property1.get("pattern", JsonValue.NULL).getString();
        String pattern2 = property2.get("pattern", JsonValue.NULL).getString();
        if (pattern1 != null) {
            if (pattern2 == null) {
                pattern2 = effectivePatternOf(property2);
            }
            resultingPattern = unionOf(pattern1, pattern2);
        } else if (pattern2 != null) {
            pattern1 = effectivePatternOf(property1);
            resultingPattern = unionOf(pattern1, pattern2);
        }

        if (resultingPattern != null) {
            result.put("pattern", resultingPattern);
        }
    }


    private JsonArray effectiveEnumOf(JsonObject property) {
        JsonValue constant = property.get("const");
        if (constant != null) {
            return JsonArray.of(constant);
        }
        return property.get("enum", JsonValue.NULL).getValue(JsonArray.class);
    }

    private String effectivePatternOf(JsonObject property) {
        JsonArray enumeration = effectiveEnumOf(property);
        if (enumeration != null) {
            return STRING_PROPERTY_ENUM_MERGER.regexOf(enumeration);
        }
        return property.get("pattern", JsonValue.NULL).getString();
    }


    public JsonObject intersectionOf(JsonObject property1, JsonObject property2) {
        JsonObjectBuilder result = JsonObject.EMPTY.builder();

        JsonArray enumFromConstIntersection = constIntersectionOf(property1, property2, result);
        JsonArray intersectionEnum = enumIntersectionOf(property1, property2);

        if (intersectionEnum != null && enumFromConstIntersection != null) {
            intersectionEnum = ENUM_MERGER.intersectionOf(intersectionEnum, enumFromConstIntersection);
            result.put("enum", intersectionEnum);
        } else if (intersectionEnum != null) {
            result.put("enum", intersectionEnum);
        } else if (enumFromConstIntersection != null) {
            result.put("enum", enumFromConstIntersection);
        }

        String pattern1 = property1.get("pattern", JsonValue.NULL).getString();
        String pattern2 = property2.get("pattern", JsonValue.NULL).getString();


        if (pattern1 == null) {
            if (pattern2 != null) {
                result.put("pattern", pattern2);
            }
        } else {
            if (pattern2 == null) {
                result.put("pattern", pattern1);
            } else {
                result.put("pattern", STRING_PROPERTY_PATTERN_MERGER.intersectionOf(JsonValue.of(pattern1), JsonValue.of(pattern2)));
            }
        }
        return result.build();
    }

    private JsonArray enumIntersectionOf(JsonObject property1, JsonObject property2) {
        JsonArray enumIntersection = null;

        JsonArray enum1 = property1.get("enum", JsonValue.NULL).getValue(JsonArray.class);
        JsonArray enum2 = property2.get("enum", JsonValue.NULL).getValue(JsonArray.class);

        if (enum1 == null) {
            if (enum2 != null) {
                enumIntersection = enum2;
            }
        } else {
            if (enum2 == null) {
                enumIntersection = enum1;
            } else {
                enumIntersection = ENUM_MERGER.intersectionOf(enum1, enum2);
            }
        }

        return enumIntersection;
    }

    private JsonArray constIntersectionOf(JsonObject property1, JsonObject property2, JsonObjectBuilder result) {
        JsonArray intersectionEnum = null;

        JsonValue const1 = property1.get("const");
        JsonValue const2 = property2.get("const");


        if (const1 == null) {
            if (const2 != null) {
                result.put("const", const2);
            }
        } else {
            if (const2 == null) {
                result.put("const", const1);
            } else {
                intersectionEnum = ENUM_MERGER.intersectionOf(JsonArray.of(const1), JsonArray.of(const2));
            }
        }
        return intersectionEnum;
    }

    public String unionOf(String pattern1, String pattern2) {
        return STRING_PROPERTY_PATTERN_MERGER.unionOf(pattern1, pattern2);
    }
}
