/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.generation;

import com.zuunr.forms.formfield.Min;
import com.zuunr.forms.formfield.MinSize;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.JsonValueUtil;

/**
 * @author Niklas Eldberger
 */
public class MinimumMerger {

    private JsonValueUtil jsonValueUtil = new JsonValueUtil();

    public JsonObject mergeMinimum(String minimumName, JsonObject formField1, JsonObject formField2, JsonObject result, MergeStrategy mergeStrategy) {

        Boolean formField1mustBeNull = formField1.get("mustBeNull", JsonValue.FALSE).getValue(Boolean.class);
        Boolean formField2mustBeNull = formField2.get("mustBeNull", JsonValue.FALSE).getValue(Boolean.class);

        boolean restrictionApplicable = !(formField1mustBeNull && formField2mustBeNull);

        if (restrictionApplicable) {
            // min
            Integer formField1min = formField1.get(minimumName, JsonValue.NULL).asInteger();
            Integer formField2min = formField2.get(minimumName, JsonValue.NULL).asInteger();

            if (formField1min == null && !formField1mustBeNull || formField2min == null && !formField2mustBeNull) {
                result = result.remove(minimumName);
            } else {
                if (formField1min == null) {
                    result = result.put(minimumName, formField2min);
                } else if (formField2min == null) {
                    result = result.put(minimumName, formField1min);
                } else if (formField1min < formField2min) {
                    result = result.put(minimumName, formField1min);
                } else {
                    result = result.put(minimumName, formField2min);
                }
            }
        }
        return result;
    }


    public MinSize intersect(MinSize minSize1, MinSize minSize2) {
        if (minSize1 == null) {
            return minSize2;
        } else if (minSize2 == null) {
            return minSize1;
        }
        JsonValue result = intersect(minSize1.asJsonValue(), minSize2.asJsonValue());
        return result == null? null : result.as(MinSize.class);
    }

    public Min intersect(Min min1, Min min2) {
        if (min1 == null) {
            return min2;
        } else if (min2 == null) {
            return min1;
        }
        JsonValue result = intersect(min1.asJsonValue(), min2.asJsonValue());
        return result == null? null : result.as(Min.class);
    }

    public JsonValue intersect(JsonValue min1, JsonValue min2) {
        if (min1 == null) {
            return min2;
        } else if (min2 == null) {
            return min1;
        }

        if (min1.equals(JsonValue.NULL) || min2.equals(JsonValue.NULL)) {
            throw new NullPointerException("JsonValue.NULL is not allowed");
        }

        return jsonValueUtil.maxOf(min1, min2);
    }
}
