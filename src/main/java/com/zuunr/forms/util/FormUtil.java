/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.util;

import com.zuunr.forms.Form;
import com.zuunr.forms.FormField;
import com.zuunr.forms.FormFields;
import com.zuunr.forms.ValueFormat;
import com.zuunr.forms.formfield.Options;
import com.zuunr.forms.formfield.Type;
import com.zuunr.forms.formfield.options.Option;
import com.zuunr.forms.formfield.options.ValidationStep;
import com.zuunr.forms.formfield.options.ValidationSteps;
import com.zuunr.forms.formfield.options.Value;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonArrayBuilder;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;

import java.util.Comparator;

public final class FormUtil {

    public static final int getFormFieldIndex(String formFieldName, JsonArray formFields) {

        return formFields
                .getIndexOfFirstMatch(jsonValue ->
                        jsonValue.getValue(JsonObject.class)
                                .get("name").getValue(String.class).equals(formFieldName));
    }

    public static final JsonArray getFormFieldPath(JsonArray formFieldNames, ValueFormat valueFormat) {

        valueFormat = valueFormat.asExplicitValueFormat();

        if (formFieldNames.isEmpty()) {
            return JsonArray.EMPTY;
        }

        JsonArrayBuilder result = null;

        for (JsonValue key : formFieldNames) {
            if (result == null) {
                result = JsonArray.EMPTY.builder();
            }

            if (key.isInteger()) {
                valueFormat = valueFormat.element().asExplicitValueFormat();
                result.add("element");
            } else {
                valueFormat = valueFormat.form().asObjectValueFormat();
                result.add("form").add("value").add(getFormFieldIndex(key.getString(), valueFormat.form().value()));
            }
        }
        
        if (result == null) {
            // FIXME Should result be nullable? Shouldn't the method return an empty JsonArray, e.g. result = JsonArray.EMPTY.builder();
            throw new IllegalStateException("No form fields");
        }
        
        return result.build();

    }

    public static ValueFormat putValueFormat(JsonArray fieldNamePath, ValueFormat toBeInserted, ValueFormat toBeUpdated) {

        ValueFormat result;

        if (toBeUpdated == null) {
            toBeUpdated = ValueFormat.EMPTY;
        }

        if (fieldNamePath.size() == 0) {
            result = toBeInserted;
        } else {
            JsonValue key = fieldNamePath.head();

            if (key.isInteger()) {

                ValueFormat elementValueFormat = nestedToBeUpdated(fieldNamePath, toBeUpdated.element());

                result = toBeUpdated.builder()
                        .element(
                                putValueFormat(fieldNamePath.tail(), toBeInserted, elementValueFormat))
                        .build();
            } else {

                String fieldName = key.getString();

                Form form = toBeUpdated.form() == null ? Form.EMPTY : toBeUpdated.form();
                FormFields formFields = form.formFields() == null ? FormFields.EMPTY : form.formFields();
                int fieldIndex = FormUtil.getFormFieldIndex(fieldName, form.formFields().asJsonArray());
                int indexOfNewOrUpdatedFormField = fieldIndex == -1 ? form.formFields().asJsonArray().size() : fieldIndex;

                Type newFieldType = newFieldType(fieldNamePath);
                FormField formField = getExistingOrCreateNewFormField(fieldName, newFieldType, fieldIndex, formFields);

                ValueFormat nextToBeupdated = nestedToBeUpdated(fieldNamePath, formField.asValueFormat());

                FormField updatedFormField = putValueFormat(fieldNamePath.tail(), toBeInserted, nextToBeupdated).asFormField(fieldName);

                FormFields updatedFormFields = form.formFields().asJsonArray()
                        .put(indexOfNewOrUpdatedFormField, updatedFormField.asJsonValue())
                        .as(FormFields.class);

                result = toBeUpdated.builder()
                        .type(Type.OBJECT)
                        .form(form.builder()
                                .value(updatedFormFields).build()).build();
            }
        }
        return result;
    }

    private static Type newFieldType(JsonArray fieldNamePath) {
        if (fieldNamePath.size() > 1) {
            return Type.OBJECT;
        }
        return Type.STRING;
    }

    private static FormField getExistingOrCreateNewFormField(String fieldName, Type newType, int fieldIndex, FormFields formFields) {
        FormField formField;
        if (fieldIndex == -1) {

            FormField.Builder builder = FormField.builder(fieldName).type(newType);
            if (newType.isObject()) {
                builder.form(Form.EMPTY);
            }
            formField = builder.build();
        } else {
            formField = formFields.asJsonArray().get(fieldIndex).as(FormField.class);
        }
        return formField;
    }

    private static ValueFormat nestedToBeUpdated(JsonArray fieldNamePath, ValueFormat nestedValueFormatToBeUpdated) {
        if (nestedValueFormatToBeUpdated == null) {
            nestedValueFormatToBeUpdated = ValueFormat.EMPTY;
        }
        if (fieldNamePath.size() <= 1) {
            return nestedValueFormatToBeUpdated;
        } else if (fieldNamePath.get(1).isInteger()) {
            if (nestedValueFormatToBeUpdated.type().isArrayOrSet()) {
                return nestedValueFormatToBeUpdated;
            } else {
                return nestedValueFormatToBeUpdated.builder().type(Type.ARRAY).build();
            }
        } else if (fieldNamePath.get(1).isString()) {
            if (nestedValueFormatToBeUpdated.type().isObject()) {
                return nestedValueFormatToBeUpdated;
            } else {
                return nestedValueFormatToBeUpdated.builder().type(Type.OBJECT).build();
            }
        } else {
            throw new RuntimeException("Only integer and strings are supported");
        }
    }

    public static ValueFormat replaceFormWithOptionForm(ValueFormat toBeUpdated) {
        ValueFormat result;
        Option option = asOptionWithFormFieldNamesAsLabel(toBeUpdated.form());
        Value value = Value.EMPTY.builder().add(option).build();
        result = toBeUpdated.builder().options(Options.builder().value(value).build()).build().asJsonObject().remove("form").as(ValueFormat.class);
        return result;
    }

    public static Option asOptionWithFormFieldNamesAsLabel(Form form) {
        JsonArray sortedFormFields = form.formFields().asJsonArray()
                .sort(Comparator.comparing(a -> ((JsonValue) a).getValue(JsonObject.class).get("name")));
        StringBuilder optionNameBuilder = null;
        for (FormField formField : sortedFormFields.asList(FormField.class)) {
            if (optionNameBuilder == null) {
                optionNameBuilder = new StringBuilder();
            } else {
                optionNameBuilder.append(",");
            }
            optionNameBuilder.append(formField.name());
        }
        String labelName = optionNameBuilder == null ? "" : optionNameBuilder.toString();
        return Option.builder(labelName).format(form.asObjectValueFormat()).build();
    }



    public static Form makeFormFieldTypesUndefined(Form form) {

        FormFields.FormFieldsBuilder formFieldsBuilder = FormFields.EMPTY.builder();
        for (FormField formField : form.asExplicitForm().formFields().asList()) {
            formFieldsBuilder.add(formField.builder().type(Type.UNDEFINED).build());
        }

        return form.builder().value(formFieldsBuilder.build()).build();
    }

    public static Form makeNonFilterFieldsOptional(Form form) {

        FormFields.FormFieldsBuilder formFieldsBuilder = FormFields.EMPTY.builder();
        for (FormField formField : form.asExplicitForm().formFields().asList()) {
            if (formField.name().startsWith("value._.")) {
                formFieldsBuilder.add(formField);
            } else {
                formFieldsBuilder.add(formField.builder().required(false).build());
            }
        }

        return form.builder().value(formFieldsBuilder.build()).build();
    }

    public static ValueFormat replaceAllValidationStepsOfIndex(ValueFormat toBeUpdated, int stepIndex, ValidationStep validationStepToBeInserted) {

        Value.Builder optionsValueBuilder = Value.EMPTY.builder();

        for (Option option : toBeUpdated.options().value().asList()) {
            optionsValueBuilder.add(replaceValidationStepsOfIndex(option, stepIndex, validationStepToBeInserted));
        }

        return toBeUpdated.builder().options(toBeUpdated.options().builder().value(optionsValueBuilder.build()).build()).build();
    }

    public static Option replaceValidationStepsOfIndex(Option option, int stepIndex, ValidationStep validationStepToBeInserted) {
        return option.builder().validationSteps(option.validationSteps().asJsonValue().put(JsonArray.of(stepIndex), validationStepToBeInserted.asJsonValue()).as(ValidationSteps.class)).build();
    }

    private FormUtil() {
    }

    public static Form removeFilterFieldsAndMakeOtherFieldsOptional(Form form) {
        FormFields.FormFieldsBuilder formFieldsBuilder = FormFields.EMPTY.builder();
        for (FormField formField : form.asExplicitForm().formFields().asList()) {
            if (!formField.name().startsWith("value._.")) {
                formFieldsBuilder.add(formField.builder().required(false).build());
            }
        }
        return form.builder().value(formFieldsBuilder.build()).build();
    }

    public static Form makeFilterFormFieldsRequired(Form form) {
        FormFields.FormFieldsBuilder formFieldsBuilder = FormFields.EMPTY.builder();
        for (FormField formField : form.asExplicitForm().formFields().asList()) {
            if (formField.name().startsWith("value._.")) {
                formFieldsBuilder.add(formField.builder().required(true).build());
            } else {
                formFieldsBuilder.add(formField);
            }
        }
        return form.builder().value(formFieldsBuilder.build()).build();
    }
}
