/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.util;

import java.util.function.UnaryOperator;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;

/**
 * @author Niklas Eldberger
 */
public final class LinkExtender {

    public JsonObject decorateLinkedResources(JsonObject resource, JsonArray patternForHrefsToBeDecorated, UnaryOperator<JsonObject> decorator) {
        return decorateAllPathsButSelf(resource, patternForHrefsToBeDecorated, decorator);
    }

    public JsonObject decorateAllPaths(JsonObject original, JsonArray pathsWithHrefs, UnaryOperator<JsonObject> hrefObjectDecorator) {

        JsonObject result = original;
        for (JsonArray path : pathsWithHrefs.asList(JsonArray.class)) {
            result = decoratePath(result, path, hrefObjectDecorator);
        }
        return result;
    }

    public JsonObject decorateAllPathsButSelf(JsonObject original, JsonArray pathsWithHrefs, UnaryOperator<JsonObject> hrefObjectDecorator) {

        JsonObject result = original;
        for (JsonArray path : pathsWithHrefs.asList(JsonArray.class)) {
            if (path.size() != 1) {
                result = decoratePath(result, path, hrefObjectDecorator);
            }
        }
        return result;
    }

    private JsonObject decoratePath(JsonObject original, JsonArray pathWithHref, UnaryOperator<JsonObject> hrefObjectDecorator) {

        JsonObject result = original;

        JsonValuePathsFinder pathsFinder = new JsonValuePathsFinder();

        JsonArray paths;
        if (pathWithHref.size() == 1) {
            // pathWithHref is always ["href"] in this case
            result = doDecorate(result, hrefObjectDecorator);
        } else {
            paths = pathsFinder.getPaths(original.jsonValue(), pathWithHref.allButLast(), true);

            for (JsonArray path : paths.asList(JsonArray.class)) {
                if (!JsonValue.NULL.equals(path.last())) {
                    JsonObject decorated = doDecorate(path.last().getValue(JsonObject.class), hrefObjectDecorator);
                    result = result.put(path.allButLast(), decorated);
                }
            }
        }
        return result;
    }

    private JsonObject doDecorate(JsonObject original, UnaryOperator<JsonObject> hrefObjectDecorator) {
        return hrefObjectDecorator.apply(original);
    }
}
