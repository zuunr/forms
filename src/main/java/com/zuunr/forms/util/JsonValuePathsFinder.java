/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.util;

import com.zuunr.forms.Form;
import com.zuunr.forms.filter.FilterResult;
import com.zuunr.forms.filter.FormFilter;
import com.zuunr.forms.filter.result.Waste;
import com.zuunr.forms.formfield.Pattern;
import com.zuunr.forms.validation.FormFieldValidationResult;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.util.JsonObjectWrapper;

import java.util.Iterator;
import java.util.Stack;

/**
 * @author Niklas Eldberger
 */
public class JsonValuePathsFinder {

    private static final FormFilter formFilter = new FormFilter();
    private static final FilterResult<JsonObjectWrapper> NON_EMPTY_FILTER_RESULT = new FilterResult<>(
            null,
            Waste.builder().add(
                    FormFieldValidationResult.builder()
                            .valid(false)
                            .badValue(JsonValue.of("badValue"))
                            .path(JsonArray.EMPTY)
                            .addParamToFiltrate(true)
                            .description("description")
                            .filtrate(null)
                            .build()).build());

    private static final JsonValue PATTERN_ANY_FIELD_NAME = JsonValue.of(".*");

    public JsonArray getPaths(JsonValue source, JsonArray pathsPattern, boolean includeValueLast) {

        Stack<JsonObject> stack = new Stack<>();
        stack.push(JsonObject.EMPTY
                .put("pathsPattern", pathsPattern) //eg ["friends",2,{"value":}]
                .put("source", source)
                .put("path", JsonArray.EMPTY)
        );
        JsonArray resultingPaths = JsonArray.EMPTY; // [{path: ["a",".*",1], value:{"a":"b"}}]

        while (!stack.empty()) {

            JsonObject pathPatternObject = stack.pop();
            JsonArray pPattern = pathPatternObject.get("pathsPattern").getValue(JsonArray.class);
            JsonArray path = pathPatternObject.get("path").getValue(JsonArray.class);
            JsonValue currentSource = pathPatternObject.get("source");
            JsonValue fieldOrIndexPattern = pPattern.head();

            if (fieldOrIndexPattern.is(String.class)) {

                resultingPaths = handleFieldName(stack, currentSource, pPattern, fieldOrIndexPattern, includeValueLast, resultingPaths, path);
            } else if (fieldOrIndexPattern.is(JsonObject.class) && currentSource.is(JsonObject.class)) {

                resultingPaths = handleJsonObject(stack, currentSource, pPattern, fieldOrIndexPattern, includeValueLast, resultingPaths, path);
            } else if (fieldOrIndexPattern.isInteger()) {
                resultingPaths = handleIndex(stack, currentSource, pPattern, fieldOrIndexPattern, includeValueLast, resultingPaths, path);
            }
        }
        return resultingPaths;
    }

    private JsonArray handleJsonObject(Stack stack, JsonValue currentSource, JsonArray pPattern, JsonValue fieldOrIndexPattern, boolean includeValueLast, JsonArray resultingPaths, JsonArray path) {
        JsonObject currentSourceJsonObject = currentSource.getValue(JsonObject.class);

        Pattern fieldNamePattern = fieldOrIndexPattern.get("fieldNamePattern", PATTERN_ANY_FIELD_NAME).as(Pattern.class);

        Form form = fieldOrIndexPattern.get("objectFormat", JsonValue.NULL).as(Form.class);

        FilterResult<JsonObjectWrapper> result =
                form == null ?
                        NON_EMPTY_FILTER_RESULT :
                        formFilter.filter(form, currentSourceJsonObject, false, true);

        if (result.waste.isEmpty()) {

            JsonArray pPatternTail = pPattern.tail();

            Iterator<JsonValue> fieldValueIter = currentSourceJsonObject.values().iterator();

            for (JsonValue fieldName: currentSourceJsonObject.keys().asList()) {
                JsonValue fieldValue = fieldValueIter.next();
                if (fieldNamePattern.compiled().matcher(fieldName.getValue(String.class)).matches()){

                    resultingPaths = addToStackOrResultingPaths(stack, fieldName, fieldValue, pPatternTail, resultingPaths, path, includeValueLast);
                }
            }
        }
        return resultingPaths;
    }

    private JsonArray handleIndex(Stack stack, JsonValue currentSource, JsonArray pPattern, JsonValue fieldOrIndexPattern, boolean includeValueLast, JsonArray resultingPaths, JsonArray path) {
        if (currentSource.is(JsonArray.class)) {

            JsonArray currentSourceJsonArray = currentSource.getValue(JsonArray.class);

            Integer index = fieldOrIndexPattern.asInteger();
            JsonArray pPatternTail = pPattern.tail();

            if (index == -1) {
                int currentIndex = 0;
                for (JsonValue value : currentSourceJsonArray.asList()) {
                    resultingPaths = addToStackOrResultingPaths(stack, JsonValue.of(currentIndex), value, pPatternTail, resultingPaths, path, includeValueLast);
                    currentIndex++;
                }
            } else if (index >= 0 && currentSourceJsonArray.size() > index) {
                JsonValue value = currentSource.get(index);
                resultingPaths = addToStackOrResultingPaths(stack, JsonValue.of(index), value, pPatternTail, resultingPaths, path, includeValueLast);
            }
        }
        return resultingPaths;
    }

    private JsonArray handleFieldName(Stack stack, JsonValue currentSource, JsonArray pPattern, JsonValue fieldOrIndexPattern, boolean includeValueLast, JsonArray resultingPaths, JsonArray path) {
        if (currentSource.is(JsonObject.class) || JsonValue.NULL.equals(currentSource)) {
            String keyOrIndex = fieldOrIndexPattern.getValue(String.class);
            JsonValue value = currentSource.get(keyOrIndex);
            if (value != null) {
                JsonArray pPatternTail = pPattern.tail();
                resultingPaths = addToStackOrResultingPaths(stack, fieldOrIndexPattern, value, pPatternTail, resultingPaths, path, includeValueLast);
            }
        }
        return resultingPaths;
    }

    private JsonArray addToStackOrResultingPaths(Stack stack, JsonValue indexOrFieldToAdd, JsonValue valueOfIndex, JsonArray pPatternTail, JsonArray resultingPaths, JsonArray path, boolean includeValueLast) {
        if (pPatternTail.isEmpty()) {
            resultingPaths = addToResultingPaths(resultingPaths, path, indexOrFieldToAdd, includeValueLast ? valueOfIndex : null);
        } else {
            stack.push(JsonObject.EMPTY
                    .put("pathsPattern", pPatternTail)
                    .put("path", path.add(indexOrFieldToAdd))
                    .put("source", valueOfIndex));
        }
        return resultingPaths;
    }

    private JsonArray addToResultingPaths(JsonArray resultingPaths, JsonArray pathSoFar, JsonValue keyOrIndex, JsonValue value) {
        if (value != null) {
            resultingPaths = resultingPaths.addFirst(pathSoFar.add(keyOrIndex).add(value));
        } else {
            resultingPaths = resultingPaths.addFirst(pathSoFar.add(keyOrIndex));
        }
        return resultingPaths;
    }
}
