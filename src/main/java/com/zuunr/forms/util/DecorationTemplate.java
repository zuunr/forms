/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.util;

import java.util.Comparator;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

import com.zuunr.forms.Form;
import com.zuunr.forms.FormField;
import com.zuunr.forms.formfield.Pattern;
import com.zuunr.forms.formfield.Type;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectBuilder;
import com.zuunr.json.JsonValue;

/**
 * @author Niklas Eldberger
 */
public final class DecorationTemplate {

    private static final int NUMBER_OF_SLASHES_IN_API_URI = "https://example.com/v1/mysys/api/collection".split("/").length - 1;

    private static final JsonValue FIELDS = JsonValue.of("fields");

    private LinkExtender linkExtender = new LinkExtender();

    private JsonArray hrefPathsPatterns;

    private static Comparator<JsonValue> internalComparator = (jsonValue1, jsonValue2) -> {

        int result = 0;
        JsonArray array1 = jsonValue1.getValue(JsonArray.class);
        JsonArray array2 = jsonValue2.getValue(JsonArray.class);

        if (array1.isEmpty() && array2.isEmpty()) {
            return 0;
        } else if (array1.isEmpty()) {
            return 1;
        } else if (array2.isEmpty()) {
            return -1;
        }

        for (int i = 0; i < array1.size(); i++) {
            JsonValue v1 = array1.get(i);

            if (i < array2.size()) {
                JsonValue v2 = array2.get(i);
                result = v1.compareTo(v2);
                if (result != 0) {
                    return result;
                }
            } else {
                return 1;
            }
        }
        return result;
    };

    private Form form;

    public DecorationTemplate(Form form) {
        this(form.asJsonValue());
    }

    public DecorationTemplate(JsonValue jsonValue) {
        form = jsonValue.as(Form.class);

        Pattern selfPattern = form.formField("href").schema().pattern();
        if (selfPattern != null) {
            hrefPathsPatterns = getHrefPaths();
        }
    }

    public JsonArray getHrefPaths() {
        JsonValue pattern = form.formField("href").schema().pattern().asJsonValue();
        String apiPattern = getApiUriPattern(pattern);
        return getHrefPaths(apiPattern);
    }

    public JsonArray getHrefPaths(String hrefPatternStart) {
        return getHrefPaths(href -> href.startsWith(hrefPatternStart));
    }

    public JsonArray getHrefPaths(Predicate<String> hrefMatcher) {

        JsonObject formFields = formFields(form);

        JsonArray paths = formFields.jsonValue().getPathsAndValue(true);

        JsonArray resultingPaths = JsonArray.EMPTY;

        for (JsonArray path : paths.asList(JsonArray.class)) {

            JsonArray objectOrArrayPath = path.allButLast().allButLast().allButLast();
            JsonValue hrefCandidate = objectOrArrayPath.last();
            if (hrefCandidate.equals(JsonValue.of("href"))) {

                JsonArray patternPathCandidate = path.allButLast();
                if (patternPathCandidate.last().equals(JsonValue.of("pattern"))) {
                    JsonArray objectPath = getObjectPath(patternPathCandidate);

                    FormField hrefFormField = getFormField(form, objectPath);

                    // FIXME Should hrefFormField be nullable?
                    String linkPattern = Optional.ofNullable(hrefFormField)
                            .map(o -> o.schema().pattern().asString())
                            .orElseThrow();

                    if (hrefMatcher.test(linkPattern)) {
                        resultingPaths = resultingPaths.add(objectPath);
                    }
                }
            }
        }
        return resultingPaths.sort(internalComparator);
    }

    protected static String getApiUriPattern(JsonValue pattern) {
        String stringPattern = pattern.getString();

        StringBuilder stringBuilder = new StringBuilder();
        short numberOfSlashes = 0;
        for (int i = 0; i < stringPattern.length() && numberOfSlashes < NUMBER_OF_SLASHES_IN_API_URI; i++) {
            char currentChar = stringPattern.charAt(i);
            if (currentChar == '/') {
                numberOfSlashes++;
            }
            stringBuilder.append(currentChar);
        }
        if (numberOfSlashes == NUMBER_OF_SLASHES_IN_API_URI) {
            return stringBuilder.toString();
        } else {
            return null;
        }
    }

    public FormField getFormField(JsonArray objectPath) {
        return getFormField(form, objectPath);
    }

    private FormField getFormField(Form form, JsonArray objectPath) {

        String head = objectPath.head().getString();
        FormField formField = form.formField(head);

        if (objectPath.tail().isEmpty()) {
            return formField;
        }

        if (formField.schema().type().isArrayOrSet() && objectPath.tail().head().equals(JsonValue.MINUS_ONE)) {
            if (formField.schema().eform() != null) {
                return getFormField(formField.schema().eform(), objectPath.tail().tail());
            }
        } else if (formField.schema().type().isObject()) {
            if (formField.schema().form() != null) {
                return getFormField(formField.schema().form(), objectPath.tail());
            }
        }
        return null;
    }

    private JsonArray getObjectPath(JsonArray path) {

        path = path.addFirst(Type.OBJECT.asJsonValue());

        JsonArray objectPath = JsonArray.EMPTY;

        for (int i = 2; i < path.size(); i = i + 3) {

            if (i >= 2) {
                JsonValue type = path.get(i - 2);

                if (type.equals(Type.OBJECT.asJsonValue())) {
                    if (path.get(i - 1).equals(FIELDS)) {
                        objectPath = objectPath.add(path.get(i));
                    }
                } else if (type.equals(Type.ARRAY.asJsonValue()) || type.equals(Type.SET.asJsonValue())) {
                    if (path.get(i - 1).equals(FIELDS)) {
                        objectPath = objectPath.add(-1).add(path.get(i));
                    }
                }
            }
        }
        return objectPath;
    }

    JsonObject formFields(Form form) {

        JsonObjectBuilder fields = JsonObject.EMPTY.builder();
        for (FormField formField : form.formFields().asList()) {

            if (formField.schema().type() != null && formField.schema().type().isObject()) {
                if (formField.schema().form() != null) {
                    fields.put(
                            formField.name(),
                            JsonObject.EMPTY.put(
                                    formField.schema().type().asJsonValue().getString(), formFields(formField.schema().form())));
                }
            } else if (formField.schema().type() != null && formField.schema().type().isArrayOrSet()) {
                if (formField.schema().eform() != null) {
                    fields.put(
                            formField.name(),
                            JsonObject.EMPTY.put(
                                    formField.schema().type().asJsonValue().getString(), formFields(formField.schema().eform())));
                }
            } else {
                fields.put(formField.name(), JsonObject.EMPTY.put(formField.schema().type() == null ? "NULL" : formField.schema().type().asJsonValue().getString(), formField));
            }
        }
        return JsonObject.EMPTY.put("fields", fields.build()).remove("value");
    }


    public JsonObject decorateLinkedResourcesOfSameApiUri(JsonObject resource, UnaryOperator<JsonObject> decorator) {
        return linkExtender.decorateAllPathsButSelf(resource, hrefPathsPatterns, decorator);
    }
}
