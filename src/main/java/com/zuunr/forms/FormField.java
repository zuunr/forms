/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms;

import com.zuunr.forms.formfield.Enum;
import com.zuunr.forms.formfield.*;
import com.zuunr.forms.formfield.options.ValidationStep;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectBuilder;
import com.zuunr.json.JsonValue;

import java.math.BigDecimal;

/**
 * <p>This class represents the form field in its raw format without applying any default values.</p>
 *
 * @author Niklas Eldberger
 */
public final class FormField extends AbstractValueFormat {

    private String name;
    private Boolean required;
    private ValueFormat schema;

    private FormField explicitFormField;
    private FormField compactFormField;
    private ValueFormat asValueFormat;

    private FormField(JsonObject jsonObject, FormatVerbosity formatVerbosity) {
        super(addNameAndRequired(init(getSchema(jsonObject)), jsonObject.get("name"), jsonObject.get("required")));

        switch (formatVerbosity) {
            case COMPACT: {
                compactFormField = this;
                break;
            }
            case EXPLICIT: {
                explicitFormField = this;
                break;
            }
        }
    }

    public FormField(JsonValue source) {
        super(source.getValue(JsonObject.class));
    }

    public Builder builder() {
        return new Builder(asJsonObject());
    }

    private static JsonValue getSchema(JsonObject jsonObject) {

        JsonObject nameAndRequiredRemoved = jsonObject.remove("name").remove("required");

        JsonValue schema = jsonObject.get("schema");
        if (schema == null) {
            schema = nameAndRequiredRemoved.jsonValue();
        } else if (!nameAndRequiredRemoved.remove("schema").isEmpty()) {
                //throw new RuntimeException("Only schema (new style) OR form field members (old style) is allowed.");
        }

        return schema;
    }

    private static JsonObject addNameAndRequired(JsonValue abstractValueFormat, JsonValue name, JsonValue required) {
        JsonObject formFieldJsonObject = abstractValueFormat.isBoolean() ?
                JsonObject.EMPTY.put("schema", abstractValueFormat) :
                abstractValueFormat.getJsonObject(); // TODO: This one should be removed - form field should only have name, required and schema fields
        formFieldJsonObject = formFieldJsonObject.put("name", name);
        if (required != null) {
            formFieldJsonObject = formFieldJsonObject.put("required", required);
        }
        return formFieldJsonObject;
    }

    public static Builder builder(String formFieldName) {
        return new FormField(JsonObject.EMPTY.builder().put("name", formFieldName).build().jsonValue()).builder();
    }

    public String name() {
        if (name == null) {
            name = asJsonObject().get("name").getString();
        }
        return name;
    }

    public Boolean required() {
        if (required == null) {
            required = asJsonObject().get("required", JsonValue.NULL).getValue(Boolean.class);
        }
        return required;
    }

    public ValueFormat schema() {
        if (schema == null) {
            JsonValue schemaIfNull =  asJsonObject().remove("required").remove("name").jsonValue();
            schema = asJsonObject().get("schema", schemaIfNull).as(ValueFormat.class);
        }
        return schema;
    }

    public FormField asCompactFormField() {
        if (compactFormField == null) {
            compactFormField = new FormField(asCompact().getJsonObject(), FormatVerbosity.COMPACT);
        }
        return compactFormField;
    }

    public FormField asExplicitFormField() {
        if (explicitFormField == null) {

            explicitFormField = new FormField(asExplicit().getJsonObject().put("required", required() != null && required() ? true : false), FormatVerbosity.EXPLICIT);
        }
        return explicitFormField;
    }

    public ValueFormat asValueFormat() {
        if (asValueFormat == null) {
            asValueFormat = asJsonObject().as(ValueFormat.class);
        }
        return asValueFormat;
    }

    @Override
    public JsonObject asJsonObject() {
        return asJsonValue().getJsonObject();
    }

    @Deprecated
    public String desc() {
        return super.desc();
    }

    @Deprecated
    public MinLength minlength() {
        return super.minlength();
    }

    @Deprecated
    public MaxLength maxlength() {
        return super.maxlength();
    }

    @Deprecated
    public MaxSize maxSize() {
        return super.maxSize();
    }

    @Deprecated
    public MinSize minSize() {
        return super.minSize();
    }

    @Deprecated
    public Boolean mustBeNull() {
        return super.mustBeNull();
    }

    @Deprecated
    public Max max() {
        return super.max();
    }

    @Deprecated
    public ExclusiveMaximum exclusiveMaximum() {
        return super.exclusiveMaximum();
    }

    @Deprecated
    public Min min() {
        return super.min();
    }

    @Deprecated
    public ExclusiveMinimum exclusiveMinimum() {
        return super.exclusiveMinimum();
    }

    @Deprecated
    public Boolean nullable() {
        return super.nullable();
    }

    @Deprecated
    public ValueFormat element() {
        return super.element();
    }

    @Deprecated
    public Options options() {
        return super.options();
    }

    @Deprecated
    public Type type() {
        return super.type();
    }

    @Deprecated
    public Pattern pattern() {
        return super.pattern();
    }

    @Deprecated
    public Form form() {
        return super.form();
    }

    @Deprecated
    public String href() {
        return super.href();
    }

    @Deprecated
    public Form eform() {
        return super.eform();
    }

    @Deprecated
    public Enum enumeration() {
        return super.enumeration();
    }

    @Deprecated
    public JsonValue constant() {
        return super.constant();
    }

    @Deprecated
    public ValidationStep asValidationStep() {
        return super.asValidationStep();
    }

    @Deprecated
    public String format() {
        return super.format();
    }

    @Deprecated
    public JsonValue defaultValue() {
        return super.defaultValue();
    }

    @Deprecated
    public boolean alwaysFails() {
        return super.alwaysFails();
    }
    ////////END

    public static class Builder {

        JsonObjectBuilder jsonObjectBuilder;

        private Builder(JsonObject jsonObject) {
            jsonObjectBuilder = jsonObject.builder();
        }

        public FormField.Builder desc(String description) {
            jsonObjectBuilder.put("desc", description);
            return this;
        }

        public FormField.Builder schema(Boolean schema) {
            jsonObjectBuilder.put("schema", schema);
            return this;
        }

        public FormField.Builder defaultValue(JsonValue defaultValue) {
            jsonObjectBuilder.put("default", defaultValue);
            return this;
        }

        public FormField.Builder eform(Form form) {
            this.eformOnly(form);
            return this;
        }

        private void eformOnly(Form form) {
            jsonObjectBuilder.put("eform", form.asJsonValue());
        }

        public FormField.Builder element(AbstractValueFormat elementValueFormat) {
            elementOnly(elementValueFormat);
            JsonValue explicitElement = elementValueFormat.asExplicit();

            if (Type.OBJECT.asJsonValue().equals(explicitElement.get("type"))) {
                Form form = explicitElement.get("form", JsonValue.NULL).as(Form.class);
                if (form != null) {
                    eformOnly(form);
                }
            }
            return this;
        }

        private void elementOnly(AbstractValueFormat elementValueFormat) {
            jsonObjectBuilder.put("element", elementValueFormat.asJsonValue());
        }

        public FormField.Builder equalTo(Equals paths) {
            jsonObjectBuilder.put("equals", paths.asJsonValue());
            return this;
        }

        public FormField.Builder form(Form form) {
            jsonObjectBuilder.put("form", form.asJsonValue());
            return this;
        }

        public FormField.Builder href(String href) {
            jsonObjectBuilder.put("href", href);
            return this;
        }

        public FormField.Builder max(Integer max) {
            jsonObjectBuilder.put("max", max);
            return this;
        }

        public FormField.Builder exclusiveMaximum(Integer exclusiveMaximum) {
            jsonObjectBuilder.put("exclusiveMaximum", exclusiveMaximum);
            return this;
        }

        public FormField.Builder max(BigDecimal max) {
            jsonObjectBuilder.put("max", max);
            return this;
        }

        public FormField.Builder max(String max) {
            jsonObjectBuilder.put("max", max);
            return this;
        }

        public FormField.Builder max(Max max) {
            jsonObjectBuilder.put("max", max);
            return this;
        }

        public FormField.Builder maxlength(Integer maxlength) {
            jsonObjectBuilder.put("maxlength", maxlength);
            return this;
        }

        public FormField.Builder min(Integer min) {
            jsonObjectBuilder.put("min", min);
            return this;
        }

        public FormField.Builder min(Min min) {
            jsonObjectBuilder.put("min", min);
            return this;
        }

        public FormField.Builder exclusiveMinimum(Integer exclusiveMinimum) {
            jsonObjectBuilder.put("exclusiveMinimum", exclusiveMinimum);
            return this;
        }

        public FormField.Builder min(BigDecimal min) {
            jsonObjectBuilder.put("min", min);
            return this;
        }

        public FormField.Builder min(String min) {
            jsonObjectBuilder.put("min", min);
            return this;
        }

        public FormField.Builder minlength(Integer minlength) {
            jsonObjectBuilder.put("minlength", minlength);
            return this;
        }

        public FormField.Builder mustBeNull(Boolean mustBeNull) {
            jsonObjectBuilder.put("mustBeNull", mustBeNull);
            return this;
        }

        public FormField.Builder nullable(Boolean nullable) {
            jsonObjectBuilder.put("nullable", nullable);
            return this;
        }

        public FormField.Builder pattern(String pattern) {
            jsonObjectBuilder.put("pattern", pattern);
            return this;
        }

        public FormField.Builder required(Boolean required) {
            jsonObjectBuilder.put("required", required);
            return this;
        }

        public FormField.Builder type(Type type) {
            jsonObjectBuilder.put("type", type);
            return this;
        }

        public FormField.Builder maxsize(Integer maxsize) {
            jsonObjectBuilder.put("maxsize", maxsize);
            return this;
        }

        public FormField.Builder maxsize(MaxSize maxsize) {
            jsonObjectBuilder.put("maxsize", maxsize);
            return this;
        }

        public FormField.Builder minsize(Integer minsize) {
            jsonObjectBuilder.put("minsize", minsize);
            return this;
        }

        public FormField.Builder minsize(MinSize minsize) {
            jsonObjectBuilder.put("minsize", minsize);
            return this;
        }

        public FormField.Builder options(Options options) {
            jsonObjectBuilder.put("options", options);
            return this;
        }

        public FormField.Builder enumeration(Enum enumeration) {
            jsonObjectBuilder.put("enum", enumeration);
            return this;
        }

        public FormField.Builder constant(JsonValue constant) {
            jsonObjectBuilder.put("const", constant);
            return this;
        }

        public FormField build() {
            FormField result = new FormField(jsonObjectBuilder.build(), FormatVerbosity.UNSPECIFIED);
            validateEformAndElementFormEqual(result);
            return result;
        }

        /**
         * As eform will be removed this method will not be needed anymore
         * @param formField
         */
        private void validateEformAndElementFormEqual(FormField formField) {

            if (formField.asExplicitFormField().type() != null &&
                    formField.asExplicitFormField().type().isArrayOrSet() &&
                    formField.asExplicitFormField().element() != null &&
                    formField.asExplicitFormField().element().form() != null &&
                    formField.asExplicitFormField().eform() != null &&
                    !formField.asExplicitFormField().element().form().equals(formField.asExplicitFormField().eform())) {
                throw new RuntimeException("eform and element.form must be equal");
            }
        }
    }
}
