/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms;

import com.zuunr.forms.formfield.Type;
import com.zuunr.forms.formfield.options.Option;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectMerger;
import com.zuunr.json.JsonValue;

/**
 * @author Niklas Eldberger
 */
public class ValueFormatDefaultsUtil {

    private static final JsonValue INTEGER_DEFAULT_MAX = JsonValue.of(Long.MAX_VALUE);
    private static final JsonValue INTEGER_DEFAULT_MIN = JsonValue.of(Long.MIN_VALUE);

    JsonObjectMerger jsonObjectMerger = new JsonObjectMerger();

    static final JsonObject DEFAULT = JsonObject.EMPTY.builder()
            .put("type", "string")
            .put("mustBeNull", false)
            .put("required", false)
            .put("nullable", false)
            .build();

    private JsonObject removeDefaults(JsonObject formField) {
        if (Type.STRING.asJsonValue().equals(formField.get("type"))) {
            formField = formField.remove("type");
        }
        if (JsonValue.FALSE.equals(formField.get("mustBeNull"))) {
            formField = formField.remove("mustBeNull");
        }
        if (JsonValue.FALSE.equals(formField.get("required"))) {
            formField = formField.remove("required");
        }
        if (JsonValue.FALSE.equals(formField.get("nullable"))) {
            formField = formField.remove("nullable");
        }
        return formField;
    }

    static final JsonObject DEFAULT_FOR_INTEGER = DEFAULT.builder()
            .put("type", Type.INTEGER)
            .put("min", INTEGER_DEFAULT_MIN)
            .put("max", INTEGER_DEFAULT_MAX)
            .build();

    public JsonObject removeIntegerDefaults(JsonObject formField) {
        if (INTEGER_DEFAULT_MIN.equals(formField.get("min", INTEGER_DEFAULT_MIN))) {
            formField = formField.remove("min");
        }
        if (INTEGER_DEFAULT_MAX.equals(formField.get("max", INTEGER_DEFAULT_MAX))) {
            formField = formField.remove("max");
        }
        return formField;
    }

    static final JsonObject DEFAULT_FOR_DECIMAL = DEFAULT.builder()
            .put("type", Type.DECIMAL)
            .build();

    public JsonObject removeDecimalDefaults(JsonObject formField) {
        return formField;
    }

    private static final JsonValue INTEGER_MAX_LENGTH = JsonValue.of(Integer.MAX_VALUE);

    static final JsonObject DEFAULT_FOR_STRING = DEFAULT.builder()
            .put("type", Type.STRING)
            .put("minlength", JsonValue.ZERO)
            .put("maxlength", INTEGER_MAX_LENGTH)
            .build();

    public JsonObject removeStringDefaults(JsonObject formField) {
        if (JsonValue.ZERO.equals(formField.get("minlength"))) {
            formField = formField.remove("minlength");
        }
        if (INTEGER_MAX_LENGTH.equals(formField.get("maxlength"))) {
            formField = formField.remove("maxlength");
        }

        return formField;
    }


    static final JsonObject DEFAULT_FOR_ARRAY = DEFAULT.builder()
            .put("type", Type.ARRAY)
            .put("minsize", JsonValue.ZERO)
            .build();

    public JsonObject removeArrayDefaults(JsonObject formField) {
        if (JsonValue.ZERO.equals(formField.get("minsize"))) {
            formField = formField.remove("minsize");
        }
        return formField;
    }

    public JsonObject removeDefaultsOfValueFormat(final JsonObject verboseValueFormatJsonObject) {

        JsonObject compactValueFormat = verboseValueFormatJsonObject;

        Type inType = verboseValueFormatJsonObject.get("type", JsonValue.NULL).as(Type.class);
        Type type = inType == null ? Type.STRING : inType;

        if (type.isString()) {
            compactValueFormat = removeStringDefaults(verboseValueFormatJsonObject);
        } else if (type.isArray()) {
            compactValueFormat = removeArrayDefaults(verboseValueFormatJsonObject);
        } else if (type.isInteger()) {
            compactValueFormat = removeIntegerDefaults(verboseValueFormatJsonObject);
        } else if (type.isDecimal()) {
            compactValueFormat = removeDecimalDefaults(verboseValueFormatJsonObject);
        }

        compactValueFormat = removeDefaults(compactValueFormat);

        return compactValueFormat;
    }

    public Option explicitOptionOf(Option option) {
        throw new RuntimeException("Not implemented yet");
    }

    public JsonObject putDefaultsInValueFormat(JsonObject valueFormat) {

        JsonObject defaultFormField = DEFAULT;

        Type inType = valueFormat.get("type", JsonValue.NULL).as(Type.class);
        Type type = inType == null ? Type.STRING : inType;

        if (Type.INTEGER.equals(type)) {
            defaultFormField = DEFAULT_FOR_INTEGER;
        } else if (Type.DECIMAL.equals(type)) {
            defaultFormField = DEFAULT_FOR_DECIMAL;
        } else if (Type.STRING.equals(type)) {
            defaultFormField = DEFAULT_FOR_STRING;
        } else if (Type.ARRAY.equals(type) || Type.SET.equals(type)) {
            defaultFormField = DEFAULT_FOR_ARRAY;
        }

        return jsonObjectMerger.merge(defaultFormField, valueFormat);
    }
}
