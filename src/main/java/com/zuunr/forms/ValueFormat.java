/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms;

import com.zuunr.forms.formfield.Enum;
import com.zuunr.forms.formfield.Equals;
import com.zuunr.forms.formfield.Options;
import com.zuunr.forms.formfield.Type;
import com.zuunr.forms.formfield.options.ValidationStep;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectBuilder;
import com.zuunr.json.JsonValue;

import java.math.BigDecimal;

/**
 * <p>This class represents the form field in its raw format without applying any default values.</p>
 *
 * @author Niklas Eldberger
 */
public final class ValueFormat extends AbstractValueFormat {

    public static final ValueFormat EMPTY = new ValueFormat(JsonObject.EMPTY.jsonValue(), FormatVerbosity.UNSPECIFIED);
    public static final ValueFormat TRUE = new ValueFormat(JsonValue.TRUE, FormatVerbosity.UNSPECIFIED);
    public static final ValueFormat FALSE = new ValueFormat(JsonValue.FALSE, FormatVerbosity.UNSPECIFIED);
    public static final ValueFormat ANY_VALUE_FORMAT = ValueFormat.EMPTY.builder().type(Type.UNDEFINED).build();
    public static final ValueFormat IMPOSSIBLE_VALUE_FORMAT = ValueFormat.EMPTY.builder().type(Type.UNDEFINED).build();

    private ValueFormat explicitValueFormat;
    private ValueFormat compactValueFormat;
    private ValueFormat asFormatOfTypeObjectWithElementField;
    private ValueFormat asRequiredFormatOfTypeObjectWithElementField;

    private Boolean alwaysSucceeds;
    private Boolean alwaysFails;


    private ValueFormat(JsonValue source) {
        this(source, FormatVerbosity.UNSPECIFIED);
    }

    private ValueFormat(JsonValue jsonValue, FormatVerbosity formatVerbosity) {
        super(init(jsonValue));
        switch (formatVerbosity) {
            case COMPACT: {
                compactValueFormat = this;
                break;
            }
            case EXPLICIT: {
                explicitValueFormat = this;
                break;
            }
        }
    }

    public Builder builder() {
        return new Builder(asJsonObject());
    }

    /**
     * @return where all defaults are applied
     */
    public ValueFormat asExplicitValueFormat() {
        if (explicitValueFormat == null) {
            explicitValueFormat = new ValueFormat(asExplicit(), FormatVerbosity.EXPLICIT);
        }
        return explicitValueFormat;
    }

    /**
     * @return where all defaults are applied
     */
    public ValueFormat asCompactValueFormat() {
        if (compactValueFormat == null) {
            compactValueFormat = new ValueFormat(asCompact(), FormatVerbosity.COMPACT);
        }
        return compactValueFormat;
    }

    public ValueFormat asFormatOfTypeObjectWithElementField() {
        if (asFormatOfTypeObjectWithElementField == null) {
            Form form = Form.EMPTY.builder()
                    .value(FormFields.EMPTY.builder()
                            .add(asFormField("element"))
                            .build())
                    .build();
            asFormatOfTypeObjectWithElementField = ValueFormat.EMPTY.builder().type(Type.OBJECT).form(form).build();
        }
        return asFormatOfTypeObjectWithElementField;
    }

    public FormField asFormField(String formFieldName) {
        return asJsonObject().put("name", formFieldName).as(FormField.class);
    }


    public ValueFormat asRequiredFormatOfTypeObjectWithElementField() {
        if (asRequiredFormatOfTypeObjectWithElementField == null) {
            Form form = Form.EMPTY.builder()
                    .value(FormFields.EMPTY.builder()
                            .add(asRequiredFormField("element"))
                            .build())
                    .build();
            asRequiredFormatOfTypeObjectWithElementField = ValueFormat.EMPTY.builder().type(Type.OBJECT).form(form).build();
        }
        return asRequiredFormatOfTypeObjectWithElementField;
    }

    public FormField asRequiredFormField(String formFieldName) {
        return asJsonObject().put("name", formFieldName).put("required", true).as(FormField.class);
    }

    public boolean alwaysSucceeds() {
        if (alwaysSucceeds == null) {

            JsonValue me = asJsonValue();
            if (me.isBoolean()) {
                alwaysSucceeds = me.getBoolean();
            } else {
                if (Type.UNDEFINED.equals(type())) {
                    alwaysSucceeds = Boolean.TRUE;
                }
                alwaysSucceeds = Boolean.FALSE;
            }
        }
        return alwaysSucceeds;
    }

    public boolean alwaysFails(){
        if (alwaysFails == null) {
            alwaysFails = alwaysFails(this);
        }
        return alwaysFails;
    }

    @Override
    public ValidationStep asValidationStep() {
        return JsonObject.EMPTY.put("format", this).as(ValidationStep.class);
    }

    public static class Builder {

        JsonObjectBuilder jsonObjectBuilder;

        public Builder(JsonObject jsonObject) {
            jsonObjectBuilder = jsonObject.builder();
        }

        public Builder desc(String description) {
            jsonObjectBuilder.put("desc", description);
            return this;
        }

        public Builder defaultValue(JsonValue defaultValue) {
            jsonObjectBuilder.put("default", defaultValue);
            return this;
        }

        public Builder eform(Form form) {
            jsonObjectBuilder.put("eform", form.asJsonValue());
            return this;
        }

        public Builder element(AbstractValueFormat elementValueFormat) {
            jsonObjectBuilder.put("element", elementValueFormat.asJsonValue());
            return this;
        }

        public Builder equalTo(Equals paths) {
            jsonObjectBuilder.put("equals", paths.asJsonValue());
            return this;
        }

        public Builder form(Form form) {
            jsonObjectBuilder.put("form", form.asJsonValue());
            return this;
        }

        public Builder href(String href) {
            jsonObjectBuilder.put("href", href);
            return this;
        }

        public Builder max(Integer max) {
            jsonObjectBuilder.put("max", max);
            return this;
        }

        public Builder max(BigDecimal max) {
            jsonObjectBuilder.put("max", max);
            return this;
        }

        public Builder max(String max) {
            jsonObjectBuilder.put("max", max);
            return this;
        }

        public Builder maxlength(Integer maxlength) {
            jsonObjectBuilder.put("maxlength", maxlength);
            return this;
        }

        public Builder min(Integer min) {
            jsonObjectBuilder.put("min", min);
            return this;
        }

        public Builder min(BigDecimal min) {
            jsonObjectBuilder.put("min", min);
            return this;
        }

        public Builder min(String min) {
            jsonObjectBuilder.put("min", min);
            return this;
        }

        public Builder minlength(Integer minlength) {
            jsonObjectBuilder.put("minlength", minlength);
            return this;
        }

        public Builder mustBeNull(Boolean mustBeNull) {
            jsonObjectBuilder.put("mustBeNull", mustBeNull);
            return this;
        }

        public Builder nullable(Boolean nullable) {
            jsonObjectBuilder.put("nullable", nullable);
            return this;
        }

        public Builder pattern(String pattern) {
            jsonObjectBuilder.put("pattern", pattern);
            return this;
        }

        public Builder required(Boolean required) {
            jsonObjectBuilder.put("required", required);
            return this;
        }

        public Builder type(Type type) {
            jsonObjectBuilder.put("type", type);
            return this;
        }

        public Builder maxsize(Integer maxsize) {
            jsonObjectBuilder.put("maxsize", maxsize);
            return this;
        }

        public Builder minsize(Integer minsize) {
            jsonObjectBuilder.put("minsize", minsize);
            return this;
        }

        public Builder options(Options options) {
            jsonObjectBuilder.put("options", options);
            return this;
        }

        public Builder enumeration(Enum enm) {
            jsonObjectBuilder.put("enum", enm);
            return this;
        }

        public ValueFormat build() {
            ValueFormat result = new ValueFormat(jsonObjectBuilder.build().jsonValue(), FormatVerbosity.UNSPECIFIED);
            validateEformAndElementFormEqual(result);
            return result;
        }

        private void validateEformAndElementFormEqual(ValueFormat valueFormat){
            if (valueFormat.asExplicitValueFormat().type().isArrayOrSet() &&
                    valueFormat.asExplicitValueFormat().element() != null &&
                    valueFormat.asExplicitValueFormat().element().form() != null &&
                    valueFormat.asExplicitValueFormat().eform() != null &&
                    !valueFormat.asExplicitValueFormat().element().form().equals(valueFormat.asExplicitValueFormat().eform())){
                throw new RuntimeException("eform and element.form must be equal");
            }
        }
    }
}
