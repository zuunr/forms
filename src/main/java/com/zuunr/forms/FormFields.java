/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms;

import com.zuunr.json.*;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class FormFields implements JsonArraySupport {

    public static final FormFields EMPTY = new FormFields(JsonArray.EMPTY, FormatVerbosity.UNSPECIFIED);

    private JsonArray jsonArray;

    private JsonObject formFieldsPerName = null;

    private FormFields explicitFormFields;
    private FormFields compactFormFields;
    private Form asExclusiveForm;

    private FormFields(JsonArray jsonArray, FormatVerbosity formatVerbosity) {
        this.jsonArray = jsonArray;
        switch (formatVerbosity) {
            case COMPACT: {
                compactFormFields = this;
                break;
            }
            case EXPLICIT: {
                explicitFormFields = this;
                break;
            }
        }
    }

    private static JsonArray init(JsonValue jsonValue) {
        if (jsonValue.is(JsonArray.class)) {
            return init(jsonValue.getValue(JsonArray.class));
        } else {
            return init(jsonValue.getValue(JsonObject.class));
        }
    }

    private static JsonArray init(JsonObject jsonObject) {
        Iterator<JsonValue> keysIter = jsonObject.keys().iterator();
        for (FormField formField: jsonObject.values().asList(FormField.class)) {
            if (!keysIter.next().getString().equals(formField.name())){
                throw new RuntimeException("Key MUST be equal to name of formField value");
            }
        }
        return jsonObject.values();
    }

    private static JsonArray init(JsonArray jsonArray) {
        JsonArrayBuilder result = JsonArray.EMPTY.builder();
        for (FormField formField: jsonArray.asList(FormField.class)){
            result.add(formField.asJsonObject());
        }
        return result.build();
    }

    public FormFields(JsonValue jsonValue) {
        this(init(jsonValue), FormatVerbosity.UNSPECIFIED);
    }

    public FormFieldsBuilder builder() {
        return new FormFieldsBuilder(jsonArray);
    }

    public List<FormField> asList() {
        return jsonArray.asList(FormField.class);
    }

    public Form asExclusiveForm(){
        if (asExclusiveForm == null) {
            asExclusiveForm = Form.EMPTY.builder().value(this).build();
        }
        return asExclusiveForm;
    }

    public JsonObject formFieldsByName() {

        if (formFieldsPerName == null) {
            JsonObjectBuilder builder = JsonObject.EMPTY.builder();
            for (FormField formField : asList()) {
                builder.put(formField.name(), formField);
            }
            formFieldsPerName = builder.build();
        }
        return formFieldsPerName;
    }

    public FormField of(String name) {
        return formFieldsByName().get(name, JsonValue.NULL).as(FormField.class);
    }

    @Override
    public JsonValue asJsonValue() {
        return jsonArray.jsonValue();
    }

    @Override
    public JsonArray asJsonArray() {
        return jsonArray;
    }

    @Override
    public String toString() {
        return jsonArray.toString();
    }

    public FormFields asCompactFormFields() {
        if (compactFormFields == null) {
            FormFieldsBuilder builder = FormFields.EMPTY.builder();
            for (FormField formField : asList()) {
                builder.add(formField.asCompactFormField());
            }
            compactFormFields = builder.buildCompact();
        }
        return compactFormFields;
    }

    public FormFields asExplicitFormFields() {
        if (explicitFormFields == null) {
            FormFieldsBuilder builder = FormFields.EMPTY.builder();
            for (FormField formField : asList()) {
                builder.add(formField.asExplicitFormField());
            }
            explicitFormFields = builder.buildExplicit();
        }
        return explicitFormFields;
    }

    public static class FormFieldsBuilder {

        private static final ByNameComparator BY_NAME_COMPARATOR = new ByNameComparator();

        private JsonArrayBuilder jsonArrayBuilder;

        public FormFieldsBuilder(JsonArray jsonArray) {
            this.jsonArrayBuilder = jsonArray.builder();
        }

        public FormFieldsBuilder add(FormField formField) {
            jsonArrayBuilder.add(formField.asJsonObject());
            return this;
        }

        public FormFieldsBuilder add(FormField.Builder formFieldBuilder) {
            jsonArrayBuilder.add(formFieldBuilder.build().asJsonObject());
            return this;
        }

        public FormFieldsBuilder add(ValueFormat.Builder formFieldBuilder) {
            jsonArrayBuilder.add(formFieldBuilder.build().asJsonObject());
            return this;
        }

        public FormFields build() {
            return new FormFields(jsonArrayBuilder.build(), FormatVerbosity.UNSPECIFIED);
        }

        public FormFields buildCompact() {
            return new FormFields(jsonArrayBuilder.build(), FormatVerbosity.COMPACT);
        }

        public FormFields buildExplicit() {
            return new FormFields(jsonArrayBuilder.build(), FormatVerbosity.EXPLICIT);
        }

        public FormFields buildSortedByName() {
            return new FormFields(jsonArrayBuilder.buildSorted(BY_NAME_COMPARATOR), FormatVerbosity.UNSPECIFIED);
        }

        public FormFields sortAndBuild() {
            return new FormFields(jsonArrayBuilder.build(), FormatVerbosity.UNSPECIFIED);

        }

        private static class ByNameComparator implements Comparator<JsonValue> {

            @Override
            public int compare(JsonValue jsonValue1, JsonValue jsonValue2) {
                JsonObject formField1 = jsonValue1.getValue(JsonObject.class);
                JsonObject formField2 = jsonValue2.getValue(JsonObject.class);

                int result = formField1.get("name").getValue(String.class).compareTo(formField2.get("name").getValue(String.class));
                if (result == 0) {
                    throw new RuntimeException("There can never be more than one form field per builder");
                }
                return result;
            }
        }
    }

    @Override
    public boolean equals(Object obj) {
        return obj == this ||
                obj != null &&
                        this.getClass() == obj.getClass() &&
                        ((FormFields) obj).jsonArray.equals(jsonArray);
    }
}
