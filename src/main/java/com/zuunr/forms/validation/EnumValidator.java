/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.validation;

import com.zuunr.forms.FormField;
import com.zuunr.forms.formfield.Enum;
import com.zuunr.json.JsonValue;

/**
 * @author Niklas Eldberger
 */
public class EnumValidator {

    public FormFieldValidationResult.Builder validate(FormField formField, JsonValue fieldValue, Boolean returnWaste, FormFieldValidationResult.Builder fieldValidationBuilder) {

        Enum enumeration = formField.schema().enumeration();
        if (enumeration != null) {
            if (!enumeration.matches(fieldValue)) {
                fieldValidationBuilder.valid(false);
                if (returnWaste) {
                    fieldValidationBuilder.add("enum", enumeration.asJsonValue());
                }
            }
        }
        return fieldValidationBuilder;
    }
}
