/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.validation;

import com.zuunr.forms.FormField;
import com.zuunr.forms.formfield.Type;
import com.zuunr.json.JsonValue;

import java.math.BigDecimal;

/**
 * @author Niklas Eldberger
 */
public class DecimalValidator {

    public FormFieldValidationResult.Builder validate(FormField param, JsonValue fieldValue, Boolean returnWaste, FormFieldValidationResult.Builder fieldValidationBuilder) {
        BigDecimal decimalValue = null;
        try {
            decimalValue = fieldValue.getBigDecimal();
        } catch (Exception e) {
            fieldValidationBuilder.valid(false);
            if (returnWaste) {
                fieldValidationBuilder.add("type", Type.DECIMAL.asJsonValue());
            }
        }
        if (decimalValue != null) {
            fieldValidationBuilder = validateMin(param.asJsonValue(), decimalValue, returnWaste, fieldValidationBuilder);
            fieldValidationBuilder = validateMax(param.asJsonValue(), decimalValue, returnWaste, fieldValidationBuilder);
        }
        return fieldValidationBuilder;
    }

    private FormFieldValidationResult.Builder validateMin(JsonValue formField, BigDecimal value, boolean returnWaste, FormFieldValidationResult.Builder formFieldValidationBuilder) {

        JsonValue minJsonValue = formField.get("min", JsonValue.NULL);
        BigDecimal min = minJsonValue.getBigDecimal();
        if (min != null && lessThan(value, min)) {
            formFieldValidationBuilder.valid(false);
            if (returnWaste) {
                formFieldValidationBuilder.add("min", minJsonValue);
            }
        }
        return formFieldValidationBuilder;
    }

    private FormFieldValidationResult.Builder validateMax(JsonValue formField, BigDecimal value, boolean returnWaste, FormFieldValidationResult.Builder formFieldValidationBuilder) {

        JsonValue maxJsonValue = formField.get("max", JsonValue.NULL);
        BigDecimal max = maxJsonValue.getBigDecimal();
        if (max != null && greaterThan(value, max)) {
            formFieldValidationBuilder.valid(false);
            if (returnWaste) {
                formFieldValidationBuilder.add("max", maxJsonValue);
            }
        }
        return formFieldValidationBuilder;
    }


    protected boolean lessThan(BigDecimal bigDecimal1, BigDecimal bigDecimal2) {
        return bigDecimal1.compareTo(bigDecimal2) < 0;
    }

    protected boolean greaterThan(BigDecimal bigDecimal1, BigDecimal bigDecimal2) {
        return bigDecimal1.compareTo(bigDecimal2) > 0;
    }
}
