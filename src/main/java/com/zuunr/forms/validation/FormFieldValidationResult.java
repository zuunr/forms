/*
 * Copyright 2018-2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.validation;

import com.zuunr.forms.formfield.FormFieldMember;
import com.zuunr.json.*;

public class FormFieldValidationResult implements JsonObjectSupport {

    public final boolean valid;
    public final JsonArray path;
    public final String description;
    public final JsonValue badValue;
    public final JsonObject violations;
    public final boolean addParamToFiltrate;
    public final JsonValue filtrate;

    private JsonObject asJsonObject;


    private FormFieldValidationResult(boolean valid, JsonArray path, String description, JsonValue badValue, JsonObject violations, boolean addParamToFiltrate, JsonValue filtrate) {
        this.valid = valid;
        this.path = path;
        this.description = description;
        this.badValue = badValue;
        this.violations = violations;
        this.addParamToFiltrate = addParamToFiltrate;
        this.filtrate = filtrate;
    }

    @Override
    public JsonObject asJsonObject() {

        if (asJsonObject == null) {
            JsonObject temp = JsonObject.EMPTY.put("path", path).put("badValue", badValue).put("violations", this.violations);
            if (description != null) {
                temp = temp.put("description", description); // For backward compatibility!
            }
            asJsonObject = temp;
        }
        return asJsonObject;
    }

    @Override
    public JsonValue asJsonValue() {
        return asJsonObject().jsonValue();
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder builder(FormFieldValidationResult formFieldValidationResult) {
        return new Builder()
                .valid(formFieldValidationResult.valid)
                .path(formFieldValidationResult.path)
                .description(formFieldValidationResult.description)
                .badValue(formFieldValidationResult.badValue)
                .violations(formFieldValidationResult.violations)
                .addParamToFiltrate(formFieldValidationResult.addParamToFiltrate)
                .filtrate(formFieldValidationResult.filtrate)
                ;
    }

    public static class Builder {

        private boolean valid = true;
        private JsonArray path;
        private String description;
        private JsonValue badValue;
        private boolean addParamToFiltrate;
        private JsonObjectBuilder violations;
        private JsonValue filtrate;

        public Builder() {
        }

        public Builder addParamToFiltrate(boolean addParamToFiltrate) {
            this.addParamToFiltrate = addParamToFiltrate;
            return this;
        }

        public Builder path(JsonArray path) {
            this.path = path;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder badValue(JsonValue badValue) {
            this.badValue = badValue;
            return this;
        }

        public Builder filtrate(JsonValue filtrate) {
            this.filtrate = filtrate;
            return this;
        }

        public Builder add(String validationName, JsonValue violation) {
            violations = violations == null ? JsonObject.EMPTY.builder() : violations;
            violations.put(validationName, violation);
            return this;
        }

        public Builder add(String validationName, FormFieldMember violation) {
            violations = violations == null ? JsonObject.EMPTY.builder() : violations;
            violations.put(validationName, violation.asJsonValue());
            return this;
        }

        public Builder valid(boolean valid) {
            this.valid = valid;
            return this;
        }

        private Builder violations(JsonObject violations) {
            this.violations = violations.builder();
            return this;
        }

        public FormFieldValidationResult build() {
            return new FormFieldValidationResult(isValid(), path, description, badValue, violations == null ? JsonObject.EMPTY: violations.build(), addParamToFiltrate, filtrate);
        }

        public boolean containsViolations() {
            return violations != null && !violations.isEmpty();
        }

        public boolean isValid() {
            return (violations == null || violations.isEmpty()) && valid;
        }

        public boolean addParamToFiltrate() {
            return addParamToFiltrate;
        }
    }
}
