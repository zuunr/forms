/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.validation;

import com.zuunr.forms.FormField;
import com.zuunr.forms.filter.FormFilter;
import com.zuunr.forms.filter.ValidationContext;
import com.zuunr.forms.filter.result.Waste;
import com.zuunr.forms.formfield.Options;
import com.zuunr.forms.formfield.options.AggregatedValidationStep;
import com.zuunr.forms.formfield.options.Option;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonArrayBuilder;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;

/**
 * @author Niklas Eldberger
 */
public class OptionsValidator {

    public FormFieldValidationResult.Builder validate(FormField formField, JsonObject rootData, JsonArray rootDataPath, JsonValue fieldValue, Boolean returnWaste, FormFieldValidationResult.Builder fieldValidationBuilder, FormFilter formFilter, ValidationContext validationContext) {

        Options options = formField.schema().options();

        if (options != null) {

            if (options.value() == null) {
                throw new RuntimeException("options form field member must have value (Json Array of forms or values)");
            }

            JsonArrayBuilder errors = null;
            boolean success = false;

            if (formField.schema().options().aggregatedValidationSteps() == null || formField.schema().options().aggregatedValidationSteps().asList().isEmpty()) {

                success = false;
                for (Option option : formField.schema().options().value().asList()) {

                    if (option.value() == null) {
                        if (option.format() == null) {
                            throw new RuntimeException("format must not be null when value is null");
                        }

                        Waste waste = formFilter.filter(option.format(), rootData, rootDataPath, fieldValue, true, validationContext);
                        if (waste.isEmpty()) {
                            success = true;
                            break;
                        } else {
                            if (errors == null) {
                                errors = JsonArray.EMPTY.builder();
                            }
                            errors.add(JsonObject.EMPTY.builder().put("label", option.label()).put("error", waste).build());
                        }
                    } else if (option.value().equals(fieldValue)) {
                        success = true;
                        break;
                    } else {
                        if (errors == null) {
                            errors = JsonArray.EMPTY.builder();
                        }
                        errors.add(option);
                    }
                }
            } else {

                JsonObject succeedingOptionsPerLabel = formField.schema().options().perLabel();
                for (AggregatedValidationStep validationStep : formField.schema().options().aggregatedValidationSteps().asList()) {

                    success = false;
                    errors = null;

                    for (String label : succeedingOptionsPerLabel.keys().asList(String.class)) {

                        Option validationStepOption = validationStep.asJsonObject().get(label, JsonValue.NULL).as(Option.class);

                        if (validationStepOption == null) {
                            // all of this option is validated ok in earlier step
                            // this means one option is validated ok - we are done :)
                            success = true;
                            break;
                        } else if (validationStepOption.format() == null) {
                            throw new RuntimeException("format must not be null!");
                        } else {
                            Waste waste = formFilter.filter(validationStepOption.format(), rootData, JsonArray.EMPTY, fieldValue,  true, validationContext);
                            if (waste.isEmpty()) {
                                // This step is ok. If there are no more steps then we are done.
                                success = true;
                            } else {
                                if (errors == null) {
                                    errors = JsonArray.EMPTY.builder();
                                }
                                errors.add(JsonObject.EMPTY.builder().put("label", validationStepOption.label()).put("error", waste).build());
                                succeedingOptionsPerLabel = succeedingOptionsPerLabel.remove(label);

                            }
                        }

                    }
                    if (succeedingOptionsPerLabel.size() == 0) {
                        success = false;
                        break;
                    }
                }
            }

            if (!success) {
                if (errors == null) {
                    // FIXME Should errors be nullable?
                    throw new IllegalStateException("Errors where null but shouldn't be");
                }
                
                fieldValidationBuilder.add("options", formField.schema().options().asJsonObject().put("value", errors.build()).remove("aggregatedValidationSteps").jsonValue());
            }
        }
        return fieldValidationBuilder;
    }
}
