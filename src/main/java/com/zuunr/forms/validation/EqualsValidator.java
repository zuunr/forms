/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.validation;

import com.zuunr.forms.FormField;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.JsonValueUtil;

/**
 * @author Niklas Eldberger
 */
public class EqualsValidator {

    private static JsonValueUtil jsonValueUtil = new JsonValueUtil();
    private static final JsonArray EQUALS_PATHS = JsonArray.of("equals", "paths");


    public FormFieldValidationResult.Builder validate(FormField formField, JsonObject rootData, JsonValue fieldValue, FormFieldValidationResult.Builder fieldValidationBuilder) {
        JsonArray equalsPaths = formField.asJsonObject().get(EQUALS_PATHS, JsonArray.EMPTY).getValue(JsonArray.class);

        if (equalsPaths != null) {
            for (JsonValue equalsPathJsonValue : equalsPaths.asList()) {
                JsonArray unindexedPaths = jsonValueUtil.pathsStartingWith(rootData.jsonValue(), equalsPathJsonValue.getValue(JsonArray.class));
                for (JsonValue unindexedPathJsonValue : unindexedPaths.asList()) {
                    JsonArray unindexedPath = unindexedPathJsonValue.getValue(JsonArray.class);
                    JsonValue jsonValueOfPath = rootData.get(unindexedPath);
                    if (!fieldValue.equals(jsonValueOfPath)) {
                        fieldValidationBuilder.addParamToFiltrate(false);
                        fieldValidationBuilder.add("equals", JsonObject.EMPTY.put("paths", unindexedPaths).jsonValue());
                        break;
                    }
                }
            }
        }
        return fieldValidationBuilder;
    }
}
