/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.validation;

import com.zuunr.forms.FormField;
import com.zuunr.forms.formfield.ExclusiveMaximum;
import com.zuunr.forms.formfield.ExclusiveMinimum;
import com.zuunr.forms.formfield.Max;
import com.zuunr.forms.formfield.Min;
import com.zuunr.json.JsonValue;

/**
 * @author Niklas Eldberger
 */
public class IntegerValidator {

    private static final JsonValue INTEGER = JsonValue.of("integer");

    public FormFieldValidationResult.Builder validate(FormField formField, JsonValue fieldValue, Boolean returnWaste, FormFieldValidationResult.Builder fieldValidationBuilder) {
        Long longValue = null;
        try {
            longValue = fieldValue.asLong();
        } catch (Exception e) {

            fieldValidationBuilder.valid(false);
            if (returnWaste) {
                fieldValidationBuilder.add("type", INTEGER);
            }
        }
        if (longValue != null) {
            fieldValidationBuilder = validateMin(formField, longValue, returnWaste, fieldValidationBuilder);
            fieldValidationBuilder = validateMax(formField, longValue, returnWaste, fieldValidationBuilder);
            fieldValidationBuilder = validateExclusiveMinimum(formField, longValue, returnWaste, fieldValidationBuilder);
            fieldValidationBuilder = validateExclusiveMaximum(formField, longValue, returnWaste, fieldValidationBuilder);
        }
        return fieldValidationBuilder;
    }

    private FormFieldValidationResult.Builder validateMin(FormField formField, Long value, boolean returnWaste, FormFieldValidationResult.Builder formFieldValidationBuilder) {
        Min min = formField.schema().min();
        if (min != null && value < min.asLong()) {
            formFieldValidationBuilder.valid(false);
            if (returnWaste) {
                formFieldValidationBuilder.add("min", min.asJsonValue());
            }
        }
        return formFieldValidationBuilder;
    }

    private FormFieldValidationResult.Builder validateMax(FormField formField, Long value, boolean returnWaste, FormFieldValidationResult.Builder formFieldValidationBuilder) {


        Max max = formField.schema().max();
        if (max != null && value > max.asLong()) {
            formFieldValidationBuilder.valid(false);
            if (returnWaste) {
                formFieldValidationBuilder.add("max", max.asJsonValue());
            }
        }
        return formFieldValidationBuilder;
    }

    private FormFieldValidationResult.Builder validateExclusiveMinimum(FormField formField, Long value, boolean returnWaste, FormFieldValidationResult.Builder formFieldValidationBuilder) {
        ExclusiveMinimum exclusiveMinimum = formField.schema().exclusiveMinimum();
        if (exclusiveMinimum != null && value <= exclusiveMinimum.asLong()) {
            formFieldValidationBuilder.valid(false);
            if (returnWaste) {
                formFieldValidationBuilder.add("exclusiveMinimum", exclusiveMinimum.asJsonValue());
            }
        }
        return formFieldValidationBuilder;
    }

    private FormFieldValidationResult.Builder validateExclusiveMaximum(FormField formField, Long value, boolean returnWaste, FormFieldValidationResult.Builder formFieldValidationBuilder) {
        ExclusiveMaximum exclusiveMaximum = formField.schema().exclusiveMaximum();
        if (exclusiveMaximum != null && value >= exclusiveMaximum.asLong()) {
            formFieldValidationBuilder.valid(false);
            if (returnWaste) {
                formFieldValidationBuilder.add("exclusiveMaximum", exclusiveMaximum.asJsonValue());
            }
        }
        return formFieldValidationBuilder;
    }
}
