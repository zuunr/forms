/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.validation;

import com.zuunr.forms.FormField;
import com.zuunr.forms.formfield.Nullable;
import com.zuunr.json.JsonValue;

/**
 * @author Niklas Eldberger
 */
public class NullableValidator {

    public FormFieldValidationResult.Builder validate(FormField formField, JsonValue fieldValue, Boolean returnWaste, FormFieldValidationResult.Builder fieldValidationBuilder) {


        if (!formField.schema().nullable() && JsonValue.NULL.equals(fieldValue)) {
            fieldValidationBuilder.addParamToFiltrate(false);
            if (returnWaste) {
                fieldValidationBuilder.add("nullable", Nullable.FALSE);
            }
        }
        return fieldValidationBuilder;
    }
}
