/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.validation;

import com.zuunr.forms.FormField;
import com.zuunr.forms.formfield.Max;
import com.zuunr.forms.formfield.Min;
import com.zuunr.forms.formfield.Pattern;
import com.zuunr.forms.formfield.Type;
import com.zuunr.json.JsonValue;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * @author Niklas Eldberger
 */
public abstract class AbstractJavaDatetimeValidator {

    public FormFieldValidationResult.Builder validate(FormField formField, JsonValue fieldValue, boolean returnWaste, FormFieldValidationResult.Builder fieldValidationBuilder) {

        if (formField.schema().type().isStringTypeOrSubtypeOfString()) {
            Pattern pattern = formField.schema().pattern();
            if (pattern != null && !pattern.compiled().matcher(fieldValue.getValue(String.class)).matches()) {
                fieldValidationBuilder.valid(false);
                if (returnWaste) {
                    fieldValidationBuilder.add("pattern", pattern.asJsonValue());
                }
            }
        }

        if (isCorrectFormat(fieldValue)) {

            min(formField, fieldValue, returnWaste, fieldValidationBuilder);
            max(formField, fieldValue, returnWaste, fieldValidationBuilder);

        } else {
            fieldValidationBuilder.valid(false);
            if (returnWaste) {
                fieldValidationBuilder.add("type", type());
            }
        }
        return fieldValidationBuilder;
    }

    public boolean isCorrectFormat(JsonValue fieldValue) {
        try {
            LocalDate.parse(fieldValue.getValue(String.class), formatter());
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private FormFieldValidationResult.Builder min(FormField formField, JsonValue fieldValue, boolean returnWaste, FormFieldValidationResult.Builder fieldValidationBuilder) {

        Min min = formField.schema().min();
        if (min != null) {
            String minString = min.asString();
            if (fieldValue.getValue(String.class).compareTo(minString) < 0) {
                fieldValidationBuilder.valid(false);
                if (returnWaste) {
                    fieldValidationBuilder.add("min", formField.schema().asJsonObject().get("min"));
                }
            }
        }
        return fieldValidationBuilder;
    }

    private FormFieldValidationResult.Builder max(FormField formField, JsonValue fieldValue, boolean returnWaste, FormFieldValidationResult.Builder fieldValidationBuilder) {

        Max max = formField.schema().max();
        if (max != null) {
            String maxString = max.asString();
            if (fieldValue.getValue(String.class).compareTo(maxString) > 0) {
                fieldValidationBuilder.valid(false);
                if (returnWaste) {
                    fieldValidationBuilder.add("max", formField.schema().asJsonObject().get("max"));
                }
            }
        }
        return fieldValidationBuilder;
    }

    abstract DateTimeFormatter formatter();
    abstract Type type();
}
