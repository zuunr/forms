package com.zuunr.forms.filter.result;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonArrayBuilder;
import com.zuunr.json.JsonValue;

/**
 * @author Niklas Eldberger
 */
public class JsonArrayFiltrateBuilder {

    private JsonArrayBuilder jsonArrayBuilder = JsonArray.EMPTY.builder();
    private boolean filtratePossible = true;

    public static JsonArrayFiltrateBuilder builder() {
        return new JsonArrayFiltrateBuilder();
    }

    /**
     * Makes build() return null, i.e no filtrate will be produced
     * @return
     */
    public JsonArrayFiltrateBuilder invalidateFiltrate() {
        this.filtratePossible = false;
        return this;
    }

    public JsonArrayFiltrateBuilder add(JsonValue filtrateElement) {
        jsonArrayBuilder.add(filtrateElement);
        return this;
    }

    public JsonArray build() {
        if (filtratePossible) {
            return jsonArrayBuilder.build();
        } else {
            return null;
        }
    }
}
