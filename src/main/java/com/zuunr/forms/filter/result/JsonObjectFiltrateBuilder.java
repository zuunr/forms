package com.zuunr.forms.filter.result;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectBuilder;
import com.zuunr.json.JsonValue;

/**
 * @author Niklas Eldberger
 */
public class JsonObjectFiltrateBuilder {

    private JsonObjectBuilder jsonObjectBuilder = JsonObject.EMPTY.builder();
    private boolean filtratePossible = true;

    public static JsonObjectFiltrateBuilder builder() {
        return new JsonObjectFiltrateBuilder();
    }

    /**
     * Makes build() return null, i.e no filtrate will be produced
     * @return
     */
    public JsonObjectFiltrateBuilder invalidateFiltrate() {
        this.filtratePossible = false;
        return this;
    }

    public JsonObjectFiltrateBuilder put(String key, JsonValue filtrateValue) {
        jsonObjectBuilder.put(key, filtrateValue);
        return this;
    }

    public JsonObjectFiltrateBuilder put(String key, JsonArray filtrateValue) {
        jsonObjectBuilder.put(key, filtrateValue);
        return this;
    }

    public JsonObjectFiltrateBuilder putAll(JsonObject jsonObject) {
        jsonObjectBuilder.putAll(jsonObject);
        return this;
    }

    public JsonObject build() {
        if (filtratePossible) {
            return jsonObjectBuilder.build();
        } else {
            return null;
        }
    }
}
