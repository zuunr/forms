/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.filter.result;

import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectSupport;
import com.zuunr.json.JsonValue;

public class Violation implements JsonObjectSupport {

    public static final Violation TYPE_STRING = Violation.of("type", JsonValue.of("string"));

    private String constraintName;
    private JsonValue constraintValue;

    @Override
    public JsonObject asJsonObject() {
        return JsonObject.EMPTY.put(constraintName, constraintValue);
    }

    @Override
    public JsonValue asJsonValue() {
        return asJsonObject().jsonValue();
    }

    public static Violation of(String constraintName, JsonValue contraintValue) {
        return new Violation(constraintName, contraintValue);
    }


    private Violation(String constraintName, JsonValue constraintValue) {
        this.constraintName = constraintName;
        this.constraintValue = constraintValue;
    }
}
