/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.filter.result;

import com.zuunr.forms.validation.FormFieldValidationResult;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectSupport;
import com.zuunr.json.JsonValue;

import java.util.ArrayList;

public class Waste implements JsonObjectSupport {

    private JsonArray wasteItems;
    private ArrayList<FormFieldValidationResult> formFieldValidationResultList;

    private Waste(ArrayList<FormFieldValidationResult> wasteItemsList) {

        this.formFieldValidationResultList = wasteItemsList;
        wasteItems = JsonArray.EMPTY;
        for (int i = 0;i < wasteItemsList.size(); i++) {
            wasteItems = wasteItems.add(wasteItemsList.get(i).asJsonValue());
        }
    }

    public Waste(JsonArray wasteItems) {
        this.wasteItems = wasteItems;
    }

    public boolean isEmpty() {
        return wasteItemsSize() == 0;
    }

    @Override
    public JsonObject asJsonObject() {
        return JsonObject.EMPTY.put("paths", wasteItems);
    }

    @Override
    public JsonValue asJsonValue() {
        return asJsonObject().jsonValue();
    }

    public int wasteItemsSize() {
        return wasteItems.size();
    }

    public FormFieldValidationResult getWasteItem(int i) {
        return formFieldValidationResultList.get(i);
    }

    public Waste prependToAllWasteItemsPaths(JsonValue nameOrIndex) {
        Builder wasteBuilder = Waste.builder();
        for (int i = 0; i < wasteItemsSize(); i++) {
            FormFieldValidationResult formFieldValidationResult = getWasteItem(i);
            FormFieldValidationResult.Builder wasteItemBuilder =
                    FormFieldValidationResult.builder(formFieldValidationResult)
                            .path(formFieldValidationResult.path.addFirst(nameOrIndex));
            wasteBuilder.add(wasteItemBuilder.build());
        }
        return wasteBuilder.build();
    }

    public static final Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private ArrayList<FormFieldValidationResult> internal = new ArrayList<>();
        public Builder add(FormFieldValidationResult formFieldValidationResult) {
            internal.add(formFieldValidationResult);
            return this;
        }

        public void addAllWasteItemsOf(Waste waste) {
            internal.addAll(waste.formFieldValidationResultList);
        }

        public Waste build() {
            return new Waste(internal);
        }
    }
}
