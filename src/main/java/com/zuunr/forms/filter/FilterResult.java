/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.filter;

import com.zuunr.forms.filter.result.Waste;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectSupport;
import com.zuunr.json.JsonValue;

public class FilterResult<M extends JsonObjectSupport> {
    
    public final M filtrate;
    public final Waste waste;

    public FilterResult(M filtrate, Waste waste) {
        this.filtrate = filtrate;
        this.waste = waste;
    }

    public JsonValue filtrateAsJsonValue() {
        return filtrate == null ? JsonValue.NULL : filtrate.asJsonObject().jsonValue();
    }

    public JsonValue wasteAsJsonValue() {
        return waste == null ? JsonValue.NULL : waste.asJsonObject().jsonValue();
    }

    public JsonObject asJsonObject() {
        return JsonObject.EMPTY
                .put("filtrate", filtrateAsJsonValue())
                .put("waste", wasteAsJsonValue());
    }

    @Override
    public String toString() {
        return asJsonObject().asJson();
    }
}