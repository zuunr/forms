/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.filter;

import com.zuunr.forms.Form;
import com.zuunr.forms.ValueFormat;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;

/**
 * @author Niklas Eldberger
 */
public final class ValidationContext {

    private JsonValue rootFormat;

    public ValidationContext(ValueFormat rootFormat) {
        this.rootFormat = rootFormat.asJsonValue();
    }

    public ValueFormat getFormat(String href){
        return rootFormat.as(HrefResources.class).asJsonObject().get(href, JsonValue.NULL).as(ValueFormat.class);
    }

    public Form getForm(String href){
        return rootFormat.as(HrefResources.class).asJsonObject().get(href, JsonValue.NULL).as(Form.class);
    }
}
