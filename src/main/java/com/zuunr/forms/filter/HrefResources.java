/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.filter;

import com.zuunr.json.*;

/**
 * @author Niklas Eldberger
 */
public class HrefResources implements JsonObjectSupport {

    private JsonObject resources;

    public HrefResources(JsonValue jsonValue) {
        resources = JsonObject.EMPTY;

        if (jsonValue.isBoolean()) {

        } else if (jsonValue.isJsonObject()) {
            JsonObject jsonObject = jsonValue.getValue(JsonObject.class);
            JsonArray sortedPaths = jsonObject.jsonValue().getPaths().sort();
            for (JsonArray path : sortedPaths.asList(JsonArray.class)) {
                if (JsonValue.of("href").equals(path.last())) {

                    String href = jsonObject.get(path).getValue(String.class);

                    JsonObject resource = resources.get(href, JsonValue.NULL).getValue(JsonObject.class);

                    if (resource == null) {
                        resource = jsonObject.get(path.allButLast()).getValue(JsonObject.class);
                        resources = resources.put(href, resource);
                    }
                }
            }
        } else {
            throw new UnsupportedTypeException("Only Boolean and JsonObject implemented so far");
        }
    }

    public JsonObject get(String href) {
        return resources.get(href, JsonValue.NULL).getValue(JsonObject.class);
    }

    @Override
    public JsonObject asJsonObject() {
        return resources;
    }

    @Override
    public JsonValue asJsonValue() {
        return resources.jsonValue();
    }
}
