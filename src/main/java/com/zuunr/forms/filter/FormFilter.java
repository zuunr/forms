/*
 * Copyright 2018 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms.filter;

import com.zuunr.forms.Form;
import com.zuunr.forms.FormField;
import com.zuunr.forms.FormFields;
import com.zuunr.forms.ValueFormat;
import com.zuunr.forms.filter.result.JsonArrayFiltrateBuilder;
import com.zuunr.forms.filter.result.JsonObjectFiltrateBuilder;
import com.zuunr.forms.filter.result.Waste;
import com.zuunr.forms.formfield.Required;
import com.zuunr.forms.formfield.Type;
import com.zuunr.forms.validation.*;
import com.zuunr.json.*;
import com.zuunr.json.util.JsonObjectWrapper;

public class FormFilter {

    private static final EnumValidator enumValidator = new EnumValidator();
    private static final IntegerValidator integerValidator = new IntegerValidator();
    private static final StringValidator stringValidator = new StringValidator();
    private static final BooleanValidator booleanValidator = new BooleanValidator();
    private static final DecimalValidator decimalValidator = new DecimalValidator();
    private static final MinSizeValidator minSizeValidator = new MinSizeValidator();
    private static final MaxSizeValidator maxSizeValidator = new MaxSizeValidator();
    private static final EqualsValidator equalsValidator = new EqualsValidator();
    private static final NullableValidator nullableValidator = new NullableValidator();
    private static final MustBeNullValidator mustBeNullValidator = new MustBeNullValidator();
    private static final DateValidator dateValidator = new DateValidator();
    private static final DatetimeValidator datetimeValidator = new DatetimeValidator();
    private static final OptionsValidator optionsValidator = new OptionsValidator();


    /**
     * @param formFields
     * @param data
     * @param returnFiltrate
     * @param returnWaste
     * @return
     * @deprecated Use filter(final Form, JsonObject, boolean, boolean) instead
     */
    @Deprecated
    public FilterResult filter(JsonArray formFields, final JsonObject data, final boolean returnFiltrate, final boolean returnWaste) {
        return filter(Form.EMPTY.builder().value(formFields.as(FormFields.class)).build(), data, returnFiltrate, returnWaste);
    }

    public FilterResult<JsonObjectWrapper> filter(final Form form, final JsonObject data, final boolean returnFiltrate, final boolean returnWaste) {
        Waste.Builder wasteBuilder = returnWaste ? Waste.builder() : null;
        ValueFormat format = form.asObjectValueFormat().asExplicitValueFormat();
        return filter(format, data, JsonArray.EMPTY, data, returnFiltrate, wasteBuilder, new ValidationContext(format));
    }

    public FilterResult<JsonObjectWrapper> filter(final ValueFormat format, final JsonObject data, final boolean returnFiltrate, final boolean returnWaste) {
        Waste.Builder wasteBuilder = returnWaste ? Waste.builder() : null;
        return filter(format, data, JsonArray.EMPTY, data, returnFiltrate, wasteBuilder, new ValidationContext(format));
    }

    public Waste filter(final ValueFormat format, final JsonObject rootData, final JsonArray rootDataPath, final JsonValue value, final boolean returnWaste, ValidationContext validationContext) {

        JsonObject data = JsonObject.EMPTY.put("element", value);

        FilterResult<JsonObjectWrapper> result = filter(format.asRequiredFormatOfTypeObjectWithElementField(), rootData, rootDataPath, data, false, returnWaste ? Waste.builder() : null, validationContext, false);
        return result.waste;
    }

    public FilterResult<JsonObjectWrapper> filter(final ValueFormat valueFormat, final JsonObject data, final boolean returnFiltrate, final boolean returnWaste, ValidationContext validationContext) {
        Waste.Builder wasteBuilder = returnWaste ? Waste.builder() : null;
        return filter(valueFormat, data, JsonArray.EMPTY, data, returnFiltrate, wasteBuilder, validationContext == null ? new ValidationContext(valueFormat) : validationContext);
    }

    private FilterResult<JsonObjectWrapper> filter(ValueFormat format, final JsonObject rootData, final JsonArray rootDataPath, final JsonObject data, final Boolean returnFiltrate, final Waste.Builder wasteBuilder, ValidationContext validationContext) {
        return filter(format, rootData, rootDataPath, data, returnFiltrate, wasteBuilder, validationContext, true);
    }

    private FilterResult<JsonObjectWrapper> filter(ValueFormat format, final JsonObject rootData, final JsonArray rootDataPath, final JsonObject data, final Boolean returnFiltrate, final Waste.Builder wasteBuilder, ValidationContext validationContext, boolean extendRootDataPathWithFormFieldName) {

        boolean returnWaste = wasteBuilder != null;
        if (format.alwaysSucceeds()) {
            return new FilterResult<>(returnFiltrate ? JsonObjectWrapper.of(data) : null, returnWaste ? new Waste(JsonArray.EMPTY) : null);
        }

        if (format.alwaysFails()) {

            FormFieldValidationResult.Builder fieldValidationBuilder = FormFieldValidationResult.builder().valid(false).path(JsonArray.EMPTY).add("alwaysFails", JsonValue.TRUE);

            wasteBuilder.add(fieldValidationBuilder
                    .path(rootDataPath)
                    .description(format.asJsonValue().get("desc", JsonValue.EMPTY_STRING).getValue(String.class))
                    .badValue(data == null ? JsonValue.NULL : data.jsonValue()).build());
            return new FilterResult<>(returnFiltrate ? JsonObjectWrapper.of(null) : null, returnWaste ? wasteBuilder.build() : null);
        }

        Form form = format.form().asExplicitForm();
        if (format != null && format.href() != null) {
            ValueFormat ctxFormat = validationContext.getFormat(format.href());
            if (ctxFormat != null) {
                form = ctxFormat.form().asExplicitForm();
            }
        }

        if (form.href() != null) {
            Form ctxForm = validationContext.getForm(form.href()).asExplicitForm();
            if (ctxForm != null) {
                form = ctxForm;
            }
        }

        JsonArray formFields = form.value();
        Boolean allowUndefinedFields = !form.exclusive();

        JsonObjectFiltrateBuilder filtrate = returnFiltrate ? JsonObjectFiltrateBuilder.builder() : null;

        /* dataToBeFiltered will contain all the params not defined in the form */
        JsonObjectBuilder dataToBeFilteredBuilder = data.builder();

        for (FormField formField : formFields.asList(FormField.class)) {

            String name = formField.name();
            final JsonValue fieldValue = data.get(name);

            validateFormField(formField.asExplicitFormField(), rootData, extendRootDataPathWithFormFieldName ? rootDataPath.add(name) : rootDataPath, fieldValue, returnFiltrate, returnWaste, filtrate, wasteBuilder, validationContext);
            dataToBeFilteredBuilder = dataToBeFilteredBuilder.remove(name);
        }

        JsonObject dataToBeFiltered = dataToBeFilteredBuilder.build();
        if (returnFiltrate && filtrate != null && allowUndefinedFields) {
            filtrate.putAll(dataToBeFiltered);
        }

        if (returnWaste) {
            if (!dataToBeFiltered.isEmpty() && !allowUndefinedFields) {
                FormFieldValidationResult formFieldValidationResult = FormFieldValidationResult.builder()
                        .path(rootDataPath)
                        .badValue(data.jsonValue())
                        .add("supportedParams", supportedParams(formFields).jsonValue())
                        .build();

                wasteBuilder.add(formFieldValidationResult);
            }
        }

        Waste waste = null;
        if (wasteBuilder != null) {
            waste = wasteBuilder.build();
        }
        return new FilterResult<>(JsonObjectWrapper.of(filtrate == null ? null : filtrate.build()), waste);
    }

    private void validateFormField(FormField formField, JsonObject rootData, JsonArray rootDataPath, JsonValue fieldValue, final Boolean returnFiltrate, final Boolean returnWaste, JsonObjectFiltrateBuilder filtrateBuilder, Waste.Builder wasteBuilder, ValidationContext validationContext) {

        FormFieldValidationResult.Builder fieldValidationBuilder = FormFieldValidationResult.builder().addParamToFiltrate(true);

        if (formField.schema().alwaysSucceeds()) {
            if (returnWaste) {
                fieldValidationBuilder.valid(true);
            }
            if (returnFiltrate.booleanValue() && fieldValue != null)  {
                filtrateBuilder.put(formField.name(), fieldValue);
            }
        } else if (formField.schema().alwaysFails()) {
            if (returnWaste) {
                fieldValidationBuilder.add("alwaysFails", JsonValue.TRUE);
            }
            if (returnFiltrate.booleanValue() && fieldValue != null)  {
                filtrateBuilder.put(formField.name(), fieldValue);
            }
        } else if (fieldValue == null) {
            if (formField.required()) {
                if (returnWaste) {
                    fieldValidationBuilder.add("required", Required.TRUE);
                }
                if (returnFiltrate.booleanValue()) {
                    filtrateBuilder.invalidateFiltrate();
                }
            }
        } else {
            fieldValidationBuilder = enumValidator.validate(formField, fieldValue, returnWaste, fieldValidationBuilder);
            if (JsonValue.NULL.equals(fieldValue)) {
                fieldValidationBuilder = nullableValidator.validate(formField, fieldValue, returnWaste, fieldValidationBuilder);

            } else if (formField.schema().mustBeNull()) {
                fieldValidationBuilder = mustBeNullValidator.validate(formField, fieldValue, returnWaste, fieldValidationBuilder);

            } else if (formField.schema().type().isString()) {
                fieldValidationBuilder = stringValidator.validate(formField, fieldValue, returnWaste, fieldValidationBuilder);

            } else if (formField.schema().type().isObject()) {
                fieldValidationBuilder = validateObject(formField, rootData, rootDataPath, fieldValue, fieldValidationBuilder, filtrateBuilder, wasteBuilder, validationContext);

            } else if (formField.schema().type().isArray() || formField.schema().type().isSet()) {
                validateArrayFormField(formField, rootData, rootDataPath, fieldValue, fieldValidationBuilder, filtrateBuilder, wasteBuilder, validationContext);

            } else if (formField.schema().type().isInteger()) {
                fieldValidationBuilder = integerValidator.validate(formField, fieldValue, returnWaste, fieldValidationBuilder);

            } else if (formField.schema().type().isDecimal()) {
                fieldValidationBuilder = decimalValidator.validate(formField, fieldValue, returnWaste, fieldValidationBuilder);

            } else if (formField.schema().type().isBoolean()) {
                fieldValidationBuilder = booleanValidator.validate(formField, fieldValue, returnWaste, fieldValidationBuilder);

            } else if (formField.schema().type().isDate()) {
                fieldValidationBuilder = dateValidator.validate(formField, fieldValue, returnWaste, fieldValidationBuilder);

            } else if (formField.schema().type().isDatetime()) {
                fieldValidationBuilder = datetimeValidator.validate(formField, fieldValue, returnWaste, fieldValidationBuilder);

            } else if (formField.schema().type().isUndefined()) {
                // Do nothing
            } else {
                throw new RuntimeException("Not supported type = " + formField.schema().type());
            }

            fieldValidationBuilder = optionsValidator.validate(formField, rootData, rootDataPath, fieldValue, returnWaste, fieldValidationBuilder, this, validationContext);

            /// Equals validation is done lastly
            equalsValidator.validate(formField, rootData, fieldValue, fieldValidationBuilder);

            if (returnFiltrate && formField.required() && !fieldValidationBuilder.isValid() && filtrateBuilder != null) {
                filtrateBuilder.invalidateFiltrate();
            }

            if (returnFiltrate &&
                    fieldValidationBuilder.isValid() && // This is how newer StringValidator and IntegerValidator returns
                    fieldValidationBuilder.addParamToFiltrate()) { // This is the older way of returning info of if the param is ok to add to filtrate, when all is refactored and moved to different validator classes this one should be removed
                filtrateBuilder.put(formField.name(), fieldValue);
            }
        }
        if (returnWaste) {
            if (fieldValidationBuilder.containsViolations()) {
                wasteBuilder.add(fieldValidationBuilder
                        .path(rootDataPath)
                        .description(formField.schema().asJsonValue().get("desc", JsonValue.EMPTY_STRING).getValue(String.class))
                        .badValue(fieldValue == null ? JsonValue.NULL : fieldValue).build());
            }
        }
    }

    private FormFieldValidationResult.Builder validateObject(FormField objectFormField, JsonObject rootData, JsonArray rootDataPath, JsonValue fieldValue, FormFieldValidationResult.Builder fieldValidationBuilder, JsonObjectFiltrateBuilder filtrateBuilder, Waste.Builder wasteBuilder, ValidationContext validationContext) {

        boolean returnFiltrate = filtrateBuilder != null;
        boolean returnWaste = wasteBuilder != null;

        if (fieldValue.getValue() != null) {

            if (fieldValue.is(JsonObject.class)) {

                Form form = objectFormField.schema().form();

                FormFields formFields = form != null ? form.formFields() : null;

                if (formFields == null) { /* Unspecified content is ok */

                } else { /* Content is specified in form */

                    fieldValidationBuilder.addParamToFiltrate(false); // Cannot make sure all of the nested content is ok, therefore handle it directly here instead

                    FilterResult<JsonObjectWrapper> filterResult = filter(objectFormField.schema(), rootData, rootDataPath, fieldValue.getValue(JsonObject.class), returnFiltrate, wasteBuilder, validationContext);
                    if (returnFiltrate) {
                        if (filterResult.filtrate == null) {
                            filtrateBuilder.invalidateFiltrate();
                        } else {
                            filtrateBuilder.put(objectFormField.name(), filterResult.filtrateAsJsonValue());
                        }
                    }
                }
            } else {
                fieldValidationBuilder.addParamToFiltrate(false);
                if (returnWaste) {
                    fieldValidationBuilder.add("type", Type.OBJECT);
                }
            }
        }
        return fieldValidationBuilder;
    }

    private FormFieldValidationResult.Builder validateArrayFormField(FormField formField, JsonObject rootData, JsonArray rootDataPath, JsonValue fieldValue, FormFieldValidationResult.Builder fieldValidationBuilder, JsonObjectFiltrateBuilder filtrateBuilder, Waste.Builder wasteBuilder, ValidationContext validationContext) {

        boolean returnWaste = wasteBuilder != null;
        boolean returnFiltrate = filtrateBuilder != null;

        if (fieldValue.is(JsonArray.class)) {

            if (formField.schema().type().isSet() && !JsonArraySet.isSet(fieldValue.getValue(JsonArray.class))) {
                addBadSet(returnWaste, fieldValidationBuilder);
            } else {

                fieldValidationBuilder = minSizeValidator.validate(formField, fieldValue, returnWaste, fieldValidationBuilder);
                fieldValidationBuilder = maxSizeValidator.validate(formField, fieldValue, returnWaste, fieldValidationBuilder);

                fieldValidationBuilder.addParamToFiltrate(false); // Cannot verify the nested form, therefore handle it directly instead

                if (fieldValue.getJsonArray() != null) {
                    validateArrayValue(rootData, rootDataPath, formField, fieldValue.getJsonArray(), filtrateBuilder, wasteBuilder, validationContext);
                }
            }
        } else {
            fieldValidationBuilder.addParamToFiltrate(false);
            if (returnWaste) {
                fieldValidationBuilder.add("type", formField.schema().type());
            }
        }
        return fieldValidationBuilder;
    }

    private void validateArrayValue(JsonObject rootData, JsonArray rootDataPath, FormField formField, JsonArray jsonArrayFieldValue, JsonObjectFiltrateBuilder jsonObjectFiltrateBuilder, Waste.Builder wasteBuilder, ValidationContext validationContext){
        boolean returnFiltrate = jsonObjectFiltrateBuilder != null;
        JsonArrayFiltrateBuilder arrayFiltrateBuilder = returnFiltrate ? JsonArrayFiltrateBuilder.builder() : null;

        if (formField.schema().eform() != null) {

            // etype is implied to be object when eform is specified
            int i = 0;
            for (JsonValue element : jsonArrayFieldValue.asList()) {
                validateEform(formField, rootData, rootDataPath.add(i++), element, wasteBuilder, arrayFiltrateBuilder, validationContext);
            }
        } else if (formField.schema().element() != null) {
            int i = 0;
            for (JsonValue element : jsonArrayFieldValue.asList()) {
                validateElement(formField, rootData, rootDataPath.add(i++), element, wasteBuilder, arrayFiltrateBuilder, validationContext);
            }
        }
        if (returnFiltrate) {
            JsonArray arrayFiltrate = arrayFiltrateBuilder.build();
            if (arrayFiltrate == null) {
                jsonObjectFiltrateBuilder.invalidateFiltrate();
            } else {
                jsonObjectFiltrateBuilder.put(formField.name(), arrayFiltrate);
            }
        }
    }

    private void validateEform(
            FormField formField,
            JsonObject rootData,
            JsonArray rootDataPath,
            JsonValue element,
            Waste.Builder wasteBuilder,
            JsonArrayFiltrateBuilder filtrateBuilder,
            ValidationContext validationContext) {

        boolean returnFiltrate = filtrateBuilder != null;
        boolean returnWaste = wasteBuilder != null;

        if (element.is(JsonObject.class)) {
            FilterResult<JsonObjectWrapper> filterResult = filter(ValueFormat.EMPTY.builder().type(Type.OBJECT).form(formField.schema().eform()).build(), rootData, rootDataPath, element.getValue(JsonObject.class), returnFiltrate, wasteBuilder, validationContext);
            if (returnFiltrate) {
                if (filterResult.filtrate == null) {
                    filtrateBuilder.invalidateFiltrate();
                } else {
                    filtrateBuilder.add(filterResult.filtrateAsJsonValue());
                }
            }

        } else {
            if (returnWaste) {
                FormFieldValidationResult formFieldValidationResult = FormFieldValidationResult.builder()
                        .path(rootDataPath).badValue(element).add("type", Type.OBJECT).build();
                wasteBuilder.add(formFieldValidationResult);
            }
            if (returnFiltrate) {
                filtrateBuilder.invalidateFiltrate();
            }
        }
    }

    private void validateElement(
            FormField formField,
            JsonObject rootData,
            JsonArray rootDataPath,
            JsonValue jsonValueToBeValidated,
            Waste.Builder wasteBuilder,
            JsonArrayFiltrateBuilder filtrateBuilder,
            ValidationContext validationContext) {

        boolean returnFiltrate = filtrateBuilder != null;

        FilterResult<JsonObjectWrapper> filterResult = filter(
                formField.schema().element().asRequiredFormatOfTypeObjectWithElementField(),
                rootData,
                rootDataPath,
                JsonObject.EMPTY.put("element", jsonValueToBeValidated),
                returnFiltrate,
                wasteBuilder,
                validationContext,
                false);
        if (returnFiltrate) {
            if (filterResult.filtrate == null) {
                filtrateBuilder.invalidateFiltrate();
            } else {
                filtrateBuilder.add(filterResult.filtrateAsJsonValue().get("element", JsonValue.NULL));
            }
        }
    }

    private void addBadSet(boolean returnWaste, FormFieldValidationResult.Builder fieldValidationBuilder) {
        fieldValidationBuilder.addParamToFiltrate(false);
        if (returnWaste) {
            fieldValidationBuilder.add("type", Type.SET);
        }
    }

    private JsonArray supportedParams(JsonArray formFields) {
        JsonArray supportedParams = JsonArray.EMPTY;
        for (JsonValue param : formFields.asList()) {
            supportedParams = supportedParams.add(param.get("name"));
        }
        return supportedParams;
    }
}
