/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.forms;

import com.zuunr.forms.formfield.Type;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectBuilder;
import com.zuunr.json.JsonObjectMerger;
import com.zuunr.json.JsonValue;

/**
 * @author Niklas Eldberger
 */
class ExplicitValueFormatCreator {

    JsonObjectMerger jsonObjectMerger = new JsonObjectMerger();

    private static final JsonObject DEFAULT = JsonObject.EMPTY.builder()
            .put("type", "string")
            .put("mustBeNull", false)
            .put("required", false)
            .put("nullable", false)
            .build();

    private static final JsonObject DEFAULT_FOR_INTEGER = DEFAULT.builder()
            .put("min", Long.MIN_VALUE)
            .put("max", Long.MAX_VALUE)
            .build();

    private static final JsonObject DEFAULT_FOR_DECIMAL = DEFAULT.builder()
            //.put("min", Long.MIN_VALUE)
            //.put("max", Long.MAX_VALUE)
            .build();


    private static final JsonObject DEFAULT_FOR_STRING = DEFAULT.builder()
            .put("minlength", JsonValue.ZERO)
            .put("maxlength", Integer.MAX_VALUE)
            .build();

    private static final JsonObject DEFAULT_FOR_ARRAY = DEFAULT.builder()
            .put("minsize", JsonValue.ZERO)
            .build();

    JsonObject createExplicitValueFormat(ValueFormat valueFormat) {

        JsonObjectBuilder builder = valueFormat.asJsonObject().builder();
        JsonObject defaultFormField = DEFAULT;

        Type type = valueFormat.type() == null ? Type.STRING : valueFormat.type();

        if (Type.INTEGER.equals(type)) {
            defaultFormField = DEFAULT_FOR_INTEGER;
        } else if (Type.DECIMAL.equals(type)) {
            defaultFormField = DEFAULT_FOR_DECIMAL;
        } else if (Type.STRING.equals(type)) {
            defaultFormField = DEFAULT_FOR_STRING;
        } else if (Type.ARRAY.equals(type) || Type.SET.equals(type)) {
            defaultFormField = DEFAULT_FOR_ARRAY;
        }

        return jsonObjectMerger.merge(defaultFormField, builder.build());
    }
}
