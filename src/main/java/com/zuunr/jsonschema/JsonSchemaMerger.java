/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.jsonschema;

import com.zuunr.forms.ValueFormat;
import com.zuunr.forms.generation.FormMerger;
import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;

/**
 * @author Niklas Eldberger
 */
public class JsonSchemaMerger {

    public static final JsonSchemaMerger JSON_SCHEMA_2019_09 = new JsonSchemaMerger(new ValueFormatConverter());
    public static final JsonSchemaMerger OPENAPI_3_0_STYLE = new JsonSchemaMerger(openApiStyleValueFormatConverter());

    private final JsonSchemaConverter jsonSchemaConverter = new JsonSchemaConverter();
    private final FormMerger formMerger = new FormMerger();
    private final ValueFormatConverter valueFormatConverter;

    public JsonSchemaMerger() {
        this(new ValueFormatConverter());
    }

    public JsonSchemaMerger(ValueFormatConverter valueFormatConverter) {
        this.valueFormatConverter = valueFormatConverter;
    }

    private static ValueFormatConverter openApiStyleValueFormatConverter() {
        return new ValueFormatConverter(valueFormatAndSchemaTuple -> {
            JsonObject result = valueFormatAndSchemaTuple.jsonSchema;

            String valueFormatType = valueFormatAndSchemaTuple.valueFormat.asExplicitValueFormat().type().toString();
            String schemaType = ValueFormatConverter.TYPE_MAPPING.get(JsonArray.of(valueFormatType, "type", 0), JsonValue.NULL).getString();
            if (schemaType != null) {
                result = result.put("type", schemaType);
            }
            if (valueFormatAndSchemaTuple.valueFormat.asExplicitValueFormat().nullable()) {
                result = result.put("nullable", true);
            }
            return result;
        });
    }

    public JsonValue intersectionOf (JsonValue jsonSchema1, JsonValue jsonSchema2, boolean simplifyAlwaysFailingSchema) {
        ValueFormat valueFormat1 = jsonSchemaConverter.translate(jsonSchema1);
        ValueFormat valueFormat2 = jsonSchemaConverter.translate(jsonSchema2);
        ValueFormat intersection = formMerger.intersectionOf(valueFormat1, valueFormat2);
        if (simplifyAlwaysFailingSchema && intersection.alwaysFails()) {
            return JsonValue.FALSE;
        } else {
            return valueFormatConverter.translate(intersection.asCompactValueFormat()).jsonValue();
        }
    }

    public JsonValue unionOf(JsonValue jsonSchema1, JsonValue jsonSchema2) {
        ValueFormat valueFormat1 = jsonSchemaConverter.translate(jsonSchema1);
        ValueFormat valueFormat2 = jsonSchemaConverter.translate(jsonSchema2);
        ValueFormat union = formMerger.unionOf(valueFormat1.asFormatOfTypeObjectWithElementField().form(), valueFormat2.asFormatOfTypeObjectWithElementField().form()).formField("element").asValueFormat();
        return valueFormatConverter.translate(union.asCompactValueFormat()).jsonValue();
    }
}