/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.jsonschema;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.jsonpointer.JsonPointer;
import com.zuunr.jsonpointer.JsonPointerFormatException;
import com.zuunr.jsonpointer.ParsingException;

/**
 * @author Niklas Eldberger
 */
public class JsonSchemaComponentsInliner {
    private static final JsonValue REF = JsonValue.of("$ref");

    public JsonObject inline(JsonObject jsonDoc, int maxLevels) {
        JsonObject previous = jsonDoc;
        JsonObject inlined = inlineOneLevel(jsonDoc);
        for (int i = 0; i < maxLevels && inlined != previous; i++) {
            previous = inlined;
            inlined = inlineOneLevel(inlined);
        }
        inlined = inlined.remove(JsonArray.of("components", "schemas"));
        return inlined;
    }

    public JsonObject inlineOneLevel(JsonObject jsonDoc) {
        JsonObject inlinedJsonDoc = jsonDoc;
        for (JsonValue pathAndValue : jsonDoc.jsonValue().getPathsAndValue(true)) {

            JsonArray candidateRefPath = pathAndValue.getValue(JsonArray.class).allButLast();
            if (REF.equals(candidateRefPath.last())) {
                JsonPointer jsonPointer = createFragmentJsonPointer(pathAndValue.getValue(JsonArray.class).last().getString());
                inlinedJsonDoc = inlineReference(candidateRefPath.allButLast(), inlinedJsonDoc, jsonPointer);
            }
        }
        return inlinedJsonDoc;
    }

    private JsonPointer createFragmentJsonPointer(String jsonPointerString) {
        if (jsonPointerString.startsWith("#")) {
            return JsonPointer.of(jsonPointerString);
        } else {
            throw new JsonPointerFormatException("'$ref' property must be of string value starting with '#'. Bad value: " + jsonPointerString);
        }
    }

    private JsonObject inlineReference(JsonArray refPath, JsonObject jsonDoc, JsonPointer jsonPointer) {

        JsonArray jsonDocPath = jsonPointer.asJsonArray();
        JsonValue jsonDocPathValue = jsonDoc.get(jsonDocPath);
        jsonDocPathValue = filterInlinedSchema(jsonDocPathValue);
        if (jsonDocPathValue == null) {
            throw new ParsingException("'" + jsonPointer.asString() + "' cannot be found in document");
        }
        return jsonDoc.put(refPath, jsonDocPathValue);
    }

    private JsonValue filterInlinedSchema(JsonValue schema) {
        if (!schema.is(JsonObject.class)) {
            return schema;
        }
        return schema.getValue(JsonObject.class)
                .remove("title")
                .remove("description")
                .jsonValue();
    }
}
