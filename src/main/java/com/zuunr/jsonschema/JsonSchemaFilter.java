/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.jsonschema;

import com.zuunr.forms.ValueFormat;
import com.zuunr.forms.filter.FilterResult;
import com.zuunr.forms.filter.FormFilter;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonValue;
import com.zuunr.json.util.JsonObjectWrapper;

/**
 * @author Niklas Eldberger
 */
public class JsonSchemaFilter {

    private ValueFormat valueFormat;
    private JsonValue jsonSchema;

    private JsonSchemaConverter jsonSchemaConverter = new JsonSchemaConverter();
    private FormFilter formFilter = new FormFilter();

    public JsonSchemaFilter(JsonValue jsonSchema) {
        this.jsonSchema = jsonSchema;
        valueFormat = jsonSchemaConverter.translate(jsonSchema);
    }

    public FilterResult<JsonObjectWrapper> filter(JsonObject toBeFiltered){
        return formFilter.filter(valueFormat, toBeFiltered, true, true);
    }

    public boolean alwaysFails(){
        return valueFormat.alwaysFails();
    }
}
