# Dynamic Forms

![Apache License](https://img.shields.io/static/v1.svg?label=License&message=Apache-2.0&color=%230af)
[![Maven Central](https://img.shields.io/maven-central/v/com.zuunr/forms.svg?label=Maven%20Central&color=%230af)](https://search.maven.org/search?q=com.zuunr forms)
[![Snyk](https://snyk-widget.herokuapp.com/badge/mvn/com.zuunr/forms/badge.svg)](https://snyk.io/vuln/maven:com.zuunr%3Aforms)

The Dynamic forms project is heavily inspired by "The Ion Hypermedia Type" specification and the great work done by Lez Hazlewood. The Dynamic forms project is not intended to comply with the specification and no shortcomings in this project can or should be blamed on that specification.

Dynamic forms is a project for dynamic validation of data.

A form consists of constraints for JSON data structures and additional data to help in presenting these constraints to a human user.

NOTE! The future of Dynamic Forms will compliant with JSON Schema. This is because JSON Schema is now the most adopted Validation format for JSON.  

## Value Objects

A Value Object is a JSON object that contains a value member.

A Value Object is used to represent a primary JSON value but also allows for other peer members to provide additional contextual information or metadata regarding the value member.

A Value Object MAY have additional members other than the value member.

## Collection Objects

A Collection Object is a Value Object where the value member is a JSON array. If a JSON value is an element in a Collection Object's value array, it is said that the Collection Object contains that value.

## Form structure

A Form describes which fields are allowed. A Form object is Value Object where description of specific fields are set in the `value` array.

_Example of form:_

    {
        "value": [
            {"name": "firstName"},                                // first field in form
            {"name": "yearOfBirth", "type": "integer"}  // second field in form
        ]
    }

_Example of valid data for form above:_

    {
        "firstName": "Peter",
        "yearOfBirth": 1981
    }


If a form allows fields which are not specified the field named `exclusive` must be set to value `false`.

_Example of non-exclusive form:_

    {
        "exclusive": false,     // Other fields are unspecified but allowed
        "value": [
            {"name": "firstName"},
            {"name": "yearOfBirth", "type": "integer"}
        ]
    }

_Example of valid data for non-exclusive form above:_

    {
        "firstName": "Peter",
        "yearOfBirth": 1981,
        "someUndefinedName": "Whatever...",
        "someOtherUndefined": {"some key": "some value"}
    }

### Form Fields
A Form Field is a JSON object in a Form’s value array that contains one or more Form Field Members.

A Form Field MUST have a string member named `name.

Each Form Field within an Form’s value array MUST have a unique name value compared to any other Form Field within the same array.

#### Form Field Members
An  Form Field contains one or more of the following members.

##### desc
The description member is a string description of the field.

Use of this member is OPTIONAL.

_Example form:_

    {
        "value": [
            {"name": "firstName", "desc": "This is the first name of a person, eg Peter is first name of 'Peter Andersson'"}
        ]
    }


##### eform
The eform member value is a Form object. The name "eform" is short for "element form".

Only applicable if the field’s type member equal to array or set.

Use of this member is OPTIONAL.

_Example form with nested eform:_

    {
        "value": [
            {"name": "friends", "type": "array", "eform": {"value": [
                {"name": "firstName"}
            ]}}
        ]
    }


_Example of valid data for form with nested eform above:_

    {
        "friends": [
            {"firstName": "Peter"},
            {"firstName": "Robert"},
            {"firstName": "Anna"}
        ]
    }

##### equals

TBD - Documentation needed.

##### form
The form member value is a Form object.

Only applicable if the field´s type member is equal to object.

Use of this member is OPTIONAL.


_Example form with nested form:_

    {
        "value": [
            {"name": "bestFriend", "type": "object", "form": {"value": [
                {"name": "firstName"}
            ]}}
        ]
    }


_Example of valid data for form with nested form above:_

    {
        "bestFriend": {
            "firstName": "Peter"
        }
    }


##### max
The max member indicates that the field value must be less than or equal to the specified max value.

Only applicable if the field´s type member is equal to string, integer, decimal or date.

Use of this member is OPTIONAL.

_Example form with max:_

    {
        "value": [
            {"name": "nameOfChild"},
            {"name": "weight", "type": "integer", "max": 100},
            {"name": "birthDate", "max": "2018-01-01"}
        ]
    }


_Example of valid data for form with max above:_

    {
        "nameOfChild": "Rob",
        "weight": 27,
        "1999-12-12"
    }


##### maxlength
The maxlength member is a non-negative integer that specifies the maximum number of characters the field value may contain.

Not applicable if the field type equals object, array, or set.

Use of this member is OPTIONAL.

_Example of form with maxlength:_

    {
        "value": [
            {"name": "firstName", "maxlength": 10}
        ]
    }

_Example of valid data for form with maxlength above:_

    {
        "firstName": "Peter",
        "yearOfBirth": 1981,
        "someUndefinedName": "Whatever...",
        "someOtherUndefined": {"some key": "some value"}
    }


##### maxsize
The maxsize member value is a non-negative integer that specifies the maximum number of field values when the field type value equals array or set.

Only applicable if the field type value equal array or set.

Use of this member is OPTIONAL.

_Example form with maxsize:_

    {
        "value": [
            {"name": "friends", "type": "set", "maxsize": 3, "eform": {"value": [
                {"name": "firstName"}
            ]}}
        ]
    }


_Example of valid data for form with maxsixe above:_

    {
        "friends": [
            {"firstName": "Peter"},
            {"firstName": "Robert"},
            {"firstName": "Anna"}
        ]
    }


##### min
The min member indicates that the field value must be greater than or equal to the specified min value.

Only applicable if the field´s type member is equal to string, integer, decimal or date.

Use of this member is OPTIONAL.

##### minlength
The minlength member is a non-negative integer that specifies the minimum number of characters the field value must contain.

Not applicable if the field type equals object, array, or set.

Use of this member is OPTIONAL.

##### minsize
The minsize member value is a non-negative integer that specifies the minimum number of field values when the field type value equals array or set.

Only applicable if the field type value equal array or set.

Use of this member is OPTIONAL.

##### mustBeNull
The mustBeNull member value indicates whether or not the field value must equal null

A true value indicates that the field value MUST equal null.

Use of this member is OPTIONAL.

_Example form with mustBeNull:_

    {
        "value": [
            {"name": "firstName"},
            {"name": "yearOfBirth", "mustBeNull": true}
        ]
    }

_Example of valid data for form with mustbeNull above:_

    {
        "firstName": "Peter",
        "yearOfBirth": null
    }


##### name
The name member is a string name assigned to the field.

The name value MUST NOT be null.

The name value MUST NOT contain only whitespace.

The name value MUST be unique compared to any other Form Field name value in the containing Form’s value array.

Use of this member is REQUIRED.

##### nullable
The nullable member indicates whether or not the field value may equal null.

The nullable member is a boolean; it must equal either true or false. null or any other JSON value MUST NOT be specified.

A true value indicates that the field value MAY equal null.

If the nullable member is not present, or if it present and equal to false, the field value MUST NOT equal null.

Use of this member is OPTIONAL.

##### options
The options member is a Collection Object where the value array contains Form Field Option objects. A Form Field Option object contains one or more members defined in Form Field Option Members.

When an options member is present and the form field type does not equal set or array, any form field value specified MUST equal one of the values found within the Option array.

When an options member is present and the form field type equals set or array, the form field value MUST be a JSON array, and the array MUST NOT contain any value not found within the Option value array.

If the field type is not set or array, Ion parsers MUST ignore any option where the option value type is not the same as the field type.

##### pattern
The pattern member is a JSON string that defines a regular expression used to validate the field value.

The pattern member MUST NOT be specified on fields with non-string or non-date/non-time value types.

Use of this member is OPTIONAL.

_Example form with required:_

    {
        "value": [
            {"name": "firstName", "pattern": "[A-Z][a-z0-9]*"} // Upper case for first character
        ]
    }

_Example of valid data for form with required above:_

    {
        "firstName": "Peter"
    }



##### required
The required member indicates whether or not the field value may be omitted.

The required member is a boolean; it must equal either true or false. null or any other JSON value MUST NOT be specified.

A true value indicates that the field value MUST NOT be omitted.

If the required member is not present, or if it present and equal to false, the field value MAY be omitted.

Use of this member is OPTIONAL.

_Example form with required:_

    {
        "value": [
            {"name": "firstName"},
            {"name": "yearOfBirth", "required": true}
        ]
    }

_Example of valid data for form with required above:_

    {
        "yearOfBirth": 1999
    }


##### type
The type member specifies the mandatory data type that the value member value must adhere to. The type value is a string and must equal to one of the octet sequences defined in data type.

If the type member is not present, a default type of string for the field is applied.

If the type member equals array or set, and the elements in the array or set must conform to a particular type and structure, those type constraints may be defined using the etype and eform members.

Use of this member is OPTIONAL.

### Types
#### array
A JSON array or if nullable member value is true - null.

If a Value Object has an array type and the Value Object also contains min, minlength, max, maxlength or pattern members, those members' validation rules apply to each element in the value array, not the array itself.

#### boolean
A JSON boolean or if nullable member value is true - null.

#### date
A JSON string or if nullable member value is true - null.

Non-null value must be a JSON string that conforms to the full-date grammar defined in RFC 3339 Section 5.6.

#### decimal
A JSON number that contains a decimal point (aka the Period . character, ASCII code 46) or if nullable member value is true - null.

#### integer
A JSON number that does not contain a decimal point (aka the Period . character, ASCII code 46) or if nullable member value is true - null.

#### object
A JSON object or if nullable member value is true - null.

If an Value Object with a type of object also contains a form member, the Value Object’s value member MUST be the Form Submission Object that would result if submitting that form.

#### set
A JSON array and the array MUST NOT contain any element that is equal to any other element within the same array or if nullable member value is true - null.

If a Value Object has a set type and also contains min, minlength, max, maxlength or pattern members, those members' validation rules apply to each item in the set array, not the set array itself.

#### string
A JSON string or if nullable member value is true - null.


## Validation and filtering

Validation and filtering goes hand in hand. The form can be seen as definition of a filter and data which is applied to that filter will end up as either _filtrate_ or _waste_.

Validation is done by checking if there is any _waste_ in the result from the FormFilter.filter() method. This is useful when the there is need to validate the JSON data structure as a whole. The content of the _waste_ shows which parts of the data were not valid.

Filtering is done by using the _filtrate_ in the result from the FormFilter.filter() method. This is useful in scenarios when only the part of data specified in the form should be used, eg in response from a Web API service.

## Combining forms

Two or more forms can be combined to achieve one effective form. This is useful when one have many constraints to be applied for the same model (ie data to be validated according to more than one form).

Forms may be combined in different ways by applying different _merge strategies_

### Soften

A = data valid according to form_A

B = data valid according to form_B

form_C = FormMerger.merge(form_A, form_B, SOFTEN)

C = data valid according to form_C = A ∪ B (Union of A and B)

![](pics/soften.png)

### Harden

A = data valid according to form_A

B = data valid according to form_B

form_C = FormMerger.merge(form_A, form_B, HARDEN)

C = data valid according to form_C = A ∩ B (Intersection of A and B)

![](pics/harden.png)

### Patch by override

A = data valid according to form_A

B = data valid according to form_B

form_C = FormMerger.merge(form_A, form_B, PATCH_BY_OVERWRITE)

C = data valid according to form_C = data fields of form_A which exists in form_B according to form_B and all other data fields according to form_A

![](pics/patch-by-override.png)